import 'dart:async';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/material.dart';
import 'package:zzin/common/firestore/document_skima/user_account.dart';
import 'package:zzin/common/widget/zbutton_white.dart';
import 'package:zzin/common/zconfig.dart';
import 'package:zzin/opentalk_page/opentalk_chat_ui/opentalk_message.dart';

class OpentalkChatUIWriteMessage extends StatefulWidget {
  final ScrollController scrollController;
  const OpentalkChatUIWriteMessage({required this.scrollController, Key? key}) : super(key: key);
  @override
  _OpentalkChatUIWriteMessageState createState() => _OpentalkChatUIWriteMessageState();
}

class _OpentalkChatUIWriteMessageState extends State<OpentalkChatUIWriteMessage> {
  final TextEditingController _textEditingController = TextEditingController();
  final FocusNode _focusNode = FocusNode();
  late ZUserAccount receiverRealTime;

  late DatabaseReference openrunRef;

  @override
  void initState() {
    openrunRef = ZConfig.database.reference().child('openrun');
    super.initState();
  }

  @override
  void dispose() {
    _focusNode.dispose();
    super.dispose();
  }

  void onReceiverDataChanged(DocumentSnapshot<ZUserAccount> userData) {
    receiverRealTime = userData.data()!;
  }

  bool isTest = false;

  @override
  Widget build(BuildContext context) {
    print('build');
    return SizedBox(
      height: AppBar().preferredSize.height,
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 5),
        child: Row(children: !isTest ? writeWidget() : buttonWidget()),
      ),
    );
  }

  List<Widget> writeWidget() {
    return [
      IconButton(
          onPressed: () {
            isTest = !isTest;
            setState(() {});
          },
          icon: const Icon(Icons.add)),
      Expanded(
        child: Container(
          decoration: BoxDecoration(
              border: Border.all(color: ZConfig.grey_border),
              borderRadius: BorderRadius.circular(20)),
          child: Padding(
              padding: const EdgeInsets.only(left: 10),
              child: TextField(
                onEditingComplete: onSendMessage,
                onTap: onTap,
                textInputAction: TextInputAction.send,
                focusNode: _focusNode,
                controller: _textEditingController,
                decoration: const InputDecoration(
                  hintText: '메시지를 입력하세요.',
                  hintStyle: TextStyle(color: ZConfig.grey_text, fontSize: 16),
                  border: InputBorder.none,
                ),
                style: const TextStyle(fontSize: ZSize.searchTextSize),
              )),
        ),
      ),
      const SizedBox(width: 5),
      GestureDetector(
        onTap: onSendMessage,
        child: Container(
          height: 25,
          width: 25,
          decoration: BoxDecoration(
              color: ZConfig.zzin_pointColor, borderRadius: BorderRadius.circular(20)),
          child: const Icon(Icons.send, color: ZConfig.zzin_white, size: 20),
        ),
      ),
    ];
  }

  List<Widget> buttonWidget() {
    return [
      IconButton(
        onPressed: () {
          isTest = !isTest;
          setState(() {});
        },
        icon: const Icon(Icons.close),
      ),
      Expanded(
        flex: 1,
        child: Container(
          decoration: BoxDecoration(
              border: Border.all(color: ZConfig.grey_border),
              borderRadius: BorderRadius.circular(20)),
          child: Padding(
              padding: const EdgeInsets.only(left: 10),
              child: TextField(
                onSubmitted: (message) => onSendMessage(),
                onTap: onTap,
                focusNode: _focusNode,
                controller: _textEditingController,
                decoration: const InputDecoration(
                  hintText: '메시지를 입력하세요.',
                  hintStyle: TextStyle(color: ZConfig.grey_text, fontSize: 16),
                  border: InputBorder.none,
                ),
                style: const TextStyle(fontSize: ZSize.searchTextSize),
              )),
        ),
      ),
      Expanded(
          flex: 1000,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              ZButtonWhite(
                text: '현재위치',
                borderRaius: 20,
                isSelected: false,
                onPressed: onMyPosition,
              ),
              ZButtonWhite(
                text: '이미지',
                borderRaius: 20,
                isSelected: false,
                onPressed: onImageShare,
              ),
              ZButtonWhite(
                text: '카메라',
                borderRaius: 20,
                isSelected: false,
                onPressed: onCameraShare,
              ),
            ],
          )),
    ];
  }

  void onTap() async {
    await Future.delayed(const Duration(milliseconds: 100));
    if (widget.scrollController.hasClients) {
      await widget.scrollController.animateTo(
        widget.scrollController.position.maxScrollExtent,
        duration: const Duration(milliseconds: 100),
        curve: Curves.fastOutSlowIn,
      );
    }
  }

  void onSendMessage() async {
    if (_textEditingController.text.trim().isEmpty) {
      return;
    }
    // 채팅창 비우기
    var message = _textEditingController.text.trim();
    _textEditingController.text = '';

    await openrunRef.push().set(
        OpentalkMessage('parkcom', OpentalkMessageType.text, message, Timestamp.now()).toJson());
    // ChatRoom chatRoom;
    // if (ZConfig.userAccount.docId == widget.product.sellerId) {
    //   // 판매자가 글쓸때
    //   chatRoom = ChatRoom(
    //       buyerRef: widget.receiver.docId!,
    //       sellerRef: widget.product.sellerId!,
    //       productRef: widget.product.docId!,
    //       receiverRef: widget.receiver.docId!);
    // } else {
    //   // 구매자가 글쓸때
    //   chatRoom = ChatRoom(
    //       buyerRef: ZConfig.userAccount.docId!,
    //       sellerRef: widget.product.sellerId!,
    //       productRef: widget.product.docId!,
    //       receiverRef: widget.receiver.docId!);
    // }

    // var recentReceiver = await recentUserAccountData();
    // if (ZConfig.userAccount.isBlocked(receiverRealTime.docId!)) {
    //   showInfo(context, '차단한 상대방에게 채팅 메시지를 보낼 수 없습니다.');
    //   return;
    // } else if (await isBlockedMe()) {
    //   showInfo(context, '상대방이 차단하여 채팅 메시지를 보낼 수 없습니다.');
    //   return;
    // }

    // await chatRoom.addMessaage(message);

    // 채팅창에 채팅표시.
    // var tempChatRoom = await chatRoom.getCurrnetChatRoom();
    // chatRoom = tempChatRoom!; // 이미 방에 있기 때문에 NULL 일수 없음

    // if (chatRoom.isLeaveRoomBuyer == true || chatRoom.isLeaveRoomSeller == true) {
    //   chatRoom = await chatRoom.joinRoom();
    // }

    // // 상대방이 온라인이 아니면 푸쉬 메시지 보내기.
    // bool isReceiverOnline;
    // bool isReceiverNotiOn;
    // if (ZConfig.userAccount.docId == widget.product.sellerId) {
    //   isReceiverOnline = chatRoom.isOnlineBuyer;
    //   isReceiverNotiOn = chatRoom.isNotiOnBuyer;
    // } else {
    //   isReceiverOnline = chatRoom.isOnlineSeller;
    //   isReceiverNotiOn = chatRoom.isNotiOnSeller;
    // }

    // if (!isReceiverOnline && isReceiverNotiOn) {
    //   final sendFCM =
    //       FirebaseFunctions.instanceFor(region: 'asia-northeast3').httpsCallable('sendFCM');
    //   try {
    //     String pushMessage = isLocationInfo(message)
    //         ? '${ZConfig.userAccount.nickname}님이 위치정보를 보냈습니다.'
    //         : isImageUrl(message)
    //             ? '${ZConfig.userAccount.nickname}님의 이미지를 공유했습니다.'
    //             : message;
    //     final result = await sendFCM.call(<String, String>{
    //       'token': receiverRealTime.pushToken,
    //       'title': '${ZConfig.userAccount.nickname}님의 거래문의 채팅이 도착하였습니다.',
    //       'body': pushMessage,
    //       'sender': ZConfig.userAccount.docId!.path,
    //       'product': widget.product.docId!.path
    //     });

    //     print(result);
    //   } catch (e) {
    //     print(e);
    //   }
    // }

    // 채팅창 자동 스크롤.
    await Future.delayed(const Duration(milliseconds: 100));
    await widget.scrollController.animateTo(
      widget.scrollController.position.maxScrollExtent,
      duration: const Duration(milliseconds: 100),
      curve: Curves.fastOutSlowIn,
    );
  }

  Future<bool> isBlockedMe() async {
    return false;
    // var result = await receiverRealTime.docId!
    //     .collection('blockUserCollection')
    //     .withConverter<BlockUser>(
    //         fromFirestore: (snapshot, _) => BlockUser.fromJson(snapshot.data()!),
    //         toFirestore: (blockUser, _) => blockUser.toJson())
    //     .where('userDocId', isEqualTo: ZConfig.userAccount.docId)
    //     .get();

    // return result.docs.isNotEmpty && result.docs[0].data().isBlocked;
  }

  bool isLocationInfo(String message) {
    return message.startsWith('locationInfo|');
  }

  bool isImageUrl(String message) {
    return Uri.parse(message).isAbsolute && message.contains('.jpg?alt=media&token');
  }

  void onMyPosition() async {
    // await EasyLoading.show(
    //     status: '현재위치 확인중...', maskType: EasyLoadingMaskType.black, dismissOnTap: false);
    // GeoPoint position;
    // try {
    //   position = await LocationController.determinePosition(desiredAccuracy: LocationAccuracy.best);
    //   List<Placemark> placemarks =
    //       await placemarkFromCoordinates(position.latitude, position.longitude);
    //   print(placemarks);
    //   if (placemarks.any((element) => element.isoCountryCode != 'KR')) {
    //     Get.rawSnackbar(message: '현재 위치를 확인할 수 없습니다.');
    //     await EasyLoading.dismiss();
    //     return;
    //   }
    // } catch (e) {
    //   Get.rawSnackbar(message: '위치정보 이용에 대한 액세스 권한이 필요합니다.');
    //   await EasyLoading.dismiss();

    //   print(e);
    //   return;
    // }

    // _textEditingController.text = 'locationInfo|${position.latitude}|${position.longitude}';
    // onSendMessage();
    // _focusNode.unfocus();
    // await EasyLoading.dismiss();
  }

  void onImageShare() async {
    // var status = await Permission.storage.status;
    // if (status == PermissionStatus.denied) {
    //   var status = await Permission.storage.request();
    //   if (status == PermissionStatus.denied) {
    //     return;
    //   } else if (status == PermissionStatus.permanentlyDenied) {
    //     var response = await showDialogYesNo(
    //         context, '사진 및 미디어에 대한 액세스 권한이 필요합니다.', '설정 앱에서 권한을 설정하십시요.', '설정하기', '취소');
    //     if (response == DialogResponse.YES) {
    //       await openAppSettings();
    //     }
    //     return;
    //   }
    // }

    // var asset = await AssetPicker.pickAssets(context,
    //     maxAssets: 1, textDelegate: KoreanTextDelegate(), gridCount: 3, pageSize: 90);

    // await EasyLoading.show(
    //     status: '이미지 업로드중...', maskType: EasyLoadingMaskType.black, dismissOnTap: false);

    // if (asset != null) {
    //   var file = await asset[0].originFile;
    //   var result = await FlutterImageCompress.compressWithFile(file!.absolute.path, quality: 80);
    //   if (result != null) {
    //     String productPath = widget.product.docId!.path.split('/').last;
    //     String chatroomDocId = widget.receiver.docId == widget.product.sellerId
    //         ? ZConfig.userAccount.docId!.path.split('/').last
    //         : widget.receiver.docId!.path.split('/').last;
    //     String fileName =
    //         file.path.split('/').last.split('.').first + DateTime.now().toString() + '.jpg';

    //     String storagePath = 'chatRooms/images/$productPath/$chatroomDocId/$fileName';

    //     await FirebaseStorage.instance.ref(storagePath).putData(result);

    //     String imagePath = await FirebaseStorage.instance.ref(storagePath).getDownloadURL();

    //     _textEditingController.text = imagePath;
    //     onSendMessage();
    //     _focusNode.unfocus();
    //     print(imagePath);
    //   }
    // }
    // await EasyLoading.dismiss();
  }

  void onCameraShare() async {
    // List<Media>? res = await ImagesPicker.openCamera(
    //   pickType: PickType.image,
    //   quality: 0.5,
    // );

    // if (res != null && res.isNotEmpty) {
    //   print(res[0].path);
    //   print(res[0].thumbPath);

    //   var file = File(res.first.path);
    //   var result = await FlutterImageCompress.compressWithFile(file.absolute.path, quality: 80);
    //   if (result != null) {
    //     String productPath = widget.product.docId!.path.split('/').last;
    //     String chatroomDocId = widget.receiver.docId == widget.product.sellerId
    //         ? ZConfig.userAccount.docId!.path.split('/').last
    //         : widget.receiver.docId!.path.split('/').last;
    //     String fileName =
    //         file.path.split('/').last.split('.').first + DateTime.now().toString() + '.jpg';

    //     String storagePath = 'chatRooms/images/$productPath/$chatroomDocId/$fileName';

    //     await FirebaseStorage.instance.ref(storagePath).putData(result);

    //     String imagePath = await FirebaseStorage.instance.ref(storagePath).getDownloadURL();

    //     _textEditingController.text = imagePath;
    //     onSendMessage();
    //     _focusNode.unfocus();
    //     print(imagePath);
    //   }
    // }
    // }
  }
}
