import 'dart:math';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:faker/faker.dart';
import 'package:get/get.dart';
import 'package:zzin/common/zconfig.dart';
import 'package:zzin/postbox/postbox.dart';
import 'package:zzin/postbox/query_postbox.dart';
import 'package:zzin/upload_page/faker_data_provider_kor.dart';

class PostboxController {
  Future refreshPostBox() async {
    QueryPostbox publicQueryPostbox = QueryPostbox();
    await Get.put(publicQueryPostbox, tag: 'public_postbox').queryPostbox(isPublic: true);
    for (var index = 0; index < publicQueryPostbox.postboxes.length; index++) {
      await PostboxController().addPostBox(publicQueryPostbox.postboxes[index].data()!, false);
    }
  }

  Future<bool> addPostBox(Postbox postbox, [bool isPublic = true]) async {
    try {
      if (isPublic) {
        var collectionRef = FirebaseFirestore.instance
            .collection(ZConfig.postboxCollection)
            .withConverter<Postbox>(
                fromFirestore: (snapshots, _) => Postbox.fromJson(snapshots.data()!),
                toFirestore: (commonPostbox, _) => commonPostbox.toJson());
        DocumentReference<Postbox> doc = await collectionRef.add(postbox);
        await doc.update({'docId': doc.id});
      } else {
        var collectionRef = FirebaseFirestore.instance
            .doc(ZConfig.userAccount.docId!.path)
            .collection(ZConfig.postboxCollection)
            .withConverter<Postbox>(
                fromFirestore: (snapshots, _) => Postbox.fromJson(snapshots.data()!),
                toFirestore: (commonPostbox, _) => commonPostbox.toJson());

        QuerySnapshot<Postbox> doc =
            await collectionRef.where('docId', isEqualTo: postbox.docId).get();
        if (doc.size == 0) {
          await collectionRef.add(postbox);
          print('알림함 추가: ${postbox.title}');
        } else {
          await doc.docs.first.reference.update(postbox.toJsonWithoutPrivate());
        }
      }

      return true;
    } catch (e) {
      print(e);
    }
    return false;
  }

  Future<void> updatePostbox(Postbox postbox) async {
    await FirebaseFirestore.instance
        .collection(ZConfig.postboxCollection)
        .withConverter<Postbox>(
            fromFirestore: (snapshots, _) => Postbox.fromJson(snapshots.data()!),
            toFirestore: (commonPostbox, _) => commonPostbox.toJson())
        .doc(postbox.docId)
        .update(postbox.toJson());
  }

  Future<void> updatePrivatePostboxItem(Postbox postbox, String key, dynamic value) async {
    var collectionRef = FirebaseFirestore.instance
        .doc(ZConfig.userAccount.docId!.path)
        .collection(ZConfig.postboxCollection)
        .withConverter<Postbox>(
            fromFirestore: (snapshots, _) => Postbox.fromJson(snapshots.data()!),
            toFirestore: (commonPostbox, _) => commonPostbox.toJson());
    QuerySnapshot<Postbox> snapshot =
        await collectionRef.where('docId', isEqualTo: postbox.docId).get();
    if (snapshot.size == 1) {
      print(snapshot.docs.first.id);
      await collectionRef.doc(snapshot.docs.first.id).update({key: value});
      print('개인 알람 삭제 추가');
    }
  }

  Future<bool> isNewPostbox() async {
    QueryPostbox publicQuery = QueryPostbox();
    QueryPostbox privateQuery = QueryPostbox();

    await publicQuery.queryPostbox(isPublic: true);
    // 날짜가 지난 알람 제거;
    publicQuery.postboxes
        .removeWhere((postbox) => postbox.data()!.expirationDate.compareTo(Timestamp.now()) < 1);
    await privateQuery.queryPostbox(isPublic: false, isContainIsRemoved: true);

    for (var index = 0; index < publicQuery.postboxes.length; index++) {
      if (privateQuery.postboxes
          .where((private) => private.data()!.docId == publicQuery.postboxes[index].data()!.docId)
          .isEmpty) {
        print('새로운 공용 알림이 있습니다. ${publicQuery.postboxes[index].data()!.docId}');

        return true;
      }
    }

    //개인 알림에서 읽지 않음 메시지가 있는가?
    if (privateQuery.postboxes.any((postbox) =>
        /* postbox.data()!.docId.startsWith('private_') && */ postbox.data()!.isRead == false)) {
      print('읽지 않거나 새로운 개인알림이 있습니다.');
      return true;
    }

    return false;
  }

  Future<List<DocumentSnapshot<Postbox>>> getUrgentNotice() async {
    QueryPostbox publicQuery = QueryPostbox();
    await publicQuery.queryPostbox(isPublic: true, isContainIsRemoved: true);
    if (publicQuery.postboxes.isNotEmpty) {
      Iterable<DocumentSnapshot<Postbox>> urgentNotices = publicQuery.postboxes
          .where((element) => element.data()!.type == PostboxType.UrgentNotice);
      return urgentNotices.toList();
    }
    return <DocumentSnapshot<Postbox>>[];
  }

////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////
  void addRandomPost([bool isPublic = true]) async {
    var random = Random();
    var fakerKor = Faker(provider: FakerDataProviderKor());

    var randomDateTime = DateTime(
      2021, // year
      random.nextInt(9), // month
      random.nextInt(30), // day
      random.nextInt(24), // hour
      random.nextInt(60), // minute
      random.nextInt(60), // second
      random.nextInt(1000), // millisecond
      random.nextInt(1000), // microsecond
    );

    if (random.nextInt(2) == 0) {
      randomDateTime = DateTime.now();
    }
    var randomTimestamp = Timestamp.fromDate(randomDateTime);

    var randomContents = generateRandomContents();
    var post = Postbox(
      reportingDate: randomTimestamp,
      expirationDate:
          Timestamp.fromDate(randomTimestamp.toDate().add(Duration(days: random.nextInt(10)))),
      type: PostboxType.values[random.nextInt(PostboxType.values.length)],
      title: fakerKor.lorem.word(),
      reporterRef: ZConfig.userAccount.docId!,
      isRemoved: false,
      docId: 'private_' + random.nextInt(100000000).toString(),
      contents: randomContents,
      isRead: false,
    );

    await addPostBox(post, isPublic);
  }

  Map generateRandomContents() {
    var fakerKor = Faker(provider: FakerDataProviderKor());
    var random = Random();

    int imageIndex = 0;
    int textIndex = 0;
    int linkIndex = 0;

    Map result = {};
    List order = <String>[];
    for (var i = 0; i < 10; i++) {
      switch (random.nextInt(3)) {
        case 0:
          result.putIfAbsent('text' + textIndex.toString(), () => fakerKor.lorem.sentence());
          order.add('text' + textIndex.toString());
          textIndex++;
          break;
        case 1:
          var width = RandomGenerator().integer(10, min: 2) * 100;

          result.putIfAbsent(
              'image' + imageIndex.toString(), () => 'https://picsum.photos/$width/${width - 100}');
          order.add('image' + imageIndex.toString());
          imageIndex++;
          break;
        case 2:
          result.putIfAbsent('link' + linkIndex.toString(), () => fakerKor.lorem.word());
          order.add('link' + linkIndex.toString());
          linkIndex++;
          break;
      }
    }

    result.putIfAbsent('order', () => order);
    return result;
  }
}
