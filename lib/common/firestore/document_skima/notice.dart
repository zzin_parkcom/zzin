import 'package:cloud_firestore/cloud_firestore.dart';

class Notice {
  String title;
  int tag;
  Timestamp registDate;
  int type;
  String contents;
  bool isPost;

  Notice({
    required this.title,
    required this.tag,

    /// a 0: 일반(태그없음), 1:NEW(태그)
    required this.registDate, // 공지 등록일
    required this.type, // 0: 텍스트, 1: 이미지
    required this.contents, //공지 내용 : 공지 타입이 1(이미지)인 경우, 스토리지 이미지 파일 링크
    required this.isPost, //게시 여부 (TRUE시 게시. 노출됨)
  });

  Notice.fromJson(Map<String, Object?> json)
      : this(
          title: json['title'] as String,
          tag: json['tag'] as int,
          registDate: json['registDate'] as Timestamp,
          type: json['type'] as int,
          contents: json['contents'] as String,
          isPost: json['isPost'] as bool,
        );

  Map<String, Object?> toJson() {
    return {
      'title': title,
      'tag': tag,
      'registDate': registDate,
      'type': type,
      'contents': contents,
      'isPost': isPost,
    };
  }
}
