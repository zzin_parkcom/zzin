// import 'dart:convert';
// import 'dart:io';

// import 'package:cloud_firestore/cloud_firestore.dart';
// import 'package:flutter/material.dart';
// import 'package:geocoding/geocoding.dart';
// import 'package:geolocator/geolocator.dart';
// import 'package:path_provider/path_provider.dart';
// import 'package:zzin/common/zconfig.dart';

// class AddressToGeo {
//   final int id;
//   final String juso;
//   final double latitude;
//   final double longitude;

//   AddressToGeo(this.id, this.juso, this.latitude, this.longitude);
//   Map<String, dynamic> toJson() =>
//       {'id': id, 'juso': juso, 'latitude': latitude, 'longitude': longitude};
// }

// class GeoProcess extends StatefulWidget {
//   @override
//   _GeoProcessState createState() => _GeoProcessState();
// }

// class _GeoProcessState extends State<GeoProcess> {
//   final getLocation = (() async* {
//     var streamList = <AddressToGeo>[];
//     final directory = await getExternalStorageDirectory();
//     final file = File('${directory!.path}/test.json');

//     for (var i = 0; i < ZConfig.saleAreaList.length; i++) {
//       // if (i > 2353 && i < 2364) {
//       //   continue;
//       // }
//       var address;
//       if (ZConfig.saleAreaList[i].trim().endsWith('읍') ||
//           ZConfig.saleAreaList[i].trim().endsWith('면')) {
//         address = ZConfig.saleAreaList[i] + '사무소';
//       } else if (ZConfig.saleAreaList[i].trim().endsWith('소')) {
//         address = ZConfig.saleAreaList[i];
//       } else {
//         address = ZConfig.saleAreaList[i] + ' 주민센터';
//       }

//       var locations;
//       try {
//         locations = await locationFromAddress(address, localeIdentifier: 'ko_KR');
//       } catch(e) {
//         print(e);
//         locations =  [Location(latitude: 999.999, longitude: 999.999, timestamp: DateTime.now())];
//       }
//         // ZConfig.saleAreaJson[i].putIfAbsent('latitude', () => locations[0].latitude);
//         // ZConfig.saleAreaJson[i].putIfAbsent('longitude', () => locations[0].longitude);

//         ZConfig.saleAreaJson[i]['latitude'] = locations[0].latitude;
//         ZConfig.saleAreaJson[i]['longitude'] = locations[0].longitude;

//         print('검색어:$address :  ${ZConfig.saleAreaJson[i]}');

//         var addressToGeo = AddressToGeo(ZConfig.saleAreaJson[i]['id'],
//             ZConfig.saleAreaJson[i]['juso'], locations[0].latitude, locations[0].longitude);
//         streamList.add(addressToGeo);
//         yield streamList;
//         print('$i : ${ZConfig.saleAreaJson[i]['juso']}');
//     }
//     var result = jsonEncode(ZConfig.saleAreaJson);

//     await file.writeAsString(result, mode: FileMode.append);

//     print('End.........................................');
//   })();

//   @override
//   Widget build(BuildContext context) {
//     return StreamBuilder(
//       stream: getLocation,
//       builder: (context, snapshot) {
//         if (!snapshot.hasData) {
//           return Center(
//             child: CircularProgressIndicator(),
//           );
//         } else {
//           var lists = snapshot.data! as List<AddressToGeo>;
//           return ListView.separated(
//               itemBuilder: (context, index) {
//                 return ListTile(
//                     title: Row(
//                   mainAxisAlignment: MainAxisAlignment.spaceBetween,
//                   children: [
//                     Expanded(
//                       flex: 1,
//                       child: Text(
//                         lists[index].juso,
//                         style: TextStyle(fontSize: 10),
//                       ),
//                     ),
//                     Expanded(
//                       flex: 1,
//                       child: Text(
//                         lists[index].latitude.toString(),
//                         style: TextStyle(fontSize: 10),
//                       ),
//                     ),
//                     Expanded(
//                       flex: 1,
//                       child: Text(
//                         lists[index].longitude.toString(),
//                         style: TextStyle(fontSize: 10),
//                       ),
//                     ),
//                   ],
//                 ));
//               },
//               separatorBuilder: (context, index) => Divider(),
//               itemCount: lists.length);
//         }
//       },
//     );
//   }

//   void test() async {
//     var position = await Geolocator.getCurrentPosition();
//     print(position);
//     var geoPoint = GeoPoint(position.latitude, position.longitude);
//     var result = await placemarkFromCoordinates(geoPoint.latitude, geoPoint.longitude,
//         localeIdentifier: 'ko_KR');
//     result.forEach((element) {
//       print(element);
//       print('\n');
//     });
//   }
// }
