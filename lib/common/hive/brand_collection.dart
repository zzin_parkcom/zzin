import 'package:hive/hive.dart';
import 'package:zzin/common/hive/zhive.dart';
import 'package:zzin/common/zconfig.dart';

part 'brand_collection.g.dart';

@HiveType(typeId: 1)
class BrandCollection extends HiveObject {
  @HiveField(0)
  final int id;
  @HiveField(1)
  final String nameKr;
  @HiveField(2)
  final String nameEn;

  BrandCollection({required this.id, required this.nameKr, required this.nameEn});

  @override
  String toString() {
    var length = nameKr.length;
    var countWhite = nameKr.split(' ').length;
    var padding = 30 - length + countWhite;
    return 'id: ${id.toString().padLeft(3)} nameKr:${nameKr.padRight(padding)} nameEn:$nameEn';
  }

  static Box<BrandCollection> get to => ZHive.brandBox;

  static String getNameKr(int id) => ZHive.brandBox.getAt(id)!.nameKr;
  static String getNameEn(int id) => ZHive.brandBox.getAt(id)!.nameEn;

  static BrandCollection getBrandById(int id) {
    return ZHive.brandBox.values.where((element) => element.id == id).first;
  }

  List<String> makeTagList() {
    var tags = <String>[];
    tags.addAll(nameKr.split(' '));
    tags.addAll(nameEn.split(' '));
    return tags;
  }

  static int findString(List<String> findList) {
    var brandResult = ZHive.brandBox.values.firstWhere(
        (brand) => brand
            .makeTagList()
            .any((tag) => findList.any((find) => tag.toLowerCase() == find.toLowerCase())),
        orElse: () => BrandCollection(id: NotFound, nameKr: '결과없음', nameEn: '결과없음'));

    return brandResult.id;
  }
}
