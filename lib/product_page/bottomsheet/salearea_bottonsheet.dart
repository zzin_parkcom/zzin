import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:zzin/common/hive/address_collection.dart';
import 'package:zzin/common/hive/zhive.dart';
import 'package:zzin/common/widget/zbutton_blue.dart';
import 'package:zzin/common/zconfig.dart';
import 'package:zzin/common/zutils.dart';
import 'package:zzin/upload_page/salearea_search_page.dart';

class SalesAreaBottomSheet extends StatefulWidget {
  const SalesAreaBottomSheet({Key? key}) : super(key: key);

  @override
  _SalesAreaBottomSheetState createState() => _SalesAreaBottomSheetState();
}

class _SalesAreaBottomSheetState extends State<SalesAreaBottomSheet> {
  int step = 3;
  int id = NotSelected;
  List areaRadiusList = ['500m', '2km', '10km', '50km+'];
  final TextEditingController _controller = TextEditingController();

  @override
  void initState() {
    super.initState();

    id = ZHive.generalBox.get('SearchOption_addressId', defaultValue: NotSelected);
    step = ZHive.generalBox.get('SearchOption_areaStep', defaultValue: 3).toInt();

    if (id == NotSelected) {
      id = ZConfig.userAccount.regionCode!;
    }
    if (step == NotSelected) {
      step = 3;
    }
    setId(id);
  }

  void setId(int idValue) {
    if (idValue == NotSelected) {
      return;
    }
    id = idValue;
    ZHive.generalBox.put('SearchOption_addressId', id);
    ZHive.generalBox.put('SearchOption_areaStep', step);
    _controller.text = AddressCollection.getRegionName(id);
  }

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: Container(
        padding: const EdgeInsets.fromLTRB(30, 30, 30, 30),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            topTitleWidget(),
            Padding(
              padding: const EdgeInsets.fromLTRB(10, 0, 10, 0),
              child: saleAreaWidget(),
            ),
            Padding(
              padding: const EdgeInsets.fromLTRB(10, 0, 10, 0),
              child: areaSizeWidget(),
            ),
          ],
        ),
      ),
    );
  }

  Widget topTitleWidget() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        const Text(
          '검색 범위',
          style: TextStyle(fontSize: ZSize.bottomSheetTitleFontSize),
          textAlign: TextAlign.left,
        ),
        TextButton(
          onPressed: () {
            ZHive.generalBox.put('SearchOption_addressId', NotSelected);
            ZHive.generalBox.put('SearchOption_areaStep', NotSelected);
            Get.back();
          },
          child: const Text(
            '초기화',
            style: TextStyle(fontSize: ZSize.bottomSheetFontSize, color: ZConfig.zzin_pointColor),
          ),
        ),
      ],
    );
  }

  Widget saleAreaWidget() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        const Text('기준 지역', style: TextStyle(fontSize: ZSize.bottomSheetFontSize)),
        Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            mainAxisSize: MainAxisSize.min,
            children: [
              Expanded(
                child: TextField(
                  controller: _controller,
                  enabled: false,
                  decoration: const InputDecoration(
                      disabledBorder: InputBorder.none, hintText: '기준 지역을 선택하세요'),
                  style: const TextStyle(fontSize: 14, color: ZConfig.grey_text),
                ),
              ),
              ZButtonYellow(
                isSelected: true,
                padding: MaterialStateProperty.all(const EdgeInsets.fromLTRB(10, 0, 10, 0)),
                text: '지역 설정',
                fontSize: 14,
                onPressed: onSaleAreaButton,
              ),
            ]),
      ],
    );
  }

  Widget areaSizeWidget() {
    return Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          const Text('범위 설정', style: TextStyle(fontSize: ZSize.bottomSheetFontSize)),
          Column(children: [
            SliderTheme(
              data: SliderTheme.of(context).copyWith(
                activeTrackColor: ZConfig.zzin_pointColor,
                inactiveTrackColor: ZConfig.grey_text,
                trackShape: const RoundedRectSliderTrackShape(),
                trackHeight: 4.0,
                inactiveTickMarkColor: ZConfig.zzin_white,
                activeTickMarkColor: Colors.indigo[100],
                thumbColor: ZConfig.zzin_pointColor,
                thumbShape: const RoundSliderThumbShape(enabledThumbRadius: 8.0),
                overlayColor: Colors.white54,
                overlayShape: const RoundSliderOverlayShape(overlayRadius: 12.0),
              ),
              child: Slider(
                  min: 0,
                  max: 3,
                  divisions: 3,
                  value: step.toDouble(),
                  onChanged: onAreaSliderChanged),
            ),
            Padding(
              padding: const EdgeInsets.fromLTRB(0, 0, 0, 0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: List.generate(areaRadiusList.length, (index) {
                  return Text(
                    areaRadiusList[index],
                    overflow: TextOverflow.ellipsis,
                    style: TextStyle(
                        fontSize: 12,
                        color: index == step ? ZConfig.zzin_pointColor : ZConfig.grey_text),
                  );
                }),
              ),
            )
          ]),
        ]);
  }

  void onAreaSliderChanged(double value) {
    step = value.toInt();
    print(step);
    ZHive.generalBox.put('SearchOption_areaStep', step);

    setState(() {});
  }

  void onSaleAreaButton() async {
    var id = await showModalBottomSheetInt(context, _saleareaBottomSheetContent);
    setId(id);
    setState(() {});
  }

  Widget _saleareaBottomSheetContent(BuildContext context) {
    return const SaleareaSearchPage();
  }
}
