import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/services.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:zzin/common/firestore/document_skima/user_account.dart';
import 'package:zzin/login/signin/social_signin.dart';

class ZGoogleSignin implements SocialSignin {
  GoogleSignIn googleSignIn = GoogleSignIn();
  GoogleSignInAccount? googleUser;

  @override
  Future<bool> login() async {
    try {
      googleUser = await googleSignIn.signIn();
      if (googleUser == null) {
        return false;
      }
      print(googleUser.toString());
      return true;
    } on PlatformException catch (e) {
      print('${e.code} ${e.message}');
    }
    return false;
  }

  @override
  Future<void> unlink() async {
    try {
      print('Google user disconnect : ${googleSignIn.currentUser}');
      await googleSignIn.disconnect();
    } on PlatformException catch (e) {
      print('${e.code} ${e.message}');
    }
  }

  @override
  Future<void> logout() async {
    try {
      print('Google user logout : ${googleSignIn.currentUser}');
      await googleSignIn.signOut();
    } on PlatformException catch (e) {
      print('${e.code} ${e.message}');
    }
  }

  @override
  Future<ZUserAccount> getAccountInfo() async {
    if (googleUser == null) {
      throw Exception('유저정보가 존재하지 않습니다.');
    }
    try {
      return ZUserAccount(google: {
        'id': googleUser!.id,
        'displayName': googleUser!.displayName,
        'email': googleUser!.email,
        'photoUrl': googleUser!.photoUrl,
      }, productList: [], recentList: [], likeList: []);
    } on PlatformException catch (e) {
      print('${e.code} ${e.details}');
    }
    return ZUserAccount.initZero();
  }

  @override
  String social() {
    return 'google';
  }

  Future<AuthCredential> authenticate() async {
    final authentication = await googleUser!.authentication;
    final AuthCredential credential = GoogleAuthProvider.credential(
        accessToken: authentication.accessToken, idToken: authentication.idToken);

    return credential;
  }
}
