import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:zzin/common/firestore/document_skima/product.dart';
import 'package:zzin/common/firestore/query_product.dart';
import 'package:zzin/common/utils/zenum.dart';
import 'package:zzin/common/zconfig.dart';
import 'package:zzin/product_page/product_detail_page/product_detail_view.dart';
import 'package:zzin/product_page/widget/product_list_generator.dart';

class PopularSearchPage extends StatefulWidget {
  final String tag;
  final String tagHeroPrefix;
  const PopularSearchPage({Key? key, required this.tag, required this.tagHeroPrefix})
      : super(key: key);

  @override
  _PopularSearchPageState createState() => _PopularSearchPageState();
}

class _PopularSearchPageState extends State<PopularSearchPage> {
  final ScrollController _scrollController = ScrollController();
  late final QueryProduct queryProductLike;

  @override
  void initState() {
    super.initState();
    _scrollController.addListener(_scrollListener);
    queryProductLike = Get.find(tag: widget.tag);
  }

  @override
  void dispose() {
    _scrollController.removeListener(_scrollListener);
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Center(child: Text('인기상품', style: TextStyle(fontSize: ZSize.appBarTextSize))),
        elevation: 1,
        actions: [SizedBox(width: AppBar().preferredSize.height)],
      ),
      body: ProductListGenerator(
        products: queryProductLike.products,
        onRefresh: onRefresh,
        scrollController: _scrollController,
        onDetailPage: onListItemTap,
        tagHeroPrefix: 'Popular_Search_',
        noProductMessages: const ['상품이 없습니다.'],
      ),
    );
  }

  Future<void> getProduct() async {
    await queryProductLike.queryProductBySort(3, saleStausIndex: ProductSaleStatus.ONSALE.index);
  }

  Future onRefresh() async {
    await queryProductLike.queryProductBySort(3, saleStausIndex: ProductSaleStatus.ONSALE.index);
  }

  bool isScrolling = false;
  void _scrollListener() async {
    var maxScroll = _scrollController.position.maxScrollExtent;
    var currentScroll = _scrollController.position.pixels;
    var delta = maxScroll * ZConfig.scrollRatioQueryMore;

    if (maxScroll - currentScroll <= delta &&
        queryProductLike.isLoading == false &&
        isScrolling == false) {
      isScrolling = true;
      await queryProductLike.queryProductBySort(3,
          saleStausIndex: ProductSaleStatus.ONSALE.index, byScroll: true);
      isScrolling = false;
    }
  }

  void onListItemTap(Product product) {
    var tag = DateTime.now().toString();
    var savedProduct = Get.put(Product.fromJson(product.toJson()), tag: tag);

    Get.to(
        () => ProductDetailView(
              tag: tag,
              tagHeroPrefix: widget.tagHeroPrefix,
            ),
        transition: Transition.fadeIn,
        duration: ZConfig.heroAnimationDuration);

    if (!savedProduct.isEqualValue(product)) {
      onRefresh();
    }

    Get.delete(tag: tag);
  }
}
