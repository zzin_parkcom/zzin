import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:zzin/common/hive/search_collection.dart';
import 'package:zzin/common/zconfig.dart';

class SortOptionBottomSheet extends StatefulWidget {
  const SortOptionBottomSheet({Key? key}) : super(key: key);

  @override
  _SortOptionBottomSheetState createState() => _SortOptionBottomSheetState();
}

class _SortOptionBottomSheetState extends State<SortOptionBottomSheet> {
  int sort = NotSelected;
  final ScrollController scrollController = ScrollController();

  @override
  void initState() {
    super.initState();
    sort = SearchCollection.instance.sort;
    // if (sort == NotSelected) {
    //   sort = 0; // 정확도 정력 기본으로 설정.
    // }
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.fromLTRB(30, 30, 30, 0),
      child: SingleChildScrollView(
        controller: scrollController,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            topTitleWidget(),
            const SizedBox(height: 5),
            sortListWidget(),
          ],
        ),
      ),
    );
  }

  Widget topTitleWidget() {
    return const Text(
      '정렬옵션',
      style: TextStyle(fontSize: ZSize.bottomSheetTitleFontSize),
      textAlign: TextAlign.left,
    );
  }

  Widget sortListWidget() {
    return ListView.separated(
      shrinkWrap: true,
      separatorBuilder: (context, index) => const Divider(),
      itemCount: ZConfig.sortNameList.length,
      itemBuilder: (context, index) {
        return ListTile(
          title: Text(
            ZConfig.sortNameList[index],
            style: const TextStyle(fontSize: ZSize.bottomSheetFontSize),
          ),
          trailing:
              (sort == index) ? const Icon(Icons.check, color: ZConfig.zzin_pointColor) : null,
          onTap: () {
            setState(() {
              SearchCollection.instance.sort = index;
              sort = index;
              Get.back();
            });
          },
        );
      },
    );
  }
}
