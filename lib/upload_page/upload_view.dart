import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:get/get.dart';
import 'package:keyboard_dismisser/keyboard_dismisser.dart';
import 'package:zzin/common/firestore/document_skima/product.dart';
import 'package:zzin/common/hive/address_collection.dart';
import 'package:zzin/common/hive/brand_collection.dart';
import 'package:zzin/common/hive/category_collection.dart';
import 'package:zzin/common/hive/upload_collection.dart';
import 'package:zzin/common/utils/zenum.dart';
import 'package:zzin/common/widget/appbar/zappbar.dart';
import 'package:zzin/common/widget/zbutton_blue.dart';
import 'package:zzin/common/zconfig.dart';
import 'package:zzin/common/zutils.dart';
import 'package:zzin/entrance/skeleton.dart';
import 'package:zzin/upload_page/selection/brand_selection.dart';
import 'package:zzin/upload_page/selection/category_selection.dart';
import 'package:zzin/upload_page/selection/description_selection.dart';
import 'package:zzin/upload_page/selection/photo_selection/photo_selection.dart';
import 'package:zzin/upload_page/selection/price_selection.dart';
import 'package:zzin/upload_page/selection/salearea_selection.dart';
import 'package:zzin/upload_page/selection/status_selection.dart';
import 'package:zzin/upload_page/selection/tip_selection.dart';
import 'package:zzin/upload_page/selection/title_selection.dart';

class UploadView extends StatefulWidget {
  final String tag;
  final UploadCollection uploadCollection;
  final bool isModify;

  const UploadView(
      {required this.uploadCollection, this.isModify = false, required this.tag, Key? key})
      : super(key: key);

  @override
  _UploadViewState createState() => _UploadViewState();
}

class _UploadViewState extends State<UploadView> {
  late Product _product;
  final ScrollController scrollController = ScrollController();
  final _formKey = GlobalKey<FormState>();

  late final PhotoSelection _photoSelectWidget;

  @override
  void initState() {
    super.initState();
    if (widget.isModify) {
      _product = Get.find(tag: widget.tag);
    }

    _photoSelectWidget = PhotoSelection(uploadCollection: widget.uploadCollection);

    SchedulerBinding.instance!.addPostFrameCallback((_) => frameCallback(context));
  }

  void frameCallback(BuildContext context) async {
    await ZConfig.analytics.logEvent(name: 'Upload_Product_View');
  }

  @override
  void dispose() {
    scrollController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return KeyboardDismisser(
      child: Scaffold(
        resizeToAvoidBottomInset: false,
        appBar: zAppBar(),
        body: SingleChildScrollView(
          padding: const EdgeInsets.all(10.0),
          controller: scrollController,
          child: Form(
            key: _formKey,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                const Padding(padding: EdgeInsets.only(top: 10)),
                _photoSelectWidget,
                const TipSelection(),
                selectionDivider(),
                CategorySelection(20, uploadCollection: widget.uploadCollection),
                selectionDivider(),
                BrandSelection(20, uploadCollection: widget.uploadCollection),
                selectionDivider(),
                StatusSelection(20, uploadCollection: widget.uploadCollection),
                selectionDivider(),
                SaleAreaSelection(20, uploadCollection: widget.uploadCollection),
                selectionDivider(),
                PriceSelection(20, uploadCollection: widget.uploadCollection),
                selectionDivider(),
                TitleSelection(20, uploadCollection: widget.uploadCollection),
                selectionDivider(),
                DescriptionSelection(20, uploadCollection: widget.uploadCollection),
                // selectionDivider(),
              ],
            ),
          ),
        ),
        bottomNavigationBar: Padding(
          padding: const EdgeInsets.fromLTRB(10, 0, 10, 0),
          child: Row(
            children: [
              Expanded(
                child: ZButtonYellow(
                  text: widget.isModify ? '상품 수정' : '상품 등록',
                  isSelected: true,
                  fontSize: 20,
                  padding: MaterialStateProperty.all(const EdgeInsets.fromLTRB(0, 20, 0, 20)),
                  onPressed: onUpload,
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget selectionDivider() =>
      const Padding(padding: EdgeInsets.fromLTRB(10, 10, 10, 10), child: Divider());

  void onUpload() async {
    _formKey.currentState!.save();
    if (_formKey.currentState!.validate()) {
      if (!ZConfig.userAccount.isRegistered()) {
        showInfo(context, '상품등록을 위해서는 회원가입이 필요합니다.');
        return;
      }

      FocusScope.of(context).unfocus();

      if (widget.isModify) {
        _updateProduct();
      } else {
        _uploadProduct();
      }
    } else {
      print('Form validate FAILED!!!');
    }
  }

  Future<void> onBack() async {
    if (widget.isModify) {
      Get.back();
    } else {
      _formKey.currentState!.save();
      MainTabController.to.setIndex(0);
    }
  }

  AppBar zAppBar() => ZAppBar(
        title: Text(
          widget.isModify ? '상품 수정' : '상품업로드',
          style: const TextStyle(color: ZConfig.zzin_black, fontSize: ZSize.appBarTextSize),
        ),
        actions: [
          widget.isModify
              ? const SizedBox.shrink()
              : Padding(
                  padding: const EdgeInsets.fromLTRB(10, 10, 10, 10),
                  child: TextButton(
                    onPressed: () async {
                      await _clearPersistence();
                      FocusManager.instance.primaryFocus!.unfocus();
                      setState(() {});
                      print(widget.uploadCollection.price);
                      _formKey.currentState!.reset();
                    },
                    child: const Text(
                      '초기화',
                      style: TextStyle(fontSize: 14, color: ZConfig.zzin_pointColor),
                    ),
                  ),
                ),
          const SizedBox(width: 5)
        ],
      );

  List<String> makeTag(
      String title, int categoryId, int brandId, AddressCollection address, int status) {
    var tags = <String>[];
    // 타이틀 태그
    // tags.addAll(title.split(' '));
    // 카테고리 태그
    var category = CategoryCollection.getCategoryById(categoryId);
    tags.addAll(category.makeTagList());
    // 브랜드 태그
    var brand = BrandCollection.getBrandById(brandId);
    tags.addAll(brand.makeTagList());
    // 주소 태그
    // tags.addAll(address.makeTagList());
    // 물건 상태
    if (status == ProductStatus.NEW.index) {
      var newProduct = [
        // '새상품','새제품','새제품','신제품','새물건','새것','새거','박스채','뜯지않은','미개봉',
        // '미사용','미사용제품','미사용상품','미사용물건', // '신상'
        '새상품',
      ];
      tags.addAll(newProduct);
    } else {
      var oldProduct = [
        '중고',
        // '빈티지','유니크','프리오운드', '세컨핸드', '사용감', '생활기스'
      ];
      tags.addAll(oldProduct);
    }

    var tagsRemoveDuplicate = tags.toSet().toList(); // 중복 제거
    return tagsRemoveDuplicate;
  }

  void _uploadProduct() async {
    var title = widget.uploadCollection.title;
    var categoryId = widget.uploadCollection.categoryId;
    var brandId = widget.uploadCollection.brandId;
    var status = widget.uploadCollection.status;
    var address = AddressCollection.getAddressByRegionCode(widget.uploadCollection.saleAreaId);

    var tags = makeTag(title, categoryId, brandId, address, status);

    var region1Depth = address.regionCode.toString().substring(0, 2);
    var region2Depth = address.regionCode.toString().substring(0, 5);
    var geoPoint = GeoPoint(address.latitude, address.longitude);

    var files = await _photoSelectWidget.getImageList();

    var defaultImagesUrl =
        List<String>.generate(files.length, (index) => ZConfig.defaultImageStorageURL);
    var defaultThumbUrl = ZConfig.defaultImageStorageURL;

    Future taskAddProduct = Product(
            articleStatus: ProductArticleStatus.REMOVED.index, // 모든 등록작업이 완료될때 활성화.
            title: title,
            contents: widget.uploadCollection.description,
            category: categoryId,
            brand: brandId,
            status: status,
            price: widget.uploadCollection.price,
            saleRegistDate: Timestamp.now(),
            saleModifyDate: Timestamp.now(),
            regionCode: address.regionCode,
            region1Depth: int.parse(region1Depth),
            region2Depth: int.parse(region2Depth),
            regionName: address.regionName,
            regionGeoPoint: geoPoint,
            saleStatus: ProductSaleStatus.ONSALE.index,
            likeCounts: 0,
            images: defaultImagesUrl,
            imagesCache: widget.uploadCollection.imagesId,
            imagesId: widget.uploadCollection.imagesId,
            thumbnail: defaultThumbUrl,
            tags: tags)
        .addProduct(files);

    await EasyLoading.show(
        status: '상품등록중...', maskType: EasyLoadingMaskType.black, dismissOnTap: false);
    var value = await taskAddProduct;
    await _clearPersistence();
    await EasyLoading.dismiss();

    Get.rawSnackbar(title: '상품등록', message: '상품 등록이 ${value ? '완료되었습니다.' : '실패하였습니다. ㅜ.ㅜ'}');
    MainTabController.to.setIndex(0);
    await ZConfig.analytics.logEvent(name: 'Upload_Product_Complete');
  }

  void _updateProductData() {
    _product.articleStatus = ProductArticleStatus.NORMAL.index;
    _product.title = widget.uploadCollection.title;
    _product.contents = widget.uploadCollection.description;
    _product.category = widget.uploadCollection.categoryId;
    _product.brand = widget.uploadCollection.brandId;
    _product.status = widget.uploadCollection.status;
    _product.price = widget.uploadCollection.price;
    _product.saleModifyDate = Timestamp.now();

    var address = AddressCollection.getAddressByRegionCode(widget.uploadCollection.saleAreaId);
    var tags = makeTag(
        _product.title, _product.category, _product.brand, address, widget.uploadCollection.status);

    var id = address.regionCode;
    var region1Depth = address.regionCode.toString().substring(0, 2);
    var region2Depth = address.regionCode.toString().substring(0, 5);
    var geoPoint = GeoPoint(address.latitude, address.longitude);
    _product.regionCode = id;
    _product.region1Depth = int.parse(region1Depth);
    _product.region2Depth = int.parse(region2Depth);
    _product.regionName = address.regionName;
    _product.regionGeoPoint = geoPoint;
    _product.saleStatus = _product.saleStatus;
    _product.likeCounts = _product.likeCounts;
    _product.images = _product.images;
    _product.thumbnail = _product.thumbnail;
    _product.imagesId = widget.uploadCollection.imagesId;
    _product.tags = tags;
  }

  void _updateProduct() async {
    var files = await _photoSelectWidget.getImageList();
    _updateProductData();

    await EasyLoading.show(
        status: '업데이트 중...', maskType: EasyLoadingMaskType.black, dismissOnTap: false);

    var bSuccess = false;
    bSuccess = await _product.updateProduct(files);

    await EasyLoading.dismiss();
    Get.back(result: _product);
    if (bSuccess) {
      Get.rawSnackbar(title: '상품등록', message: '상품 업데이트가 완료되었습니다.');
    } else {
      Get.rawSnackbar(title: '상품등록', message: '상품 업데이트가 실패하였습니다.');
    }
  }

  Future _clearPersistence() async {
    print('clearPersistence');

    await widget.uploadCollection.initialize();
    _photoSelectWidget.clearState();
    print(widget.uploadCollection.price);
  }
}
