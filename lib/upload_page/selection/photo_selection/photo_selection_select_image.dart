import 'package:flutter/material.dart';
import 'package:wechat_assets_picker/wechat_assets_picker.dart';
import 'package:zzin/common/zconfig.dart';
import 'package:zzin/upload_page/customFormfield/custom_formfield.dart';

class PhotoSelectionSelectImage extends StatefulWidget {
  final List<AssetEntity> assets;
  final void Function(int?) onSaved;
  final String? Function(int?) validator;
  final void Function() multiPickup;

  const PhotoSelectionSelectImage(
      {Key? key,
      required this.assets,
      required this.onSaved,
      required this.validator,
      required this.multiPickup})
      : super(key: key);

  @override
  _PhotoSelectionSelectImageState createState() => _PhotoSelectionSelectImageState();
}

class _PhotoSelectionSelectImageState extends State<PhotoSelectionSelectImage> {
  FormFieldState<int>? stateFormField;

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(5),
        border: Border.all(color: ZConfig.grey_border, width: 3, style: BorderStyle.solid),
      ),
      alignment: Alignment.center,
      width: 103,
      child: Stack(
        fit: StackFit.expand,
        children: [
          IconButton(
            icon: const Icon(Icons.add_a_photo),
            tooltip: '사진을 선택해 주세요.',
            onPressed: widget.multiPickup,
          ),
          Align(
              alignment: Alignment.bottomRight,
              child: Padding(
                padding: const EdgeInsets.all(3.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    CustomFormField(
                      initialValue: widget.assets.length,
                      onSaved: widget.onSaved,
                      validator: validator,
                      autovalidateMode: AutovalidateMode.disabled,
                      builder: listTileBuilder,
                    ),
                    const Text(
                      '/10',
                      style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
                    ),
                  ],
                ),
              )),
        ],
      ),
    );
  }

  Widget listTileBuilder(FormFieldState<int> state) {
    stateFormField = state;
    return state.hasError
        ? Text(
            '${widget.assets.length}',
            style: const TextStyle(fontSize: 16, fontWeight: FontWeight.bold, color: Colors.red),
          )
        : Text(
            '${widget.assets.length}',
            style: const TextStyle(
                fontSize: 16, fontWeight: FontWeight.bold, color: ZConfig.zzin_pointColor),
          );
  }

  String? validator(int? value) {
    widget.validator(value);

    if (widget.assets.isEmpty) {
      return '0';
    }
  }
}
