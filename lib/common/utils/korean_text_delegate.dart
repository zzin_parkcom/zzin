
import 'package:wechat_assets_picker/wechat_assets_picker.dart';

class KoreanTextDelegate implements AssetsPickerTextDelegate {
  factory KoreanTextDelegate() => _instance;

  KoreanTextDelegate._internal();

  static final KoreanTextDelegate _instance = KoreanTextDelegate._internal();

  @override
  String confirm = '확인';

  @override
  String cancel = '취소';

  @override
  String edit = '편집';

  @override
  String gifIndicator = 'GIF';

  @override
  String heicNotSupported = 'HEIC 포맷은 지원되지 않습니다.';

  @override
  String loadFailed = '로딩실패';

  @override
  String original = '원본';

  @override
  String preview = '미리보기';

  @override
  String select = '선택';

  @override
  String unSupportedAssetType = '지원되지 않는 포맷입니다.';

  @override
  String durationIndicatorBuilder(Duration duration) {
    const separator = ':';
    final minute = duration.inMinutes.toString().padLeft(2, '0');
    final second =
        ((duration - Duration(minutes: duration.inMinutes)).inSeconds)
            .toString()
            .padLeft(2, '0');
    return '$minute$separator$second';
  }
}
