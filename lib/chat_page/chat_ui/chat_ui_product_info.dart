import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:zzin/common/firestore/document_skima/block_user_controller.dart';
import 'package:zzin/common/firestore/document_skima/product.dart';
import 'package:zzin/common/hive/address_collection.dart';
import 'package:zzin/common/zconfig.dart';
import 'package:zzin/common/zutils.dart';
import 'package:zzin/product_page/product_detail_page/product_detail_view.dart';
import 'package:zzin/product_page/widget/product_image.dart';

class ChatUIProductInfo extends StatelessWidget {
  final Product product;
  final String tagHeroPrefix;
  const ChatUIProductInfo({required this.product, required this.tagHeroPrefix, Key? key})
      : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.fromLTRB(10, 0, 10, 0),
      child: InkWell(
        onTap: () => onDetailPage(product, context),
        child: ClipRRect(
          borderRadius: BorderRadius.circular(5),
          child: Container(
            height: 90,
            color: ZConfig.grey_border,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                SizedBox(
                  width: 90,
                  height: 90,
                  child: ClipRRect(
                    borderRadius: BorderRadius.circular(5),
                    child: Stack(children: [
                      Opacity(
                        opacity: product.articleStatus == 0 ? 1 : 0.5,
                        child: ProductImage(
                          product: product,
                          tagHeroPrefix: tagHeroPrefix,
                        ),
                      ),
                      product.articleStatus != 0
                          ? const Center(
                              child: Text(
                              '삭제된 상품',
                              overflow: TextOverflow.ellipsis,
                            ))
                          : const SizedBox.shrink(),
                    ]),
                  ),
                ),
                const SizedBox(width: 10),
                Expanded(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    mainAxisSize: MainAxisSize.max,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Flexible(
                        child: Text(
                          product.title,
                          overflow: TextOverflow.ellipsis,
                          style: const TextStyle(fontSize: 18),
                        ),
                      ),
                      Text(
                        '${AddressCollection.getRegionName(product.regionCode)}  ${formatDateTimeBefore(product.saleModifyDate.toDate())}',
                        overflow: TextOverflow.ellipsis,
                        style: TextStyle(fontSize: 14, color: Colors.grey[600]),
                      ),
                      Text(
                        '${numberWithComma(product.price)}원',
                        style: const TextStyle(fontSize: 18.0, color: ZConfig.zzin_pointColor),
                      ),
                    ],
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }

  void onDetailPage(Product product, BuildContext context) async {
    if (product.articleStatus == 1) {
      return;
    }
    // 이미지 프리로딩...
    for (var image in product.images) {
      await precacheImage(CachedNetworkImageProvider(image), context);
    }

    var tag = DateTime.now().toString();
    Get.put(Product.fromJson(product.toJson()), tag: tag);

    if (BlockUserController(user: product.sellerId!).isBlocked()) {
      Get.rawSnackbar(message: '차단 유저의 상품입니다.');
      return;
    }

    await Get.to(() => ProductDetailView(tag: tag, tagHeroPrefix: tagHeroPrefix),
        curve: Curves.linear,
        transition: Transition.fadeIn,
        duration: ZConfig.heroAnimationDuration);

    await Get.delete(tag: tag);
  }
}
