import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:zzin/common/firestore/query_notice.dart';
import 'package:zzin/common/zconfig.dart';
import 'package:zzin/common/zutils.dart';

class NoticeListView extends StatefulWidget {
  const NoticeListView({Key? key}) : super(key: key);

  @override
  _NoticeListViewState createState() => _NoticeListViewState();
}

class _NoticeListViewState extends State<NoticeListView> {
  final ScrollController _scrollController = ScrollController();
  @override
  void initState() {
    super.initState();
    _scrollController.addListener(_scrollListener);
  }

  @override
  void dispose() {
    _scrollController.removeListener(_scrollListener);
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: getNoticeList(),
      builder: (context, snapshot) {
        if (snapshot.connectionState == ConnectionState.done) {
          return Expanded(
            child: QueryNotice.to.notices.isEmpty
                ? const NoNotice()
                : RefreshIndicator(
                    onRefresh: onRefresh,
                    child: ListView.separated(
                      physics: const BouncingScrollPhysics(parent: AlwaysScrollableScrollPhysics()),
                      separatorBuilder: (context, index) => const Divider(),
                      controller: _scrollController,
                      itemCount: QueryNotice.to.notices.length,
                      itemBuilder: (context, index) {
                        var notice = QueryNotice.to.notices[index].data();
                        if (notice == null) {
                          throw Exception('notice is NULL!!!');
                        }
                        return QueryNotice.to.notices.isEmpty
                            ? const NoNotice()
                            : ExpansionTile(
                                title: Text(
                                  notice.title,
                                  style: const TextStyle(
                                    fontSize: 18,
                                  ),
                                ),
                                textColor: ZConfig.zzin_black,
                                collapsedTextColor: ZConfig.zzin_black,
                                subtitle: Row(
                                  children: [
                                    Text(formatTimestamp(notice.registDate),
                                        style: const TextStyle(
                                            fontSize: 16, color: ZConfig.grey_text)),
                                    notice.tag == 1
                                        ? const Text('  NEW',
                                            style: TextStyle(
                                              fontSize: 16,
                                              fontWeight: FontWeight.bold,
                                              color: ZConfig.zzin_pointColor,
                                            ))
                                        : const SizedBox.shrink(),
                                  ],
                                ),
                                children: <Widget>[
                                  notice.type == 1
                                      ? Image.network(notice.contents)
                                      : Container(
                                          padding: const EdgeInsets.all(10),
                                          color: ZConfig.grey_border,
                                          alignment: Alignment.topLeft,
                                          child: Text(notice.contents.replaceAll('\\n', '\n'),
                                              style: const TextStyle(fontSize: 14.0)),
                                        ),
                                ],
                              );
                      },
                    )),
          );
        } else {
          return const SizedBox.shrink();
        }
      },
    );
  }

  Future<void> getNoticeList() async {
    var result = await Get.put(QueryNotice()).queryNotice();
    return result;
  }

  Future onRefresh() async {
    await QueryNotice.to.queryNotice();
  }

  void _scrollListener() {
    var maxScroll = _scrollController.position.maxScrollExtent;
    var currentScroll = _scrollController.position.pixels;
    var delta = maxScroll * ZConfig.scrollRatioQueryMore;

    if (maxScroll - currentScroll <= delta) {
      QueryNotice.to.queryNotice(byScroll: true);
    }
  }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
class NoNotice extends StatelessWidget {
  const NoNotice({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return const Center(
      child: Text('공지사항이 없습니다.',
          style: TextStyle(
            fontSize: 20,
            color: ZConfig.grey_text,
          )),
    );
  }
}
