import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:zzin/chat_page/chatroom_listtile.dart';
import 'package:zzin/common/firestore/query_chatroom.dart';
import 'package:zzin/common/zconfig.dart';

class MyInquiryChatListView extends StatefulWidget {
  const MyInquiryChatListView({Key? key}) : super(key: key);

  @override
  _MyInquiryChatListViewState createState() => _MyInquiryChatListViewState();
}

class _MyInquiryChatListViewState extends State<MyInquiryChatListView> {
  final ScrollController _scrollController = ScrollController();
  late final QueryChatRoom queryChatRoom;
  static const List<String> noProductMessages = ['채팅중인 상품이 없습니다.'];

  @override
  void initState() {
    super.initState();
    queryChatRoom = Get.put(QueryChatRoom(), tag: widget.toString() + 1.toString());
    _scrollController.addListener(_scrollListener);
  }

  @override
  void dispose() {
    _scrollController.removeListener(_scrollListener);
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: getChatRoom(userRef: ZConfig.userAccount.docId, isSeller: false),
      builder: (context, snapshot) {
        if (snapshot.connectionState == ConnectionState.done) {
          if (queryChatRoom.chatrooms.isEmpty) {
            return noChatRoom();
          }
          return RefreshIndicator(
            onRefresh: () async {
              await onRefresh(userRef: ZConfig.userAccount.docId, isSeller: false);
            },
            child: ListView.separated(
              separatorBuilder: (context, index) => const Divider(),
              padding: const EdgeInsets.all(0),
              physics: const BouncingScrollPhysics(parent: AlwaysScrollableScrollPhysics()),
              controller: _scrollController,
              itemCount: queryChatRoom.chatrooms.length,
              itemBuilder: (context, index) {
                var chatRoom = queryChatRoom.chatrooms[index].data()!;
                return ChatRoomListTile(chatRoom, index);
              },
            ),
          );
        } else {
          return const SizedBox.shrink();
        }
      },
    );
  }

  Future<void> getChatRoom({required userRef, required isSeller}) async {
    return await queryChatRoom.queryChatRoom(userRef: userRef, isSeller: isSeller, byScroll: false);
  }

  Future onRefresh({required userRef, required isSeller}) async {
    await queryChatRoom.queryChatRoom(userRef: userRef, isSeller: isSeller, byScroll: false);
    setState(() {});
  }

  void _scrollListener() async {
    var maxScroll = _scrollController.position.maxScrollExtent;
    var currentScroll = _scrollController.position.pixels;
    var delta = maxScroll * ZConfig.scrollRatioQueryMore;

    if (maxScroll - currentScroll <= delta) {
      await queryChatRoom.queryChatRoom(
          userRef: ZConfig.userAccount.docId, isSeller: false, byScroll: true);
    }
  }

  Widget noChatRoom() {
    return RefreshIndicator(
      onRefresh: () async {
        await onRefresh(userRef: ZConfig.userAccount.docId, isSeller: false);
      },
      child: ListView.builder(
        itemExtent: Get.height / 3,
        itemCount: 1,
        itemBuilder: (context, index) {
          return Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: List<Widget>.generate(noProductMessages.length, (index) {
                return Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Text(
                    noProductMessages[index],
                    style: const TextStyle(fontSize: 20, color: ZConfig.grey_text),
                  ),
                );
              }));
        },
      ),
    );
  }
}
