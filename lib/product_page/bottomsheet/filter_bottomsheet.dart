import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:zzin/common/zconfig.dart';

class FilterBottomSheet extends StatefulWidget {
  final int _indexStatus;
  final int _indexSaleStatus;
  const FilterBottomSheet(this._indexStatus, this._indexSaleStatus, {Key? key}) : super(key: key);

  @override
  FilterBottomSheetState createState() => FilterBottomSheetState();
}

class FilterBottomSheetState extends State<FilterBottomSheet> {
  final List<String> statusList = ['전체', '미사용', '중고'];
  List<bool> isSelectedStatusList = [false, false, false];
  bool isIncludeSaleCompleted = false;

  @override
  void initState() {
    super.initState();
    isSelectedStatusList[widget._indexStatus] = true;
    isIncludeSaleCompleted = widget._indexSaleStatus == 0 ? true : false;
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.fromLTRB(28, 18, 28, 0),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          topTitleWidget(),
          const SizedBox(height: 20),
          statusGroupWidget(),
          const SizedBox(height: 20),
          saleStatusGroupWidget(),
          const SizedBox(height: 20),
          Column(crossAxisAlignment: CrossAxisAlignment.stretch, children: [
            OutlinedButton(
              style: ButtonStyle(
                  padding: MaterialStateProperty.all(const EdgeInsets.symmetric(vertical: 20)),
                  shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                    RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(5.0),
                      side: const BorderSide(color: ZConfig.zzin_pointColor),
                    ),
                  ),
                  foregroundColor: MaterialStateProperty.all<Color>(ZConfig.zzin_white),
                  backgroundColor: MaterialStateProperty.all<Color>(ZConfig.zzin_pointColor)),
              onPressed: onSubmit,
              child: const Text(
                '적용하기',
                style: TextStyle(fontWeight: FontWeight.bold, fontSize: 19),
              ),
            ),
          ]),
        ],
      ),
    );
  }

  void selectStatus(int index) {
    //버튼 그룹 처리.
    setState(() {
      for (var buttonIndex = 0; buttonIndex < isSelectedStatusList.length; buttonIndex++) {
        if (buttonIndex == index) {
          isSelectedStatusList[buttonIndex] = true;
        } else {
          isSelectedStatusList[buttonIndex] = false;
        }
      }
    });
  }

  void includeSaleCompleted() {
    setState(() {
      isIncludeSaleCompleted = true;
    });
  }

  void excludeSaleCompleted() {
    setState(() {
      isIncludeSaleCompleted = false;
    });
  }

  void onSubmit() {
    var indexSelected = isSelectedStatusList.indexOf(true, 0);
    // 0 전체
    // 1 미사용
    // 2 중고
    var retValue = indexSelected << 2;
    retValue = retValue | (isIncludeSaleCompleted ? 0 : 1);
    // 0 거래완료 포함
    // 1 거래완료 제외
    print('filter bottom sheet return: $retValue');
    Get.back(result: retValue);
  }

  Widget topTitleWidget() {
    return Align(
        alignment: Alignment.centerLeft,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            const Text(
              '상세 필터',
              style: TextStyle(fontSize: 26, fontWeight: FontWeight.bold),
              textAlign: TextAlign.left,
            ),
            TextButton(
              onPressed: () => Get.back(result: AllItems),
              child: const Text(
                '전체보기',
                style: TextStyle(
                    fontSize: 18, fontWeight: FontWeight.bold, color: ZConfig.zzin_pointColor),
              ),
            ),
          ],
        ));
  }

  Widget statusGroupWidget() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        const Text(
          '상품 상태',
          style: TextStyle(fontWeight: FontWeight.bold, fontSize: 19),
        ),
        const SizedBox(height: 10),
        SizedBox(
          height: 45,
          width: double.infinity,
          child:
              Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: statusButtonList()),
        ),
      ],
    );
  }

  List<Widget> statusButtonList() {
    return List.generate(statusList.length, (index) {
      return OutlinedButton(
        style: ButtonStyle(
          padding: MaterialStateProperty.all(const EdgeInsets.fromLTRB(35, 0, 35, 0)),
          shape: MaterialStateProperty.all<RoundedRectangleBorder>(
            RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(25.0),
              side: const BorderSide(color: ZConfig.zzin_pointColor),
            ),
          ),
          foregroundColor: isSelectedStatusList[index]
              ? MaterialStateProperty.all<Color>(ZConfig.zzin_white)
              : MaterialStateProperty.all<Color>(ZConfig.zzin_black),
          backgroundColor: isSelectedStatusList[index]
              ? MaterialStateProperty.all<Color>(ZConfig.zzin_pointColor)
              : MaterialStateProperty.all<Color>(ZConfig.zzin_white),
        ),
        onPressed: isSelectedStatusList[index]
            ? null
            : () {
                selectStatus(index);
              },
        child: Text(
          statusList[index],
          style: const TextStyle(fontWeight: FontWeight.bold, fontSize: 17),
        ),
      );
    });
  }

  Widget saleStatusGroupWidget() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        const Text('거래완료 상품', style: TextStyle(fontSize: 19, fontWeight: FontWeight.bold)),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            OutlinedButton(
              style: ButtonStyle(
                padding: MaterialStateProperty.all(const EdgeInsets.fromLTRB(35, 0, 35, 0)),
                shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                  RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(25.0),
                    side: const BorderSide(color: ZConfig.zzin_pointColor),
                  ),
                ),
                foregroundColor: isIncludeSaleCompleted
                    ? MaterialStateProperty.all<Color>(ZConfig.zzin_white)
                    : MaterialStateProperty.all<Color>(ZConfig.zzin_black),
                backgroundColor: isIncludeSaleCompleted
                    ? MaterialStateProperty.all<Color>(ZConfig.zzin_pointColor)
                    : MaterialStateProperty.all<Color>(ZConfig.zzin_white),
              ),
              onPressed: includeSaleCompleted,
              child: const Text(
                '포함',
                style: TextStyle(fontWeight: FontWeight.bold, fontSize: 17),
              ),
            ),
            const SizedBox(width: 15),
            OutlinedButton(
              style: ButtonStyle(
                padding: MaterialStateProperty.all(const EdgeInsets.fromLTRB(35, 0, 35, 0)),
                shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                  RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(18.0),
                    side: const BorderSide(color: ZConfig.zzin_pointColor),
                  ),
                ),
                foregroundColor: !isIncludeSaleCompleted
                    ? MaterialStateProperty.all<Color>(ZConfig.zzin_white)
                    : MaterialStateProperty.all<Color>(ZConfig.zzin_black),
                backgroundColor: !isIncludeSaleCompleted
                    ? MaterialStateProperty.all<Color>(ZConfig.zzin_pointColor)
                    : MaterialStateProperty.all<Color>(ZConfig.zzin_white),
              ),
              onPressed: excludeSaleCompleted,
              child: const Text(
                '제외',
                style: TextStyle(fontWeight: FontWeight.bold, fontSize: 17),
              ),
            ),
          ],
        ),
      ],
    );
  }
}
