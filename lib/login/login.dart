import 'dart:io';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:flutter_signin_button/flutter_signin_button.dart';
import 'package:get/get.dart';
import 'package:version/version.dart';
import 'package:zzin/common/firestore/document_skima/user_account.dart';
import 'package:zzin/common/utils/device_info.dart';
import 'package:zzin/common/zconfig.dart';
import 'package:zzin/login/join_page.dart';
import 'package:zzin/login/signin/apple_signin.dart';
import 'package:zzin/login/signin/google_signin.dart';
import 'package:zzin/login/signin/social_signin.dart';
import 'package:zzin/login/signin/zkakaosignin.dart';
import 'package:zzin/login/user_process.dart';
import 'package:app_tracking_transparency/app_tracking_transparency.dart';
import 'package:advertising_id/advertising_id.dart';

class LogIn extends StatefulWidget {
  const LogIn({Key? key}) : super(key: key);

  @override
  _LogInState createState() => _LogInState();
}

class _LogInState extends State<LogIn> with SingleTickerProviderStateMixin {
  ZKakaoSignin kakao = ZKakaoSignin();
  ZGoogleSignin google = ZGoogleSignin();
  ZAppleSignin apple = ZAppleSignin();

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: AppBar(
        title: const Text(
          '로그인',
          style: TextStyle(fontSize: ZSize.appBarTextSize),
        ),
      ),
      body: Center(
        child: SizedBox(
          width: Get.width * 3 / 4,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Image.asset('assets/login_title.png'),
              Column(
                children: [
                  SizedBox(
                    width: Get.width * 3 / 4,
                    height: AppBar().preferredSize.height / 4 * 3,
                    child: ElevatedButton(
                        style: ButtonStyle(
                            backgroundColor:
                                MaterialStateProperty.all<Color>(const Color(0xffFEE500))),
                        onPressed: () => _kakaoSignIn(),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: [
                            Expanded(
                              flex: 1,
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.end,
                                children: const [
                                  Image(
                                      image: AssetImage('assets/kakao_symbol.png'),
                                      height: 15,
                                      width: 15),
                                  Text('SSi', style: TextStyle(color: Color(0xffFEE500)))
                                ],
                              ),
                            ),
                            const Text('카카오 로그인', style: TextStyle(color: Color(0xDA000000))),
                            const Expanded(
                              flex: 1,
                              child: Icon(
                                Icons.ac_unit,
                                color: Color(0xffFEE500),
                              ),
                            )
                          ],
                        )),
                  ),
                  const SizedBox(height: 15),
                  Platform.isAndroid
                      ? SizedBox(
                          width: Get.width * 3 / 4,
                          height: AppBar().preferredSize.height / 4 * 3,
                          child: SignInButton(
                            Buttons.Google,
                            // padding: EdgeInsets.all(4),
                            text: 'Sign in with Google',
                            onPressed: () => _googleSignin(),
                          ),
                        )
                      : SizedBox(
                          width: Get.width * 3 / 4,
                          height: AppBar().preferredSize.height / 4 * 3,
                          child: SignInButton(
                            Buttons.Apple,
                            padding: const EdgeInsets.all(4),
                            text: 'Sign in with Apple',
                            onPressed: () => _appleSignin(),
                          ),
                        ),
                  // SizedBox(height: 10)
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }

  Future<String> getAdvertisingId() async {
    if (Platform.isIOS) {
      print('iOS');
      var currentVersion = Version.parse(Platform.operatingSystemVersion);
      var minVersion = Version(14, 5, 0);
      if (currentVersion >= minVersion) {
        print('14.5 이상');
        var status = await AppTrackingTransparency.requestTrackingAuthorization();
        if (status == TrackingStatus.authorized) {
          var advertisingId = await AppTrackingTransparency.getAdvertisingIdentifier();
          print(advertisingId);
          return advertisingId;
        }
      } else {
        print('14.5 이하');
        var advertisingId = await AppTrackingTransparency.getAdvertisingIdentifier();
        print(advertisingId);
        return advertisingId;
      }
    }

    try {
      print('android');
      var advertisingId = await AdvertisingId.id(true);
      print(advertisingId);
      return advertisingId ?? '';
    } catch (e) {
      print(e);
    }

    return '';
  }

  Future<void> _signinProcess(SocialSignin method) async {
    print(ZConfig.authUser);
    var userProcess = ZUserProcess();
    var bSuccess = false;
    try {
      bSuccess = await method.login();
    } catch (e) {
      print(e);
    }

    if (bSuccess) {
      // 3. 유저 정보 획득.
      var userAccount = await method.getAccountInfo();
      var isNewUser = await userProcess.isNewUser(context, userAccount);

      if (isNewUser) {
        // 4. 약관 및 동의 처리 // 5. GPS 정보 획득
        var result = await processTermsOfService(userAccount);
        if (result == null) {
          await loginFail(method.logout, '약관 및 동의 처리를 하지 않아서 회원가입을 할 수 없습니다.');
          return;
        }
        userAccount = result;
      }

      userAccount.advertisingId = await getAdvertisingId();

      await EasyLoading.show(
          status: 'loading...', maskType: EasyLoadingMaskType.black, dismissOnTap: false);

      // 2. authenticate.

      dynamic authCredential;
      if (method.social() == 'kakao') {
        authCredential = await kakao.authenticate();
      } else if (method.social() == 'apple') {
        authCredential = await apple.authenticate();
      } else if (method.social() == 'google') {
        authCredential = await google.authenticate();
      }

      print('guest user auth delete');
      try {
        await FirebaseAuth.instance.currentUser!.delete();
      } on FirebaseAuthException catch (e) {
        print('Failed with error code: ${e.code}');
        print(e.message);
      }
      // 6. 파이어베이스 인증
      print('signInWithCredential');
      UserCredential? userCredential;
      if (method.social() == 'apple' || method.social() == 'google') {
        try {
          userCredential = await FirebaseAuth.instance.signInWithCredential(authCredential);
        } on FirebaseAuthException catch (e) {
          print('Failed with error code: ${e.code}');
          print(e.message);
          await loginFail(method.logout, '로그인 에러(errorCode:${e.code}).');
          await EasyLoading.dismiss();
          return;
        }
      } else if (method.social() == 'kakao') {
        try {
          userCredential =
              await FirebaseAuth.instance.signInWithCustomToken(authCredential.data['token']);
          ZConfig.authUser = userCredential.user;
        } on FirebaseAuthException catch (e) {
          print('Failed with error code: ${e.code}');
          print(e.message);
          await loginFail(method.logout, '로그인 에러(errorCode:${e.code}).');
          await EasyLoading.dismiss();
          return;
        }
      }

      // 7. 디바이스 정보 추가.
      print('getDeviceInfo');
      var deviceinfo = await DeviceInfo.getDeviceInfo();
      userAccount.device = [deviceinfo];

      // 8. DB에 추가. 이미 있으면 실패 통과.
      print('addUser');

      bSuccess = await userProcess.addUser(context, userAccount, userCredential!.user!.uid);

      await EasyLoading.dismiss();
    }

    if (bSuccess) {
      Get.back(result: bSuccess);
    }
  }

  Future<void> _googleSignin() async {
    await _signinProcess(google);
  }

  Future<void> _appleSignin() async {
    await _signinProcess(apple);
  }

  Future<void> _kakaoSignIn() async {
    await _signinProcess(kakao);
  }

////////////////////////////////////////////////////////////////////////////////////////////////////
  Future<ZUserAccount?> processTermsOfService(ZUserAccount userAccount) async {
    var result = await Get.to(() => const JoinPage(),
        arguments: userAccount, transition: Transition.rightToLeftWithFade);
    if (result != null && result is ZUserAccount) {
      return result;
    }
    return null;
  }

  Future<void> loginFail(Future Function() logoutFunction, String message) async {
    await logoutFunction();
    print(message);
  }
}
