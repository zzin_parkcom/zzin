import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';

import 'package:get/get.dart';
import 'package:keyboard_dismisser/keyboard_dismisser.dart';
import 'package:zzin/common/elastic_search/es_product.dart';
import 'package:zzin/common/firestore/document_skima/product.dart';
import 'package:zzin/common/hive/search_collection.dart';
import 'package:zzin/common/widget/appbar/zappbar.dart';
import 'package:zzin/common/widget/searchbar.dart';
import 'package:zzin/common/zconfig.dart';
import 'package:zzin/product_page/product_detail_page/product_detail_view.dart';
import 'package:zzin/search_page/search_has_product.dart';
import 'package:zzin/search_page/search_home_view.dart';
import 'package:zzin/search_page/search_no_product.dart';
import 'package:zzin/search_page/search_option/search_option_view.dart';

class SearchPage extends StatefulWidget {
  const SearchPage({Key? key}) : super(key: key);

  @override
  _SearchPageState createState() => _SearchPageState();
}

class _SearchPageState extends State<SearchPage> {
  final ScrollController _scrollController = ScrollController();
  final TextEditingController editingController = TextEditingController();
  final FocusNode searchNode = FocusNode();

  final ESProduct esProduct = ESProduct();

  String previousSearch = '';
  bool isSearched = false;

  late SearchHomeView searchHomeView;
  late SearchHasProduct searchHasProduct;
  late SearchNoProduct searchNoProduct;

  final String tagHeroPrefix = 'SearchPage_';

  @override
  void initState() {
    super.initState();
    SearchCollection.instance.initialize();
    editingController.text = '';
    _scrollController.addListener(_scrollListener);

    searchHomeView = SearchHomeView(
        controller: editingController,
        onSearchProduct: onSearchProduct,
        tagHeroPrefix: tagHeroPrefix);

    searchHasProduct = SearchHasProduct(
        onDetailPage: onDetailPage,
        products: esProduct.products,
        onSearchOption: onSearchOptionButton,
        scrollController: _scrollController,
        tagHeroPrefix: tagHeroPrefix,
        onRefresh: onRefresh);

    searchNoProduct = SearchNoProduct(
        controller: editingController,
        onSearchProduct: onSearchProduct,
        onClear: clearSearch,
        onSearchOption: onSearchOptionButton);
  }

  @override
  void dispose() {
    _scrollController.removeListener(_scrollListener);
    searchNode.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    print('SearchPage build');
    return KeyboardDismisser(
      child: SafeArea(
        child: Scaffold(
          resizeToAvoidBottomInset: false,
          appBar: zAppBar(),
          body: isSearched == false
              ? searchHomeView
              : esProduct.products.isNotEmpty
                  ? searchHasProduct
                  : searchNoProduct,
        ),
      ),
    );
  }

  AppBar zAppBar() => ZAppBar(
        leading: IconButton(
            icon: const Icon(Icons.arrow_back, color: ZConfig.zzin_black), onPressed: onBack),
        title: SearchBar(
          focusnode: searchNode,
          controller: editingController,
          hintText: '상품/브랜드/키워드 입력',
          onResetAndRefresh: clearSearch,
          onSubmitted: onSearchProduct,
        ),
      );

  void onDetailPage(Product product) async {
    // 이미지 프리로딩...
    for (var image in product.images) {
      precacheImage(CachedNetworkImageProvider(image), context);
    }

    var tag = DateTime.now().toString();
    Get.put(Product.fromJson(product.toJson()), tag: tag);

    await Get.to(() => ProductDetailView(tag: tag, tagHeroPrefix: tagHeroPrefix),
        transition: Transition.fadeIn, duration: ZConfig.heroAnimationDuration);
    await Get.delete(tag: tag);
  }

  Future onRefresh() async {
    await esProduct.boolSearch(
        queryString: previousSearch,
        category: SearchCollection.instance.categoryId,
        brand: SearchCollection.instance.brandId,
        status: SearchCollection.instance.status,
        saleStatus: SearchCollection.instance.saleStatus,
        regionCode: SearchCollection.instance.addressId,
        distance: SearchCollection.instance.addressStep != -1
            ? ZConfig.areaRadiusMeters[SearchCollection.instance.addressStep]
            : ZConfig.areaRadiusMeters[3],
        sortField: getSortField(),
        sortOrder: getSortOrder(),
        isClear: true);
  }

  bool isScrolling = false;
  void _scrollListener() async {
    var maxScroll = _scrollController.position.maxScrollExtent;
    var currentScroll = _scrollController.position.pixels;
    var delta = maxScroll * ZConfig.scrollRatioQueryMore;

    if (maxScroll - currentScroll <= delta && isScrolling == false) {
      isScrolling = true;
      await esProduct.searchNext();
      isScrolling = false;
    }
  }

  String getSortField() {
    switch (SearchCollection.instance.sort) {
      case 0:
        return '';
      case 1:
        return 'saleModifyDate';
      case 2:
        return 'price';
      case 3:
        return 'price';
      case 4:
        return 'likeCounts';
      default:
        return '';
    }
  }

  String getSortOrder() {
    switch (SearchCollection.instance.sort) {
      case 0:
        return 'desc';
      case 1:
        return 'desc';
      case 2:
        return 'asc';
      case 3:
        return 'desc';
      case 4:
        return 'desc';
      default:
        return 'desc';
    }
  }

  Future onSearchProduct(String search) async {
    print(ZConfig.regExpKo.allMatches(editingController.text).length);
    print(editingController.text.length);
    var noSpaceSearch = editingController.text.replaceAll(' ', '');
    if (ZConfig.regExpKo.allMatches(noSpaceSearch).length == noSpaceSearch.length) {
      var search = editingController.text.trim();
      if (search.split(' ').length > 10) return;
    }

    await esProduct.boolSearch(
        queryString: search,
        category: SearchCollection.instance.categoryId,
        brand: SearchCollection.instance.brandId,
        status: SearchCollection.instance.status,
        saleStatus: SearchCollection.instance.saleStatus,
        regionCode: SearchCollection.instance.addressId,
        distance: SearchCollection.instance.addressStep != -1
            ? ZConfig.areaRadiusMeters[SearchCollection.instance.addressStep]
            : ZConfig.areaRadiusMeters[3],
        sortField: getSortField(),
        sortOrder: getSortOrder(),
        isClear: true);

    if (esProduct.products.isNotEmpty) {
      previousSearch = search;
      searchNode.unfocus();
      SearchCollection.instance.searchString = search;
      addRecentKeyword(search);
      await SearchCollection.instance.save();
    }
    if (isSearched == false) {
      isSearched = true;
    }
    setState(() {});
  }

  void addRecentKeyword(String recent) {
    if (recent.isEmpty) {
      return;
    }
    if (SearchCollection.instance.recentKeyword.contains(recent)) {
      print('이미 리스트에 있습니다.');
      return;
    }

    if (SearchCollection.instance.recentKeyword.length < ZConfig.maxRecentListCount) {
      SearchCollection.instance.recentKeyword.add(recent);
    } else if (SearchCollection.instance.recentKeyword.length == ZConfig.maxRecentListCount) {
      SearchCollection.instance.recentKeyword.removeAt(0);
      SearchCollection.instance.recentKeyword.add(recent);
      SearchCollection.instance.save();
    }
  }

  Future<bool> onSearchOptionButton() async {
    var result =
        await Get.to(() => const SearchOptionView(), transition: Transition.rightToLeftWithFade);
    if (result == true) {
      await onSearchProduct(editingController.text);
    }
    return result;
  }

  void clearSearch() {
    SearchCollection.instance.searchString = '';
    SearchCollection.instance.save();
    editingController.text = '';
    esProduct.clearProducts();
    isSearched = false;
    setState(() {});
  }

  void onBack() {
    Get.back(result: -1);
  }
}
