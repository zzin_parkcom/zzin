import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/scheduler.dart';
import 'package:flutter/services.dart';
import 'package:zzin/common/hive/upload_collection.dart';
import 'package:zzin/common/utils/upload_utils.dart';
import 'package:get/get.dart';
import 'package:zzin/common/zconfig.dart';

class DescriptionSelection extends StatefulWidget {
  final UploadCollection uploadCollection;
  final double fontSize;
  final String defaultName = '';

  const DescriptionSelection(this.fontSize, {required this.uploadCollection, Key? key})
      : super(key: key);

  @override
  _DescriptionSelectionState createState() => _DescriptionSelectionState();
}

class _DescriptionSelectionState extends State<DescriptionSelection> {
  final TextEditingController controller = TextEditingController();
  final FocusNode nodeDescription = FocusNode();
  var description = Empty;

  @override
  void dispose() {
    // onSaved(controller.text);
    nodeDescription.dispose();
    controller.dispose();
    super.dispose();
  }

  @override
  void initState() {
    super.initState();
    SchedulerBinding.instance!.addPostFrameCallback((_) => frameCallback(context));
  }

  void frameCallback(BuildContext context) async {
    description = widget.uploadCollection.description;
    controller.text = description;
  }

  @override
  Widget build(BuildContext context) {
    var heightTextEdit = (Get.height - AppBar().preferredSize.height) / 2;

    return Container(
      constraints: BoxConstraints(maxHeight: heightTextEdit),
      padding: const EdgeInsets.symmetric(horizontal: 15),
      child: /*  KeyboardActions(
        config: _buildConfig(context),
        child: */
          TextFormField(
        autovalidateMode: AutovalidateMode.disabled,
        validator: validator,
        onSaved: onSaved,
        onChanged: (value) async {
          widget.uploadCollection.description = value;
          await widget.uploadCollection.save();
        },
        style: normalStyle(false, widget.fontSize),
        focusNode: nodeDescription,
        controller: controller,
        keyboardType: TextInputType.multiline,
        maxLines: null,
        textInputAction: TextInputAction.newline,
        decoration: InputDecoration(
          border: InputBorder.none,
          hintText: '상품설명 입력\n\n'
              '판매하시는 상품의 정품 신뢰도를 높이기\n 위해 다음 사진도 첨부해 보세요\n\n'
              '1.명품보증서\n'
              '2.구매영수증\n'
              '3.상표 라벨과 시리얼 넘버',
          hintStyle: normalStyle(true, widget.fontSize - 2.0),
        ),
      ),
      // ),
    );
  }

  void onSaved(String? value) async {
    if (controller.text.length > 5000) {
      description = controller.text.substring(0, 5000);
    } else {
      description = controller.text;
    }

    widget.uploadCollection.description = description;
    await widget.uploadCollection.save();
  }

  String? validator(String? value) {
    if (value == null || value.isEmpty) {
      return '상품설명을 입력해 주세요';
    }
    return null;
  }

  // KeyboardActionsConfig _buildConfig(BuildContext context) {
  //   return KeyboardActionsConfig(
  //     keyboardActionsPlatform: KeyboardActionsPlatform.ALL,
  //     keyboardBarColor: Colors.white,
  //     nextFocus: true,
  //     actions: [
  //       KeyboardActionsItem(
  //         // displayArrows: false,
  //         focusNode: nodeDescription,
  //         toolbarButtons: [
  //           (node) {
  //             return TextButton(
  //               onPressed: () {
  //                 node.unfocus();
  //                 setState(() {});
  //               },
  //               child: Text(
  //                 '완료',
  //                 style: TextStyle(fontSize: widget.fontSize),
  //               ),
  //             );
  //           }
  //         ],
  //       ),
  //     ],
  //   );
  // }
}
