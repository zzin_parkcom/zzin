import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_cache_manager/flutter_cache_manager.dart';

class ZCacheManager {
  static const key = 'ZCacheManager';
  static CacheManager instance = CacheManager(Config(
    key,
    stalePeriod: const Duration(days: 7),
    maxNrOfCacheObjects: 2000,
    repo: JsonCacheInfoRepository(databaseName: key),
  ));

  static Image cacheImage(String url) {
    return Image(image: CachedNetworkImageProvider(url));
  }
}


// class Zimage {
//   String url;
//   Zimage(this.url);
//   Image get() {
//     return Image(image: CachedNetworkImageProvider(url));
//   }
// }

