import 'package:cached_network_image/cached_network_image.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:transparent_image/transparent_image.dart';
import 'package:zzin/common/elastic_search/es_product.dart';
import 'package:zzin/common/firestore/document_skima/product.dart';
import 'package:zzin/common/zcache_manager.dart';
import 'package:zzin/common/zconfig.dart';
import 'package:zzin/product_page/product_detail_page/product_detail_view.dart';
import 'package:zzin/search_page/salearea_search_page.dart';

class SaleAreaHorizontalView extends StatefulWidget {
  final String tagHeroPrefix;
  const SaleAreaHorizontalView({Key? key, required this.tagHeroPrefix}) : super(key: key);

  @override
  _SaleAreaHorizontalViewState createState() => _SaleAreaHorizontalViewState();
}

class _SaleAreaHorizontalViewState extends State<SaleAreaHorizontalView> {
  final ScrollController _scrollController = ScrollController();
  final ESProduct esProduct = ESProduct();
  double imageSize = 150;
  String areaName = '';

  @override
  void initState() {
    super.initState();
    _scrollController.addListener(_scrollListener);
  }

  @override
  void dispose() {
    _scrollController.removeListener(_scrollListener);
    esProduct.clearScroll();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: getProduct(),
      builder: (context, snapshot) {
        if (snapshot.connectionState == ConnectionState.done) {
          return Column(
            children: [
              const Divider(),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Row(
                    children: [
                      Text(
                        '$areaName 주변 상품',
                        style: const TextStyle(fontSize: 20),
                      ),
                    ],
                  ),
                  Flexible(
                      child: TextButton(onPressed: onMoreRegion, child: const Text('주변 상품 더보기')))
                ],
              ),
              // queryProductSaleArea.products.isEmpty
              esProduct.products.isEmpty
                  ? noProduct()
                  : SizedBox(
                      height: imageSize,
                      child: Obx(() => ListView.separated(
                          controller: _scrollController,
                          scrollDirection: Axis.horizontal,
                          itemCount: esProduct.products.length,
                          separatorBuilder: (context, index) => const SizedBox(width: 10),
                          itemBuilder: (context, index) {
                            var product = esProduct.products[index];
                            return GestureDetector(
                              onTap: () {
                                onListItemTap(context, product);
                              },
                              child: Container(
                                width: imageSize,
                                height: imageSize,
                                color: ZConfig.zzin_white,
                                child: ClipRRect(
                                  borderRadius: BorderRadius.circular(10),
                                  child: Hero(
                                    tag: widget.tagHeroPrefix + product.docId.toString(),
                                    child: CachedNetworkImage(
                                      fadeInDuration: const Duration(milliseconds: 0),
                                      fadeOutDuration: const Duration(milliseconds: 0),
                                      placeholderFadeInDuration: const Duration(milliseconds: 0),
                                      placeholder: (_, __) => Image.memory(kTransparentImage),
                                      imageUrl: product.thumbnail,
                                      fit: BoxFit.contain,
                                      errorWidget: (context, url, error) => const Icon(Icons.error),
                                      cacheManager: ZCacheManager.instance,
                                    ),
                                  ),
                                ),
                              ),
                            );
                          })),
                    ),
            ],
          );
        } else {
          return const SizedBox.shrink();
        }
      },
    );
  }

  Future<void> getProduct() async {
    GeoPoint position;
    position = ZConfig.userAccount.regionGeoPoint!;
    areaName = ZConfig.userAccount.regionName!.split(' ').last;

    await esProduct.searchByDistance(
        lat: position.latitude,
        lon: position.longitude,
        size: 10,
        radius: 500,
        isAscending: true,
        isClear: true);
  }

  bool isScrolling = false;
  void _scrollListener() async {
    var maxScroll = _scrollController.position.maxScrollExtent;
    var currentScroll = _scrollController.position.pixels;
    var delta = maxScroll * ZConfig.scrollRatioQueryMore;

    if (maxScroll - currentScroll <= delta && isScrolling == false) {
      isScrolling = true;
      await esProduct.searchNext();
      isScrolling = false;
    }
  }

  void onListItemTap(BuildContext context, Product product) {
    var tag = DateTime.now().toString();
    var savedProduct = Get.put(Product.fromJson(product.toJson()), tag: tag);
    Get.to(() => ProductDetailView(tag: tag, tagHeroPrefix: widget.tagHeroPrefix),
        transition: Transition.fadeIn, duration: ZConfig.heroAnimationDuration);
    if (!savedProduct.isEqualValue(product)) {
      setState(() {});
    }

    Get.delete(tag: tag);
  }

  Widget noProduct() {
    return const Padding(
      padding: EdgeInsets.all(20.0),
      child: Text(
        '주변 상품이 없습니다',
        style: TextStyle(fontSize: 20, color: ZConfig.grey_text),
      ),
    );
  }

  void onMoreRegion() {
    Get.to(() => SaleAreaSearchPage(
          tagHeroPrefix: widget.tagHeroPrefix,
        ));
  }
}
