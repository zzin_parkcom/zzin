import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:zzin/common/widget/zchip.dart';
import 'package:zzin/common/zconfig.dart';
import 'package:zzin/search_page/popular_horizontal_view.dart';
import 'package:zzin/search_page/recent_keyword_widget.dart';
import 'package:zzin/search_page/salearea_horizontal_view.dart';

class SearchHomeView extends StatefulWidget {
  final void Function(String search) onSearchProduct;
  final TextEditingController controller;
  final List suggestKeywordList = [];
  final String tagHeroPrefix;

  SearchHomeView(
      {Key? key,
      required this.controller,
      required this.onSearchProduct,
      required this.tagHeroPrefix})
      : super(key: key);

  @override
  _SearchHomeViewState createState() => _SearchHomeViewState();
}

class _SearchHomeViewState extends State<SearchHomeView> {
  late final PopularHorizontalView popularHorizontalView;
  late final SaleAreaHorizontalView saleAreaHorizontalView;

  @override
  void initState() {
    super.initState();
    popularHorizontalView = PopularHorizontalView(tagHeroPrefix: widget.tagHeroPrefix + 'Popular_');
    saleAreaHorizontalView =
        SaleAreaHorizontalView(tagHeroPrefix: widget.tagHeroPrefix + 'SaleArea_');
  }

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Column(
        children: [
          Padding(
            padding: const EdgeInsets.fromLTRB(20, 20, 20, 10),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    const Text(
                      '추천 검색어',
                      style: TextStyle(fontSize: 20),
                    ),
                    TextButton(
                        style: ButtonStyle(
                            padding:
                                MaterialStateProperty.all(const EdgeInsets.fromLTRB(0, 0, 0, 0))),
                        onPressed: null,
                        child: const Text('', style: TextStyle(color: ZConfig.grey_text)))
                  ],
                ),
                FutureBuilder(
                  future: getSuggestedKeyword(),
                  builder: (context, snapshot) {
                    if (snapshot.connectionState == ConnectionState.done) {
                      return Wrap(
                        spacing: 5,
                        children: ZChip.generate(
                            textList: widget.suggestKeywordList,
                            length: widget.suggestKeywordList.length,
                            onSelectedList: onSuggestPressed()),
                      );
                    } else {
                      return const SizedBox.shrink();
                    }
                  },
                ),
                const Divider(
                  height: 10,
                  color: ZConfig.zzin_white,
                ),
                RecentKeywordWidget(
                    controller: widget.controller, onSearchProduct: widget.onSearchProduct)
              ],
            ),
          ),
          Padding(
            padding: const EdgeInsets.fromLTRB(20, 10, 20, 10),
            child: popularHorizontalView,
          ),
          Padding(
            padding: const EdgeInsets.fromLTRB(20, 10, 20, 10),
            child: saleAreaHorizontalView,
          ),
        ],
      ),
    );
  }

  List<ValueChanged<bool>> onSuggestPressed() {
    return List.generate(widget.suggestKeywordList.length, (index) {
      return (isSelected) {
        if (isSelected) {
          print('choiceChip : ${widget.suggestKeywordList[index]}');
          widget.controller.text = widget.suggestKeywordList[index];
          widget.onSearchProduct(widget.suggestKeywordList[index]);
        }
      };
    });
  }

  Future<int> getSuggestedKeyword() async {
    var snapshot = await FirebaseFirestore.instance
        .collection(ZConfig.keywordCollection)
        .doc('suggested')
        .get();
    if (snapshot.exists) {
      widget.suggestKeywordList.clear();
      widget.suggestKeywordList.addAll(snapshot.data()!['keyword']);
      return snapshot.data()!.length;
    }
    return -1;
  }
}
