import 'package:cached_network_image/cached_network_image.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:zzin/common/firestore/document_skima/product.dart';
import 'package:zzin/common/hive/address_collection.dart';
import 'package:zzin/common/zconfig.dart';
import 'package:zzin/product_page/product_detail_page/product_detail_view.dart';
import 'package:zzin/product_page/widget/product_griditem.dart';
import 'package:zzin/product_page/widget/product_image.dart';
import 'package:zzin/product_page/widget/product_listitem.dart';

class ProductListGenerator extends StatelessWidget {
  final List<DocumentSnapshot<Product>> products;
  final List<Product>? esProducts;
  final Future Function() onRefresh;
  final ScrollController scrollController;
  final void Function(Product product)? onDetailPage;
  final List<String> noProductMessages;
  final bool isListView;
  final String tagHeroPrefix;

  const ProductListGenerator(
      {required this.products,
      this.esProducts,
      required this.onRefresh,
      required this.scrollController,
      required this.onDetailPage,
      required this.noProductMessages,
      required this.tagHeroPrefix,
      this.isListView = true,
      Key? key})
      : super(key: key);

  bool isEmptyProduct() {
    if (esProducts != null) {
      return esProducts!.isEmpty;
    } else {
      return products.isEmpty;
    }
  }

  Product getProduct(int index) {
    if (esProducts != null) {
      return esProducts![index];
    } else {
      return products[index].data()!;
    }
  }

  int getProductsLength() {
    if (esProducts != null) {
      return esProducts!.length;
    } else {
      return products.length;
    }
  }

  @override
  Widget build(BuildContext context) {
    return isEmptyProduct()
        ? noProduct()
        : RefreshIndicator(
            onRefresh: onRefresh,
            child: Obx(
              () => isListView
                  ? ListView.separated(
                      padding: const EdgeInsets.all(0),
                      addAutomaticKeepAlives: true,
                      cacheExtent: double.infinity,
                      shrinkWrap: true,
                      physics: const BouncingScrollPhysics(parent: AlwaysScrollableScrollPhysics()),
                      separatorBuilder: (context, index) =>
                          const Divider(indent: 10, endIndent: 10),
                      controller: scrollController,
                      itemCount: getProductsLength(),
                      itemBuilder: (context, index) {
                        var product = getProduct(index);
                        return GestureDetector(
                          behavior: HitTestBehavior.opaque,
                          onTap: () => onTap(context, product),
                          child: ProductListItem(
                            goodsImage:
                                ProductImage(product: product, tagHeroPrefix: tagHeroPrefix),
                            goodsTitle: product.title,
                            goodsPrice: product.price,
                            address: AddressCollection.getRegionName(product.regionCode),
                            registerDateTime: product.saleModifyDate.toDate(),
                            favoriteCount: product.likeCounts,
                            productStatus: product.status,
                          ),
                        );
                      },
                    )
                  : GridView.builder(
                      padding: const EdgeInsets.fromLTRB(10, 10, 10, 10),
                      shrinkWrap: true,
                      itemCount: getProductsLength(),
                      controller: scrollController,
                      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                        childAspectRatio: 3 / (3.2 + Get.mediaQuery.textScaleFactor),
                        crossAxisCount: 2,
                        mainAxisSpacing: 10,
                        crossAxisSpacing: 10,
                      ),
                      itemBuilder: (context, index) {
                        var product = getProduct(index);
                        return InkWell(
                            onTap: () => onTap(context, product),
                            child: ProductGridItem(product: product, tagHeroPrefix: tagHeroPrefix));
                      },
                    ),
            ),
          );
  }

  void onTap(BuildContext context, Product product) {
    if (onDetailPage == null) {
      onDetailPageDefault(product, context);
    } else {
      onDetailPage!(product);
    }
  }

  void onDetailPageDefault(Product product, BuildContext context) async {
    // 이미지 프리로딩...
    for (var image in product.images) {
      precacheImage(CachedNetworkImageProvider(image), context);
    }

    var tag = DateTime.now().toString();
    /* var savedProduct = */ Get.put(Product.fromJson(product.toJson()), tag: tag);

    await Get.to(
      () => ProductDetailView(
        tag: tag,
        tagHeroPrefix: tagHeroPrefix,
      ),
      transition: Transition.fadeIn,
      duration: ZConfig.heroAnimationDuration,
    );

    // if (!savedProduct.isEqualValue(product)) {
    //   await onRefresh();
    // }
    await Get.delete(tag: tag);
  }

  Widget noProduct() {
    return RefreshIndicator(
      onRefresh: onRefresh,
      child: ListView.builder(
        itemExtent: Get.height / 3,
        itemCount: 1,
        itemBuilder: (context, index) {
          return Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: List<Widget>.generate(noProductMessages.length, (index) {
                return Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Text(
                    noProductMessages[index],
                    style: const TextStyle(fontSize: 20, color: ZConfig.grey_text),
                  ),
                );
              }));
        },
      ),
    );
  }
}
