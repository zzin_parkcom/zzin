import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:keyboard_dismisser/keyboard_dismisser.dart';
import 'package:zzin/common/firestore/document_skima/report_user.dart';
import 'package:zzin/common/widget/zbutton_blue.dart';
import 'package:zzin/common/zconfig.dart';

class ReportUserSecondPage extends StatefulWidget {
  final DocumentReference productDocId;
  final DocumentReference? chatroomDocId;
  final DocumentReference targetUserDocId;
  const ReportUserSecondPage(
      {Key? key,
      required this.productDocId,
      required this.chatroomDocId,
      required this.targetUserDocId})
      : super(key: key);

  @override
  _ReportUserSecondPageState createState() => _ReportUserSecondPageState();
}

class _ReportUserSecondPageState extends State<ReportUserSecondPage> {
  final TextEditingController controller = TextEditingController();
  int reportUserReasonIndex = 0;
  bool isValid = false;

  @override
  Widget build(BuildContext context) {
    reportUserReasonIndex = Get.arguments;
    print(reportUserReasonIndex);
    return KeyboardDismisser(
      child: SafeArea(
        child: Scaffold(
          resizeToAvoidBottomInset: true,
          appBar: AppBar(),
          body: ListView(
            children: [
              Padding(
                padding: const EdgeInsets.all(30.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: const [
                    Text(
                      '신고하기',
                      style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
                    ),
                    Text('2/2')
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 20),
                child: ListTile(
                  title: Text(
                    '[${reportReasonTitle[reportUserReasonIndex]}] 신고 요청',
                    style:
                        const TextStyle(height: 2, fontWeight: FontWeight.bold, color: Colors.red),
                  ),
                  subtitle: Text(reportReasonSubTitle[reportUserReasonIndex],
                      style: const TextStyle(fontSize: 12)),
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(38),
                child: Column(
                  children: [
                    Row(children: const [
                      Text(
                        '신고 사유 추가 입력 ',
                        style: TextStyle(fontWeight: FontWeight.bold),
                      ),
                      Text(
                        '(필수 입력)',
                        style: TextStyle(
                          fontSize: 12,
                          color: ZConfig.grey_text,
                        ),
                      )
                    ]),
                    const SizedBox(height: 12),
                    TextFormField(
                      validator: validator,
                      autovalidateMode: AutovalidateMode.onUserInteraction,
                      autocorrect: false,
                      controller: controller,
                      maxLines: null,
                      minLines: 5,
                      decoration: const InputDecoration(
                          border: InputBorder.none,
                          hintText: '신고 사유를 자세히 입력해 주세요.(최소 10자 이상)',
                          hintStyle: TextStyle(fontSize: 12),
                          fillColor: ZConfig.grey_border,
                          filled: true),
                    )
                  ],
                ),
              )
            ],
          ),
          bottomNavigationBar: Padding(
            padding: ZSize.bottomNavigationBarButtonOutPadding,
            child: ZButtonYellow(
              isSelected: true,
              text: '신고하기 완료',
              fontSize: ZSize.BNB_ButtonFontSize,
              padding: ZSize.bottomNavigationBarButtonInPadding,
              onPressed: () => onSubmit(),
            ),
          ),
        ),
      ),
    );
  }

  String? validator(String? value) {
    if (value!.length < 10) {
      isValid = false;
      return '최소 10자 이상 사유를 적어주십시오';
    }
    isValid = true;
    return null;
  }

  void onSubmit() async {
    if (isValid) {
      ReportUser reportUser = ReportUser(
        reportChannel: widget.chatroomDocId == null
            ? ReportChannel.productDetail.index
            : ReportChannel.chatRoom.index,
        producDoctId: widget.productDocId,
        chatRoomDocId: widget.chatroomDocId,
        reportReasonType: reportUserReasonIndex,
        reportUserDocId: ZConfig.userAccount.docId!,
        targetUserDocId: widget.targetUserDocId,
        reportDetail: controller.text.trim(),
      );
      var result = await reportUser.addReportUser();

      Get.back(result: result);
    } else {
      Get.rawSnackbar(message: '최소 10자 이상 사유를 적어주십시오');
    }
  }
}
