import 'package:flutter/material.dart';

class LoadingWidget extends StatelessWidget {
  const LoadingWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Center(
      child: Image.asset(
        'assets/splash_android.png',
        fit: BoxFit.cover,
        height: double.infinity,
        width: double.infinity,
      ),
    ));
  }
}
