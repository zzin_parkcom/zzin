import 'package:flutter/material.dart';
import 'package:zzin/common/hive/brand_collection.dart';
import 'package:zzin/common/hive/search_collection.dart';
import 'package:zzin/common/utils/upload_utils.dart';
import 'package:zzin/upload_page/bottomsheet/bottomsheet_upload.dart';
import 'package:zzin/common/zconfig.dart';
import 'package:zzin/common/zutils.dart';
import 'package:zzin/upload_page/customFormfield/custom_formfield.dart';
import 'package:get/get.dart';

class BrandOption extends StatefulWidget {
  final String defaultName = '브랜드 선택';
  final double fontSize;

  // ignore: prefer_const_constructors_in_immutables
  BrandOption(this.fontSize, {Key? key}) : super(key: key);

  late final _BrandOptionState _state;
  @override
  // ignore: no_logic_in_create_state
  _BrandOptionState createState() {
    _state = _BrandOptionState();
    return _state;
  }

  int getBrandId() => _state.id.value;
  void setBrandId(int id) {
    _state.id.value = id;
    _state.name.value = BrandCollection.getNameKr(id);
  }

  bool isEmpty() => _state.id.value == NotSelected;

  void clearState() {
    SearchCollection.instance.brandId = NotSelected;
    SearchCollection.instance.brandName = defaultName;
    SearchCollection.instance.save();
    _state.id.value = NotSelected;
    _state.name.value = defaultName;
    _state.clearState();
  }
}

class _BrandOptionState extends State<BrandOption> {
  var id = SearchCollection.instance.brandId.obs;
  var name = SearchCollection.instance.brandName.obs;

  void clearState() {
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return CustomFormField(
      initialValue: id.value,
      validator: validator,
      onSaved: onSaved,
      autovalidateMode: AutovalidateMode.onUserInteraction,
      builder: listTileBuilder,
    );
  }

  Widget listTileBuilder(FormFieldState<int> state) {
    return Column(
      children: [
        ListTile(
          title: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              brandTitle(),
              state.hasError
                  ? Text(
                      state.errorText!,
                      style: errorStyle(widget.fontSize),
                    )
                  : const SizedBox.shrink()
            ],
          ),
          trailing: const Icon(Icons.expand_more),
          onTap: () async {
            var idReturn = await showModalBottomSheetInt(context, _brandBottomSheetContent);
            if (idReturn != NotSelected && idReturn != Init) {
              id.value = idReturn;
              name.value = BrandCollection.getNameKr(id.value);
              state.didChange(id.value);
            } else if (idReturn == Init) {
              id.value = NotSelected;
              name.value = widget.defaultName;
              state.didChange(id.value);
            }
          },
        ),
      ],
    );
  }

  String? validator(int? value) {
    // return id.value == NotSelected ? '브랜드를 선택해 주세요' : null;
    return null;
  }

  void onSaved(int? value) {}

  Widget brandTitle() {
    return id.value == NotSelected
        ? Text(widget.defaultName, style: normalStyle(id.value == NotSelected, widget.fontSize))
        : Row(
            children: [
              Text(
                BrandCollection.getNameKr(id.value),
                style: normalStyle(id.value == NotSelected, widget.fontSize),
              ),
              const SizedBox(width: 5),
              Expanded(
                child: Text(BrandCollection.getNameEn(id.value),
                    overflow: TextOverflow.ellipsis, style: greyEngStyle(widget.fontSize - 4.0)),
              )
            ],
          );
  }

  Widget _brandBottomSheetContent(BuildContext context) {
    return BottomSheetUpload('브랜드', '초기화', Init, BrandCollection.to.length,
        BrandCollection.to.values.map((e) => e.nameKr + '|' + e.nameEn).toList(), true,
        height: Get.height - AppBar().preferredSize.height * 2);
  }
}
