import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:zzin/common/hive/search_collection.dart';
import 'package:zzin/common/widget/zbutton_blue.dart';
import 'package:zzin/common/zconfig.dart';

class ProductStatusBottomSheet extends StatefulWidget {
  // ignore: prefer_const_constructors_in_immutables
  ProductStatusBottomSheet({Key? key}) : super(key: key);

  late final ProductStatusBottomSheetState _state;

  @override
  // ignore: no_logic_in_create_state
  ProductStatusBottomSheetState createState() {
    _state = ProductStatusBottomSheetState();
    return _state;
  }
}

class ProductStatusBottomSheetState extends State<ProductStatusBottomSheet> {
  // int status = NotSelected;
  // int saleStatus = NotSelected;
  final List<String> statusList = ['전체', '새상품', '중고'];
  List<bool> isSelectedStatusList = [false, false, false];
  bool isIncludeSaleCompleted = false;

  @override
  void initState() {
    super.initState();

    isSelectedStatusList[SearchCollection.instance.status == AllItems
        ? 0
        : SearchCollection.instance.status + 1] = true;
    isIncludeSaleCompleted = (SearchCollection.instance.saleStatus == NotSelected ||
            SearchCollection.instance.saleStatus == AllItems)
        ? true
        : false;
  }

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: Container(
        padding: const EdgeInsets.fromLTRB(30, 30, 30, 30),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            topTitleWidget(),
            Padding(
              padding: const EdgeInsets.fromLTRB(10, 0, 10, 0),
              child: statusGroupWidget(),
            ),
            Padding(
              padding: const EdgeInsets.fromLTRB(10, 0, 10, 0),
              child: saleStatusGroupWidget(),
            ),
          ],
        ),
      ),
    );
  }

  Widget topTitleWidget() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        const Text(
          '상세 필터',
          style: TextStyle(fontSize: ZSize.bottomSheetTitleFontSize),
          textAlign: TextAlign.left,
        ),
        TextButton(
          onPressed: () {
            SearchCollection.instance.status = AllItems;
            SearchCollection.instance.saleStatus = AllItems;
            Get.back();
          },
          child: const Text(
            '초기화',
            style: TextStyle(
                fontSize: 18, fontWeight: FontWeight.bold, color: ZConfig.zzin_pointColor),
          ),
        ),
      ],
    );
  }

  void selectStatus(int index) {
    //버튼 그룹 처리.
    setState(() {
      for (var buttonIndex = 0; buttonIndex < isSelectedStatusList.length; buttonIndex++) {
        if (buttonIndex == index) {
          isSelectedStatusList[buttonIndex] = true;
        } else {
          isSelectedStatusList[buttonIndex] = false;
        }
      }
      var indexSelected = isSelectedStatusList.indexOf(true, 0);
      SearchCollection.instance.status = indexSelected - 1;
      // 0 전체 NotSelected
      // 1 미사용 NEW
      // 2 중고 OLD
    });
  }

  void includeSaleCompleted() {
    setState(() {
      isIncludeSaleCompleted = true;
      SearchCollection.instance.saleStatus = -1;
    });
  }

  void excludeSaleCompleted() {
    setState(() {
      isIncludeSaleCompleted = false;
      SearchCollection.instance.saleStatus = 0;
    });
  }

  Widget statusGroupWidget() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        const Text('상품 상태', style: TextStyle(fontSize: ZSize.bottomSheetFontSize)),
        const SizedBox(height: 10),
        Row(mainAxisAlignment: MainAxisAlignment.spaceAround, children: statusButtonList()),
      ],
    );
  }

  List<Widget> statusButtonList() {
    return List.generate(statusList.length, (index) {
      return ZButtonYellow(
        // padding: MaterialStateProperty.all(EdgeInsets.fromLTRB(10, 0, 10, 0)),
        text: statusList[index],
        isSelected: isSelectedStatusList[index],
        onPressed: isSelectedStatusList[index]
            ? null
            : () {
                selectStatus(index);
              },
      );
    });
  }

  Widget saleStatusGroupWidget() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        const Text('거래완료 상품', style: TextStyle(fontSize: ZSize.bottomSheetFontSize)),
        const SizedBox(height: 10),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            ZButtonYellow(
              text: '포함',
              isSelected: isIncludeSaleCompleted,
              onPressed: includeSaleCompleted,
            ),
            const SizedBox(width: 15),
            ZButtonYellow(
                text: '제외', onPressed: excludeSaleCompleted, isSelected: !isIncludeSaleCompleted),
          ],
        ),
      ],
    );
  }
}
