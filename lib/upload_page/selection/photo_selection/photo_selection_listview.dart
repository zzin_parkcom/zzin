import 'package:flutter/material.dart';
import 'package:wechat_assets_picker/wechat_assets_picker.dart';
import 'package:zzin/common/zconfig.dart';

class PhotoSelectionListView extends StatefulWidget {
  final List<AssetEntity> assets;
  final int mainImageIndex;
  final void Function(int index) onTapImage;
  final void Function(int index) onTapRemove;
  const PhotoSelectionListView(
      {Key? key,
      required this.assets,
      required this.mainImageIndex,
      required this.onTapImage,
      required this.onTapRemove})
      : super(key: key);

  @override
  _PhotoSelectionListViewState createState() => _PhotoSelectionListViewState();
}

class _PhotoSelectionListViewState extends State<PhotoSelectionListView> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: ListView.builder(
        physics: const BouncingScrollPhysics(),
        padding: const EdgeInsets.symmetric(horizontal: 3.0),
        scrollDirection: Axis.horizontal,
        itemCount: widget.assets.length,
        itemBuilder: (BuildContext context, int index) {
          return Padding(
            padding: const EdgeInsets.symmetric(
              horizontal: 1.5,
              vertical: 1.5,
            ),
            child: AspectRatio(
              aspectRatio: 1.0,
              child: Stack(
                children: <Widget>[
                  pickedImage(index),
                  deleteButton(index),
                  widget.mainImageIndex == index ? mainImageButton() : const SizedBox.shrink(),
                ],
              ),
            ),
          );
        },
      ),
    );
  }

  Widget pickedImage(int index) {
    final asset = widget.assets.elementAt(index);
    return Positioned.fill(
      child: GestureDetector(
        onTap: () => onTapImage(index),
        child: RepaintBoundary(
          child: ClipRRect(
            borderRadius: BorderRadius.circular(5.0),
            child: imageBuilder(asset),
          ),
        ),
      ),
    );
  }

  void onTapImage(int index) {
    widget.onTapImage(index);
    setState(() {});
  }

  Widget? imageBuilder(AssetEntity asset) {
    Widget? widget;
    switch (asset.type) {
      case AssetType.image:
        widget =
            Image(image: AssetEntityImageProvider(asset, isOriginal: false), fit: BoxFit.cover);
        break;
      default:
        break;
    }
    return widget;
  }

  Widget mainImageButton() {
    return AnimatedPositioned(
      duration: kThemeAnimationDuration,
      left: -5.0,
      bottom: -10.0,
      child: OutlinedButton(
        style: ButtonStyle(
          minimumSize: MaterialStateProperty.all(const Size(30, 20)),
          padding: MaterialStateProperty.all(const EdgeInsets.fromLTRB(0, 0, 0, 0)),
          shape: MaterialStateProperty.all<RoundedRectangleBorder>(
            RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(5.0),
              side: const BorderSide(color: ZConfig.zzin_pointColor),
            ),
          ),
          foregroundColor: MaterialStateProperty.all<Color>(ZConfig.zzin_white),
          backgroundColor: MaterialStateProperty.all<Color>(ZConfig.zzin_pointColor),
        ),
        onPressed: () {},
        child: const Text(
          '대표',
          style: TextStyle(fontSize: 12),
        ),
      ),
    );
  }

  Widget deleteButton(int index) {
    return AnimatedPositioned(
      duration: kThemeAnimationDuration,
      right: 5.0,
      top: 5.0,
      child: GestureDetector(
        onTap: () => widget.onTapRemove(index),
        child: DecoratedBox(
          decoration: BoxDecoration(
            color: ZConfig.zzin_black,
            backgroundBlendMode: BlendMode.overlay,
            borderRadius: BorderRadius.circular(10.0),
          ),
          child: const Icon(
            Icons.close,
            size: 20.0,
            color: ZConfig.zzin_white,
          ),
        ),
      ),
    );
  }
}
