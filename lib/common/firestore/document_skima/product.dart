import 'dart:io';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/foundation.dart';
import 'package:zzin/common/firestore/document_skima/user_account.dart';
import 'package:zzin/common/utils/image_processing.dart';
import 'package:zzin/common/utils/zenum.dart';
import 'package:zzin/common/zconfig.dart';
import 'package:get/get.dart';

class Product extends GetxController {
  Product(
      {this.docId,
      this.sellerId,
      required this.articleStatus,
      required this.title,
      required this.category,
      required this.brand,
      required this.thumbnail,
      required this.status,
      required this.price,
      required this.likeCounts,
      required this.saleRegistDate,
      required this.saleModifyDate,
      required this.regionCode,
      required this.region1Depth,
      required this.region2Depth,
      required this.regionName,
      required this.regionGeoPoint,
      required this.saleStatus,
      required this.images,
      required this.imagesCache,
      required this.imagesId,
      required this.contents,
      required this.tags});

  DocumentReference? docId;
  DocumentReference? sellerId;
  int articleStatus;
  String title;
  int category;
  int brand;
  String thumbnail;
  int status;
  int price;
  int likeCounts;
  final Timestamp saleRegistDate;
  Timestamp saleModifyDate;
  int regionCode;
  int region1Depth;
  int region2Depth;
  String regionName;
  GeoPoint regionGeoPoint;
  int saleStatus;
  List<String> images;
  List<String> imagesCache;
  List<String> imagesId;
  String contents;
  List<String> tags;

  Product clone() {
    return Product.fromJson(toJson());
  }

  bool isEqualValue(Product other) {
    return docId == other.docId &&
        sellerId == other.sellerId &&
        articleStatus == other.articleStatus &&
        title == other.title &&
        category == other.category &&
        brand == other.brand &&
        thumbnail == other.thumbnail &&
        status == other.status &&
        price == other.price &&
        likeCounts == other.likeCounts &&
        saleRegistDate == other.saleRegistDate &&
        saleModifyDate == other.saleModifyDate &&
        regionCode == other.regionCode &&
        region1Depth == other.region1Depth &&
        region2Depth == other.region2Depth &&
        regionName == other.regionName &&
        regionGeoPoint == other.regionGeoPoint &&
        saleStatus == other.saleStatus &&
        contents == other.contents &&
        listEquals(images, other.images) &&
        listEquals(imagesCache, other.imagesCache) &&
        listEquals(imagesId, other.imagesId) &&
        listEquals(tags, other.tags);
  }

  Product.fromJson(Map<String, Object?> json)
      : this(
          docId: json['docId'] as DocumentReference,
          sellerId: json['sellerId']! as DocumentReference,
          articleStatus: json['articleStatus'] as int,
          title: json['title']! as String,
          category: json['category']! as int,
          brand: json['brand']! as int,
          thumbnail: json['thumbnail']! as String,
          status: json['status']! as int,
          price: json['price']! as int,
          likeCounts: json['likeCounts']! as int,
          saleRegistDate: json['saleRegistDate']! as Timestamp,
          saleModifyDate: json['saleModifyDate']! as Timestamp,
          regionCode: json['regionCode']! as int,
          region1Depth: json['region1Depth'] as int,
          region2Depth: json['region2Depth'] as int,
          regionName: json['regionName']! as String,
          regionGeoPoint: json['regionGeoPoint']! as GeoPoint,
          saleStatus: json['saleStatus']! as int,
          images: (json['images']! as List).cast<String>(),
          imagesCache: (json['imagesCache']! as List).cast<String>(),
          imagesId: (json['imagesId']! as List).cast<String>(),
          contents: json['contents']! as String,
          tags: (json['tags']! as List).cast<String>(),
        );

  Product.fromElastic(Map<dynamic, dynamic> json)
      : this(
          docId: FirebaseFirestore.instance
              .collection(json['docId']['_path']['segments'].first)
              .doc(json['docId']['_path']['segments'].last),
          //       sellerId: json['sellerId']! as DocumentReference,
          sellerId: FirebaseFirestore.instance
              .collection(json['sellerId']['_path']['segments'].first)
              .doc(json['sellerId']['_path']['segments'].last),
          articleStatus: json['articleStatus'] as int,
          title: json['title']! as String,
          category: json['category']! as int,
          brand: json['brand']! as int,
          thumbnail: json['thumbnail']! as String,
          status: json['status']! as int,
          price: json['price']! as int,
          likeCounts: json['likeCounts']! as int,
          saleRegistDate: Timestamp(json['saleRegistDate']!, 0),
          saleModifyDate: Timestamp(json['saleModifyDate']!, 0),
          regionCode: json['regionCode']! as int,
          region1Depth: json['region1Depth'] as int,
          region2Depth: json['region2Depth'] as int,
          regionName: json['regionName']! as String,
          regionGeoPoint: GeoPoint(double.parse(json['regionGeoPoint'].split(',').first),
              double.parse(json['regionGeoPoint'].split(',').last)),
          saleStatus: json['saleStatus']! as int,
          images: (json['images']! as List).cast<String>(),
          imagesCache: (json['imagesCache']! as List).cast<String>(),
          imagesId: (json['imagesId']! as List).cast<String>(),
          contents: json['contents']! as String,
          tags: (json['tags']! as List).cast<String>(),
        );

  Map<String, dynamic> toJson() {
    return {
      'docId': docId,
      'sellerId': sellerId,
      'articleStatus': articleStatus,
      'title': title,
      'category': category,
      'brand': brand,
      'thumbnail': thumbnail,
      'status': status,
      'price': price,
      'likeCounts': likeCounts,
      'saleRegistDate': saleRegistDate,
      'saleModifyDate': saleModifyDate,
      'regionCode': regionCode,
      'region1Depth': region1Depth,
      'region2Depth': region2Depth,
      'regionName': regionName,
      'regionGeoPoint': regionGeoPoint,
      'saleStatus': saleStatus,
      'images': images,
      'imagesCache': imagesCache,
      'imagesId': imagesId,
      'contents': contents,
      'tags': tags,
    };
  }

  bool isMyProduct() {
    return sellerId == ZConfig.userAccount.docId;
  }

  Future<bool> addProduct(List<File> _files) async {
    var products = FirebaseFirestore.instance
        .collection(ZConfig.productsCollection)
        .withConverter<Product>(
            fromFirestore: (snapshots, _) => Product.fromJson(snapshots.data()!),
            toFirestore: (product, _) => product.toJson());

    var user = FirebaseFirestore.instance
        .doc(ZConfig.userAccount.docId!.path)
        .withConverter<ZUserAccount>(
            fromFirestore: (snapshot, _) => ZUserAccount.fromJson(snapshot.data()!),
            toFirestore: (userAccout, _) => userAccout.toJson());

    sellerId = FirebaseFirestore.instance.doc(ZConfig.userAccount.docId!.path);
    if (sellerId == null) {
      throw Exception('addProduct: sellerID is null');
    }

    // 이미지 프로세싱...

    try {
      var param = await ImageProcessing(_files).processImage();

      docId = FirebaseFirestore.instance.collection(ZConfig.productsCollection).doc();
      // 상품정보 저장
      await products.add(this).then((product) async {
        var storagePath = '${ZConfig.productsCollection}/${user.id}/${product.id}/${product.id}';
        var storageUrls = <String>[];
        var localfilePaths = <String>[];

        // 이미지 스토리지에 저장
        localfilePaths = await uploadImages(param, storagePath, storageUrls);
        // 썸네일 이미지 스토리지에 저장.
        await uploadThumbnail(storagePath, param.fileThumb);

        // 이미지 경로 업데이트
        print('image url update');
        await product.update({
          'articleStatus': ProductArticleStatus.NORMAL.index,
          'saleRegistDate': FieldValue.serverTimestamp(),
          'saleModifyDate': FieldValue.serverTimestamp(),
          'images': storageUrls,
          'imagesCache': localfilePaths,
          'thumbnail': thumbnail,
          'docId':
              FirebaseFirestore.instance.collection(ZConfig.productsCollection).doc(product.id),
        });

        print(
            FirebaseFirestore.instance.collection(ZConfig.productsCollection).doc(product.id).path);

        // 유저 테이블에 상품리스트 추가.
        var userSnapShot = await user.get();
        var productList = userSnapShot.data()!.productList;
        productList.add(FirebaseFirestore.instance
            .collection(ZConfig.productsCollection)
            // .doc(regionCode.toString())
            // .collection(ZConfig.productGroupCollection)
            .doc(product.id));
        await user.update({'productList': productList});

        print('상품 등록 완료~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~');
      });
    } catch (e) {
      print('addProduct Exception: $e');
    }
    return true;
  }

  Future<List<String>> uploadImages(
      ThumbnailParam param, String storagePath, List<String> imagesUrl) async {
    var getUrlTasks = <Future<String>>[];
    var filePaths = <String>[];
    for (var index = 0; index < param.filesWebp.length; index++) {
      // 이미지 스토리지에 저장.
      var taskUpload = FirebaseStorage.instance
          .ref('${storagePath}_image$index.webp')
          .putFile(param.filesWebp[index]);
      try {
        var snapshot = await taskUpload;
        print('Uploaded ${snapshot.bytesTransferred} bytes');
      } on FirebaseException catch (e) {
        print(taskUpload.snapshot);
        print(e);
      }

      // delete temp file.

      await param.filesWebp[index].delete();
      print('임시파일 삭제: ${param.filesWebp[index].path}');

      print('upload complete[$index] : ${param.filesWebp[index]}');
      // 이미지 URL 얻기.
      var taskGetURL =
          FirebaseStorage.instance.ref('${storagePath}_image$index.webp').getDownloadURL();
      getUrlTasks.add(taskGetURL);
      filePaths.add(param.filesWebp[index].path);
    }

    await Future.wait(getUrlTasks).then((url) {
      imagesUrl.addAll(url);
    });

    return filePaths;
  }

  Future uploadThumbnail(String storagePath, File file) async {
    var taskUpload = FirebaseStorage.instance.ref('${storagePath}_thumbnail.jpg').putFile(file);
    await taskUpload;
    print('upload complete : $file');

    // delete temp file...
    await file.delete();
    print('임시파일 삭제: ${file.path}');
    // 썸네일 이미지 URL 얻기.
    var taskGetURL = FirebaseStorage.instance.ref('${storagePath}_thumbnail.jpg').getDownloadURL();
    thumbnail = await taskGetURL;
  }

  Future<bool> updateProduct(List<File> _files) async {
    var bSuccess = false;
    var product = FirebaseFirestore.instance
        .collection(ZConfig.productsCollection)
        .doc(docId!.path.split('/').last)
        .withConverter<Product>(
            fromFirestore: (snapshots, _) => Product.fromJson(snapshots.data()!),
            toFirestore: (product, _) => product.toJson());

    var user = FirebaseFirestore.instance
        .doc(ZConfig.userAccount.docId!.path)
        .withConverter<ZUserAccount>(
            fromFirestore: (snapshot, _) => ZUserAccount.fromJson(snapshot.data()!),
            toFirestore: (userAccout, _) => userAccout.toJson());
    try {
      // 이미지 프로세싱...
      var param = await ImageProcessing(_files).processImage();
      {
        await product.update({
          'title': title,
          'category': category,
          'brand': brand,
          'status': status,
          'price': price,
          'likeCounts': likeCounts,
          'saleRegistDate': saleRegistDate,
          'saleModifyDate': FieldValue.serverTimestamp(),
          'regionCode': regionCode,
          'region1Depth': region1Depth,
          'region2Depth': region2Depth,
          'regionName': regionName,
          'regionGeoPoint': regionGeoPoint,
          'saleStatus': saleStatus,
          'imagesId': imagesId,
          'contents': contents,
          'tags': tags
        }).then((value) async {
          var storageUrls = <String>[];
          var localfilePaths = <String>[];

          var storagePath = '${ZConfig.productsCollection}/${user.id}/${product.id}/${product.id}';

          // 이미지 스토리지에 저장
          localfilePaths = await uploadImages(param, storagePath, storageUrls);
          // 썸네일 이미지 스토리지에 저장.
          await uploadThumbnail(storagePath, param.fileThumb);

          // 이미지 경로 업데이트
          print('image url update');
          await product.update({
            'images': storageUrls,
            'imagesCache': localfilePaths,
            'thumbnail': thumbnail,
          });

          images = storageUrls;
          imagesCache = localfilePaths;
          thumbnail = thumbnail;

          print('상품 수정 완료~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~');
          // notifyListeners();
          bSuccess = true;
        });
      }
    } catch (e) {
      print('updateProduct Exception: $e');
    }

    return bSuccess;
  }

  Future<bool> deleteProduct() async {
    var product = FirebaseFirestore.instance
        .collection(ZConfig.productsCollection)
        .doc(docId!.path.split('/').last);

    var user = FirebaseFirestore.instance
        .doc(ZConfig.userAccount.docId!.path)
        .withConverter<ZUserAccount>(
            fromFirestore: (snapshot, _) => ZUserAccount.fromJson(snapshot.data()!),
            toFirestore: (userAccout, _) => userAccout.toJson());

    try {
      for (var image in images) {
        var ref = FirebaseStorage.instance.refFromURL(image);
        await ref.delete();
      }

      var ref = FirebaseStorage.instance.refFromURL(thumbnail);
      await ref.delete();

      var snapshot = await user
          .withConverter<ZUserAccount>(
              fromFirestore: (snapshot, _) => ZUserAccount.fromJson(snapshot.data()!),
              toFirestore: (userAccout, _) => userAccout.toJson())
          .get();
      var productList = snapshot.data()!.productList;
      productList.remove(product);
      print(productList.toString());
      print(product);
      await user.update({'productList': productList});
      await product.delete();
    } catch (e) {
      print('deleteProduct Exception');
      return false;
    }

    return true;
  }

  Future<Product> getRealTime() async {
    var snapshot = await docId!
        .withConverter<Product>(
            fromFirestore: (snapshots, _) => Product.fromJson(snapshots.data()!),
            toFirestore: (product, _) => product.toJson())
        .get();

    return snapshot.data()!;
  }
}
