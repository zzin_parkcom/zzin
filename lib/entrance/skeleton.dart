import 'dart:convert';
import 'dart:io';
import 'package:badges/badges.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:firebase_remote_config/firebase_remote_config.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:get/get.dart';
import 'package:new_version/new_version.dart';
import 'package:package_info_plus/package_info_plus.dart';
import 'package:version/version.dart';
import 'package:zzin/chat_page/chat_view.dart';
import 'package:zzin/common/hive/upload_collection.dart';
import 'package:zzin/common/hive/zhive.dart';
import 'package:zzin/common/zconfig.dart';
import 'package:zzin/entrance/system_maintenance.dart';
import 'package:zzin/entrance/update_bottomsheet.dart';
import 'package:zzin/favorite_page/favorite_view.dart';
import 'package:zzin/login/login.dart';
import 'package:zzin/my_page/my_view.dart';
import 'package:zzin/notification/notification_count.dart';
import 'package:zzin/opentalk_page/opentalk_view.dart';
import 'package:zzin/product_page/product_view.dart';
import 'package:zzin/upload_page/upload_view.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'dart:async';
import 'package:ntp/ntp.dart';

import 'package:zzin/zzin_page/zzin_view.dart';

class MainTabController extends GetxController {
  var tabIndex = 0.obs;
  var isShowTab = true.obs;
  static MainTabController get to => Get.find();

  void setIndex(int index) {
    tabIndex.value = index;
  }

  @override
  String toString() {
    return 'MainTabController';
  }
}

class Skeleton extends StatefulWidget {
  const Skeleton({Key? key}) : super(key: key);
  @override
  _SkeletonState createState() => _SkeletonState();
}

class _SkeletonState extends State<Skeleton> {
  late final MainTabController mainTabController;
  late StreamSubscription<ConnectivityResult> streamSubscription;
  final List<Widget> _tabPageList = [
    const ProductView(),
    OpentalkView(),
    ChatView(),
    UploadView(tag: 'Skeleton', uploadCollection: UploadCollection.instance),
    const MyView(),
    FavoriteView(),
  ];

  final ZzinView zzinView = const ZzinView();

  @override
  void initState() {
    super.initState();

    mainTabController = Get.put(MainTabController());
    streamSubscription = Connectivity().onConnectivityChanged.listen(onData);
    ZHive.to.put('MainTabController_ZTab', 0);
  }

  @override
  void dispose() {
    super.dispose();
    streamSubscription.cancel();
  }

  void onData(ConnectivityResult result) async {
    switch (result) {
      case ConnectivityResult.ethernet:
        print(
            'online: ethernet network ***********************************************************');
        await EasyLoading.dismiss();
        break;
      case ConnectivityResult.mobile:
        print('online: mobile network ***********************************************************');
        await EasyLoading.dismiss();
        break;
      case ConnectivityResult.wifi:
        print('online: wifi network ***********************************************************');
        await EasyLoading.dismiss();
        break;
      case ConnectivityResult.none:
        print('offline: no network !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!');
        await EasyLoading.show(
          status: '인터넷 연결상태를 확인해 주세요.',
          indicator: const Icon(Icons.wifi_off),
          maskType: EasyLoadingMaskType.black,
          dismissOnTap: false,
        );
        break;
    }
  }

  Future initialize(BuildContext context) async {
    // await versionCheck(context);
    await initializeRemoteConfig(context);
    var result = await processUpdate();
    if (result == 1) {
      exit(result);
    }
    print(result);

    await processSystemMaintenance();
  }

  Future initializeRemoteConfig(BuildContext context) async {
    print(
        'Remote Config Initialize................................................................');
    ZConfig.remoteConfig = RemoteConfig.instance;
    bool updated = await ZConfig.remoteConfig.fetchAndActivate();
    if (updated) {
      print('the config has been updated, new parameter values are available.');
    } else {
      print('the config values were previously updated.');
    }
    await ZConfig.remoteConfig.setConfigSettings(
      RemoteConfigSettings(
        fetchTimeout: const Duration(seconds: 10),
        minimumFetchInterval: const Duration(seconds: 1),
      ),
    );
  }

  Future<int> processUpdate() async {
    String update = ZConfig.remoteConfig.getString('update');
    print(update);
    Map updateJson = jsonDecode(update);
    updateJson.forEach((key, value) {
      print('$key: $value');
    });

    bool isUpdateApp = false;

    if (Platform.isAndroid && (updateJson['updateOS'] as List).contains('android')) {
      isUpdateApp = true;
    } else if (Platform.isIOS && (updateJson['updateOS'] as List).contains('ios')) {
      isUpdateApp = true;
    }

    if (isUpdateApp) {
      PackageInfo packageInfo = await PackageInfo.fromPlatform();

      var packageVersion = Version.parse(packageInfo.version);
      var updateVersion = Version.parse(updateJson['tagetVersion']);
      isUpdateApp = updateVersion > packageVersion;
    }

    if (isUpdateApp) {
      final newVersion = NewVersion(iOSAppStoreCountry: 'KR');
      final status = await newVersion.getVersionStatus();
      bool isForced = updateJson['option'] == 'forced';

      var result = await showModalBottomSheet<int>(
        shape: const RoundedRectangleBorder(
          borderRadius: BorderRadius.vertical(top: Radius.circular(10)),
        ),
        isScrollControlled: true,
        isDismissible: false,
        enableDrag: false,
        elevation: 10,
        context: context,
        builder: (context) {
          return WillPopScope(
            onWillPop: () async => false,
            child: UpdateBottomSheet(
              appStoreLink: status!.appStoreLink,
              isForced: isForced,
              sizeToRatio: 0.3,
            ),
          );
        },
      );

      print(result);
      return result ?? Cancel;
    }
    return Cancel;
  }

  Future processSystemMaintenance() async {
    String systemMaintenance = ZConfig.remoteConfig.getString('systemMaintenance');
    print(systemMaintenance);
    Map systemMaintenanceJson = jsonDecode(systemMaintenance);
    systemMaintenanceJson.forEach(
      (key, value) {
        print('$key: $value');
      },
    );

    DateTime startDateTime = DateTime.parse(systemMaintenanceJson['startDateTime']);
    DateTime endDateTime = DateTime.parse(systemMaintenanceJson['endDateTime']);

    DateTime ntpTime = await NTP.now();
    DateTime koTime = ntpTime.add(const Duration(hours: 9));
    print(koTime);

    bool isMaintenance = systemMaintenanceJson['systemMaintenance'];
    if (isMaintenance) {
      Timestamp startTimestamp = Timestamp.fromDate(startDateTime);
      Timestamp endTimestamp = Timestamp.fromDate(endDateTime);

      await showModalBottomSheet<int>(
        shape: const RoundedRectangleBorder(
          borderRadius: BorderRadius.vertical(top: Radius.circular(10)),
        ),
        isScrollControlled: true,
        isDismissible: false,
        enableDrag: false,
        elevation: 10,
        context: context,
        builder: (context) {
          return WillPopScope(
            onWillPop: () async => false,
            child: SystemMaintenanceBottomSheet(
              startTimestamp: startTimestamp,
              endTimestamp: endTimestamp,
              description: systemMaintenanceJson['description'],
              sizeToRatio: 0.3,
            ),
          );
        },
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    print('Skeleton build');
    if (ZConfig.userAccount.isZzinMember()) {
      if (!_tabPageList.contains(zzinView)) {
        _tabPageList.add(zzinView);
      }
    }

    return FutureBuilder(
      future: initialize(context),
      builder: (context, AsyncSnapshot snapshot) {
        if (snapshot.connectionState == ConnectionState.waiting) {
          return Scaffold(
            resizeToAvoidBottomInset: false,
            body: Center(
              child: Image.asset(
                'assets/splash_android.png',
                fit: BoxFit.cover,
                height: double.infinity,
                width: double.infinity,
              ),
            ),
          );
        } else {
          return Obx(() => Scaffold(
                resizeToAvoidBottomInset: true,
                body: _tabPageList[mainTabController.tabIndex.value],
                bottomNavigationBar:
                    mainTabController.isShowTab.value ? _bottomNavigationBar() : null,
              ));
        }
      },
    );
  }

  // BottomNavigationBar
  final double iconSizeSelected = 20;
  final double iconSize = 20;
  BottomNavigationBar _bottomNavigationBar() {
    List<BottomNavigationBarItem> bottomList = [
      BottomNavigationBarItem(
        label: '홈',
        activeIcon:
            FaIcon(FontAwesomeIcons.home, color: ZConfig.zzin_pointColor, size: iconSizeSelected),
        icon: FaIcon(FontAwesomeIcons.home, size: iconSize),
      ),
      BottomNavigationBarItem(
        label: '명품톡',
        activeIcon: Icon(Icons.chat, color: ZConfig.zzin_pointColor, size: iconSizeSelected),
        icon: Icon(Icons.chat, size: iconSize),
      ),
      BottomNavigationBarItem(
        label: '채팅',
        icon: chatIcon(),
        activeIcon: FaIcon(FontAwesomeIcons.commentDots,
            color: ZConfig.zzin_pointColor, size: iconSizeSelected),
      ),
      BottomNavigationBarItem(
        label: '상품등록',
        activeIcon:
            FaIcon(FontAwesomeIcons.edit, color: ZConfig.zzin_pointColor, size: iconSizeSelected),
        icon: FaIcon(FontAwesomeIcons.edit, size: iconSize),
      ),
      BottomNavigationBarItem(
        label: '내정보',
        activeIcon:
            FaIcon(FontAwesomeIcons.user, color: ZConfig.zzin_pointColor, size: iconSizeSelected),
        icon: FaIcon(FontAwesomeIcons.user, size: iconSize),
      ),
      BottomNavigationBarItem(
        label: '찐',
        activeIcon:
            FaIcon(FontAwesomeIcons.heart, color: ZConfig.zzin_pointColor, size: iconSizeSelected),
        icon: FaIcon(FontAwesomeIcons.heart, size: iconSize),
      ),
    ];

    if (ZConfig.userAccount.isZzinMember()) {
      bottomList.add(BottomNavigationBarItem(
        label: 'ZZIN',
        activeIcon: Icon(Icons.code, color: ZConfig.zzin_pointColor, size: iconSizeSelected),
        icon: Icon(Icons.code, size: iconSize),
      ));
    }

    return BottomNavigationBar(
      selectedItemColor: ZConfig.zzin_pointColor,
      currentIndex: mainTabController.tabIndex.value,
      type: BottomNavigationBarType.fixed,
      items: bottomList,
      onTap: onTapBottomNavigationBar,
    );
  }

  void onTapBottomNavigationBar(int index) async {
    if (ZConfig.userAccount.isZzinMember()) {
      if (!_tabPageList.contains(zzinView)) {
        _tabPageList.add(zzinView);
      }
      // if (index == _tabPageList.length - 1) {
      //   var result = await showPasswordDialog(context, '넌 뉘요?', '나다!');
      //   if (result != '111') {
      //     Get.rawSnackbar(
      //         message: '불법 침입자를 112에 신고하는중....',
      //         snackbarStatus: (status) {
      //           if (status == SnackbarStatus.CLOSED) {
      //             Get.rawSnackbar(
      //                 message: '경찰이 문앞에 도착했음. 넌 끝이야!',
      //                 snackbarStatus: (status) {
      //                   if (status == SnackbarStatus.CLOSED) {
      //                     Get.rawSnackbar(message: '콩밥 먹고 부자되세요!!!  ^.^;;;');
      //                   }
      //                 });
      //           }
      //         },
      //         duration: Duration(seconds: 3));

      //     return;
      //   }
      // }
    }

    if (index != 0 && ZConfig.userAccount.getSocial() == null) {
      var result = await Get.to(() => const LogIn(), transition: Transition.rightToLeftWithFade);

      if (result == null || result == false) {
        index = 0;
      }
    } else if (index == 1) {
      NotificationCounter.to.isExistNotification.value = false;
    }
    mainTabController.setIndex(index);
  }

  Widget chatIcon() {
    return Obx(
      () => Badge(
        badgeContent: NotificationCounter.to.isExistNotification.value
            ? const Text('', style: TextStyle(fontSize: 12, color: ZConfig.zzin_white))
            : null,
        padding: NotificationCounter.to.isExistNotification.value
            ? const EdgeInsets.all(5)
            : const EdgeInsets.all(0),
        position: const BadgePosition(top: -5, end: 0),
        badgeColor: Colors.red,
        child: FaIcon(FontAwesomeIcons.commentDots, size: iconSize),
      ),
    );
  }
}
