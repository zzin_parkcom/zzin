import 'package:zzin/common/firestore/document_skima/user_account.dart';

abstract class SocialSignin {
  Future<bool> login();
  Future<void> logout();
  Future<ZUserAccount> getAccountInfo();
  Future<void> unlink();
  String social();
}
