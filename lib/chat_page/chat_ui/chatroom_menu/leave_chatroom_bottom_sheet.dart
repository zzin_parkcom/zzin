import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:zzin/common/widget/zbutton_blue.dart';

class LeaveChatRoomBottomSheet extends StatelessWidget {
  const LeaveChatRoomBottomSheet({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          const Text(
            '채팅방을 나가시겠습니까?',
            style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20),
          ),
          const SizedBox(height: 24),
          const Padding(
            padding: EdgeInsets.symmetric(horizontal: 60),
            child: Text('채팅방을 나가는 경우 상대방과의 대화 내용은 복구할 수 없습니다.',
                style: TextStyle(fontSize: 14), textAlign: TextAlign.center),
          ),
          const SizedBox(height: 24),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              ZButtonYellow(
                onPressed: () => Get.back(result: 0),
                text: '취소하기',
                fontSize: 14,
                isSelected: true,
              ),
              ZButtonYellow(
                text: '채팅방 나가기',
                isSelected: true,
                fontSize: 14,
                onPressed: () => Get.back(result: 1),
              ),
            ],
          )
        ],
      ),
    );
  }
}
