import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:flutter_image_compress/flutter_image_compress.dart';
import 'package:get/get.dart';
import 'package:keyboard_dismisser/keyboard_dismisser.dart';
import 'package:wechat_assets_picker/wechat_assets_picker.dart';
import 'package:zzin/common/utils/korean_text_delegate.dart';
import 'package:zzin/common/widget/zbutton_blue.dart';
import 'package:zzin/common/zconfig.dart';
import 'package:date_time_picker/date_time_picker.dart';
import 'package:zzin/my_page/notice_page.dart';
import 'package:zzin/postbox/postbox.dart';
import 'package:zzin/postbox/postbox_controller.dart';
import 'package:zzin/postbox/postbox_editor/order_editing_widget.dart';
import 'package:zzin/postbox/postbox_viewr.dart';

class PostboxEditor extends StatefulWidget {
  final Postbox? postbox;
  const PostboxEditor({this.postbox, Key? key}) : super(key: key);

  @override
  _PostboxEditorState createState() => _PostboxEditorState();
}

class _PostboxEditorState extends State<PostboxEditor> {
  TextEditingController textEditingController = TextEditingController();
  FocusNode textFocusNode = FocusNode();
  String dropdownValue = 'text';
  List<Widget> addedWidget = [];

  @override
  void initState() {
    super.initState();
    if (widget.postbox != null) {
      title = widget.postbox!.title;
      type = widget.postbox!.type;
      reportingDate = widget.postbox!.reportingDate;
      expirationDate = widget.postbox!.expirationDate;
      contents = widget.postbox!.contents;
      for (var element in (contents['order'] as List)) {
        order.add(element);
      }
      contents.remove('order');
      docId = widget.postbox!.docId;

      imageIndex = order.where((element) => (element as String).startsWith('image')).length;
      textIndex = order.where((element) => (element as String).startsWith('text')).length;
      imageIndex = order.where((element) => (element as String).startsWith('link')).length;
    }

    addedWidget.add(getTextEditor());
  }

  ////////////////////////////////////////////////////////////////////////
  String title = '';
  PostboxType type = PostboxType.Notice;
  Timestamp reportingDate = Timestamp.now();
  Timestamp expirationDate = Timestamp.now();
  DocumentReference reporterRef = ZConfig.userAccount.docId!;
  bool isRemoved = false;
  String docId = '데이타가 추가될때 갱신됨.';
  Map contents = {}.obs;
  List order = <String>[].obs;

  int imageIndex = 0;
  int textIndex = 0;
  int linkIndex = 0;

  List expandedList = [true, true, true, true];
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: KeyboardDismisser(
        child: Scaffold(
          appBar: AppBar(title: const Text('알림함 메시지 작성')),
          body: Padding(
            padding: const EdgeInsets.all(10.0),
            child: SingleChildScrollView(
              child: Column(
                children: [
                  ExpansionPanelList(
                    animationDuration: const Duration(milliseconds: 300),
                    children: [
                      ExpansionPanel(
                        headerBuilder: (context, isExpanded) => const Text('편집 패널'),
                        body: Column(
                          children: [
                            getTitleWidget(),
                            getTypeWidget(),
                            getReportingDate(),
                            getExpirationDate(),
                            getContentsEditor(),
                            Column(children: addedWidget),
                          ],
                        ),
                        isExpanded: expandedList[0],
                      ),
                      ExpansionPanel(
                        headerBuilder: (context, isExpanded) => const Text('순서편집 패널'),
                        body: OrderEditingWidget(order: order, contents: contents),
                        isExpanded: expandedList[1],
                      ),
                      ExpansionPanel(
                        headerBuilder: (context, isExpanded) => const Text('순서 미리보기 패널'),
                        body: Obx(() => Column(children: processContents(contents))),
                        isExpanded: expandedList[2],
                      ),
                    ],
                    expansionCallback: (int item, bool status) {
                      print(item.toString());
                      print(status.toString());
                      expandedList[item] = !status;
                      setState(() {});
                    },
                  ),
                  ZButtonYellow(text: '미리보기', isSelected: true, onPressed: onPreview),
                  ZButtonYellow(
                      text: widget.postbox == null ? '등록하기' : '수정하기',
                      isSelected: true,
                      onPressed: onSubmit),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  Widget getTextEditor() => TextField(
        minLines: 2,
        maxLines: 1000,
        controller: textEditingController,
        focusNode: textFocusNode,
      );

  Widget getImageEditor() => Column(
        children: [
          IconButton(
            icon: const Icon(Icons.add_a_photo),
            tooltip: '사진을 선택해 주세요.',
            onPressed: onPickImage,
          ),
          asset != null
              ? Row(
                  children: [
                    Expanded(
                      child: Image(
                        image: AssetEntityImageProvider(asset!, isOriginal: false),
                        fit: BoxFit.fill,
                      ),
                    ),
                  ],
                )
              : const Text('No Image'),
        ],
      );

  Widget getTitleWidget() => Row(
        children: [
          const Padding(
            padding: EdgeInsets.only(right: 8.0),
            child: Text('제목:'),
          ),
          Expanded(
              child: TextFormField(
            initialValue: title,
            onChanged: (value) => title = value,
            decoration: const InputDecoration(
                border: InputBorder.none,
                hintText: '제목 입력',
                hintStyle: TextStyle(color: Colors.red)),
          ))
        ],
      );

  Widget getTypeWidget() => Row(
        children: [
          const Padding(
            padding: EdgeInsets.only(right: 8.0),
            child: Text('타입:'),
          ),
          DropdownButton<String>(
            value: type.korText,
            icon: const Icon(Icons.arrow_downward),
            iconSize: 16,
            elevation: 16,
            style: const TextStyle(color: ZConfig.zzin_pointColor),
            underline: Container(height: 2, color: Colors.black),
            onChanged: (String? newValue) {
              setState(() {
                if (newValue == PostboxType.Notice.korText) {
                  type = PostboxType.Notice;
                } else if (newValue == PostboxType.Event.korText) {
                  type = PostboxType.Event;
                } else if (newValue == PostboxType.UrgentNotice.korText) {
                  type = PostboxType.UrgentNotice;
                }
                print(type.korText);
              });
            },
            items: List.generate(
                    PostboxType.values.length, (index) => PostboxType.values[index].korText)
                .map<DropdownMenuItem<String>>((String value) {
              return DropdownMenuItem<String>(
                value: value,
                child: Text(value),
              );
            }).toList(),
          )
        ],
      );

  Widget getReportingDate() => Row(
        children: [
          const Padding(
            padding: EdgeInsets.only(right: 8.0),
            child: Text('작성날짜: '),
          ),
          Expanded(
            child: DateTimePicker(
              readOnly: true,
              type: DateTimePickerType.dateTimeSeparate,
              initialValue: reportingDate.toDate().toString(),
              firstDate: reportingDate.toDate(),
              lastDate: DateTime(2100),
              dateLabelText: '날짜',
              style: const TextStyle(fontSize: 12),
              timeLabelText: '시간',
              onChanged: (val) {
                print(val);
                DateTime dateTime = DateTime.parse(val);
                reportingDate = Timestamp.fromDate(dateTime);
                print(dateTime);
              },
              validator: (val) {
                print(val);
                return null;
              },
              onSaved: (val) {},
            ),
          ),
        ],
      );

  Widget getExpirationDate() => Row(
        children: [
          const Padding(
            padding: EdgeInsets.only(right: 8.0),
            child: Text('만료날짜: '),
          ),
          Expanded(
            child: DateTimePicker(
              type: DateTimePickerType.dateTimeSeparate,
              initialValue: expirationDate.toDate().toString(),
              firstDate: expirationDate.toDate(),
              lastDate: DateTime(2100),
              dateLabelText: '날짜',
              style: const TextStyle(fontSize: 12),
              timeLabelText: '시간',
              dateHintText: '만료날짜를 선택하세요',
              onChanged: (val) {
                print(val);
                DateTime dateTime = DateTime.parse(val);
                expirationDate = Timestamp.fromDate(dateTime);
                print(dateTime);
              },
              validator: (val) {
                print(val);
                return null;
              },
              onSaved: (val) {},
            ),
          ),
        ],
      );

  Widget getContentsEditor() => Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          DropdownButton<String>(
            value: dropdownValue,
            icon: const Icon(Icons.arrow_downward),
            iconSize: 16,
            elevation: 16,
            style: const TextStyle(color: ZConfig.zzin_pointColor),
            underline: Container(
              height: 2,
              color: Colors.black,
            ),
            onChanged: (String? newValue) {
              setState(() {
                dropdownValue = newValue!;
                print(dropdownValue);
                switch (dropdownValue) {
                  case 'text':
                    addedWidget.clear();
                    addedWidget.add(getTextEditor());
                    setState(() {});
                    break;
                  case 'image':
                    addedWidget.clear();
                    addedWidget.add(getImageEditor());
                    break;

                  case 'link':
                    addedWidget.clear();
                    addedWidget.add(getTextEditor());
                    break;
                }
              });
            },
            items: <String>['text', 'image', 'link'].map<DropdownMenuItem<String>>((String value) {
              return DropdownMenuItem<String>(
                value: value,
                child: Text(value),
              );
            }).toList(),
          ),
          ZButtonYellow(text: '추가', isSelected: true, onPressed: onAdd)
        ],
      );

  void onAdd() async {
    print(dropdownValue);
    switch (dropdownValue) {
      case 'text':
        contents.putIfAbsent(
            'text' + textIndex.toString(), () => textEditingController.text.trim());
        order.add('text' + textIndex.toString());
        textIndex++;
        textEditingController.text = '';
        textFocusNode.unfocus();

        setState(() {});
        break;
      case 'image':
        await EasyLoading.show(
            status: '이미지 업로드중...', maskType: EasyLoadingMaskType.black, dismissOnTap: false);

        if (asset != null) {
          var file = await asset!.originFile;
          var result =
              await FlutterImageCompress.compressWithFile(file!.absolute.path, quality: 80);
          if (result != null) {
            String fileName =
                file.path.split('/').last.split('.').first + DateTime.now().toString() + '.jpg';

            String storagePath =
                'postbox/images/${ZConfig.userAccount.docId!.path.split('/').last}/$fileName';

            await FirebaseStorage.instance.ref(storagePath).putData(result);

            String imagePath = await FirebaseStorage.instance.ref(storagePath).getDownloadURL();

            print(imagePath);
            contents.putIfAbsent('image' + imageIndex.toString(), () => imagePath);
            order.add('image' + imageIndex.toString());
            imageIndex++;

            asset = null;
            addedWidget.clear();
            addedWidget.add(getImageEditor());
          }
        }
        await EasyLoading.dismiss();
        setState(() {});

        break;

      case 'link':
        contents.putIfAbsent(
            'link' + linkIndex.toString(), () => textEditingController.text.trim());
        order.add('link' + linkIndex.toString());
        linkIndex++;
        textEditingController.text = '';
        textFocusNode.unfocus();

        setState(() {});
        break;
    }
  }

  void onPreview() {
    Map finalContents = {};
    finalContents.addAll(contents);
    finalContents.putIfAbsent('order', () => order);
    reportingDate = Timestamp.now();

    Postbox postbox = Postbox(
        reportingDate: reportingDate,
        expirationDate: expirationDate,
        type: type,
        title: title,
        reporterRef: reporterRef,
        isRemoved: isRemoved,
        docId: docId,
        contents: finalContents,
        isRead: false);
    Get.to(() => PostboxViewer(postbox: postbox),
        transition: Transition.upToDown, duration: ZConfig.heroAnimationDuration);
  }

  void onSubmit() async {
    Map finalContents = {};
    finalContents.addAll(contents);
    finalContents.putIfAbsent('order', () => order);

    reportingDate = Timestamp.now();

    if (title.isEmpty) {
      Get.rawSnackbar(message: '제목을 입력해 주세요');
      return;
    }
    if (expirationDate
            .toDate()
            .difference(reportingDate.toDate())
            .compareTo(const Duration(days: 1)) <
        1) {
      Get.rawSnackbar(message: '만료날짜는 오늘날짜보다 하루이상 나중이어야 됩니다.');
      return;
    }

    if (finalContents.length < 2) {
      //'order' 기본추가됨.
      Get.rawSnackbar(message: '컨텐츠를 추가해 주세요');
      return;
    }

    Postbox postbox = Postbox(
        reportingDate: reportingDate,
        expirationDate: expirationDate,
        type: type,
        title: title,
        reporterRef: reporterRef,
        isRemoved: isRemoved,
        docId: docId,
        contents: finalContents,
        isRead: false);
    if (widget.postbox == null) {
      // 등록하기

      await PostboxController().addPostBox(postbox);
    } else {
      // 업데이트 하기
      await PostboxController().updatePostbox(postbox);

      Get.back();
    }

    textEditingController.text = '';
    title = '';
    expirationDate = Timestamp.now();
    contents.clear();
    order.clear();
    Get.rawSnackbar(message: '${widget.postbox == null ? '등록' : '수정'}이 완료되었습니다.');
    setState(() {});
  }

  AssetEntity? asset;
  void onPickImage() async {
    var assets = await AssetPicker.pickAssets(context,
        maxAssets: 1, textDelegate: KoreanTextDelegate(), gridCount: 3, pageSize: 90);
    if (assets != null) {
      asset = assets.first;
      addedWidget.clear();
      addedWidget.add(getImageEditor());
      setState(() {});
    }
  }

  List<Widget> processContents(Map map) {
    List<Widget> result = <Widget>[];

    if (map.isEmpty) {
      return result;
    }

    List orderList = order;
    for (var order in orderList) {
      if (order is String && order.startsWith('text')) {
        result.add(Text(map[order]));
      } else if (order is String && order.startsWith('image')) {
        result.add(Row(
          children: [
            Expanded(
              child: Padding(
                padding: const EdgeInsets.symmetric(vertical: 10.0),
                child: Image.network(
                  map[order],
                  fit: BoxFit.fill,
                ),
              ),
            ),
          ],
        ));
      } else if (order is String && order.startsWith('link')) {
        result.add(GestureDetector(
            child: Text(map[order],
                style: const TextStyle(decoration: TextDecoration.underline, color: Colors.blue)),
            onTap: () {
              Get.to(() => const NoticePage());
            }));
      }
    }
    return result;
  }
}
