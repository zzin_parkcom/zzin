import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:zzin/common/firestore/document_skima/user_account.dart';
import 'package:zzin/common/zconfig.dart';
import 'package:get/get.dart';

class QueryBlockUser extends GetxController {
  List<DocumentSnapshot<ZUserAccount>> blockUsers = [];
  DocumentSnapshot? lastDocument;
  bool isLoading = false; // track if products fetching
  bool hasMore = true; // flag for more products available or not
  int documentLimit = ZConfig.maxDocumentCount; // documents to

  int _startIndex = 0;
  int _endIndex = 0;

  static QueryBlockUser get to => Get.find();

  List makeBlockUserList() {
    if (ZConfig.userAccount.isRegistered()) {
      if (_startIndex == 0 && _endIndex == 0) {
        if (ZConfig.blockUsers.length > ZConfig.maxDocumentCount) {
          _endIndex = ZConfig.maxDocumentCount;
        } else {
          _endIndex = ZConfig.blockUsers.length;
        }
      } else {
        if (ZConfig.blockUsers.length > ZConfig.maxDocumentCount &&
            ZConfig.blockUsers.length > _endIndex) {
          if (ZConfig.blockUsers.length > _endIndex + ZConfig.maxDocumentCount) {
            _startIndex += ZConfig.maxDocumentCount;
            _endIndex += ZConfig.maxDocumentCount;
          } else {
            _startIndex += ZConfig.maxDocumentCount;
            _endIndex += ZConfig.blockUsers.length - _endIndex;
          }
        } else {
          return List.empty();
        }
      }

      var likeList =
          ZConfig.blockUsers.getRange(_startIndex, _endIndex).map((e) => e.userDocId).toList();
      return likeList;
    }
    return [];
  }

  Future<bool> queryBlockUser({bool byScroll = false}) async {
    if (!byScroll) {
      clearProduct();
    }
    if (!hasMore) {
      print('더이상 차단 유저가 없습니다.');
      return false;
    }
    if (isLoading) {
      print('쿼리중... ');
      return false;
    }
    isLoading = true;

    var blockList = [];
    blockList = makeBlockUserList();
    if (blockList.isEmpty) {
      print('더이상 차단 유저가 없습니다.');
      return false;
    }

    Query query = FirebaseFirestore.instance
        .collection(ZConfig.userCollection)
        .where('docId', whereIn: blockList)
        .withConverter<ZUserAccount>(
            fromFirestore: (snapshot, _) => ZUserAccount.fromJson(snapshot.data()!),
            toFirestore: (chatMessage, _) => chatMessage.toJson());

    if (lastDocument == null) {
      // 처음 검색하는 경우.
      query = query.limit(documentLimit);
    } else {
      query = query.startAfterDocument(lastDocument!).limit(documentLimit);
    }

    // 쿼리 실행.
    try {
      var snapshots = await query
          .withConverter<ZUserAccount>(
              fromFirestore: (snapshot, _) => ZUserAccount.fromJson(snapshot.data()!),
              toFirestore: (chatMessage, _) => chatMessage.toJson())
          .get();
      print('차단유저 검색된 갯수: ${snapshots.docs.length}');

      if (snapshots.docs.length < documentLimit) {
        hasMore = false;
      }

      if (snapshots.docs.isNotEmpty) {
        lastDocument = snapshots.docs[snapshots.docs.length - 1];
        blockUsers.addAll(snapshots.docs);
        print('차단유저 리스트 갯수: ${blockUsers.length}');

        isLoading = false;
      }
      return true;
    } catch (e) {
      print('복합 인덱스가 필요한지 보세요~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~');
      print('복합 인덱스가 필요한지 보세요~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~');
      print('복합 인덱스가 필요한지 보세요~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~');

      print(e);
    }
    return false;
  }

  void clearProduct() {
    blockUsers.clear();
    lastDocument = null;
    isLoading = false;
    hasMore = true;
    _startIndex = _endIndex = 0;
  }
}
