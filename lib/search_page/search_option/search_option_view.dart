import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:zzin/common/hive/search_collection.dart';
import 'package:zzin/common/widget/appbar/zappbar.dart';
import 'package:zzin/common/widget/zbutton_blue.dart';
import 'package:zzin/common/zconfig.dart';
import 'package:zzin/search_page/search_option/brand_option.dart';
import 'package:zzin/search_page/search_option/category_option.dart';
import 'package:zzin/search_page/search_option/product_status_option.dart';
import 'package:zzin/search_page/search_option/salearea_option.dart';
import 'package:zzin/search_page/search_option/sort_option.dart';

class SearchOptionView extends StatefulWidget {
  const SearchOptionView({Key? key}) : super(key: key);

  @override
  _SearchOptionViewState createState() => _SearchOptionViewState();
}

class _SearchOptionViewState extends State<SearchOptionView> {
  final CategoryOption categoryOption = CategoryOption(20);
  final BrandOption brandOption = BrandOption(20);
  final SaleAreaOption saleAreaOption = SaleAreaOption(20);
  final ProductStatusOption productStatusOption = ProductStatusOption(20);
  final SortOption sortOption = SortOption(20);

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        appBar: zAppBar(),
        resizeToAvoidBottomInset: false,
        body: Padding(
          padding: const EdgeInsets.all(10.0),
          child: SingleChildScrollView(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                categoryOption,
                const Divider(),
                brandOption,
                const Divider(),
                saleAreaOption,
                const Divider(),
                productStatusOption,
                const Divider(),
                sortOption,
                const Divider(),
                const SizedBox(height: 50)
              ],
            ),
          ),
        ),
        bottomNavigationBar: Padding(
          padding: ZSize.bottomNavigationBarButtonOutPadding,
          child: ZButtonYellow(
              text: '적용하기',
              padding: ZSize.bottomNavigationBarButtonInPadding,
              fontSize: ZSize.BNB_ButtonFontSize,
              onPressed: onSubmit,
              isSelected: true),
        ),
      ),
    );
  }

  AppBar zAppBar() => ZAppBar(
        leading: IconButton(
            icon: const Icon(Icons.arrow_back, color: ZConfig.zzin_black), onPressed: onBack),
        title: const Text(
          '검색 설정',
          style: TextStyle(fontSize: ZSize.appBarTextSize, color: ZConfig.zzin_black),
        ),
        actions: [
          Padding(
            padding: const EdgeInsets.all(10.0),
            child: TextButton.icon(
              style: ButtonStyle(
                  backgroundColor: MaterialStateProperty.all<Color>(ZConfig.zzin_white)),
              icon: const Icon(Icons.clear, color: ZConfig.zzin_white, size: 14),
              onPressed: onInitialize,
              label: const Text(
                '초기화',
                style: TextStyle(fontSize: 14, color: ZConfig.zzin_pointColor),
              ),
            ),
          ),
          const SizedBox(width: 5)
        ],
      );

  void onBack() {
    Get.back(result: false);
  }

  void onInitialize() {
    SearchCollection.instance.initialize();
    categoryOption.clearState();
    brandOption.clearState();
    saleAreaOption.clearState();
    productStatusOption.clearState();
    sortOption.clearState();
    setState(() {});
  }

  void onSubmit() {
    SearchCollection.instance.categoryId = categoryOption.getCategoryId();
    SearchCollection.instance.brandId = brandOption.getBrandId();
    SearchCollection.instance.addressId = saleAreaOption.getAddressId();
    SearchCollection.instance.addressStep = saleAreaOption.getAddressStep();
    SearchCollection.instance.status = productStatusOption.getStatus();
    SearchCollection.instance.saleStatus = productStatusOption.getSaleStatus();
    SearchCollection.instance.sort = sortOption.getSort();
    SearchCollection.instance.save();
    Get.back(result: true);
  }
}
