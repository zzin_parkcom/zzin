import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:zzin/common/firestore/document_skima/notice.dart';
import 'package:zzin/common/zconfig.dart';
import 'package:get/get.dart';

class QueryNotice extends GetxController {
  List<DocumentSnapshot<Notice>> notices = [];
  DocumentSnapshot? lastDocument;
  bool isLoading = false; // track if products fetching
  bool hasMore = true; // flag for more products available or not
  int documentLimit = ZConfig.maxDocumentCount; // documents to

  static QueryNotice get to => Get.find();

  Future<void> queryNotice({bool byScroll = false}) async {
    if (!byScroll) {
      clearProduct();
    }
    if (!hasMore) {
      print('더이상 공지사항이 없습니다.');
      return;
    }
    if (isLoading) {
      print('쿼리중... ');
      return;
    }
    isLoading = true;

    Query query = FirebaseFirestore.instance
        .collection(ZConfig.noticeCollection)
        .where('isPost', isEqualTo: true)
        .orderBy('registDate', descending: true)
        .withConverter<Notice>(
            fromFirestore: (snapshots, _) => Notice.fromJson(snapshots.data()!),
            toFirestore: (notice, _) => notice.toJson());

    if (lastDocument == null) {
      // 처음 검색하는 경우.
      query = query.limit(documentLimit);
    } else {
      query = query.startAfterDocument(lastDocument!).limit(documentLimit);
    }

    // 쿼리 실행.
    try {
      var snapshots = await query
          .withConverter<Notice>(
              fromFirestore: (snapshots, _) => Notice.fromJson(snapshots.data()!),
              toFirestore: (notice, _) => notice.toJson())
          .get();
      print('공지사항 검색된 갯수: ${snapshots.docs.length}');

      if (snapshots.docs.length < documentLimit) {
        hasMore = false;
      }

      if (snapshots.docs.isNotEmpty) {
        lastDocument = snapshots.docs[snapshots.docs.length - 1];
        notices.addAll(snapshots.docs);
        print('공지사항 리스트 갯수: ${notices.length}');

        isLoading = false;
      }
    } catch (e) {
      print('복합 인덱스가 필요한지 보세요~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~');
      print('복합 인덱스가 필요한지 보세요~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~');
      print('복합 인덱스가 필요한지 보세요~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~');

      print(e);
    }
  }

  void clearProduct() {
    notices.clear();
    lastDocument = null;
    isLoading = false;
    hasMore = true;
  }
}
