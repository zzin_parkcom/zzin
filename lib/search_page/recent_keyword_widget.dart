import 'package:flutter/material.dart';
import 'package:zzin/common/hive/search_collection.dart';
import 'package:zzin/common/widget/zchip.dart';
import 'package:zzin/common/zconfig.dart';

class RecentKeywordWidget extends StatefulWidget {
  final TextEditingController controller;
  final void Function(String) onSearchProduct;
  const RecentKeywordWidget({Key? key, required this.controller, required this.onSearchProduct})
      : super(key: key);

  @override
  _RecentKeywordWidgetState createState() => _RecentKeywordWidgetState();
}

class _RecentKeywordWidgetState extends State<RecentKeywordWidget> {
  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            const Text(
              '최근 검색어',
              style: TextStyle(fontSize: 20),
            ),
            TextButton(
                style: ButtonStyle(
                    padding: MaterialStateProperty.all(const EdgeInsets.fromLTRB(0, 0, 0, 0))),
                onPressed: onDeleteRecentKeyword,
                child: const Text('모두 지우기', style: TextStyle(color: ZConfig.grey_text)))
          ],
        ),
        Wrap(
          spacing: 5,
          children: ZChip.generate(
              textList: SearchCollection.instance.recentKeyword,
              length: SearchCollection.instance.recentKeyword.length,
              onSelectedList: onRecentPressed()),
        ),
      ],
    );
  }

  void onDeleteRecentKeyword() {
    SearchCollection.instance.recentKeyword.clear();
    SearchCollection.instance.save();
    setState(() {});
  }

  List<ValueChanged<bool>> onRecentPressed() {
    return List.generate(SearchCollection.instance.recentKeyword.length, (index) {
      return (isSelected) {
        if (isSelected) {
          print('choiceChip : ${SearchCollection.instance.recentKeyword[index]}');
          widget.controller.text = SearchCollection.instance.recentKeyword[index];
          widget.onSearchProduct(SearchCollection.instance.recentKeyword[index]);
        }
      };
    });
  }
}
