import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:zzin/common/firestore/document_skima/opentalk_data.dart';
import 'package:zzin/opentalk_page/opentalk_chat_ui/opentalk_chat_ui.dart';

class JoinOpentalkView extends StatefulWidget {
  final OpentalkData opentalk;
  const JoinOpentalkView({required this.opentalk, Key? key}) : super(key: key);

  @override
  _JoinOpentalkViewState createState() => _JoinOpentalkViewState();
}

class _JoinOpentalkViewState extends State<JoinOpentalkView> {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        appBar: AppBar(),
        body: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              const Text('프로파일 생성'),
              TextButton(
                  onPressed: () {
                    Get.off(() => OpentalkChatUI(opentalk: widget.opentalk));
                  },
                  child: const Text('참가하기'))
            ],
          ),
        ),
      ),
    );
  }
}
