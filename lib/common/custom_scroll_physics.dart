import 'dart:math';

import 'package:flutter/material.dart';

class CustomSimulation extends Simulation {
  final double initPosition;
  final double velocity;

  CustomSimulation({required this.initPosition, required this.velocity});

  @override
  double x(double time) {
    var maxValue = max(min(initPosition, 0.0), initPosition + velocity * time);

    // print(max.toString());

    return maxValue;
  }

  @override
  double dx(double time) {
    // print(velocity.toString());
    // return velocity;
    var friction = 300;
    if (velocity >= 0) {
      return (velocity - friction) > 0 ? (velocity - friction) : 0;
    } else {
      return (velocity + friction) < 0 ? (velocity + friction) : 0;
    }
  }

  @override
  bool isDone(double time) {
    return false;
  }
}

class CustomScrollPhysics extends ScrollPhysics {
  @override
  ScrollPhysics applyTo(ScrollPhysics? ancestor) {
    return CustomScrollPhysics();
  }

  @override
  Simulation createBallisticSimulation(ScrollMetrics position, double velocity) {
    return CustomSimulation(
      initPosition: position.pixels,
      velocity: velocity,
    );
  }
}
