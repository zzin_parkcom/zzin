import 'package:flutter/material.dart';

class CustomFormField extends FormField<int> {
  const CustomFormField({
    Key? key,
    required builder,
    FormFieldSetter<int>? onSaved,
    FormFieldValidator<int>? validator,
    required int initialValue,
    bool enabled = true,
    AutovalidateMode autovalidateMode = AutovalidateMode.disabled,
  }) : super(
            onSaved: onSaved,
            validator: validator,
            initialValue: initialValue,
            autovalidateMode: autovalidateMode,
            enabled: enabled,
            builder: builder,
            key: key);
}
