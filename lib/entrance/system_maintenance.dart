import 'dart:io';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:zzin/common/widget/zbutton_blue.dart';
import 'package:zzin/common/zutils.dart';

class SystemMaintenanceBottomSheet extends StatelessWidget {
  final Timestamp startTimestamp;
  final Timestamp endTimestamp;
  final String description;
  final double sizeToRatio;
  const SystemMaintenanceBottomSheet(
      {required this.startTimestamp,
      required this.endTimestamp,
      required this.description,
      required this.sizeToRatio,
      Key? key})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    String startString = formatyMd(startTimestamp) + '  ' + formatHHMM(startTimestamp);
    String endString = formatyMd(endTimestamp) + '  ' + formatHHMM(endTimestamp);
    return Padding(
      padding: const EdgeInsets.all(10.0),
      child: SizedBox(
        height: Get.height * sizeToRatio,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: const [
                Icon(
                  Icons.report_problem,
                  size: 30,
                  color: Colors.red,
                ),
                Text(' 서비스 점검중',
                    style: TextStyle(fontWeight: FontWeight.bold, fontSize: 30, color: Colors.red))
              ],
            ),
            Padding(
              padding: const EdgeInsets.fromLTRB(40, 20, 40, 20),
              child: Text(description),
            ),
            Text(
              '점검시간: $startString ~ $endString',
              style: const TextStyle(fontSize: 12),
            ),
            ZButtonYellow(
              text: '종료',
              isSelected: true,
              onPressed: () => exit(1),
            ),
          ],
        ),
      ),
    );
  }
}
