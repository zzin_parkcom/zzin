import 'package:hive/hive.dart';
import 'package:zzin/common/hive/zhive.dart';

part 'forbidden_collection.g.dart';

@HiveType(typeId: 3)
class ForbiddenCollection extends HiveObject {
  @HiveField(0)
  String word;

  ForbiddenCollection({required this.word});

  @override
  String toString() {
    return word;
  }

  static Box<ForbiddenCollection> get to => ZHive.forbiddenBox;
}
