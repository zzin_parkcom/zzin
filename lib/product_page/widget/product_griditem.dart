import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:transparent_image/transparent_image.dart';
import 'package:zzin/common/firestore/document_skima/product.dart';
import 'package:zzin/common/hive/brand_collection.dart';
import 'package:zzin/common/zcache_manager.dart';
import 'package:zzin/common/zconfig.dart';
import 'package:zzin/common/zutils.dart';

class ProductGridItem extends StatelessWidget {
  const ProductGridItem({Key? key, required this.product, required this.tagHeroPrefix})
      : super(key: key);
  final Product product;
  final String tagHeroPrefix;
  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Align(
              alignment: Alignment.topCenter,
              child: ClipRRect(
                borderRadius: BorderRadius.circular(10),
                child: Hero(
                  tag: tagHeroPrefix + product.docId.toString(),
                  child: CachedNetworkImage(
                    fadeInDuration: const Duration(milliseconds: 0),
                    fadeOutDuration: const Duration(milliseconds: 0),
                    placeholderFadeInDuration: const Duration(milliseconds: 0),
                    placeholder: (_, __) => Image.memory(kTransparentImage),
                    imageUrl: product.thumbnail,
                    fit: BoxFit.contain,
                    errorWidget: (context, url, error) => const Icon(Icons.error),
                    cacheManager: ZCacheManager.instance,
                  ),
                ),
              ),
            ),
            gridFooter(product),
          ],
        ),
        product.likeCounts > 0
            ? Positioned(
                right: 5,
                bottom: 5,
                child: Row(
                  mainAxisSize: MainAxisSize.max,
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    Icon(Icons.favorite_outline, size: 14 * Get.mediaQuery.textScaleFactor),
                    Text(
                      '${product.likeCounts}',
                      overflow: TextOverflow.ellipsis,
                      style: const TextStyle(fontSize: 14.0, color: ZConfig.zzin_black),
                    ),
                  ],
                ),
              )
            : const SizedBox.shrink(),
      ],
    );
  }

  Widget gridFooter(Product product) {
    return Padding(
      padding: const EdgeInsets.fromLTRB(10, 5, 10, 0),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            product.title,
            maxLines: 1,
            overflow: TextOverflow.ellipsis,
            style: const TextStyle(fontSize: 15.0),
          ),
          Text(
            BrandCollection.getNameEn(product.brand),
            overflow: TextOverflow.ellipsis,
            style: TextStyle(fontSize: 13, color: Colors.grey[600]),
          ),
          Text(
            '${numberWithComma(product.price)}원',
            overflow: TextOverflow.ellipsis,
            style: const TextStyle(
              fontSize: 13.0,
              color: ZConfig.zzin_black,
            ),
          ),
        ],
      ),
    );
  }
}
