// import 'package:cloud_firestore/cloud_firestore.dart';
// import 'package:flutter/material.dart';
// import 'package:get/get.dart';
// import 'package:provider/provider.dart';
// import 'package:zzin/common/hive/address_collection.dart';
// import 'package:zzin/common/hive/brand_collection.dart';
// import 'package:zzin/upload_page/bottomsheet/bottomsheet_upload.dart';
// import 'package:zzin/common/bottomsheet_custom.dart';
// import 'package:zzin/common/firestore/query_product.dart';
// import 'package:zzin/common/zconfig.dart';
// import 'package:zzin/common/zutils.dart';
// import 'package:zzin/product_page/bottomsheet/filter_bottomsheet.dart';
// import 'package:zzin/product_page/bottomsheet/salearea_bottonsheet.dart';
// import 'package:zzin/product_page/bottomsheet/sort_bottomsheet.dart';

// class SearchFilterTab extends StatefulWidget {
//   @override
//   _SearchFilterTabState createState() => _SearchFilterTabState();
// }

// class _SearchFilterTabState extends State<SearchFilterTab> {
//   int idAddress = -1;
//   final _saleAreaString = ['반경 500m', '반경 2Km', '반경 10Km', '거래 범위 전체'];
//   int _areaSizeStep = 3;
//   int _indexSort = 0;
//   int _indexBrand = -1;
//   int _indexStatus = 0;
//   int _indexSaleStatus = 0;

//   @override
//   Widget build(BuildContext context) {
//     return Container(
//       padding: const EdgeInsets.symmetric(horizontal: 10),
//       child: Row(
//         mainAxisAlignment: MainAxisAlignment.spaceBetween,
//         children: [
//           Expanded(
//             flex: 6,
//             child: Row(children: [
//               OutlinedButton(
//                 style: ButtonStyle(
//                   shape: MaterialStateProperty.all<RoundedRectangleBorder>(
//                     RoundedRectangleBorder(
//                       borderRadius: BorderRadius.circular(18.0),
//                       side: BorderSide(color: Colors.black),
//                     ),
//                   ),
//                   foregroundColor: _areaSizeStep >= 3
//                       ? MaterialStateProperty.all<Color>(ZConfig.zzin_black)
//                       : MaterialStateProperty.all<Color>(ZConfig.zzin_white),
//                   backgroundColor: _areaSizeStep >= 3
//                       ? MaterialStateProperty.all<Color>(ZConfig.zzin_white)
//                       : MaterialStateProperty.all<Color>(ZConfig.zzin_pointColor),
//                 ),
//                 onPressed: onSalesArea,
//                 child: Text(
//                   _saleAreaString[_areaSizeStep],
//                   style: TextStyle(fontWeight: FontWeight.bold, fontSize: 14),
//                 ),
//               ),
//               SizedBox(
//                 width: 10,
//               ),
//               Flexible(
//                 child: OutlinedButton(
//                   style: ButtonStyle(
//                     shape: MaterialStateProperty.all<RoundedRectangleBorder>(
//                       RoundedRectangleBorder(
//                         borderRadius: BorderRadius.circular(18.0),
//                         side: BorderSide(color: Colors.black),
//                       ),
//                     ),
//                     foregroundColor: _indexBrand == -1
//                         ? MaterialStateProperty.all<Color>(ZConfig.zzin_black)
//                         : MaterialStateProperty.all<Color>(ZConfig.zzin_white),
//                     backgroundColor: _indexBrand == -1
//                         ? MaterialStateProperty.all<Color>(ZConfig.zzin_white)
//                         : MaterialStateProperty.all<Color>(ZConfig.zzin_pointColor),
//                   ),
//                   onPressed: onBrand,
//                   child: Text(
//                     _indexBrand == -1 ? '브랜드' : BrandCollection.getNameKr(_indexBrand),
//                     overflow: TextOverflow.fade,
//                     softWrap: false,
//                     style: TextStyle(fontWeight: FontWeight.bold, fontSize: 14),
//                   ),
//                 ),
//               ),
//             ]),
//           ),
//           Expanded(
//             flex: 4,
//             child: Row(
//               mainAxisAlignment: MainAxisAlignment.end,
//               children: [
//                 // 최신순
//                 GestureDetector(
//                   onTap: onSort,
//                   child: Row(
//                     children: [
//                       Text(
//                         ZConfig.sortNameList[_indexSort],
//                         style: TextStyle(
//                           color: ZConfig.zzin_black,
//                           fontWeight: FontWeight.bold,
//                           fontSize: 14,
//                         ),
//                       ),
//                       Icon(Icons.keyboard_arrow_down, size: 19),
//                     ],
//                   ),
//                 ),
//                 SizedBox(
//                   height: 15,
//                   width: 20,
//                   child: VerticalDivider(
//                     color: ZConfig.grey_border,
//                     width: 2,
//                     thickness: 2,
//                   ),
//                 ),
//                 GestureDetector(
//                   onTap: onFilter,
//                   child: Row(
//                     children: [
//                       Text(
//                         '필터',
//                         style: TextStyle(
//                             fontWeight: FontWeight.bold,
//                             fontSize: 14,
//                             color: (_indexStatus != 0 || _indexSaleStatus != 0)
//                                 ? ZConfig.zzin_pointColor
//                                 : ZConfig.zzin_black),
//                       ),
//                       Icon(
//                         Icons.add,
//                         size: 19,
//                         color: (_indexStatus != 0 || _indexSaleStatus != 0)
//                             ? ZConfig.zzin_pointColor
//                             : ZConfig.zzin_black,
//                       ),
//                     ],
//                   ),
//                 ),
//               ],
//             ),
//           ),
//         ],
//       ),
//     );
//   }

//   // 거래지역 //////////////////////////////////////////////////////////////////////////////////////
//   void onSalesArea() async {
//     idAddress = await showModalBottomSheetInt(
//         context, (_) => BottomSheetCustom(contents: SalesAreaBottomSheet(), sizeRatio: 0.55));
//     print(idAddress);

//     setState(() {
//       idAddress;
//     });

//     // var choice = ZConfig.pref.getDouble('areaSizeStep_Search') as double;
//     var choice = AppBar().preferredSize.height;
//     _areaSizeStep = choice.toInt();
//     setState(() {
//       _areaSizeStep;
//     });

//     if (idAddress == NotSelected) {
//       return;
//     } else if (idAddress == AllItems || _areaSizeStep == 3) {
//       var storeProducts = Provider.of<QueryProduct>(context, listen: false);
//       await storeProducts.queryProductByLocation(-1, -1);
//     } else {
//       // 0: 500m    // 1: 2Km    // 2: 10km    // 3: 50km+
//       var radius = 3250.0 * _areaSizeStep * _areaSizeStep - 1750 * _areaSizeStep + 500;

//       // 0: 500m    // 1: 2Km    // 2: 5km    // 3: 50km+
//       // var radius = 750.0 * _areaSizeStep * _areaSizeStep + 750 * _areaSizeStep + 500;

//       // // 검색 범위 설정.
//       var deltaGeoPoint =
//           GeoPoint(radius * ZConfig.latitudePerMeter, radius * ZConfig.longitudePerMeter);

//       var address = AddressCollection.getAddressByRegionCode(idAddress);

//       var userGeoPoint = GeoPoint(address.latitude, address.longitude);

//       var lesser = GeoPoint(userGeoPoint.latitude - deltaGeoPoint.latitude,
//           userGeoPoint.longitude - deltaGeoPoint.longitude);
//       var greater = GeoPoint(userGeoPoint.latitude + deltaGeoPoint.latitude,
//           userGeoPoint.longitude + deltaGeoPoint.longitude);

//       var resultList = AddressCollection.to.values.where((e) =>
//           e.latitude > lesser.latitude &&
//           e.latitude < greater.latitude &&
//           e.longitude > lesser.longitude &&
//           e.longitude < greater.longitude);

//       var locationList;
//       var scope = 0;
//       var regionCodes = resultList.map<int>((e) => e.regionCode).toSet();
//       print(regionCodes.length);
//       print('동 리스트: $regionCodes');
//       if (regionCodes.length > 10) {
//         scope++;
//         var sigunguIds = regionCodes.map<int>((e) => e ~/ 100000).toSet();
//         print(sigunguIds.length);
//         print('시군구 리스트: $sigunguIds');
//         if (sigunguIds.length > 10) {
//           scope++;
//           var sidoIds = sigunguIds.map<int>((e) => e ~/ 1000).toSet();
//           print(sidoIds.length);
//           print('시도 리스트: $sidoIds');
//           if (sidoIds.length > 10) {
//             locationList = null;
//             scope = -1;
//           } else {
//             locationList = sidoIds.toList();
//           }
//         } else {
//           locationList = sigunguIds.toList();
//         }
//       } else {
//         locationList = regionCodes.toList();
//       }

//       var storeProducts = Provider.of<QueryProduct>(context, listen: false);
//       await storeProducts.queryProductByLocation(scope, locationList);
//     }
//   }

//   // 브랜드 ////////////////////////////////////////////////////////////////////////////////////////
//   void onBrand() async {
//     var retIndex = await showModalBottomSheetInt(
//         context,
//         (_) => BottomSheetUpload('브랜드', '전체보기', AllItems, BrandCollection.to.length,
//             BrandCollection.to.values.map((e) => e.nameKr + '|' + e.nameEn).toList(), true,
//             height: (Get.height - AppBar().preferredSize.height) / Get.height));

//     if (retIndex == -1) return;

//     retIndex = (retIndex == AllItems ? -1 : retIndex);

//     if (_indexBrand == retIndex) return; // 이전과 동일한 검색이므로

//     setState(() {
//       _indexBrand = retIndex;
//     });

//     print(_indexBrand);

//     var storeProducts = Provider.of<QueryProduct>(context, listen: false);

//     await storeProducts.queryProductByBrand(_indexBrand);
//   }

//   // 정렬 //////////////////////////////////////////////////////////////////////////////////////////
//   void onSort() async {
//     var retValue = await showModalBottomSheetInt(
//         context, (_) => BottomSheetCustom(contents: SortBottomSheet(_indexSort), sizeRatio: 0.28));

//     if (retValue == -1) return;
//     setState(() {
//       _indexSort = retValue;
//     });

//     var storeProducts = Provider.of<QueryProduct>(context, listen: false);
//     await storeProducts.queryProductBySort(_indexSort);

//     print(_indexSort);
//   }

//   // 필터 처리//////////////////////////////////////////////////////////////////////////////////////
//   void onFilter() async {
//     var retValue = await showModalBottomSheetInt(
//         context,
//         (_) => BottomSheetCustom(
//             contents: FilterBottomSheet(_indexStatus, _indexSaleStatus), sizeRatio: 0.45));

//     if (retValue == -1) return;
//     // 0 전체
//     // 1 새제품
//     // 2 중고

//     // 0 거래완료 포함
//     // 1 거래완료 제외

//     var retStatus;
//     var retSaleStatus;

//     if (retValue == AllItems) {
//       retStatus = 0;
//       retSaleStatus = 0;
//     } else {
//       retSaleStatus = retValue & 1;
//       retStatus = retValue >> 2;
//     }

//     if (_indexStatus == retStatus && _indexSaleStatus == retSaleStatus) return;

//     setState(() {
//       _indexStatus = retStatus;
//       _indexSaleStatus = retSaleStatus;
//     });

//     // Status                 UI                DB
//     // 전체, 새제품, 중고 : (0 | 1 | 2,) -> (-1 | 0 | 2)
//     // SaleStatus                UI           DB               DB`
//     // 판매중, 판매완료, 예약중 (0 | 1) -> ( 0,1,2 | 0 ) -> (-1 | 0)

//     var storeProducts = Provider.of<QueryProduct>(context, listen: false);
//     await storeProducts.queryProductByFilter(_indexStatus - 1, _indexSaleStatus - 1);
//   }
// }
