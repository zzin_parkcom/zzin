import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:get/get.dart';
import 'package:zzin/common/firestore/document_skima/product.dart';
import 'package:zzin/common/zconfig.dart';

class QueryFavorite extends GetxController {
  List<DocumentSnapshot<Product>> products = <DocumentSnapshot<Product>>[].obs;
  DocumentSnapshot? lastDocument;
  bool isLoading = false; // track if products fetching
  bool hasMore = true; // flag for more products available or not
  int documentLimit = ZConfig.maxDocumentCount; // documents to

  int _startIndex = 0;
  int _endIndex = 0;

  static QueryFavorite get to => Get.find();

  List makeFavoriteList() {
    if (_startIndex == 0 && _endIndex == 0) {
      if (ZConfig.userAccount.likeList.length > ZConfig.maxDocumentCount) {
        _endIndex = ZConfig.maxDocumentCount;
      } else {
        _endIndex = ZConfig.userAccount.likeList.length;
      }
    } else {
      if (ZConfig.userAccount.likeList.length > ZConfig.maxDocumentCount &&
          ZConfig.userAccount.likeList.length > _endIndex) {
        if (ZConfig.userAccount.likeList.length > _endIndex + ZConfig.maxDocumentCount) {
          _startIndex += ZConfig.maxDocumentCount;
          _endIndex += ZConfig.maxDocumentCount;
        } else {
          _startIndex += ZConfig.maxDocumentCount;
          _endIndex += ZConfig.userAccount.likeList.length - _endIndex;
        }
      } else {
        return List.empty();
      }
    }

    var likeList = ZConfig.userAccount.likeList.getRange(_startIndex, _endIndex).toList();
    // likeList.forEach((element) {
    //   print(element.toString());
    // });
    return likeList;
  }

  Future<bool> queryFavorite({bool byScroll = false}) async {
    if (!byScroll) {
      clearProduct();
    }
    if (!hasMore) {
      // print('더이상 상품이 없습니다.');
      return false;
    }
    if (isLoading) {
      // print('쿼리중... ');
      return false;
    }
    isLoading = true;

    var likeList = [];
    likeList = makeFavoriteList();
    if (likeList.isEmpty) {
      print('더이상 상품이 없습니다.');
      return false;
    }

    Query query = FirebaseFirestore.instance
        .collection(ZConfig.productsCollection)
        .where('articleStatus', isEqualTo: 0)
        .where('docId', whereIn: likeList.toList())
        .withConverter<Product>(
            fromFirestore: (snapshots, _) => Product.fromJson(snapshots.data()!),
            toFirestore: (product, _) => product.toJson());

    // 쿼리 실행.
    try {
      var snapshots = await query
          .withConverter<Product>(
              fromFirestore: (snapshots, _) => Product.fromJson(snapshots.data()!),
              toFirestore: (product, _) => product.toJson())
          .get();
      print('Favorite 검색된 갯수: ${snapshots.docs.length}');

      if (snapshots.docs.length < documentLimit) {
        hasMore = false;
      }

      if (snapshots.docs.isNotEmpty) {
        lastDocument = snapshots.docs[snapshots.docs.length - 1];

        // 정렬을 위해서.
        var reversedLikeList = likeList.reversed.toList();
        var tempProducts = snapshots.docs;
        for (var i = 0; i < reversedLikeList.length; i++) {
          for (var element in tempProducts) {
            if (element.data().docId == reversedLikeList[i]) {
              products.add(element);

              if (ZConfig.userAccount.isRegistered()) {
                products.removeWhere((product) => ZConfig.blockUsers
                    .any((element) => element.userDocId == product.data()!.sellerId));
              }
            }
          }
        }
        // products.addAll(snapshots.docs);
        print('Favorite 상품리스트 갯수: ${products.length}');

        isLoading = false;

        if (products.length < documentLimit && hasMore) {
          queryFavorite(byScroll: true);
        }
        return true;
      }
    } catch (e) {
      print('복합 인덱스가 필요한지 보세요~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~');
      print('복합 인덱스가 필요한지 보세요~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~');
      print('복합 인덱스가 필요한지 보세요~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~');

      print(e);

      return false;
    }

    return false;
  }

  void clearProduct() {
    products.clear();
    lastDocument = null;
    isLoading = false;
    hasMore = true;
    _startIndex = 0;
    _endIndex = 0;
  }
}
