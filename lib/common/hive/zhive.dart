import 'dart:convert';
import 'package:flutter/services.dart';
import 'package:hive/hive.dart';
import 'package:zzin/common/hive/address_collection.dart';
import 'package:zzin/common/hive/brand_collection.dart';
import 'package:zzin/common/hive/category_collection.dart';
import 'package:zzin/common/hive/forbidden_collection.dart';
import 'package:zzin/common/hive/search_collection.dart';
import 'package:zzin/common/hive/upload_collection.dart';
import 'package:zzin/common/utils/zenum.dart';
import 'package:zzin/common/zconfig.dart';

class ZHive {
  static late Box<BrandCollection> brandBox;
  static late Box<AddressCollection> addressBox;
  static late Box<ForbiddenCollection> forbiddenBox;
  static late Box<CategoryCollection> categoryBox;
  static late Box<UploadCollection> uploadBox;
  static late Box<SearchCollection> searchBox;

  static late Box generalBox;

  static String brandBoxName = 'brand';
  static String addressBoxName = 'address';
  static String forbiddenBoxName = 'forbidden';
  static String categoryBoxName = 'category';
  static String uploadBoxName = 'upload';
  static String searchBoxName = 'search';

  static String generalBoxName = 'general';

  static Box get to => generalBox;

  static Future<void> initialize() async {
    await _initializeBrand();
    await _initializeAddress();
    await _initializeForbidden();
    await _initializeCategory();
    await _initializeUpload();
    await _initializeGeneral();
    await _initializeSearch();
  }

  static Future<void> clearHive() async {
    await _clearBrand();
    await _clearAddress();
    await _clearForbidden();
    await _clearCategory();
    await _clearUpload();
    await _clearGeneral();
    await _clearSearch();
  }

////////////////////////////////////////////////////////////////////////////////////////////////////
  static Future<void> _initializeBrand() async {
    Hive.registerAdapter(BrandCollectionAdapter());
    brandBox = await Hive.openBox<BrandCollection>(brandBoxName);

    if (brandBox.isEmpty) {
      final string = await rootBundle.loadString('assets/json/brand.json');

      final data = jsonDecode(string);
      var brandJson = data['brand'];
      brandJson.forEach((e) {
        brandBox.add(BrandCollection(id: e['id'], nameKr: e['nameKr'], nameEn: e['nameEn']));
      });

      for (var index = 0; index < brandBox.length; index++) {
        var result = brandBox.getAt(index);
        print(result.toString());
      }
    }
    print('$brandBoxName Size:  ${brandBox.length}');
  }

  static Future<void> _clearBrand() async {
    await Hive.deleteBoxFromDisk(brandBoxName);
  }

////////////////////////////////////////////////////////////////////////////////////////////////////
  static Future<void> _initializeAddress() async {
    Hive.registerAdapter(AddressCollectionAdapter());
    addressBox = await Hive.openBox<AddressCollection>(addressBoxName);
    if (addressBox.isEmpty) {
      final string = await rootBundle.loadString('assets/json/address_table.json');

      final data = jsonDecode(string);
      var addressJon = data['AddressTable'];
      addressJon.forEach((e) {
        addressBox.add(AddressCollection(
            regionCode: e['regionCode'],
            regionName: e['regionName'],
            latitude: e['latitude'],
            longitude: e['longitude']));
      });

      for (var index = 0; index < addressBox.length; index++) {
        print(addressBox.getAt(index));
      }
    }
    print('$addressBoxName Size: ${addressBox.length}');
  }

  static Future<void> _clearAddress() async {
    await Hive.deleteBoxFromDisk(addressBoxName);
  }

////////////////////////////////////////////////////////////////////////////////////////////////////
  static Future<void> _initializeForbidden() async {
    Hive.registerAdapter(ForbiddenCollectionAdapter());
    forbiddenBox = await Hive.openBox<ForbiddenCollection>(forbiddenBoxName);

    if (forbiddenBox.isEmpty) {
      final string = await rootBundle.loadString('assets/json/forbidden.json');

      final data = jsonDecode(string);
      var addressJon = data;
      addressJon.forEach((e) {
        forbiddenBox.add(ForbiddenCollection(word: e));
      });

      for (var index = 0; index < forbiddenBox.length; index++) {
        print(forbiddenBox.getAt(index));
      }
    }
    print('$forbiddenBoxName Size: ${forbiddenBox.length}');
  }

  static Future<void> _clearForbidden() async {
    await Hive.deleteBoxFromDisk(forbiddenBoxName);
  }

  //////////////////////////////////////////////////////////////////////////////////////////////////
  static Future<void> _initializeCategory() async {
    Hive.registerAdapter(CategoryCollectionAdapter());
    categoryBox = await Hive.openBox<CategoryCollection>(categoryBoxName);

    if (categoryBox.isEmpty) {
      await categoryBox.add(CategoryCollection(id: 0, nameKr: '가방', nameEn: 'BAGS'));
      await categoryBox.add(CategoryCollection(id: 1, nameKr: '의류', nameEn: 'CLOTHES'));
      await categoryBox.add(CategoryCollection(id: 2, nameKr: '신발', nameEn: 'SHOES'));
      await categoryBox.add(CategoryCollection(id: 3, nameKr: '시계', nameEn: 'WATCHES'));
      await categoryBox.add(CategoryCollection(id: 4, nameKr: '기타', nameEn: 'ACCESSORIES'));
      for (var index = 0; index < categoryBox.length; index++) {
        print(categoryBox.getAt(index));
      }
    }

    print('$categoryBoxName Size: ${categoryBox.length}');
  }

  static Future<void> _clearCategory() async {
    await Hive.deleteBoxFromDisk(categoryBoxName);
  }

  //////////////////////////////////////////////////////////////////////////////////////////////////
  static Future<void> _initializeUpload() async {
    Hive.registerAdapter(UploadCollectionAdapter());
    uploadBox = await Hive.openBox<UploadCollection>(uploadBoxName);

    if (uploadBox.isEmpty) {
      await uploadBox.put(
        0,
        UploadCollection(
          brandId: NotSelected,
          brandName: '브랜드 선택',
          categoryId: NotSelected,
          categoryName: '카테고리 선택',
          description: '',
          price: 0,
          saleAreaId: NotSelected,
          saleAreaName: '거래지역 선택',
          status: ProductStatus.NEW.index,
          title: '',
          mainImageIndex: 0,
          images: [],
          imagesCache: [],
          imagesId: [],
        ),
      );

      // For Modify
      await uploadBox.put(
          1,
          UploadCollection(
            brandId: NotSelected,
            brandName: '브랜드 선택',
            categoryId: NotSelected,
            categoryName: '카테고리 선택',
            description: '',
            price: 0,
            saleAreaId: NotSelected,
            saleAreaName: '거래지역 선택',
            status: ProductStatus.NEW.index,
            title: '',
            mainImageIndex: 0,
            images: [],
            imagesCache: [],
            imagesId: [],
          ));

      for (var index = 0; index < uploadBox.length; index++) {
        print(uploadBox.getAt(index));
      }
    }

    print('$uploadBoxName Size: ${uploadBox.length}');
  }

  static Future<void> _clearUpload() async {
    await Hive.deleteBoxFromDisk(uploadBoxName);
  }

  //////////////////////////////////////////////////////////////////////////////////////////////////
  static Future<void> _initializeGeneral() async {
    generalBox = await Hive.openBox(generalBoxName);
    print('$generalBoxName Size: ${generalBox.length}');
  }

  static Future<void> _clearGeneral() async {
    await Hive.deleteBoxFromDisk(generalBoxName);
  }
  //////////////////////////////////////////////////////////////////////////////////////////////////

  static Future<void> _initializeSearch() async {
    Hive.registerAdapter(SearchCollectionAdapter());
    searchBox = await Hive.openBox<SearchCollection>(searchBoxName);

    if (searchBox.isEmpty) {
      await searchBox.add(SearchCollection(
        searchString: '',
        brandId: NotSelected,
        brandName: '브랜드 선택',
        categoryId: NotSelected,
        categoryName: '카테고리 선택',
        addressId: NotSelected,
        addressStep: NotSelected,
        addressName: '검색지역 선택',
        status: AllItems,
        saleStatus: AllItems,
        recentKeyword: [],
        sort: 0, // 정확도 검색 기본 설정.
      ));

      for (var index = 0; index < searchBox.length; index++) {
        print(searchBox.getAt(index));
      }
    }

    print('$searchBoxName Size: ${searchBox.length}');
  }

  static Future<void> _clearSearch() async {
    await Hive.deleteBoxFromDisk(searchBoxName);
  }
}
