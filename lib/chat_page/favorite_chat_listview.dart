import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:zzin/common/firestore/qeury_favorite.dart';
import 'package:zzin/common/zconfig.dart';
import 'package:zzin/product_page/widget/product_list_generator.dart';

class FavoriteChatListView extends StatefulWidget {
  const FavoriteChatListView({Key? key}) : super(key: key);

  @override
  _FavoriteChatListViewState createState() => _FavoriteChatListViewState();
}

class _FavoriteChatListViewState extends State<FavoriteChatListView> {
  final QueryFavorite queryFavorite = QueryFavorite();
  final ScrollController _scrollController = ScrollController();

  @override
  void initState() {
    super.initState();
    _scrollController.addListener(_scrollListener);
    SchedulerBinding.instance!.addPostFrameCallback((_) => frameCallback(context));
  }

  @override
  void dispose() {
    _scrollController.removeListener(_scrollListener);
    super.dispose();
  }

  void frameCallback(BuildContext context) async {
    await queryFavorite.queryFavorite();
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return ProductListGenerator(
        products: queryFavorite.products,
        onRefresh: onRefresh,
        scrollController: _scrollController,
        onDetailPage: null,
        tagHeroPrefix: 'Favorite_Chat_',
        noProductMessages: const ['대화중인 상대가 없습니다.']);
  }

  Future onRefresh() async {
    queryFavorite.clearProduct();
    await queryFavorite.queryFavorite();
  }

  void _scrollListener() async {
    var maxScroll = _scrollController.position.maxScrollExtent;
    var currentScroll = _scrollController.position.pixels;
    var delta = maxScroll * ZConfig.scrollRatioQueryMore;

    if (maxScroll - currentScroll <= delta && queryFavorite.isLoading == false) {
      var result = await queryFavorite.queryFavorite(byScroll: true);
      if (result == true) {
        setState(() {});
      }
    }
  }
}
