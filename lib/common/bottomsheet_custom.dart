import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class BottomSheetCustom extends StatefulWidget {
  final double sizeRatio;
  final Widget contents;
  const BottomSheetCustom({required this.contents, this.sizeRatio = 0.4, Key? key})
      : super(key: key);
  @override
  _BottomSheetCustomState createState() => _BottomSheetCustomState();
}

class _BottomSheetCustomState extends State<BottomSheetCustom> {
  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: MediaQuery.of(context).size.height * widget.sizeRatio,
      child: Column(
        children: [
          widget.contents,
        ],
      ),
    );
  }
}
