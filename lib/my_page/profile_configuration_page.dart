import 'dart:io';
import 'dart:typed_data';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:image_picker/image_picker.dart';
import 'package:path_provider/path_provider.dart';
import 'package:transparent_image/transparent_image.dart';
import 'package:zzin/common/hive/brand_collection.dart';
import 'package:zzin/common/hive/forbidden_collection.dart';
import 'package:zzin/common/utils/make_thumbnail.dart';
import 'package:zzin/common/widget/zbutton_blue.dart';
import 'package:zzin/common/widget/zbutton_white.dart';
import 'package:zzin/common/zconfig.dart';

class ProfileConfigurationPage extends StatefulWidget {
  const ProfileConfigurationPage({Key? key}) : super(key: key);

  @override
  _ProfileConfigurationPageState createState() => _ProfileConfigurationPageState();
}

class _ProfileConfigurationPageState extends State<ProfileConfigurationPage> {
  final TextEditingController _controller = TextEditingController();
  final FocusNode _node = FocusNode();

  String profileImagePath = '';
  File? profileFile;
  Uint8List? bytesProfile;
  MakeThumbnail? thumbnailMaker;

  bool isChangeProfile = false;
  bool isChangeNickname = false;
  bool enableEdit = false;
  bool enableDuplicateButton = false;
  String hintText = '';

  bool isCheckRequired = false;
  bool isNicknameChecked = false;
  Color errorColor = Colors.red;
  String errorMessage = '';
  @override
  void initState() {
    super.initState();

    var today = Timestamp.now().toDate();
    var diff = today.difference(ZConfig.userAccount.nicknameChangeDate!.toDate());
    var thirty = const Duration(days: 30);

    if (diff.compareTo(thirty) > 0) {
      enableEdit = true;
      enableDuplicateButton = false;
      hintText = ZConfig.userAccount.nickname ?? '';
      errorMessage = '닉네임 변경시, 30일 동안 변경 불가 합니다.';
      errorColor = ZConfig.grey_text;
    } else {
      enableEdit = false;
      enableDuplicateButton = false;
      hintText = ZConfig.userAccount.nickname!;
      errorMessage = '닉네임 변경은 ${30 - diff.inDays}일 이후 가능합니다.';
      errorColor = Colors.red;
    }

    SchedulerBinding.instance!.addPostFrameCallback((_) => frameCallback(context));
  }

  void frameCallback(BuildContext context) async {
    profileImagePath = ZConfig.userAccount.getThumbnailPath();
    if (isNetworkImage(profileImagePath)) {
      bytesProfile = (await NetworkAssetBundle(Uri.parse(profileImagePath)).load(profileImagePath))
          .buffer
          .asUint8List();

      setState(() {});
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: AppBar(
        title: const Text('프로필 설정', style: TextStyle(fontSize: ZSize.appBarTextSize)),
      ),
      body: SafeArea(
        child: Padding(
          padding: const EdgeInsets.fromLTRB(20, 40, 20, 40),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Column(
                children: [
                  profileSection(),
                  // ignore: prefer_const_constructors
                  SizedBox(height: 10),
                  nicknameSection(),
                ],
              ),
            ],
          ),
        ),
      ),
      bottomNavigationBar: submitSection(),
    );
  }

  Widget profileSection() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            profileImage(),
            ZButtonWhite(
              padding: MaterialStateProperty.all(const EdgeInsets.fromLTRB(5, 5, 5, 5)),
              fontSize: 12,
              text: '사진 선택',
              isSelected: true,
              onPressed: onChangeProfileImage,
            )
          ],
        ),
      ],
    );
  }

  Widget nicknameSection() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Row(
          children: [
            Expanded(
              flex: 6,
              child: TextField(
                enabled: enableEdit,
                textInputAction: TextInputAction.none,
                focusNode: _node,
                controller: _controller,
                decoration: InputDecoration(
                  hintText: hintText,
                  border: InputBorder.none,
                  enabledBorder: InputBorder.none,
                  focusedBorder: InputBorder.none,
                  disabledBorder: InputBorder.none,
                  fillColor: ZConfig.grey_border,
                  filled: true,
                ),
                onChanged: (_) {
                  isNicknameChecked = false;
                  setErrorMessage('', Colors.red);
                  setState(() {});
                },
              ),
            ),
            Expanded(
              flex: 2,
              child: ColoredBox(
                color: ZConfig.grey_border,
                child: Padding(
                  padding: const EdgeInsets.only(right: 5),
                  child: ZButtonYellow(
                    text: '중복확인',
                    isSelected: _controller.text.isNotEmpty,
                    // borderRaius: 20,
                    padding: MaterialStateProperty.all(const EdgeInsets.all(0.0)),
                    onPressed: enableEdit
                        ? () {
                            checkNickname();
                          }
                        : null,
                  ),
                ),
              ),
            ),
          ],
        ),
        Padding(
          padding: const EdgeInsets.fromLTRB(15, 10, 0, 0),
          child: Text(
            errorMessage,
            style: TextStyle(fontSize: 12, color: errorColor),
          ),
        ),
      ],
    );
  }

  bool isProcessing = false;
  Widget submitSection() {
    return Padding(
      padding: ZSize.bottomNavigationBarButtonOutPadding,
      child: ZButtonYellow(
        text: '저장하기',
        isSelected: isNicknameChecked && mounted || isChangeProfile,
        fontSize: ZSize.BNB_ButtonFontSize,
        padding: ZSize.bottomNavigationBarButtonInPadding,
        onPressed: (isChangeNickname || isChangeProfile) && !isProcessing
            ? () async {
                if (isChangeProfile || isChangeNickname) {
                  var _isChangeProfile = isChangeProfile;
                  var _isChangeNickname = isChangeNickname;
                  isChangeNickname = false;
                  isChangeProfile = false;
                  setState(() {
                    isProcessing = true;
                  });

                  if (_isChangeProfile) {
                    profileFile = await thumbnailMaker!.makeFile(bytesProfile!);

                    var path =
                        '${ZConfig.userAccount.docId!.path}/${profileImagePath.split('/').last}';
                    var taskUpload = FirebaseStorage.instance.ref(path).putFile(profileFile!);
                    await taskUpload;
                    print('upload complete : ${profileFile!.path}');

                    // 썸네일 이미지 URL 얻기.
                    var taskGetURL = FirebaseStorage.instance.ref(path).getDownloadURL();
                    var thumbnail = await taskGetURL;

                    await ZConfig.userAccount.docId!.update({'profileImage': thumbnail});
                    ZConfig.userAccount.profileImage = thumbnail;
                    isChangeProfile = false;
                    print(thumbnail);
                  }

                  if (_isChangeNickname) {
                    var now = Timestamp.now();
                    await ZConfig.userAccount.docId!
                        .update({'nickname': _controller.text.trim(), 'nicknameChangeDate': now});
                    ZConfig.userAccount.nickname = _controller.text.trim();
                    ZConfig.userAccount.nicknameChangeDate = now;

                    print('닉네임이 변경되었습니다.');
                    hintText = _controller.text.trim();
                    isNicknameChecked = false;
                  }
                }

                Get.back();
                Get.rawSnackbar(message: '변경되었습니다.');
              }
            : null,
      ),
    );
  }

  Future onChange() async {}

  ///////////////////////////////////////////////////////////////////

  void setErrorMessage(String message, Color color) {
    errorMessage = message;
    errorColor = color;
    setState(() {});
  }

  void checkNickname() async {
    var inputString = _controller.text.trim();
    if (inputString.length < 2) {
      setErrorMessage('닉네임은 2자 이상 입력해 주세요.', Colors.red);
      return;
    }
    if (inputString.length > 10) {
      setErrorMessage('닉네임은 10자 이하로 입력해 주세요.', Colors.red);
      return;
    }

    if (!ZConfig.regExpNickname.hasMatch(inputString)) {
      setErrorMessage('닉네임은 공백없이 한글, 영문, 숫자만 가능합니다.', Colors.red);
      return;
    }

    if (inputString.contains('찐마켓') || inputString.toLowerCase().contains('zzinmarket')) {
      setErrorMessage('사용할 수 없는 닉네임 입니다.', Colors.red);
      return;
    }

    if (ForbiddenCollection.to.values
        .any((forbidden) => forbidden.word == inputString.toLowerCase())) {
      setErrorMessage('사용할 수 없는 닉네임 입니다.', Colors.red);
      return;
    }

    if (BrandCollection.to.values.any((brand) {
      if (brand.nameKr.contains(inputString.toLowerCase()) ||
          brand.nameEn.contains(inputString.toLowerCase())) {
        return true;
      } else {
        return false;
      }
    })) {
      setErrorMessage('사용할 수 없는 닉네임 입니다.', Colors.red);
      return;
    }

    var users = FirebaseFirestore.instance.collection(ZConfig.userCollection);

    await users.where('nickname', isEqualTo: inputString).get().then((value) {
      if (value.size > 0) {
        setErrorMessage('이미 사용중인 닉네임 입니다.', Colors.red);
      } else {
        ZConfig.userAccount.nickname = inputString;
        isNicknameChecked = true;
        isChangeNickname = true;
        setErrorMessage('사용 가능한 닉네임입니다.', ZConfig.zzin_pointColor);
        _node.unfocus();
      }
    });
  }

  void onChangeProfileImage() async {
    final picker = ImagePicker();
    var pickedFile = await picker.pickImage(source: ImageSource.gallery);
    if (pickedFile != null) {
      var temporaryDirectory = await getApplicationDocumentsDirectory();
      var targetFilePath = temporaryDirectory.path + '/profileImage.jpg';

      thumbnailMaker =
          MakeThumbnail(originFilePath: pickedFile.path, targetFilePath: targetFilePath);

      var result = await thumbnailMaker!.makeMemory();
      bytesProfile = Uint8List.fromList(result);

      profileImagePath = targetFilePath;
      isChangeProfile = true;

      setState(() {});
    }
  }

  bool isNetworkImage(String path) {
    return path.startsWith('http');
  }

  Widget profileImage() {
    return CircleAvatar(
        radius: 50,
        backgroundImage:
            bytesProfile != null ? MemoryImage(bytesProfile!) : MemoryImage(kTransparentImage));
  }
}
