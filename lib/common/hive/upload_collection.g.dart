// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'upload_collection.dart';

// **************************************************************************
// TypeAdapterGenerator
// **************************************************************************

class UploadCollectionAdapter extends TypeAdapter<UploadCollection> {
  @override
  final int typeId = 5;

  @override
  UploadCollection read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return UploadCollection(
      brandId: fields[0] as int,
      brandName: fields[1] as String,
      categoryId: fields[2] as int,
      categoryName: fields[3] as String,
      description: fields[4] as String,
      price: fields[5] as int,
      saleAreaId: fields[6] as int,
      saleAreaName: fields[7] as String,
      status: fields[8] as int,
      title: fields[9] as String,
      mainImageIndex: fields[10] as int,
      images: (fields[11] as List).cast<String>(),
      imagesCache: (fields[12] as List).cast<String>(),
      imagesId: (fields[13] as List).cast<String>(),
    );
  }

  @override
  void write(BinaryWriter writer, UploadCollection obj) {
    writer
      ..writeByte(14)
      ..writeByte(0)
      ..write(obj.brandId)
      ..writeByte(1)
      ..write(obj.brandName)
      ..writeByte(2)
      ..write(obj.categoryId)
      ..writeByte(3)
      ..write(obj.categoryName)
      ..writeByte(4)
      ..write(obj.description)
      ..writeByte(5)
      ..write(obj.price)
      ..writeByte(6)
      ..write(obj.saleAreaId)
      ..writeByte(7)
      ..write(obj.saleAreaName)
      ..writeByte(8)
      ..write(obj.status)
      ..writeByte(9)
      ..write(obj.title)
      ..writeByte(10)
      ..write(obj.mainImageIndex)
      ..writeByte(11)
      ..write(obj.images)
      ..writeByte(12)
      ..write(obj.imagesCache)
      ..writeByte(13)
      ..write(obj.imagesId);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is UploadCollectionAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}
