import 'package:flutter/material.dart';
import 'package:zzin/common/zconfig.dart';
import 'package:zzin/common/zutils.dart';
import 'package:zzin/upload_page/bottomsheet/tip_bottomsheet.dart';

class TipSelection extends StatelessWidget {
  const TipSelection({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListTile(
      title: const Text(
        '사진 선택시 대표 사진으로 설정가능합니다',
        overflow: TextOverflow.ellipsis,
        style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold, color: ZConfig.grey_text),
      ),
      trailing: SizedBox(
        height: 25,
        width: 40,
        child: TextButton(
          style: ButtonStyle(
              padding: MaterialStateProperty.all(const EdgeInsets.fromLTRB(0, 0, 0, 0)),
              shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(30.0),
                  side: const BorderSide(color: ZConfig.grey_border),
                ),
              ),
              backgroundColor: MaterialStateProperty.all(ZConfig.grey_border)),
          onPressed: () {
            showModalBottomSheetInt(context, _tipBottomSheetContent);
          },
          child: const Text(
            'TIP',
            style: TextStyle(
              fontSize: 14,
            ),
          ),
        ),
      ),
      onTap: () {
        showModalBottomSheetInt(context, _tipBottomSheetContent);
      },
    );
  }

  Widget _tipBottomSheetContent(BuildContext context) {
    var title = '정품 신뢰도 높이는 Tip';
    var subTitle = '판매하시는 상품의 정품 신뢰도를 높이기 위해 다음 사진도 첨부해 보세요.';
    var itemList = ['1. 명품보증서', '2. 구매영수증', '3. 상표 라벨과 시리얼 넘버'];

    return TipBottomSheet(title, subTitle, '닫기', Cancel, itemList.length, itemList, sizeRatio: 0.3);
  }
}
