import 'package:flutter/material.dart';
import 'package:zzin/common/zconfig.dart';

TextStyle normalStyle(bool condition, double fontSize) {
  return TextStyle(
      fontSize: fontSize,
      fontWeight: FontWeight.normal,
      color: condition ? ZConfig.grey_text : ZConfig.zzin_black);
}

TextStyle priceStyle(double fontSize) {
  return TextStyle(fontSize: fontSize, color: ZConfig.zzin_pointColor);
}

TextStyle normalStyle2(double fontSize) {
  return TextStyle(fontSize: fontSize, fontWeight: FontWeight.normal, color: ZConfig.zzin_black);
}

TextStyle greyEngStyle(double fontSize) {
  return TextStyle(fontSize: fontSize, fontWeight: FontWeight.normal, color: ZConfig.grey_text);
}

TextStyle errorStyle(double fontSize) {
  return TextStyle(
    fontSize: fontSize - 8,
    fontWeight: FontWeight.normal,
    color: Colors.red,
  );
}
