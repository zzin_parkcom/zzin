import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:zzin/common/bottomsheet_custom.dart';
import 'package:zzin/common/hive/search_collection.dart';
import 'package:zzin/common/utils/upload_utils.dart';
import 'package:zzin/common/zconfig.dart';
import 'package:zzin/common/zutils.dart';
import 'package:zzin/search_page/search_option/product_status_bottom_sheet.dart';
import 'package:zzin/upload_page/customFormfield/custom_formfield.dart';

class ProductStatusOption extends StatefulWidget {
  final String defaultName = '상품상태 선택';
  final double fontSize;

  // ignore: prefer_const_constructors_in_immutables
  ProductStatusOption(this.fontSize, {Key? key}) : super(key: key);

  late final _ProductStatusOptionState _state;
  @override
  // ignore: no_logic_in_create_state
  _ProductStatusOptionState createState() {
    _state = _ProductStatusOptionState();
    return _state;
  }

  int getStatus() => _state.status;
  void setStatus(int status) => _state.status = status;

  int getSaleStatus() => _state.saleStatus;
  void setSaleStatus(int saleStatus) => _state.saleStatus = saleStatus;

  bool isEmpty() => _state.status == NotSelected && _state.saleStatus == NotSelected;

  void clearState() {
    _state.status = AllItems;
    _state.saleStatus = AllItems;

    SearchCollection.instance.status = AllItems;
    SearchCollection.instance.saleStatus = AllItems;
    SearchCollection.instance.save();
    _state.clearState();
  }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
class _ProductStatusOptionState extends State<ProductStatusOption> {
  int status = AllItems;
  int saleStatus = AllItems;

  void clearState() {
    setState(() {});
  }

  @override
  void initState() {
    super.initState();
    status = SearchCollection.instance.status;
    saleStatus = SearchCollection.instance.saleStatus;
  }

  @override
  Widget build(BuildContext context) {
    return CustomFormField(
      initialValue: status,
      validator: validator,
      onSaved: onSaved,
      autovalidateMode: AutovalidateMode.onUserInteraction,
      builder: listTileBuilder,
    );
  }

  Widget listTileBuilder(FormFieldState<int> state) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        ListTile(
            title: Row(
              children: [
                Text(getProductStatus(),
                    style: normalStyle(
                        ((status == AllItems || status == NotSelected) &&
                            (saleStatus == NotSelected || saleStatus == AllItems)),
                        widget.fontSize)),
                Expanded(
                    child: Text('  ${getSaleStatus()}',
                        overflow: TextOverflow.ellipsis,
                        style: normalStyle(true, widget.fontSize))),
                state.hasError
                    ? Text(
                        state.errorText!,
                        style: errorStyle(widget.fontSize),
                      )
                    : const SizedBox.shrink(),
              ],
            ),
            trailing: const Icon(Icons.expand_more),
            onTap: onProductStatus),
      ],
    );
  }

  String? validator(int? value) {
    return status == Cancel ? '상품상태를 선택해주세요.' : null;
  }

  void onSaved(int? value) {}

////////////////////////////////////////////////////////////////////////////////////////////////////
  void onProductStatus() async {
    var productStatusBottomSheet = ProductStatusBottomSheet();
    await showModalBottomSheetInt(
        context,
        (_) => BottomSheetCustom(
            contents: productStatusBottomSheet,
            sizeRatio: Get.mediaQuery.textScaleFactor >= 1.5 ? 0.43 : 0.35));

    status = SearchCollection.instance.status;
    saleStatus = SearchCollection.instance.saleStatus;

    print('status:$status, saleStatus:$saleStatus');

    setState(() {});
  }

  String getProductStatus() {
    switch (status) {
      case 0:
        return '새상품';
      case 1:
        return '중고상품';
      case NotSelected:
        return (saleStatus == NotSelected || saleStatus == AllItems) ? widget.defaultName : '전체상품';
      default:
        return (saleStatus == NotSelected || saleStatus == AllItems) ? widget.defaultName : '전체상품';
    }
  }

  String getSaleStatus() {
    switch (saleStatus) {
      case 0:
        return '판매중 상품만';
      case 1:
        return '거래완료 상품 포함';
      case -1:
        return (status == NotSelected || status == AllItems) ? '' : '거래완료 상품 포함';
      default:
        return '';
    }
  }
}
