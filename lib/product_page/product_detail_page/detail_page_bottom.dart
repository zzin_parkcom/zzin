import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:zzin/chat_page/chat_ui/chat_ui.dart';
import 'package:zzin/chat_page/chat_view.dart';
import 'package:zzin/common/firestore/document_skima/product.dart';
import 'package:zzin/common/firestore/document_skima/user_account.dart';
import 'package:zzin/common/utils/zenum.dart';
import 'package:zzin/common/widget/zbutton_blue.dart';
import 'package:zzin/common/zconfig.dart';
import 'package:zzin/common/zutils.dart';
import 'package:zzin/login/login.dart';

class DetailPageBottom extends StatefulWidget {
  final String tag;
  final ZUserAccount? seller;
  const DetailPageBottom({required this.seller, required this.tag, Key? key}) : super(key: key);
  @override
  _DetailPageBottomState createState() => _DetailPageBottomState();
}

class _DetailPageBottomState extends State<DetailPageBottom> {
  late Product _product;
  bool _isMyProduct = false;
  @override
  void initState() {
    super.initState();
    _product = Get.find(tag: widget.tag);
  }

  @override
  Widget build(BuildContext context) {
    _isMyProduct = _product.sellerId == ZConfig.userAccount.docId;
    return SizedBox(
      height: AppBar().preferredSize.height,
      child: widget.seller != null
          ? Padding(
              padding: const EdgeInsets.fromLTRB(20, 0, 20, 0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      GestureDetector(onTap: onTapFavorite, child: favoriteIcon()),
                      const SizedBox(width: 10),
                      Text(
                        '${numberWithComma(_product.price)}원',
                        overflow: TextOverflow.ellipsis,
                        style: const TextStyle(
                          fontSize: 18.0,
                          fontWeight: FontWeight.w500,
                          color: ZConfig.zzin_pointColor,
                        ),
                      ),
                    ],
                  ),
                  ZConfig.userAccount.isVerified
                      ? Flexible(
                          child: ZButtonYellow(
                            text: _product.isMyProduct() ? '거래 문의보기' : '거래 문의하기',
                            isSelected: true,
                            onPressed: onDealAndChat,
                            fontSize: 14,
                          ),
                        )
                      : const SizedBox.shrink(),
                ],
              ),
            )
          : Row(
              children: [
                Expanded(
                  child: SizedBox(
                    height: AppBar().preferredSize.height,
                    child: const Center(child: Text('')),
                  ),
                )
              ],
            ),
    );
  }

  Widget favoriteIcon() {
    if (ZConfig.userAccount.likeList.contains(_product.docId)) {
      return const Icon(Icons.favorite, color: ZConfig.zzin_pointColor);
    } else {
      return const Icon(Icons.favorite_outline_outlined, color: ZConfig.grey_text);
    }
  }

  void onTapFavorite() async {
    if (!ZConfig.userAccount.isVerified) {
      Get.rawSnackbar(title: '로그인필요', message: '상품을 찐하기 위해서는 로그인이 필요합니다.');
      return;
    }

    var productRealTime = await _product.getRealTime();

    if (productRealTime.articleStatus == ProductArticleStatus.REMOVED.index) {
      Get.rawSnackbar(title: '상품정보', message: '해당 상품이 삭제되어 찐할 수 없습니다.');
      return;
    }

    var userRef = ZConfig.userAccount.docId!.withConverter<ZUserAccount>(
        fromFirestore: (snapshots, _) => ZUserAccount.fromJson(snapshots.data()!),
        toFirestore: (userAccount, _) => userAccount.toJson());

    var productRef = _product.docId!.withConverter<Product>(
        fromFirestore: (snapshots, _) => Product.fromJson(snapshots.data()!),
        toFirestore: (product, _) => product.toJson());

    try {
      await FirebaseFirestore.instance.runTransaction((transaction) async {
        var snapshot = await transaction.get(productRef);

        if (!snapshot.exists) {
          return;
        }

        var likeCounts = snapshot.data()!.likeCounts;

        if (ZConfig.userAccount.likeList.contains(_product.docId)) {
          ZConfig.userAccount.likeList.remove(_product.docId!);
          likeCounts--;
        } else {
          ZConfig.userAccount.likeList.add(_product.docId!);
          likeCounts++;
        }
        _product.likeCounts = likeCounts;
        transaction.update(userRef, {'likeList': ZConfig.userAccount.likeList});
        transaction.update(productRef, {'likeCounts': likeCounts});
      });
    } catch (e) {
      print(e);
    }
    setState(() {});
  }

  void onDealAndChat() async {
    if (!ZConfig.userAccount.isVerified) {
      await Get.off(() => const LogIn(), transition: Transition.rightToLeftWithFade);
      return;
    }

    if (_isMyProduct) {
      // MainTabController.to.setIndex(1);
      // Get.back();

      await Get.to(() => Scaffold(body: ChatView(product: _product)));
      return;
    }

    var productRealTime = await _product.getRealTime();

    if (productRealTime.articleStatus == ProductArticleStatus.REMOVED.index) {
      Get.rawSnackbar(title: '상품정보', message: '해당 상품이 삭제되어 문의할 수 없습니다.');
      return;
    }

    await Get.to(
        () => ChatUI(sender: ZConfig.userAccount, receiver: widget.seller!, product: _product),
        transition: Transition.rightToLeftWithFade);
  }
}
