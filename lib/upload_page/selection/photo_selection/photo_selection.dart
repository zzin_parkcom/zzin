import 'dart:io';
import 'package:flutter/material.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:wechat_assets_picker/wechat_assets_picker.dart';
import 'package:zzin/common/hive/upload_collection.dart';
import 'package:zzin/common/utils/korean_text_delegate.dart';
import 'package:zzin/common/zconfig.dart';
import 'package:zzin/common/zutils.dart';
import 'package:zzin/upload_page/selection/photo_selection/photo_selection_listview.dart';
import 'package:zzin/upload_page/selection/photo_selection/photo_selection_select_image.dart';

class PhotoSelection extends StatefulWidget {
  final UploadCollection uploadCollection;
  late final _PhotoSelectionState _state;
  // ignore: prefer_const_constructors_in_immutables
  PhotoSelection({required this.uploadCollection, Key? key}) : super(key: key);

  @override
  // ignore: no_logic_in_create_state
  _PhotoSelectionState createState() {
    _state = _PhotoSelectionState();
    return _state;
  }

  Future<List<File>> getImageList() async {
    var files = _state.getImageList().then((value) => value);
    return files;
  }

  void clearState() {
    if (_state.mounted) {
      _state._clearState();
    }
  }

  List<String> getImagesId() {
    return _state.imageIds;
  }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
class _PhotoSelectionState extends State<PhotoSelection> {
  final TextEditingController _controller = TextEditingController();

  late List<String> imageIds;
  late int mainImageIndex;
  List<AssetEntity> assets = <AssetEntity>[];
  List<AssetEntity> preAssets = <AssetEntity>[];

  Future<List<File>> getImageList() async {
    var files = <File>[];
    // 대표 이미지 부터 리스트에 추가.
    for (var i = 0; i < assets.length; i++) {
      if (mainImageIndex == i) {
        var file = await assets[i].originFile;
        if (file != null) {
          files.add(file);
        }
      }
    }
    // 대표 이미지 제외 추가.
    var i = 0;
    for (var asset in assets) {
      var file = await asset.originFile;
      if (file != null && mainImageIndex != i) {
        files.add(file);
      }
      i++;
    }
    return files;
  }

  Future _loadAssetEntities() async {
    imageIds = widget.uploadCollection.imagesId;
    mainImageIndex = widget.uploadCollection.mainImageIndex;

    if (imageIds.isNotEmpty) {
      for (var image in imageIds) {
        final asset = await AssetEntity.fromId(image);
        if (asset != null && !assets.contains(asset)) {
          assets.add(asset);
        }
      }
    }
    // setState(() {});
  }

  void _saveAssetEntities() {
    _controller.text = assets.length.toString();
    imageIds.clear();
    for (var item in assets) {
      imageIds.add(item.id);
    }

    widget.uploadCollection.imagesId = imageIds;
    widget.uploadCollection.mainImageIndex = mainImageIndex;
    widget.uploadCollection.save();
    validator(assets.length);
    setState(() {});
  }

  void _clearState() {
    assets.clear();
    imageIds.clear();
    mainImageIndex = 0;
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: _loadAssetEntities(),
      builder: (context, snapshot) {
        if (snapshot.connectionState == ConnectionState.done) {
          return Column(
            children: [
              SizedBox(
                height: 100,
                child: Row(
                  children: [
                    PhotoSelectionSelectImage(
                      assets: assets,
                      onSaved: onSaved,
                      validator: validator,
                      multiPickup: multiPickup,
                    ),
                    PhotoSelectionListView(
                      assets: assets,
                      mainImageIndex: mainImageIndex,
                      onTapImage: onTapImage,
                      onTapRemove: onTapRemove,
                    ),
                  ],
                ),
              ),
            ],
          );
        } else {
          return Column(
            children: [
              SizedBox(
                height: 100,
                child: Row(
                  children: [
                    PhotoSelectionSelectImage(
                      assets: assets,
                      onSaved: onSaved,
                      validator: validator,
                      multiPickup: multiPickup,
                    ),
                    PhotoSelectionListView(
                      assets: assets,
                      mainImageIndex: mainImageIndex,
                      onTapImage: onTapImage,
                      onTapRemove: onTapRemove,
                    ),
                  ],
                ),
              ),
            ],
          );
        }
      },
    );
  }

  String? validator(int? value) {
    if (assets.isEmpty) {
      return '이미지를 선택하세요';
    }
    return null;
  }

  void onSaved(int? value) {
    _saveAssetEntities();
  }

  /////////////////////////////////////////////////////////////////////////////
  ///

  void multiPickup() async {
    var status = await Permission.storage.status;
    if (status == PermissionStatus.denied) {
      var status = await Permission.storage.request();
      if (status == PermissionStatus.denied) {
        return;
      } else if (status == PermissionStatus.permanentlyDenied) {
        var response = await showDialogYesNo(
            context, '사진 및 미디어에 대한 액세스 권한이 필요합니다.', '설정 앱에서 권한을 설정하십시요.', '설정하기', '취소');
        if (response == DialogResponse.YES) {
          await openAppSettings();
        }
        return;
      }
    }

    AssetEntity? asset;
    preAssets.clear();
    for (var i = 0; i < assets.length; i++) {
      asset = await AssetEntity.fromId(assets[i].id);
      if (asset != null) {
        preAssets.add(asset);
      }
    }
    print(preAssets.toList());

    var assetsTemp = await AssetPicker.pickAssets(context,
        selectedAssets: preAssets,
        maxAssets: ZConfig.maxUploadImageCount,
        textDelegate: KoreanTextDelegate(),
        gridCount: 3,
        pageSize: 90);

    if (assetsTemp != null) {
      if (assets.isNotEmpty) {
        assets.clear();
        assets.addAll(assetsTemp);
      } else {
        assets = assetsTemp;
        mainImageIndex = 0;
        widget.uploadCollection.mainImageIndex = mainImageIndex;
      }

      _saveAssetEntities();
    }
  }

  Future test() async {}

  void onTapImage(int index) {
    mainImageIndex = index;
    widget.uploadCollection.mainImageIndex = mainImageIndex;
    setState(() {});
  }

  void onTapRemove(int index) {
    assets.removeAt(index);
    if (assets.isNotEmpty) {
      mainImageIndex = 0;
      widget.uploadCollection.mainImageIndex = mainImageIndex;
    }
    _saveAssetEntities();
  }
}
