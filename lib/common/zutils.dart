import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:get/get.dart';
import 'package:intl/date_symbol_data_local.dart';
import 'package:intl/intl.dart';
import 'package:zzin/common/utils/location_controller.dart';
import 'package:zzin/common/zconfig.dart';

import 'hive/address_collection.dart';

Future<String?> showPasswordDialog(
  BuildContext context,
  String title,
  String comfirmString,
) async {
  var password = '';
  await showDialog<String>(
    context: context,
    builder: (BuildContext context) {
      // return object of type Dialog
      return AlertDialog(
        title: Text(title),
        content: TextField(onChanged: (value) => password = value),
        actions: <Widget>[
          ElevatedButton(onPressed: () => Get.back(), child: Text(comfirmString)),
        ],
      );
    },
  );
  return password;
}

Future<String> showInputDialog(
  BuildContext context,
  String title,
  String comfirmString,
) async {
  var input = '';
  await showDialog<String>(
    context: context,
    builder: (BuildContext context) {
      // return object of type Dialog
      return AlertDialog(
        title: Text(title),
        content: TextField(onChanged: (value) => input = value),
        actions: <Widget>[
          ElevatedButton(onPressed: () => Get.back(), child: Text(comfirmString)),
        ],
      );
    },
  );
  return input;
}

Future<DialogResponse?> showDialogYesNo(
    BuildContext context, String title, String contents, String yesString, String noString) async {
  var response = showDialog<DialogResponse>(
    context: context,
    builder: (BuildContext context) {
      // return object of type Dialog
      return AlertDialog(
        title: Text(title),
        content: Text(contents),
        actions: <Widget>[
          ElevatedButton(
            onPressed: () => Get.back(result: DialogResponse.YES),
            child: Text(yesString),
          ),
          ElevatedButton(
            onPressed: () => Get.back(result: DialogResponse.NO),
            child: Text(noString),
          ),
        ],
      );
    },
  );
  return response;
}

void showInfo(BuildContext context, String message) {
  ScaffoldMessenger.of(context).hideCurrentSnackBar();
  ScaffoldMessenger.of(context).showSnackBar(
    SnackBar(
      content: Text(message),
    ),
  );
}

MaterialColor createMaterialColor(Color color) {
  List strengths = <double>[.05];
  Map swatch = <int, Color>{};
  final r = color.red, g = color.green, b = color.blue;

  for (var i = 1; i < 10; i++) {
    strengths.add(0.1 * i);
  }
  for (var strength in strengths) {
    final ds = 0.5 - strength;
    swatch[(strength * 1000).round()] = Color.fromRGBO(
      r + ((ds < 0 ? r : (255 - r)) * ds).round(),
      g + ((ds < 0 ? g : (255 - g)) * ds).round(),
      b + ((ds < 0 ? b : (255 - b)) * ds).round(),
      1,
    );
  }
  return MaterialColor(color.value, swatch.cast<int, Color>());
}

Future<int> showModalBottomSheetInt(
    BuildContext context, Widget Function(BuildContext) builderFunction) async {
  return await showModalBottomSheet<int>(
        shape: const RoundedRectangleBorder(
          borderRadius: BorderRadius.vertical(top: Radius.circular(10)),
        ),
        isScrollControlled: true,
        isDismissible: true,
        elevation: 10,
        context: context,
        builder: builderFunction,
      ) ??
      Cancel;
}

String formatTimestamp(Timestamp timestamp) {
  var format = DateFormat('y-MM-d'); // 'hh:mm' for hour & min
  return format.format(timestamp.toDate());
}

String formatYYYYMMDDW(Timestamp timestamp) {
  initializeDateFormatting('ko_KR');
  return DateFormat.yMMMMEEEEd('ko_KR').format(timestamp.toDate());
}

String formatyMd(Timestamp timestamp) {
  initializeDateFormatting('ko_KR');
  // return DateFormat.yMMMMEEEEd('ko_KR').format(timestamp.toDate());
  return DateFormat.yMd('ko_KR').format(timestamp.toDate());
}

String formatHHMM(Timestamp timestamp) {
  initializeDateFormatting('ko_KR');
  return DateFormat.jm('ko_KR').format(timestamp.toDate()); // 'hh:mm' for hour & min
}

String formatJMV(Timestamp timestamp) {
  initializeDateFormatting('ko_KR');
  return DateFormat.Hm('ko_KR').format(timestamp.toDate()); // 'hh:mm' for hour & min
}

String formatDateTimeBefore(DateTime later) {
  var difference = DateTime.now().toUtc().difference(later.toUtc());

  if (difference.inDays >= 365) {
    return '${difference.inDays ~/ 365}년 전';
  } else if (difference.inDays >= 30) {
    return '${difference.inDays ~/ 30}개월 전';
  } else if (difference.inDays >= 7) {
    return '${difference.inDays ~/ 7}주 전';
  } else if (difference.inDays >= 1) {
    return '${difference.inDays}일 전';
  } else if (difference.inHours >= 1) {
    return '${difference.inHours}시간 전';
  } else if (difference.inMinutes >= 1) {
    return '${difference.inMinutes}분 전';
  } else {
    return '${difference.inSeconds}초 전';
  }
}

const MaterialColor white = MaterialColor(
  0xfffffffff,
  <int, Color>{
    50: Color(0x0fffffff),
    100: Color(0x1fffffff),
    200: Color(0x2fffffff),
    300: Color(0x3fffffff),
    400: Color(0x4fffffff),
    500: Color(0x5fffffff),
    600: Color(0x6fffffff),
    700: Color(0x7fffffff),
    800: Color(0x8fffffff),
    900: Color(0x9fffffff),
  },
);

String numberWithComma(int param) {
  return NumberFormat('###,###,###,###,###').format(param).replaceAll(' ', '');
}

void showTopSheet(BuildContext context, List<Widget> children) {
  showGeneralDialog(
    context: context,
    barrierDismissible: false,
    transitionDuration: const Duration(milliseconds: 0),
    barrierLabel: '',
    barrierColor: Colors.black.withOpacity(0.1),
    pageBuilder: (context, _, __) {
      return Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          ClipRRect(
            borderRadius:
                const BorderRadius.vertical(top: Radius.zero, bottom: Radius.circular(20)),
            child: Container(
              width: MediaQuery.of(context).size.width,
              color: ZConfig.zzin_white,
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: children,
              ),
            ),
          ),
          // Expanded(
          //     child: ColoredBox(
          //   color: Colors.black.withOpacity(0.5),
          //   child: Container(),
          // ))
        ],
      );
    },
    transitionBuilder: (context, animation, secondaryAnimation, child) {
      return SlideTransition(
          position: CurvedAnimation(
            parent: animation,
            curve: Curves.easeOut,
          ).drive(Tween<Offset>(
            begin: Offset(
                0, (context.mediaQueryPadding.top + AppBar().preferredSize.height) / Get.height),
            end: Offset(
                0, (context.mediaQueryPadding.top + AppBar().preferredSize.height) / Get.height),
          )),
          child: child);
    },
  );
}

Future<dynamic> getGPSAddressList(
    {int initDistance = 500,
    int maxDistance = 20000,
    int deltaDistance = 100,
    int minResultCount = 5}) async {
  // var initDistance = 500; // 시작 거리. 미터 단위
  // var maxDistance = 20000; // 최대 거리. 미터 단위
  // var deltaDistance = 100; // 단계별 증가 거리. 미터 단위
  // var minResultCount = 5; // 최종 결과물이 5개 정도 나오게.

  // GPS로 현재 위치 찾기.

  GeoPoint position;
  try {
    position = await LocationController.determinePosition(desiredAccuracy: LocationAccuracy.best);
    print(position);
  } catch (e) {
    print(e);
    return;
  }

  var distance = initDistance;

  Iterable<AddressCollection> resultList;
  do {
    var latitudeMin = position.latitude - distance * ZConfig.latitudePerMeter;
    var latitudeMax = position.latitude + distance * ZConfig.latitudePerMeter;
    var longitudeMin = position.longitude - distance * ZConfig.longitudePerMeter;
    var longitudeMax = position.longitude + distance * ZConfig.longitudePerMeter;

    // print('반경:$distance');
    resultList = AddressCollection.to.values.where((e) =>
        e.latitude > latitudeMin &&
        e.latitude < latitudeMax &&
        e.longitude > longitudeMin &&
        e.longitude < longitudeMax);

    for (var element in resultList) {
      print(element);
      var result = Geolocator.distanceBetween(
          position.latitude, position.longitude, element.latitude, element.longitude);
      print(result);
      print('\n');
    }

    // 검색 범위를 100m 씩 늘려준다.
    if (distance < maxDistance) {
      distance += deltaDistance;
    } else if (distance >= maxDistance) {
      break;
    }
  } while (resultList.length < minResultCount);

  var resultDistanceList = resultList
      .map((e) => {
            'regionCode': e.regionCode,
            'regionName': e.regionName,
            'latitude': e.latitude,
            'longitude': e.longitude,
            'distance': Geolocator.distanceBetween(
                position.latitude, position.longitude, e.latitude, e.longitude)
          })
      .toList();

  // 거리순으로 올림차순 정렬
  resultDistanceList.sort((a, b) {
    return (a['distance'] as double) >= (b['distance'] as double) ? 1 : -1;
  });

  for (var element in resultDistanceList) {
    print(element);
  }

  return resultDistanceList;
}
