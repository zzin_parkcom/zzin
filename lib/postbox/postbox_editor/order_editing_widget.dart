import 'package:flutter/material.dart';

class OrderEditingWidget extends StatefulWidget {
  final List order;
  final Map contents;
  const OrderEditingWidget({required this.order, required this.contents, Key? key})
      : super(key: key);

  @override
  _OrderEditingWidgetState createState() => _OrderEditingWidgetState();
}

class _OrderEditingWidgetState extends State<OrderEditingWidget> {
  @override
  Widget build(BuildContext context) {
    return ReorderableListView(
      physics: const NeverScrollableScrollPhysics(),
      shrinkWrap: true,
      children: widget.order
          .map((item) => ListTile(
                key: Key("$item"),
                title: Text(
                  "$item : ${widget.contents[item]}",
                  overflow: TextOverflow.ellipsis,
                ),
                trailing: Row(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    IconButton(onPressed: () => onRemove(item), icon: const Icon(Icons.close)),
                    const Icon(Icons.menu),
                  ],
                ),
              ))
          .toList(),
      onReorder: (int start, int current) {
        // dragging from top to bottom
        if (start < current) {
          int end = current - 1;
          String startItem = widget.order[start];
          int i = 0;
          int local = start;
          do {
            widget.order[local] = widget.order[++local];
            i++;
          } while (i < end - start);
          widget.order[end] = startItem;
        }
        // dragging from bottom to top
        else if (start > current) {
          String startItem = widget.order[start];
          for (int i = start; i > current; i--) {
            widget.order[i] = widget.order[i - 1];
          }
          widget.order[current] = startItem;
        }
        setState(() {});
      },
    );
  }

  void onRemove(String item) {
    widget.contents.remove(item);
    widget.order.remove(item);
    setState(() {});
  }
}
