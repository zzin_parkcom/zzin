import 'package:flutter/material.dart';
import 'package:zzin/common/widget/appbar/zappbar.dart';
import 'package:zzin/common/widget/ztab.dart';
import 'package:zzin/common/zconfig.dart';
import 'package:zzin/entrance/skeleton.dart';
import 'package:zzin/favorite_page/favorite_list.dart';
import 'package:zzin/favorite_page/recent_list.dart';

class FavoriteView extends StatelessWidget {
  final List<Tab> tabs = [
    const Tab(
      child: Text('나의 찐 목록', style: TextStyle(fontSize: 18)),
    ),
    const Tab(child: Text('최근 본 상품', style: TextStyle(fontSize: 18))),
  ];

  final FavoriteList favoriteList = const FavoriteList();
  final RecentList recentList = const RecentList();

  FavoriteView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        zAppBar(),
        // topTab(),
        Expanded(
          child: ZTab(
            keyString: MainTabController.to.tabIndex.value.toString(),
            tabs: tabs,
            tabViews: [favoriteList, recentList],
          ),
        ),
      ],
    );
  }

  Widget zAppBar() => ZAppBar(
        title: const Text('찐 목록',
            style: TextStyle(fontSize: ZSize.appBarTextSize, color: ZConfig.zzin_black)),
        // actions: [
        //   IconButton(onPressed: () {}, icon: Icon(Icons.search)),
        // ],
      );
}
