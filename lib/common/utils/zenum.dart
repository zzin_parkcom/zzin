// ignore_for_file: constant_identifier_names

enum ProductCategory { BAGS, CLOTHES, SHOES, WATCHES, ACCESORIES, ALL }
enum ProductStatus { NEW, OLD }
enum ProductSaleStatus { ONSALE, SOLDOUT, RESERVED }
enum ProductArticleStatus { NORMAL, REMOVED }
