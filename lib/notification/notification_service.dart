import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:get/get.dart';
import 'package:zzin/chat_page/chat_ui/chat_ui.dart';
import 'package:zzin/common/firestore/controller/product_controller.dart';
import 'package:zzin/common/firestore/controller/user_account_controller.dart';
import 'package:zzin/common/zconfig.dart';
import 'package:zzin/notification/notification_count.dart';

class NotificationService {
  static final NotificationService _notificationService = NotificationService._internal();

  late FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin;

  factory NotificationService() {
    return _notificationService;
  }

  NotificationService._internal();

  late AndroidNotificationChannel channel;
  // late final FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin;

  Future<void> init() async {
    channel = const AndroidNotificationChannel(
      'high_importance_channel', // id
      'High Importance Notifications', // title
      'This channel is used for important notifications.', // description
      importance: Importance.high,
    );

    flutterLocalNotificationsPlugin = FlutterLocalNotificationsPlugin();

    const initializationSettingsAndroid = AndroidInitializationSettings('ic_push');
    final initializationSettingsIOS =
        IOSInitializationSettings(onDidReceiveLocalNotification: onDidReceiveLocalNotification);
    final initializationSettings = InitializationSettings(
      android: initializationSettingsAndroid,
      iOS: initializationSettingsIOS,
    );
    await flutterLocalNotificationsPlugin.initialize(initializationSettings,
        onSelectNotification: selectNotification);

    if (GetPlatform.isAndroid) {
      await flutterLocalNotificationsPlugin
          .resolvePlatformSpecificImplementation<AndroidFlutterLocalNotificationsPlugin>()
          ?.createNotificationChannel(channel);
    } else if (GetPlatform.isIOS) {
      await flutterLocalNotificationsPlugin
          .resolvePlatformSpecificImplementation<IOSFlutterLocalNotificationsPlugin>()
          ?.requestPermissions(
            alert: true,
            badge: true,
            sound: true,
          );
    }

    FirebaseMessaging.onMessage.listen(onMessage);
    FirebaseMessaging.onMessageOpenedApp.listen(onMessageOpenedApp);
    FirebaseMessaging.onBackgroundMessage(onBackgroundMessage);

    // 푸쉬 토근 저장.
    var token = await FirebaseMessaging.instance.getToken();

    print('푸쉬 토큰 : $token');
    ZConfig.pushToken = token;

    if (ZConfig.userAccount.isVerified) {
      print(ZConfig.userAccount.docId);

      await ZConfig.userAccount.docId!.update({'pushToken': ZConfig.pushToken});
      print('pushToken update: ${ZConfig.pushToken}');
    }

    await FirebaseMessaging.instance.getInitialMessage().then((RemoteMessage? message) async {
      if (message != null) {
        print(
            'getInitialMessage event----------------------------------------------------------------');
        NotificationCounter.to.isExistNotification.value = true;
      }
    });

    print('NotificationService init end...');
  }

  void onMessage(RemoteMessage message) async {
    print('onMessage event----------------------------------------------------------------');
    printMessage(message);
    var notification = message.notification;
    var android = message.notification?.android;
    var apple = message.notification?.apple;
    if (notification != null && (android != null || apple != null)) {
      await flutterLocalNotificationsPlugin.show(
          notification.hashCode,
          notification.title,
          notification.body,
          NotificationDetails(
            android: AndroidNotificationDetails(
              channel.id,
              channel.name,
              channel.description,
              icon: 'ic_push',
            ),
          ),
          payload: message.data['sender'] + '|' + message.data['product']);

      NotificationCounter.to.isExistNotification.value = true;
    }
    // await gotoChatUI(message);
  }

  void onMessageOpenedApp(RemoteMessage message) async {
    print(
        'onMessageOpenedApp event----------------------------------------------------------------');
    printMessage(message);

    if (ZConfig.userAccount.isVerified) {
      await gotoChatUI(message);
    } else {
      print('로그인하지 않아서 해당 채팅방으로 이동할 수 없습니다.');
      Get.rawSnackbar(title: '로그인 필요', message: '채팅방으로 이동하기 위해서는 로그인이 필요합니다.');
    }
  }

  Future selectNotification(String? payload) async {
    print(
        'selectNotification event----------------------------------------------------------------');
    if (payload != null) {
      var sender = payload.split('|').first;
      var product = payload.split('|').last;

      if (ZConfig.userAccount.isVerified) {
        await gotoChatUIByString(sender, product);
      } else {
        print('로그인하지 않아서 해당 채팅방으로 이동할 수 없습니다.');
        Get.rawSnackbar(title: '로그인 필요', message: '채팅방으로 이동하기 위해서는 로그인이 필요합니다.');
      }
    }
  }

  Future onDidReceiveLocalNotification(int id, String? title, String? body, String? payload) async {
    print(
        'onDidReceiveLocalNotification event----------------------------------------------------------------');
    print('id : $id');
    print('title : $title');
    print('body : $body');
    print('payload : $payload');
  }

  Future<void> gotoChatUI(RemoteMessage message) async {
    await Get.put(ZUserAccountController()).queryUserAccountByPath(message.data['sender']);
    await Get.put(ProductController()).queryProductByPath(message.data['product']);

    await Get.to(
        () => ChatUI(
            sender: ZConfig.userAccount, // 메시지를 처음 보낸 사람.
            receiver: ZUserAccountController.to.userAccount,
            product: ProductController.to.product),
        transition: Transition.rightToLeftWithFade);

    NotificationCounter.to.isExistNotification.value = false;
    await flutterLocalNotificationsPlugin.cancelAll();
  }

  Future<void> gotoChatUIByString(String senderPath, String productPath) async {
    await Get.put(ZUserAccountController()).queryUserAccountByPath(senderPath);
    await Get.put(ProductController()).queryProductByPath(productPath);

    await Get.to(
        () => ChatUI(
            sender: ZConfig.userAccount, // 메시지를 처음 보낸 사람.
            receiver: ZUserAccountController.to.userAccount,
            product: ProductController.to.product),
        transition: Transition.rightToLeftWithFade);
    NotificationCounter.to.isExistNotification.value = false;
    await flutterLocalNotificationsPlugin.cancelAll();
  }
}

Future onBackgroundMessage(RemoteMessage message) async {
  // await Firebase.initializeApp();
  print(
      'onBackgroundMessage event----------------------------------------------------------------');
  printMessage(message);

  // var notiCounter = ZConfig.pref.getInt('notification_counter');
  // if (notiCounter != null) {
  //   await ZConfig.pref.setInt('notification_counter', 1);
  // } else {
  //   await ZConfig.pref.setInt('notification_counter', notiCounter = notiCounter! + 1);
  // }
}

void printMessage(RemoteMessage message) {
  print('${message.messageId}');
  print(message.notification!.body.toString());
  print(message.data.toString());
  print('${message.data.hashCode}');
}
