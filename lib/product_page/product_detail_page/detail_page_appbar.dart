import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:get/get.dart';
import 'package:transparent_image/transparent_image.dart';
import 'package:zzin/chat_page/chat_ui/chatroom_menu/block_user_bottom_sheet.dart';
import 'package:zzin/chat_page/chat_ui/chatroom_menu/report_user_first_page.dart';
import 'package:zzin/common/bottomsheet_custom.dart';
import 'package:zzin/common/firestore/document_skima/block_user_controller.dart';
import 'package:zzin/common/firestore/document_skima/product.dart';
import 'package:zzin/common/firestore/document_skima/user_account.dart';
import 'package:zzin/common/hive/brand_collection.dart';
import 'package:zzin/common/hive/category_collection.dart';
import 'package:zzin/common/utils/zenum.dart';
import 'package:zzin/common/zcache_manager.dart';
import 'package:zzin/common/zconfig.dart';
import 'package:zzin/common/zutils.dart';
import 'package:zzin/product_page/product_detail_page/dots_indicator.dart';
import 'package:zzin/common/utils/full_screen_image.dart';
import 'package:zzin/product_page/product_detail_page/modify_product_page.dart';
import 'package:zzin/product_page/product_detail_page/remove_product_bottom_sheet.dart';

class DetailPageAppBar extends StatefulWidget {
  final String tag;
  final String tagHeroPrefix;
  final void Function() onRefresh;
  const DetailPageAppBar(
      {required this.tag, required this.tagHeroPrefix, required this.onRefresh, Key? key})
      : super(key: key);
  @override
  _DetailPageAppBarState createState() => _DetailPageAppBarState();
}

class _DetailPageAppBarState extends State<DetailPageAppBar> {
  late Product _product;

  static const _kDuration = Duration(milliseconds: 10);
  static const _kCurve = Curves.easeInOut;

  final _controllerPage = PageController();

  @override
  void initState() {
    super.initState();
    _product = Get.find(tag: widget.tag);
  }

  @override
  void dispose() {
    _controllerPage.dispose();

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return SliverAppBar(
      pinned: true,
      snap: false,
      floating: false,
      leading: IconButton(
        icon: const CircleAvatar(
            radius: 15,
            backgroundColor: Colors.white24,
            child: Icon(Icons.arrow_back, color: ZConfig.zzin_black)),
        onPressed: () => onExit(context),
      ),
      expandedHeight: MediaQuery.of(context).size.width +
          (AppBar().preferredSize.height * 1.5) +
          (Get.mediaQuery.textScaleFactor >= 1.5 ? AppBar().preferredSize.height : 0),
      flexibleSpace: LayoutBuilder(builder: (BuildContext context, BoxConstraints constraints) {
        // print('constraints=' + constraints.toString());
        return FlexibleSpaceBar(
          stretchModes: const [StretchMode.blurBackground],
          title: SABT(
              child: Text(_product.title,
                  overflow: TextOverflow.ellipsis,
                  style: const TextStyle(fontSize: 15, color: ZConfig.zzin_black))),
          collapseMode: CollapseMode.parallax,
          background: Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [_imagePart(), _titlePart()],
          ),
        );
      }),
      actions: ZConfig.userAccount.isRegistered() ? [_actionPart()] : [],
    );
  }

  void onExit(BuildContext context) {
    Get.back(result: _product);
  }

  void onSelected(int index) async {
    print(index);

    switch (index) {
      // 거래 완료하기 ///////////
      case 0:
        _product.saleStatus = (_product.saleStatus == ProductSaleStatus.ONSALE.index)
            ? ProductSaleStatus.SOLDOUT.index
            : ProductSaleStatus.ONSALE.index;
        await _product.docId!
            .withConverter<Product>(
                fromFirestore: (snapshots, _) => Product.fromJson(snapshots.data()!),
                toFirestore: (product, _) => product.toJson())
            .update({'saleStatus': _product.saleStatus});
        setState(() {});
        await ZConfig.analytics.logEvent(name: 'Sale_Complete');
        break;
      // 수정하기 //////////////
      case 1:
        var result = await Get.to(() => ModifyProductPage(tag: widget.tag),
            transition: Transition.rightToLeftWithFade);
        if (result != null) {
          widget.onRefresh();
        }
        break;
      // 삭제하기 ///////////////
      case 2:
        // var response = await showDialogYesNo(context, '상품 삭제', '정말로 상품을 삭제하시겠습니까?', '예', '아니요');

        var response = await showModalBottomSheetInt(context,
            (_) => const BottomSheetCustom(contents: RemoveProductBottomSheet(), sizeRatio: 0.25));

        if (response == DialogResponse.YES.index) {
          await _product.docId!
              .withConverter<Product>(
                  fromFirestore: (snapshots, _) => Product.fromJson(snapshots.data()!),
                  toFirestore: (product, _) => product.toJson())
              .update({'articleStatus': ProductArticleStatus.REMOVED.index});

          Get.back(result: 'refresh');
          Get.rawSnackbar(title: '상품삭제', message: '새로고침을 하시면 리스트에 반영됩니다.');
        }
        break;

      // 신고하기
      case 100:
        reportUser();
        break;
      // 차단하기
      case 101:
        blockUserOnOff();
        break;
    }
  }

  void reportUser() async {
    var result = await Get.to(
        () => ReportUserFirstPage(
              productDocId: _product.docId!,
              chatroomDocId: null,
              targetUserDocId: _product.sellerId!,
            ),
        transition: Transition.rightToLeftWithFade);
    if (result == true) {}
  }

  void blockUserOnOff() async {
    var snapshot = await _product.sellerId!
        .withConverter<ZUserAccount>(
            fromFirestore: (snapshot, _) => ZUserAccount.fromJson(snapshot.data()!),
            toFirestore: (chatMessage, _) => chatMessage.toJson())
        .get();
    var blockUserNickname = snapshot.data()!.nickname;

    var result = await showModalBottomSheetInt(
        context,
        (_) => BottomSheetCustom(
            contents: BlockUserBottomSheet(blockUser: '$blockUserNickname', isBlockedUser: false),
            sizeRatio: 0.3));
    print(result);

    if (result == 1) {
      await BlockUserController(user: _product.sellerId!).addUser();
    }
    Get.rawSnackbar(message: '$blockUserNickname님을 차단하였습니다.');
  }

  Widget _imagePart() {
    return Opacity(
      opacity: _product.saleStatus == ProductSaleStatus.SOLDOUT.index ? ZConfig.opacitySoldout : 1,
      child: Stack(
        children: [
          SizedBox(
            height: MediaQuery.of(context).size.width,
            child: PageView.builder(
              controller: _controllerPage,
              itemCount: _product.images.length,
              itemBuilder: (context, index) {
                return GestureDetector(
                  onTap: () => Get.to(
                      () => FullScreenImage(
                          images: _product.images,
                          tag: widget.tagHeroPrefix + _product.docId.toString(),
                          initialPage: _controllerPage.page!.toInt()),
                      transition: Transition.fadeIn,
                      duration: ZConfig.heroAnimationDuration),
                  child: Hero(
                    tag: widget.tagHeroPrefix + _product.docId.toString(),
                    child: CachedNetworkImage(
                      fadeInDuration: const Duration(milliseconds: 0),
                      fadeOutDuration: const Duration(milliseconds: 0),
                      placeholderFadeInDuration: const Duration(milliseconds: 0),
                      placeholder: (_, __) => index == 0
                          ? Image(
                              image: CachedNetworkImageProvider(_product.thumbnail),
                              fit: BoxFit.cover,
                            )
                          : Image.memory(kTransparentImage),
                      imageUrl: _product.images[index],
                      fit: BoxFit.cover,
                      errorWidget: (context, url, error) => const Icon(Icons.error),
                      cacheManager: ZCacheManager.instance,
                    ),
                  ),
                );
              },
            ),
          ),
          Positioned(
            top: MediaQuery.of(context).size.width - 30,
            left: 0.0,
            right: 0.0,
            child: Container(
              color: Colors.transparent,
              child: Center(
                child: DotsIndicator(
                  controller: _controllerPage,
                  itemCount: _product.images.length,
                  onPageSelected: (int page) {
                    _controllerPage.animateToPage(
                      page,
                      duration: _kDuration,
                      curve: _kCurve,
                    );
                  },
                ),
              ),
            ),
          ),
          _product.status == ProductStatus.NEW.index
              ? Positioned(
                  left: 5.0,
                  bottom: 0.0,
                  child: TextButton(
                    style: ButtonStyle(
                        backgroundColor: MaterialStateProperty.all<Color>(ZConfig.zzin_black),
                        foregroundColor: MaterialStateProperty.all<Color>(ZConfig.zzin_white)),
                    onPressed: null,
                    child: const Text('새상품'),
                  ))
              : const SizedBox.shrink(),
          _product.saleStatus == ProductSaleStatus.SOLDOUT.index
              ? Positioned(
                  right: 5.0,
                  bottom: 0.0,
                  child: TextButton(
                    style: ButtonStyle(
                        backgroundColor: MaterialStateProperty.all<Color>(ZConfig.zzin_black),
                        foregroundColor: MaterialStateProperty.all<Color>(ZConfig.zzin_white)),
                    onPressed: null,
                    child: const Text('거래완료 상품'),
                  ))
              : const SizedBox.shrink(),
        ],
      ),
    );
  }

  Widget _titlePart() {
    return Expanded(
      child: Padding(
        padding: const EdgeInsets.fromLTRB(20, 0, 20, 0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Padding(
              padding: const EdgeInsets.only(left: 20),
              child: Row(
                children: [
                  Text(BrandCollection.getNameEn(_product.brand),
                      style: TextStyle(fontSize: 13, color: Colors.grey[600])),
                  const Text('  |  ', style: TextStyle(fontSize: 13, color: ZConfig.grey_text)),
                  Text(
                      /* ZConfig.categoryListString[_product.category].split('|').last */
                      CategoryCollection.to.values
                          .where((element) => element.id == _product.category)
                          .first
                          .nameEn,
                      style: TextStyle(fontSize: 13, color: Colors.grey[600]))
                ],
              ),
            ),
            Text(_product.title,
                maxLines: 2,
                overflow: TextOverflow.ellipsis,
                style: const TextStyle(fontSize: 17, color: ZConfig.zzin_black)),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 20.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    '${_product.regionName}  ${formatDateTimeBefore(_product.saleModifyDate.toDate())}',
                    overflow: TextOverflow.ellipsis,
                    style: TextStyle(fontSize: 13, color: Colors.grey[600]),
                  ),
                  Expanded(
                    child: Text(
                      '관심 ${_product.likeCounts}',
                      textAlign: TextAlign.end,
                      overflow: TextOverflow.ellipsis,
                      style: TextStyle(fontSize: 13.0, color: Colors.grey[600]),
                    ),
                  ),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }

  Widget _actionPart() {
    return PopupMenuButton<int>(
      icon: CircleAvatar(
          radius: 15,
          backgroundColor: Colors.white24,
          child: Icon(Icons.adaptive.more, color: ZConfig.zzin_black)),
      offset: Offset(0, context.mediaQueryPadding.top),
      onSelected: onSelected,
      itemBuilder: (context) => _product.isMyProduct()
          ? [
              PopupMenuItem(
                  value: 0,
                  child: Text(_product.saleStatus == ProductSaleStatus.ONSALE.index
                      ? '거래 완료하기'
                      : '판매중으로 변경하기')),
              const PopupMenuDivider(),
              const PopupMenuItem(value: 1, child: Text('수정하기')),
              const PopupMenuDivider(),
              const PopupMenuItem(value: 2, child: Text('삭제하기')),
            ]
          : ZConfig.userAccount.isRegistered()
              ? [
                  const PopupMenuItem(value: 100, child: Text('신고하기')),
                  const PopupMenuItem(value: 101, child: Text('이 사용자 차단하기')),
                ]
              : [],
    );
  }
}

class SABT extends StatefulWidget {
  final Widget? child;
  const SABT({
    Key? key,
    required this.child,
  }) : super(key: key);
  @override
  _SABTState createState() {
    return _SABTState();
  }
}

class _SABTState extends State<SABT> {
  ScrollPosition? _position;
  bool _visible = false;
  @override
  void dispose() {
    _removeListener();
    super.dispose();
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    _removeListener();
    _addListener();
  }

  void _addListener() {
    _position = Scrollable.of(context)?.position;
    _position?.addListener(_positionListener);
    _positionListener();
  }

  void _removeListener() {
    _position?.removeListener(_positionListener);
  }

  void _positionListener() {
    final FlexibleSpaceBarSettings? settings = context.dependOnInheritedWidgetOfExactType()!;
    var visible = settings == null || settings.currentExtent > settings.minExtent + 10;
    if (_visible != visible) {
      setState(() {
        _visible = visible;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return AnimatedOpacity(
      duration: const Duration(milliseconds: 300),
      opacity: _visible ? 0 : 1,
      child: widget.child,
    );
  }
}
