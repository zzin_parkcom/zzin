// ignore_for_file: constant_identifier_names

import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:firebase_analytics/observer.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:firebase_remote_config/firebase_remote_config.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:zzin/common/firestore/document_skima/block_user.dart';
import 'package:zzin/common/firestore/document_skima/user_account.dart';
import 'package:zzin/common/hive/zhive.dart';
import 'package:zzin/common/utils/zenum.dart';

class ZConfig {
  static Future<void> initialize() async {
    await ZHive.initialize();

    // await ZHive.clearHive();
  }

  static const bool isRealServer = kReleaseMode;

  static late FirebaseDatabase database;
  static late FirebaseAnalytics analytics;
  static late FirebaseAnalyticsObserver analyticsObserver;
  static late RemoteConfig remoteConfig;

  static bool isShowUrgentNotice = true;

  static String? pushToken;

  static User? authUser;
  static late ZUserAccount userAccount = ZUserAccount.initZero();
  static late List<BlockUser> blockUsers = <BlockUser>[];

  static const int maxDocumentCount = 10;
  static const double scrollRatioQueryMore = 0.8;
  static const int maxRecentListCount = 10;

  static const int maxUploadImageCount = 10;

  static const double opacitySoldout = 0.5;

  static const String productsCollection = isRealServer ? 'products_0701' : 'products_0804';
  static const String userCollection = isRealServer ? 'users_0701' : 'users_0804';

  static const String noticeCollection = 'notice';
  static const String postboxCollection = 'postbox';

  static const String chatRoomsGroupCollection = 'chatRooms';
  static const String chatmessagesCollection = 'messages';
  static const String keywordCollection = 'keyword';
  static const String reportUserCollection = 'reportUser';

  static const String opentalkListCollection = 'opentalk_list';

  static const Color zzin_blue = Color(0xFF1E1EC8);
  static const Color zzin_black = Color(0xFF000000);
  static const Color zzin_white = Color(0xFFFFFFFF);
  static const Color grey_text = Color(0xFFC0C4CB);
  static const Color grey_border = Color(0xFFF4F4F4);
  static const Color chat_yellow = Color(0xFFFEE967);

  //new color

  static const Color zzin_primaryColor = zzin_white;

  static const Color zzin_pointColor = Color(0xFF2949A0);
  static const Color zzin_background = zzin_white;

  // ZHive.generalBox 에 위치정보 저장키
  static String currentGeoKey = 'currentGeoKey';
  static int areaRadiusKm = 5;

  // 위도 경도 단위
  static const double latitudePerMeter = 0.000008983152841192; // 단위 1미터를 위도 십진법으로 하면.
  static const double longitudePerMeter = 0.000011317616594452; // 단위 1미터를 경도 십진법으로 하면.

  // 이용약관
  static const String termsAndConditions = 'https://rebrand.ly/zzin_termsandconditions';
  // 개인정보 처리방침
  static const String privacyPolicy = 'https://rebrand.ly/zzin_privacypolicy';

  // 한글 완성형
  static RegExp regExpKo = RegExp(r'[a-z|A-Z|가-힣0-9]', unicode: true);

  static RegExp regExpNickname = RegExp(r'^[a-z|A-Z|가-힣0-9]{2,10}$', unicode: true);

  static String defaultImageStorageURL =
      'https://firebasestorage.googleapis.com/v0/b/zzin-b9524.appspot.com/o/default%2Fdefault.jpg?alt=media&token=f7fc9b15-450a-4416-afdd-370ed964efb4';
  static String defaultImageStorageURL2 =
      'https://firebasestorage.googleapis.com/v0/b/zzin-b9524.appspot.com/o/default%2Ffinal_icon_512_ios.png?alt=media&token=83d43161-6a98-4a7d-b07d-74f246ba123b';
  static List<String> sortNameList = ['정확도순', '최신순', '가격낮은순', '가격높은순' /* , '인기순' */];

  static List<int> areaRadiusMeters = [500, 2000, 10000, 500000];

  static const Duration transitionDuration = Duration(milliseconds: 500);
  static Duration heroAnimationDuration = const Duration(milliseconds: 500);
}

////////////////////////////////////////////////////////////////////////////////////////////////////

class ProductStatusString {
  static String toStr(int v) {
    return ProductStatus.values[v] == ProductStatus.OLD ? '중고' : '새상품';
  }
}

enum DialogResponse { YES, NO }

const int AllItems = -20210428;

const int NotFound = -1;
const int Cancel = -1;
const int NotSelected = -1;
const String Empty = '';
const int Init = -2;

////////////////////////////////////////////////////////////////////////////////////////////////////
class ZSize {
  static const double appBarTextSize = 22;
  static const double searchTextSize = 20;

  static const double bottomSheetTitleFontSize = 26;
  static const double bottomSheetFontSize = 19;

  static const EdgeInsetsGeometry bottomNavigationBarButtonOutPadding =
      EdgeInsets.fromLTRB(20, 10, 20, 10);

  static MaterialStateProperty<EdgeInsetsGeometry> bottomNavigationBarButtonInPadding =
      MaterialStateProperty.all(const EdgeInsets.fromLTRB(0, 15, 0, 15));

  static const double BNB_ButtonFontSize = 24;
}

////////////////////////////////////////////////////////////////////////////////////////
// TEST 용
const int uploadAutoCount = 1000;
