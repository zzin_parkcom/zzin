// import 'package:flutter/material.dart';
// import 'package:flutter/services.dart';

// class MyTextFormField extends FormField<String> {
//   MyTextFormField({
//     Key? key,
//     this.controller,
//     String? initialValue,
//     FocusNode? focusNode,
//     bool autofocus = false,
//     bool autovalidate = false,
 
//     ValueChanged<String>? onChanged,
//     GestureTapCallback? onTap,
//     VoidCallback? onEditingComplete,
//     ValueChanged<String>? onFieldSubmitted,
//     FormFieldSetter<String>? onSaved,
//     FormFieldValidator<String>? validator,
//     List<TextInputFormatter>? inputFormatters,
//     bool enabled = true,
//     InputCounterWidgetBuilder? buildCounter,
//   })  :  super(
//           key: key,
//           initialValue: controller!.text,
//           onSaved: onSaved,
//           validator: validator,
//           autovalidate: autovalidate,
//           enabled: enabled,
//           builder: (FormFieldState<String> field) {
//             final state = field as _MyTextFormFieldState;
//                  void onChangedHandler(String value) {
//               if (onChanged != null) {
//                 onChanged(value);
//               }
//               if (field.hasError) {
//                 field.validate();
//               }
//               field.didChange(value);
//             }

//             return TextField(
//               controller: state._effectiveController,
//               focusNode: focusNode,
//               autofocus: autofocus,
//               toolbarOptions: toolbarOptions,
//               readOnly: readOnly,
//               showCursor: showCursor,
//               onChanged: onChangedHandler,
//               onTap: onTap,
//               onEditingComplete: onEditingComplete,
//               onSubmitted: onFieldSubmitted,
//               inputFormatters: inputFormatters,
//               enabled: enabled,
//               buildCounter: buildCounter,
//             );
//           },
//         );

//   /// Controls the text being edited.
//   ///
//   /// If null, this widget will create its own [TextEditingController] and
//   /// initialize its [TextEditingController.text] with [initialValue].
//   final TextEditingController? controller;

//   @override
//   _MyTextFormFieldState createState() => _MyTextFormFieldState();
// }

// class _MyTextFormFieldState extends FormFieldState<String> {
//   late TextEditingController _controller;

//   TextEditingController get _effectiveController => widget.controller ?? _controller;

//   @override
//   MyTextFormField get widget => super.widget as MyTextFormField;

//   @override
//   void initState() {
//     super.initState();
//     if (widget.controller == null) {
//       _controller = TextEditingController(text: widget.initialValue);
//     } else {
//       widget.controller!.addListener(_handleControllerChanged);
//     }
//   }

//   @override
//   void didUpdateWidget(MyTextFormField oldWidget) {
//     super.didUpdateWidget(oldWidget);
//     if (widget.controller != oldWidget.controller) {
//       oldWidget.controller?.removeListener(_handleControllerChanged);
//       widget.controller?.addListener(_handleControllerChanged);

//       if (oldWidget.controller != null && widget.controller == null) {
//         _controller = TextEditingController.fromValue(oldWidget.controller!.value);
//       }
//       if (widget.controller != null) {
//         setValue(widget.controller!.text);
//       }
//     }
//   }

//   @override
//   void dispose() {
//     widget.controller?.removeListener(_handleControllerChanged);
//     super.dispose();
//   }

//   @override
//   void reset() {
//     super.reset();
//     setState(() {
//       _effectiveController.text = widget.initialValue!;
//     });
//   }

//   void _handleControllerChanged() {
//     // Suppress changes that originated from within this class.
//     //
//     // In the case where a controller has been passed in to this widget, we
//     // register this change listener. In these cases, we'll also receive change
//     // notifications for changes originating from within this class -- for
//     // example, the reset() method. In such cases, the FormField value will
//     // already have been set.
//     if (_effectiveController.text != value) {
//       didChange(_effectiveController.text);
//     }
//   }
// }
