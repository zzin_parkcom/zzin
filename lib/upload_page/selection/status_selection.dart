import 'package:flutter/material.dart';
import 'package:zzin/common/hive/upload_collection.dart';
import 'package:zzin/common/utils/upload_utils.dart';
import 'package:zzin/common/utils/zenum.dart';
import 'package:zzin/common/zconfig.dart';
import 'package:zzin/upload_page/customFormfield/custom_formfield.dart';

class StatusSelection extends StatefulWidget {
  final double fontSize;
  final UploadCollection uploadCollection;
  final int defaultValue = ProductStatus.NEW.index;

  StatusSelection(this.fontSize, {required this.uploadCollection, Key? key}) : super(key: key);

  @override
  _StatusSelectionState createState() => _StatusSelectionState();
}

class _StatusSelectionState extends State<StatusSelection> {
  var status = 0;

  @override
  Widget build(BuildContext context) {
    status = widget.uploadCollection.status;
    return CustomFormField(
      initialValue: ProductStatus.NEW.index,
      onSaved: onSaved,
      builder: listTileBuilder,
    );
  }

  Widget listTileBuilder(FormFieldState<int> state) {
    return Column(
      children: [
        ListTile(
          title: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text('상품 상태', style: normalStyle(false, widget.fontSize)),
              Expanded(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    Radio(
                      materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
                      activeColor: ZConfig.zzin_pointColor,
                      groupValue: status,
                      value: ProductStatus.OLD.index,
                      onChanged: (value) {
                        status = value as int;
                        widget.uploadCollection.status = status;
                        widget.uploadCollection.save();
                        setState(() {});
                      },
                    ),
                    Text('중고',
                        textAlign: TextAlign.end,
                        overflow: TextOverflow.ellipsis,
                        style: normalStyle2(widget.fontSize - 4.0)),
                    Radio(
                      materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
                      activeColor: ZConfig.zzin_pointColor,
                      groupValue: status,
                      value: ProductStatus.NEW.index,
                      onChanged: (value) {
                        status = value as int;
                        widget.uploadCollection.status = status;
                        widget.uploadCollection.save();
                        setState(() {});
                      },
                    ),
                    Flexible(
                      child: Text(
                        '새상품',
                        textAlign: TextAlign.end,
                        overflow: TextOverflow.ellipsis,
                        style: normalStyle2(widget.fontSize - 4.0),
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
        state.hasError
            ? Text(
                state.errorText!,
                style: const TextStyle(color: Colors.red),
              )
            : Container()
      ],
    );
  }

  void onSaved(int? value) {}
}
