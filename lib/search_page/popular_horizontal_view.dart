import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:transparent_image/transparent_image.dart';
import 'package:zzin/common/firestore/document_skima/product.dart';
import 'package:zzin/common/firestore/query_product.dart';
import 'package:zzin/common/utils/zenum.dart';
import 'package:zzin/common/zcache_manager.dart';
import 'package:zzin/common/zconfig.dart';
import 'package:zzin/product_page/product_detail_page/product_detail_view.dart';
import 'package:zzin/search_page/popular_search_page.dart';

class PopularHorizontalView extends StatefulWidget {
  final String tagHeroPrefix;
  const PopularHorizontalView({Key? key, required this.tagHeroPrefix}) : super(key: key);

  @override
  _PopularHorizontalViewState createState() => _PopularHorizontalViewState();
}

class _PopularHorizontalViewState extends State<PopularHorizontalView> {
  final ScrollController _scrollController = ScrollController();
  late final QueryProduct queryProductLike;
  double imageSize = 150;

  @override
  void initState() {
    super.initState();
    _scrollController.addListener(_scrollListener);
    queryProductLike = Get.put(QueryProduct(), tag: widget.toString());
  }

  @override
  void dispose() {
    _scrollController.removeListener(_scrollListener);
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: getProduct(),
      builder: (context, snapshot) {
        if (snapshot.connectionState == ConnectionState.done) {
          return Column(
            children: [
              const Divider(),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Row(
                    children: const [
                      Text(
                        '인기 상품',
                        style: TextStyle(fontSize: 20),
                      ),
                    ],
                  ),
                  TextButton(onPressed: onMoreFavorite, child: const Text('인기 상품 더보기'))
                ],
              ),
              queryProductLike.products.isEmpty
                  ? noProduct()
                  : SizedBox(
                      height: imageSize,
                      child: Obx(() => ListView.separated(
                          controller: _scrollController,
                          scrollDirection: Axis.horizontal,
                          itemCount: queryProductLike.products.length,
                          separatorBuilder: (context, index) => const SizedBox(width: 10),
                          itemBuilder: (context, index) {
                            var product = queryProductLike.products[index].data();
                            if (product == null) {
                              throw Exception('Product is NULL!!!');
                            }
                            return GestureDetector(
                              onTap: () {
                                onListItemTap(context, product);
                              },
                              child: Hero(
                                tag: widget.tagHeroPrefix + product.docId.toString(),
                                child: Container(
                                  width: imageSize,
                                  height: imageSize,
                                  color: ZConfig.zzin_white,
                                  child: ClipRRect(
                                    borderRadius: BorderRadius.circular(10),
                                    child: CachedNetworkImage(
                                      fadeInDuration: const Duration(milliseconds: 0),
                                      fadeOutDuration: const Duration(milliseconds: 0),
                                      placeholderFadeInDuration: const Duration(milliseconds: 0),
                                      placeholder: (_, __) => Image.memory(kTransparentImage),
                                      imageUrl: product.thumbnail,
                                      fit: BoxFit.contain,
                                      errorWidget: (context, url, error) => const Icon(Icons.error),
                                      cacheManager: ZCacheManager.instance,
                                    ),
                                  ),
                                ),
                              ),
                            );
                          })),
                    ),
            ],
          );
        } else {
          return const SizedBox.shrink();
        }
      },
    );
  }

  Future<void> getProduct() async {
    await queryProductLike.queryProductBySort(3, saleStausIndex: ProductSaleStatus.ONSALE.index);
  }

  Future onRefresh() async {
    await queryProductLike.queryProductBySort(3, saleStausIndex: ProductSaleStatus.ONSALE.index);
  }

  void _scrollListener() async {
    // if (maxScroll - currentScroll <= delta && queryProductLike.isLoading == false) {
    //   await queryProductLike.queryProductBySort(3, byScroll: true);
    // }
  }

  void onListItemTap(BuildContext context, Product product) {
    var tag = DateTime.now().toString();
    var savedProduct = Get.put(Product.fromJson(product.toJson()), tag: tag);

    Get.to(
        () => ProductDetailView(
              tag: tag,
              tagHeroPrefix: widget.tagHeroPrefix,
            ),
        transition: Transition.fadeIn,
        duration: ZConfig.heroAnimationDuration);

    if (!savedProduct.isEqualValue(product)) {
      onRefresh();
    }

    Get.delete(tag: tag);
  }

  Widget noProduct() {
    return const Padding(
      padding: EdgeInsets.all(20.0),
      child: Text(
        '인기 상품이 없습니다',
        style: TextStyle(fontSize: 20, color: ZConfig.grey_text),
      ),
    );
  }

  void onMoreFavorite() {
    Get.to(() => PopularSearchPage(
          tag: widget.toString(),
          tagHeroPrefix: widget.tagHeroPrefix,
        ));
  }
}
