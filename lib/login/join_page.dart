import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:zzin/common/firestore/document_skima/user_account.dart';
import 'package:zzin/common/hive/brand_collection.dart';
import 'package:zzin/common/hive/forbidden_collection.dart';
import 'package:zzin/common/widget/zbutton_blue.dart';
import 'package:zzin/common/zconfig.dart';
import 'package:zzin/login/region_setting_page.dart';
import 'package:zzin/login/webbrowserpage.dart';

class JoinPage extends StatefulWidget {
  const JoinPage({Key? key}) : super(key: key);

  @override
  _JoinPageState createState() => _JoinPageState();
}

class _JoinPageState extends State<JoinPage> {
  bool isCheckRequired = false;
  bool isNicknameChecked = false;
  Color errorColor = Colors.red;
  String errorMessage = '';
  final TextEditingController _controller = TextEditingController();
  final FocusNode _node = FocusNode();

  late final CircleCheckBoxGroup requiredGroup;
  GlobalKey<_CircleCheckBoxGroupState> requiredGroupKey = GlobalKey();

  late final CircleCheckBoxGroup selectionGroup;
  GlobalKey<_CircleCheckBoxGroupState> selectionGroupKey = GlobalKey();

  ZUserAccount? userAccount;

  @override
  void initState() {
    super.initState();

    requiredGroup = CircleCheckBoxGroup(
      '[필수] 만 14세 이상이며 모두 동의 합니다.',
      const ['이용 약관', '개인정보 처리방침'],
      const ['이용약관', '개인정보 처리 방침'],
      onRefresh,
      key: requiredGroupKey,
    );
    selectionGroup = CircleCheckBoxGroup(
      '[선택] 광고성 정보 수신에 모두 동의합니다.',
      const ['앱 푸시', '문자 메세지', '이메일'],
      const [],
      onRefresh,
      key: selectionGroupKey,
    );

    if (Get.arguments != null) {
      userAccount = Get.arguments;
    } else {
      userAccount = ZUserAccount.initZero();
    }
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
          resizeToAvoidBottomInset: false,
          appBar: AppBar(
            title: const Text('회원가입', style: TextStyle(fontSize: ZSize.appBarTextSize)),
          ),
          bottomNavigationBar: Padding(
            padding: ZSize.bottomNavigationBarButtonOutPadding,
            child: ZButtonYellow(
                isSelected:
                    (isNicknameChecked && mounted && requiredGroupKey.currentState!.isCheckGroup),
                text: '다음 단계',
                fontSize: ZSize.BNB_ButtonFontSize,
                padding: ZSize.bottomNavigationBarButtonInPadding,
                onPressed:
                    isNicknameChecked && mounted && requiredGroupKey.currentState!.isCheckGroup
                        ? onComplete
                        : () {}),
          ),
          body: Padding(
            padding: const EdgeInsets.fromLTRB(20, 10, 20, 20),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                const Padding(
                  padding: EdgeInsets.fromLTRB(5, 0, 10, 10),
                  child: Text(
                    '닉네임',
                    style: TextStyle(fontSize: 22),
                  ),
                ),
                ClipRRect(
                  borderRadius: BorderRadius.circular(10),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: [
                      Expanded(
                        flex: 6,
                        child: TextField(
                          textInputAction: TextInputAction.done,
                          focusNode: _node,
                          controller: _controller,
                          decoration: const InputDecoration(
                            border: InputBorder.none,
                            enabledBorder: InputBorder.none,
                            focusedBorder: InputBorder.none,
                            disabledBorder: InputBorder.none,
                            fillColor: ZConfig.grey_border,
                            filled: true,
                          ),
                          onChanged: (value) {
                            isNicknameChecked = false;
                            setErrorMessage('', Colors.red);
                            setState(() {});
                          },
                        ),
                      ),
                      Expanded(
                        flex: 2,
                        child: ColoredBox(
                          color: ZConfig.grey_border,
                          child: Padding(
                            padding: const EdgeInsets.fromLTRB(0, 0, 10, 0),
                            child: ZButtonYellow(
                              text: '중복확인',
                              isSelected: _controller.text.isNotEmpty,
                              padding: MaterialStateProperty.all(const EdgeInsets.all(0.0)),
                              onPressed: () => checkNickname(),
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.fromLTRB(15, 5, 0, 0),
                  child: Text(
                    errorMessage,
                    style: TextStyle(fontSize: 14, color: errorColor),
                  ),
                ),
                requiredGroup,
                selectionGroup,
              ],
            ),
          )),
    );
  }

  void checkNickname() async {
    var inputString = _controller.text.trim();
    if (inputString.length < 2) {
      setErrorMessage('닉네임은 2자 이상 입력해 주세요.', Colors.red);
      return;
    }
    if (inputString.length > 10) {
      setErrorMessage('닉네임은 10자 이하로 입력해 주세요.', Colors.red);
      return;
    }

    if (!ZConfig.regExpNickname.hasMatch(inputString)) {
      setErrorMessage('닉네임은 공백없이 한글, 영문, 숫자만 가능합니다.', Colors.red);
      return;
    }

    if (inputString.contains('찐마켓') || inputString.toLowerCase().contains('zzinmarket')) {
      setErrorMessage('사용할 수 없는 닉네임 입니다.', Colors.red);
      return;
    }

    if (ForbiddenCollection.to.values
        .any((forbidden) => forbidden.word == inputString.toLowerCase())) {
      setErrorMessage('사용할 수 없는 닉네임 입니다.', Colors.red);
      return;
    }

    if (BrandCollection.to.values.any((brand) {
      if (brand.nameKr.toLowerCase().contains(inputString.toLowerCase()) ||
          brand.nameEn.toLowerCase().contains(inputString.toLowerCase())) {
        return true;
      } else {
        return false;
      }
    })) {
      setErrorMessage('사용할 수 없는 닉네임 입니다.', Colors.red);
      return;
    }

    var users = FirebaseFirestore.instance.collection(ZConfig.userCollection);

    await users.where('nickname', isEqualTo: inputString).get().then((value) {
      if (value.size > 0) {
        setErrorMessage('이미 사용중인 닉네임 입니다.', Colors.red);
      } else {
        if (userAccount != null) {
          userAccount!.nickname = inputString;
          isNicknameChecked = true;
          setErrorMessage('사용 가능한 닉네임입니다.', ZConfig.zzin_pointColor);
          _node.unfocus();
        }
      }
    });
  }

  void setErrorMessage(String message, Color color) {
    errorMessage = message;
    errorColor = color;
    setState(() {});
  }

  void onComplete() async {
    var agreeAdsInfo = List.generate(selectionGroupKey.currentState!.checkList.length, (index) {
      switch (index) {
        case 0:
          return {
            'push': {
              'agree': selectionGroupKey.currentState!.checkList[index].currentState!.isCheck,
              'date': DateTime.now()
            }
          };
        case 1:
          return {
            'sms': {
              'agree': selectionGroupKey.currentState!.checkList[index].currentState!.isCheck,
              'date': DateTime.now()
            }
          };
        case 2:
          return {
            'email': {
              'agree': selectionGroupKey.currentState!.checkList[index].currentState!.isCheck,
              'date': DateTime.now()
            }
          };
        default:
      }
    });
    if (agreeAdsInfo.isEmpty) {
      Get.back(result: null);
    } else {
      userAccount!.agreeAdsInfo = agreeAdsInfo;
      // Get.back(result: args);
      var result = await Get.to(() => const RegionSettingPage(),
          transition: Transition.rightToLeftWithFade, arguments: userAccount);

      if (result != null && result is ZUserAccount) {
        Get.back(result: result);
      }
      /* else {
        Get.rawSnackbar(title: '로그인 문제 발생', message: '다시 로그인하십시오');
      } */
    }
  }

  void onRefresh() {
    setState(() {});
  }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
class CircleCheckBoxGroup extends StatefulWidget {
  final String mainText;
  final List<String> subTexts;
  final List<String?> subLinks;
  final GestureTapCallback onRefresh;
  const CircleCheckBoxGroup(this.mainText, this.subTexts, this.subLinks, this.onRefresh, {Key? key})
      : super(key: key);

  @override
  _CircleCheckBoxGroupState createState() => _CircleCheckBoxGroupState();
}

class _CircleCheckBoxGroupState extends State<CircleCheckBoxGroup> {
  bool isCheck = false;
  late CircleCheckBox mainCheckbox;
  GlobalKey<_CircleCheckBoxState> mainCheckboxKey = GlobalKey();
  bool isCallapsed = true;

  List<CircleCheckBox> subCheckBoxList = [];
  List<GlobalKey<_CircleCheckBoxState>> subCheckBoxKeyList = [];
  List<bool> subChecks = [];

  @override
  void initState() {
    super.initState();
    mainCheckbox = CircleCheckBox(iconSize: 18, onTap: onTapMain, key: mainCheckboxKey);

    List.generate(widget.subTexts.length, (index) {
      subCheckBoxKeyList.add(GlobalKey());
      subCheckBoxList.add(CircleCheckBox(
        onTap: onTapSub,
        key: subCheckBoxKeyList[index],
      ));
    });
  }

  var radius = Checkbox.width * 1.414;

  bool get isCheckGroup => mainCheckboxKey.currentState!.isCheck;

  List<GlobalKey<_CircleCheckBoxState>> get checkList => subCheckBoxKeyList;

  @override
  Widget build(BuildContext context) {
    return Column(children: [
      Row(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          mainCheckbox,
          Flexible(
            child: Text(widget.mainText,
                overflow: TextOverflow.ellipsis, style: const TextStyle(fontSize: 14)),
          ),
          IconButton(
              icon: isCallapsed ? const Icon(Icons.add) : const Icon(Icons.remove),
              onPressed: () {
                setState(() {
                  isCallapsed = !isCallapsed;
                });
              }),
        ],
      ),
      Padding(
        padding: const EdgeInsets.fromLTRB(20, 0, 10, 0),
        child: Visibility(
          maintainState: true,
          visible: !isCallapsed,
          child: Column(
            children: List<Widget>.generate(widget.subTexts.length, (index) {
              return Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Expanded(
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        subCheckBoxList[index],
                        Flexible(
                          child: Text(widget.subTexts[index],
                              overflow: TextOverflow.ellipsis,
                              style: const TextStyle(fontSize: 14)),
                        ),
                      ],
                    ),
                  ),
                  widget.subLinks.isNotEmpty && widget.subLinks[index] != null
                      ? GestureDetector(
                          onTap: () async {
                            onTapWebview(index);
                          },
                          child: const Padding(
                            padding: EdgeInsets.only(right: 20),
                            child: Text('내용 보기',
                                style: TextStyle(
                                    decoration: TextDecoration.underline,
                                    color: ZConfig.zzin_pointColor,
                                    fontSize: 14)),
                          ),
                        )
                      : const SizedBox.shrink(),
                ],
              );
            }),
          ),
        ),
      ),
      // SizedBox(height: 20)
    ]);
  }

  void onTapWebview(int index) async {
    final url = index == 0 ? ZConfig.termsAndConditions : ZConfig.privacyPolicy;
    await Get.to(() => WebBrowserPage(url, index == 0 ? '이용 약관' : '개인정보 처리방침'),
        transition: Transition.rightToLeftWithFade);
  }

  void onTapMain() {
    setState(() {
      isCheck = !isCheck;
      mainCheckboxKey.currentState!.setCheck(isCheck);

      for (var e in subCheckBoxKeyList) {
        e.currentState!.setCheck(isCheck);
      }
      widget.onRefresh();
    });
  }

  void onTapSub() {
    setState(() {
      if (subCheckBoxKeyList.every((element) => element.currentState!.isCheck)) {
        isCheck = true;
        mainCheckboxKey.currentState!.setCheck(isCheck);
      } else {
        isCheck = false;
        mainCheckboxKey.currentState!.setCheck(isCheck);
      }
    });
  }
}

class CircleCheckBox extends StatefulWidget {
  final double iconSize;

  final GestureTapCallback onTap;

  const CircleCheckBox({this.iconSize = 16, required this.onTap, Key? key}) : super(key: key);

  @override
  _CircleCheckBoxState createState() => _CircleCheckBoxState();
}

class _CircleCheckBoxState extends State<CircleCheckBox> {
  bool _isCheck = false;

  bool get isCheck => _isCheck;
  void setCheck(bool isCheck) {
    setState(() {
      _isCheck = isCheck;
    });
  }

  _CircleCheckBoxState();

  @override
  Widget build(BuildContext context) {
    return Center(
      child: GestureDetector(
        onTap: () {
          setState(() {
            _isCheck = !_isCheck;
          });
          widget.onTap();
        },
        child: Container(
          decoration: const BoxDecoration(shape: BoxShape.circle, color: ZConfig.zzin_white),
          child: Padding(
            padding: _isCheck
                ? const EdgeInsets.fromLTRB(12, 5, 12, 5)
                : const EdgeInsets.fromLTRB(10, 5, 10, 5),
            child: _isCheck
                ? Icon(Icons.check_circle, size: widget.iconSize, color: ZConfig.zzin_pointColor)
                : Icon(Icons.radio_button_unchecked_outlined,
                    size: widget.iconSize + 4, color: ZConfig.grey_border),
          ),
        ),
      ),
    );
  }
}
