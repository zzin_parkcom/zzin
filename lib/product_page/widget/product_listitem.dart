import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:zzin/common/zconfig.dart';
import 'package:zzin/product_page/widget/product_listitem_detail.dart';

class ProductListItem extends StatelessWidget {
  const ProductListItem({
    Key? key,
    required this.goodsImage,
    required this.goodsTitle,
    required this.goodsPrice,
    required this.address,
    required this.registerDateTime,
    required this.favoriteCount,
    required this.productStatus,
  }) : super(key: key);

  final Widget goodsImage;
  final String goodsTitle;
  final int goodsPrice;
  final String address;
  final DateTime registerDateTime;
  final int favoriteCount;
  final int productStatus;

  final double heightListTile = 150;
  final double verticalPadding = 10;

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: heightListTile + (Get.mediaQuery.textScaleFactor >= 1.5 ? 80 : 0),
      child: Padding(
        padding: const EdgeInsets.fromLTRB(10, 3, 10, 3),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Container(
              width: heightListTile - 2 * verticalPadding,
              height: heightListTile - 2 * verticalPadding,
              color: ZConfig.zzin_background,
              child: ClipRRect(borderRadius: BorderRadius.circular(10), child: goodsImage),
            ),
            Expanded(
              child: ProductListItemDetail(
                goodsTitle: goodsTitle,
                goodsPrice: goodsPrice,
                address: address,
                registerDateTime: registerDateTime,
                favoriteCount: favoriteCount,
                productStatus: productStatus,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
