import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:zzin/common/firestore/qeury_favorite.dart';
import 'package:zzin/common/zconfig.dart';
import 'package:zzin/product_page/widget/product_list_generator.dart';

class FavoriteList extends StatefulWidget {
  const FavoriteList({Key? key}) : super(key: key);

  @override
  _FavoriteListState createState() => _FavoriteListState();
}

class _FavoriteListState extends State<FavoriteList> {
  final ScrollController _scrollController = ScrollController();
  late final QueryFavorite queryFavorite;

  @override
  void initState() {
    super.initState();
    queryFavorite = Get.put(QueryFavorite(), tag: widget.toString());
    _scrollController.addListener(_scrollListener);
  }

  @override
  void dispose() {
    _scrollController.removeListener(_scrollListener);
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: getFavoriteList(),
      builder: (context, snapshot) {
        if (snapshot.connectionState == ConnectionState.done) {
          return ProductListGenerator(
            products: queryFavorite.products,
            onRefresh: onRefresh,
            scrollController: _scrollController,
            onDetailPage: null,
            tagHeroPrefix: 'Favorite_',
            noProductMessages: const ['찜한 상품이 없습니다.', '하트를 눌러 찐 목록을 만들어 보세요'],
          );
        } else {
          return const SizedBox.shrink();
        }
      },
    );
  }

  Future<bool> getFavoriteList() async {
    var result = queryFavorite.queryFavorite();
    return result;
  }

  Future onRefresh() async {
    await queryFavorite.queryFavorite();
  }

  void _scrollListener() async {
    var maxScroll = _scrollController.position.maxScrollExtent;
    var currentScroll = _scrollController.position.pixels;
    var delta = maxScroll * ZConfig.scrollRatioQueryMore;

    if (maxScroll - currentScroll <= delta && queryFavorite.isLoading == false) {
      await queryFavorite.queryFavorite(byScroll: true);
    }
  }
}
