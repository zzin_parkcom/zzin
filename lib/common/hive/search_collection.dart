import 'package:hive/hive.dart';
import 'package:zzin/common/hive/zhive.dart';
import 'package:zzin/common/zconfig.dart';

part 'search_collection.g.dart';

@HiveType(typeId: 6)
class SearchCollection extends HiveObject {
  @HiveField(0)
  String searchString;
  @HiveField(1)
  int brandId;
  @HiveField(2)
  String brandName;
  @HiveField(3)
  int categoryId;
  @HiveField(4)
  String categoryName;
  @HiveField(5)
  int addressId;
  @HiveField(6)
  int addressStep;
  @HiveField(7)
  String addressName;
  @HiveField(8)
  int status;
  @HiveField(9)
  int saleStatus;
  @HiveField(10)
  List<String> recentKeyword;
  @HiveField(11)
  int sort;

  SearchCollection(
      {required this.searchString,
      required this.brandId,
      required this.brandName,
      required this.categoryId,
      required this.categoryName,
      required this.addressId,
      required this.addressStep,
      required this.addressName,
      required this.status,
      required this.saleStatus,
      required this.recentKeyword,
      required this.sort});

  static Box<SearchCollection> get to => ZHive.searchBox;
  static SearchCollection get instance => ZHive.searchBox.get(0)!;

  void initialize() {
    searchString = '';
    brandId = NotSelected;
    brandName = '브랜드 선택';
    categoryId = NotSelected;
    categoryName = '카테고리 선택';
    addressId = NotSelected;
    addressStep = NotSelected;
    addressName = '검색지역 선택';
    status = AllItems;
    saleStatus = AllItems;
    sort = 0; // 정확도 검색 기본 설정.

    save();
  }
}
