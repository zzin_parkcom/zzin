import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:zzin/common/zconfig.dart';

class CategoryTab extends StatefulWidget {
  const CategoryTab({Key? key}) : super(key: key);

  @override
  _CategoryTabState createState() => _CategoryTabState();
}

class _CategoryTabState extends State<CategoryTab> {
  static final List<String> _buttonList = [
    ' ALL ',
    ' BAGS ',
    ' CLOTHES ',
    ' SHOES ',
    ' WATCHES ',
    ' ACCESORIES '
  ];
  final List<bool> _isSelectedList = List.generate(_buttonList.length, (index) {
    return index == 0 ? true : false;
  });

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SizedBox(
        height: 45.0,
        child: ListView.builder(
          itemBuilder: (context, index) {
            return TextButton(
              style: _isSelectedList[index]
                  ? ButtonStyle(
                      padding: MaterialStateProperty.all(const EdgeInsets.fromLTRB(5, 5, 5, 0)),
                      foregroundColor: MaterialStateProperty.all<Color>(ZConfig.zzin_black))
                  : ButtonStyle(
                      padding: MaterialStateProperty.all(const EdgeInsets.fromLTRB(5, 5, 5, 0)),
                      foregroundColor: MaterialStateProperty.all<Color>(ZConfig.grey_text)),
              onPressed: _isSelectedList[index]
                  ? null
                  : () {
                      searchByCategory(context, index);
                    },
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: [
                  Text(
                    _buttonList[index],
                    style: const TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
                  ),
                  SizedBox(
                    height: 3,
                    child: Text(
                      _buttonList[index],
                      style: _isSelectedList[index]
                          ? const TextStyle(
                              fontSize: 18,
                              fontWeight: FontWeight.bold,
                              backgroundColor: ZConfig.zzin_black)
                          : const TextStyle(
                              color: ZConfig.zzin_white,
                              fontSize: 18,
                              fontWeight: FontWeight.bold,
                              backgroundColor: ZConfig.zzin_white),
                    ),
                  )
                ],
              ),
            );
          },
          scrollDirection: Axis.horizontal,
          itemCount: _buttonList.length,
        ));
  }

  void searchByCategory(BuildContext context, int index) {
    //버튼 그룹 처리.
    setState(() {
      for (var buttonIndex = 0; buttonIndex < _isSelectedList.length; buttonIndex++) {
        if (buttonIndex == index) {
          _isSelectedList[buttonIndex] = true;
        } else {
          _isSelectedList[buttonIndex] = false;
        }
      }
    });
  }
}
