// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'search_collection.dart';

// **************************************************************************
// TypeAdapterGenerator
// **************************************************************************

class SearchCollectionAdapter extends TypeAdapter<SearchCollection> {
  @override
  final int typeId = 6;

  @override
  SearchCollection read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return SearchCollection(
      searchString: fields[0] as String,
      brandId: fields[1] as int,
      brandName: fields[2] as String,
      categoryId: fields[3] as int,
      categoryName: fields[4] as String,
      addressId: fields[5] as int,
      addressStep: fields[6] as int,
      addressName: fields[7] as String,
      status: fields[8] as int,
      saleStatus: fields[9] as int,
      recentKeyword: (fields[10] as List).cast<String>(),
      sort: fields[11] as int,
    );
  }

  @override
  void write(BinaryWriter writer, SearchCollection obj) {
    writer
      ..writeByte(12)
      ..writeByte(0)
      ..write(obj.searchString)
      ..writeByte(1)
      ..write(obj.brandId)
      ..writeByte(2)
      ..write(obj.brandName)
      ..writeByte(3)
      ..write(obj.categoryId)
      ..writeByte(4)
      ..write(obj.categoryName)
      ..writeByte(5)
      ..write(obj.addressId)
      ..writeByte(6)
      ..write(obj.addressStep)
      ..writeByte(7)
      ..write(obj.addressName)
      ..writeByte(8)
      ..write(obj.status)
      ..writeByte(9)
      ..write(obj.saleStatus)
      ..writeByte(10)
      ..write(obj.recentKeyword)
      ..writeByte(11)
      ..write(obj.sort);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is SearchCollectionAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}
