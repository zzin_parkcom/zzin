import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:zzin/common/firestore/document_skima/product.dart';
import 'package:zzin/common/firestore/queryvalue.dart';

abstract class QueryBase {
  List<DocumentSnapshot<Product>> products = [];
  QueryValue queryValue = QueryValue();

  // For QueryProduct
  Future<void> queryProductBySort(int sortIndex, {bool byScroll = false});
  Future<void> queryProductDetail(
      {required String keyword,
      required int categoryIndex,
      required int brandIndex,
      required int statusIndex,
      required int saleStausIndex,
      required int sortIndex,
      required int locationScope,
      required List<int>? locationList,
      required bool byScroll});

  // For QueryUserProduct
  Future<void> queryUserProduct(DocumentReference seller, int saleStatusIndex,
      {bool byScroll = false});
}
