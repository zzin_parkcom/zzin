import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/material.dart';
import 'package:zzin/common/zconfig.dart';

class OpenTalkChat extends StatefulWidget {
  const OpenTalkChat({Key? key}) : super(key: key);

  @override
  _OpenTalkChatState createState() => _OpenTalkChatState();
}

class _OpenTalkChatState extends State<OpenTalkChat> {
  late DatabaseReference openrunRef;

  @override
  void initState() {
    openrunRef = ZConfig.database.reference().child('openrun');
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      // child: FutureBuilder(
      //   future: getTest(),
      //   builder: (context, snapshot) {
      //     if (snapshot.connectionState == ConnectionState.done) {
      //       readData();
      //       // if (queryChatRoom.chatrooms.isEmpty) {
      //       //   return noChatRoom();
      //       // }
      //       // return RefreshIndicator(
      //       //   onRefresh: () async {
      //       //     await onRefresh(userRef: ZConfig.userAccount.docId, isSeller: false);
      //       //   },
      //       //   child: ListView.separated(
      //       //     separatorBuilder: (context, index) => const Divider(),
      //       //     padding: const EdgeInsets.all(0),
      //       //     physics: const BouncingScrollPhysics(parent: AlwaysScrollableScrollPhysics()),
      //       //     controller: _scrollController,
      //       //     itemCount: queryChatRoom.chatrooms.length,
      //       //     itemBuilder: (context, index) {
      //       //       var chatRoom = queryChatRoom.chatrooms[index].data()!;
      //       //       return ChatRoomListTile(chatRoom, index);
      //       //     },
      //       //   ),
      //       // );
      //       return Text('ddddd');
      //     } else {
      //       return const SizedBox.shrink();
      //     }
      //   },
      // ),
      child: StreamBuilder(
          stream: openrunRef.orderByChild('age').onValue,
          builder: (BuildContext context, snap) {
            if (snap.hasData) {
              Map data = (snap.data as Event).snapshot.value;
              data.forEach((key, value) {
                print('$key : $value');
              });

              return Text((snap.data as Event).snapshot.value.toString());
            } else if (snap.hasError) {
              return const Text('에러');
            } else {
              return const Text('몰라');
            }
          }),
    );
  }

  Future getTest() async {
    await ZConfig.database.goOnline();

    print(DateTime.now().toString());
    for (int i = 0; i < 10000; i++) {
      print(i.toString());
      await openrunRef
          .push()
          .set({'name': 'p$i', 'age': i, 'creationDateTime': ServerValue.timestamp});
    }
    print(DateTime.now().toString());
  }

  void readData() {
    print('readData');
    // openrunRef.once().then((DataSnapshot snapshot) {
    //   print('Data : ${snapshot.value}');
    // });

    Query query = openrunRef.orderByChild('age').limitToFirst(2);
    query.once().then((snapshot) {
      Map dd = snapshot.value;
      dd.forEach((key, value) {
        Map ff = value;
        ff.forEach((key, value) {
          print('$key : $value');
        });
      });
    });

    openrunRef.orderByChild('age').startAt(37).once().then((snapshot) {
      Map dd = snapshot.value;
      dd.forEach((key, value) {
        print('---------------------------');
        Map ff = value;
        ff.forEach((key, value) {
          print('$key : $value');
        });
      });
    });

    // openrunRef.orderByChild('age').startAt(value)
  }
}
