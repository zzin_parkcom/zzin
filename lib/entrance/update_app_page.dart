import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:new_version/new_version.dart';

class UpdateAppPage extends StatefulWidget {
  final NewVersion newVersion;
  final VersionStatus status;
  const UpdateAppPage({Key? key, required this.newVersion, required this.status}) : super(key: key);

  @override
  _UpdateAppPageState createState() => _UpdateAppPageState();
}

class _UpdateAppPageState extends State<UpdateAppPage> {
  @override
  void initState() {
    super.initState();

    widget.newVersion.showUpdateDialog(
      context: Get.context!,
      versionStatus: widget.status,
      dialogTitle: '업데이트 알림',
      dialogText: '보다 나은 찐마켓 이용을 위해 지금 바로 업데이트 해 주세요.',
      updateButtonText: '지금 업데이트',
      allowDismissal: false,
      // dismissButtonText: 'Custom dismiss button text',
      dismissAction: () => {},
    );
  }

  @override
  Widget build(BuildContext context) {
    return Container();
  }
}


// class UpdateAppPage extends StatelessWidget {
//   final NewVersion newVersion;
//   final VersionStatus status;
//   const UpdateAppPage({Key? key, required this.newVersion, required this.status}) : super(key: key);

//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       body: Padding(
//         padding: const EdgeInsets.all(1.0),
//         child: Center(
//           child: Column(
//             mainAxisAlignment: MainAxisAlignment.center,
//             children: [
//               Icon(
//                 Icons.wifi_off,
//                 size: 100,
//                 color: ZConfig.grey_text,
//               ),
//               Text(
//                 '인터넷 끊김',
//                 textAlign: TextAlign.center,
//                 style: TextStyle(fontSize: 20, color: ZConfig.grey_text),
//               ),
//               Text(
//                 '네트워크 상태를 확인해 주세요',
//                 textAlign: TextAlign.center,
//                 style: TextStyle(fontSize: 14, color: ZConfig.grey_text),
//               ),
//               TextButton(onPressed: onUpdate, child: Text('update'))
//             ],
//           ),
//         ),
//       ),
//     );
//   }

//   void onUpdate() {
//     newVersion.showUpdateDialog(
//       context: Get.context!,
//       versionStatus: status,
//       dialogTitle: '업데이트 알림',
//       dialogText: '보다 나은 찐마켓 이용을 위해 지금 바로 업데이트 해 주세요.',
//       updateButtonText: '지금 업데이트',
//       allowDismissal: false,
//       // dismissButtonText: 'Custom dismiss button text',
//       dismissAction: () => {},
//     );
//   }
// }
