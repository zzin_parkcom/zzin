import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:zzin/common/firestore/document_skima/product.dart';
import 'package:zzin/common/zconfig.dart';
import 'package:get/get.dart';

class QueryRecent extends GetxController {
  List<DocumentSnapshot<Product>> products = <DocumentSnapshot<Product>>[].obs;
  DocumentSnapshot? lastDocument;
  bool isLoading = false; // track if products fetching
  bool hasMore = true; // flag for more products available or not
  int documentLimit = ZConfig.maxDocumentCount; // documents to

  static QueryRecent get to => Get.find();

  Future<void> queryRecent({bool isFavorite = true, bool byScroll = false}) async {
    clearProduct();

    var recentList = ZConfig.userAccount.recentList;
    if (recentList.isEmpty) {
      return;
    }
    Query query = FirebaseFirestore.instance
        // .collectionGroup(ZConfig.productGroupCollection)
        .collection(ZConfig.productsCollection)
        .where('articleStatus', isEqualTo: 0)
        .where('docId', whereIn: recentList.toList())
        .withConverter<Product>(
            fromFirestore: (snapshots, _) => Product.fromJson(snapshots.data()!),
            toFirestore: (product, _) => product.toJson());

    // 쿼리 실행.
    try {
      var snapshots = await query
          .withConverter<Product>(
              fromFirestore: (snapshots, _) => Product.fromJson(snapshots.data()!),
              toFirestore: (product, _) => product.toJson())
          .get();
      print('Recent 검색된 갯수: ${snapshots.docs.length}');

      if (snapshots.docs.length < documentLimit) {
        hasMore = false;
      }

      if (snapshots.docs.isNotEmpty) {
        lastDocument = snapshots.docs[snapshots.docs.length - 1];

        var reversedRecentList = recentList.reversed.toList();
        var tempProducts = snapshots.docs;
        for (var i = 0; i < recentList.length; i++) {
          for (var element in tempProducts) {
            if (element.data().docId == reversedRecentList[i]) {
              products.add(element);

              if (ZConfig.userAccount.getSocial() != null) {
                products.removeWhere((product) => ZConfig.blockUsers
                    .any((element) => element.userDocId == product.data()!.sellerId));
              }
            }
          }
        }

        // products.addAll(snapshots.docs);

        print('Recent 상품리스트 갯수: ${products.length}');

        // notifyListeners();
        isLoading = false;
      }
    } catch (e) {
      print('복합 인덱스가 필요한지 보세요~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~');
      print(e);
    }
  }

  void clearProduct() {
    products.clear();
    lastDocument = null;
    isLoading = false;
    hasMore = true;
  }
}
