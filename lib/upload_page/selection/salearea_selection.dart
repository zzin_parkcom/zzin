import 'package:flutter/material.dart';
import 'package:zzin/common/hive/address_collection.dart';
import 'package:zzin/common/hive/upload_collection.dart';
import 'package:zzin/common/utils/upload_utils.dart';
import 'package:zzin/common/zconfig.dart';
import 'package:zzin/common/zutils.dart';
import 'package:zzin/upload_page/customFormfield/custom_formfield.dart';
import 'package:zzin/upload_page/salearea_search_page.dart';

class SaleAreaSelection extends StatefulWidget {
  final UploadCollection uploadCollection;
  final String defaultName = '거래지역 선택';
  final double fontSize;

  const SaleAreaSelection(this.fontSize, {required this.uploadCollection, Key? key})
      : super(key: key);

  @override
  _SaleAreaSelectionState createState() => _SaleAreaSelectionState();
}

class _SaleAreaSelectionState extends State<SaleAreaSelection> {
  var id = NotSelected;
  var name = Empty;

  @override
  Widget build(BuildContext context) {
    id = widget.uploadCollection.saleAreaId;
    name = widget.uploadCollection.saleAreaName;
    if (id == NotSelected) {
      id = ZConfig.userAccount.regionCode!;
      name = ZConfig.userAccount.regionName!;
      widget.uploadCollection.saleAreaId = id;
      widget.uploadCollection.saleAreaName = name;
    }

    return CustomFormField(
      initialValue: id,
      validator: validator,
      onSaved: onSaved,
      autovalidateMode: AutovalidateMode.disabled,
      builder: listTileBuilder,
    );
  }

  Widget listTileBuilder(FormFieldState<int> state) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        ListTile(
          title: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(name, style: normalStyle(id == Cancel, widget.fontSize)),
              state.hasError
                  ? Text(
                      state.errorText!,
                      style: errorStyle(widget.fontSize),
                    )
                  : const SizedBox.shrink(),
            ],
          ),
          trailing: const Icon(Icons.expand_more),
          onTap: () async {
            // var idReturn =
            //     await Get.to(() => SaleareaSearchPage(), transition: Transition.downToUp);
            var idReturn = await showModalBottomSheetInt(context, _saleareaBottomSheetContent);

            if (idReturn != Cancel) {
              id = idReturn;
              name = AddressCollection.getRegionName(id);
              widget.uploadCollection.saleAreaId = id;
              widget.uploadCollection.saleAreaName = AddressCollection.getRegionName(id);
              state.didChange(id);
            }
          },
        ),
      ],
    );
  }

  String? validator(int? value) {
    return id == Cancel ? '거래지역을 선택해 주세요' : null;
  }

  void onSaved(int? value) {}

  Widget _saleareaBottomSheetContent(BuildContext context) {
    return const SaleareaSearchPage();
  }
}
