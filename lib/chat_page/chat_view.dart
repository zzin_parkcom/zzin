import 'package:flutter/material.dart';
// ignore: unused_import
import 'package:get/get.dart';
import 'package:zzin/chat_page/myinquiry_chat_listview.dart';
import 'package:zzin/chat_page/onsale_chat_listview.dart';
import 'package:zzin/common/firestore/document_skima/product.dart';
import 'package:zzin/common/widget/appbar/zappbar.dart';
import 'package:zzin/common/widget/ztab.dart';
import 'package:zzin/common/zconfig.dart';
import 'package:zzin/entrance/skeleton.dart';

class ChatView extends StatelessWidget {
  final Product? product;
  final List<Tab> tabs = [];
  final List<Widget> tabViews = [];
  ChatView({this.product, Key? key}) : super(key: key) {
    final List<Tab> tempTabs = product == null
        ? [
            const Tab(
              child: Text('내가 판매중인 상품', style: TextStyle(fontSize: 18)),
            ),
            const Tab(child: Text('내가 관심있는 상품', style: TextStyle(fontSize: 18))),
          ]
        : [
            Tab(
              child: Text('상품:' + product!.title, style: const TextStyle(fontSize: 18)),
            )
          ];
    tabs.addAll(tempTabs);

    final List<Widget> tempTabViews = product == null
        ? [const OnsaleChatListView(), const MyInquiryChatListView()]
        : [OnsaleChatListView(product: product)];
    tabViews.addAll(tempTabViews);
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      children: [
        zAppBar(),
        Expanded(
          child: ZTab(
            keyString: MainTabController.to.tabIndex.value.toString() + toString(),
            tabs: tabs,
            tabViews: tabViews,
          ),
        )
        // Divider(),
      ],
    );
  }

  Widget zAppBar() => ZAppBar(
        title: Text(product == null ? '채팅' : '거래문의 리스트',
            style: const TextStyle(color: ZConfig.zzin_black, fontSize: ZSize.appBarTextSize)),
        // actions: [
        //   IconButton(onPressed: () {}, icon: Icon(Icons.search, color: ZConfig.zzin_white)),
        //   // IconButton(onPressed: () {}, icon: Icon(Icons.alarm)),
        // ],
      );
}
