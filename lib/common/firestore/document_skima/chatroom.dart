import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:zzin/common/firestore/document_skima/chat_message.dart';
import 'package:zzin/common/zconfig.dart';

class ChatRoom {
  DocumentReference buyerRef;
  DocumentReference sellerRef;
  DocumentReference productRef;
  DocumentReference receiverRef;
  DocumentReference? lastMessageRef;
  Timestamp? lastMessageTime;

  bool isOnlineSeller = false;
  bool isOnlineBuyer = false;

  bool isNotiOnSeller = true;
  bool isNotiOnBuyer = true;

  bool isLeaveRoomSeller = false;
  bool isLeaveRoomBuyer = false;
  String pushTokenSeller = '';
  String pushTokenBuyer = '';

  ChatRoom({
    required this.buyerRef,
    required this.sellerRef,
    required this.productRef,
    required this.receiverRef,
    this.lastMessageRef,
    this.lastMessageTime,
    this.isOnlineSeller = false,
    this.isOnlineBuyer = false,
    this.isNotiOnSeller = true,
    this.isNotiOnBuyer = true,
    this.isLeaveRoomSeller = false,
    this.isLeaveRoomBuyer = false,
    this.pushTokenSeller = '',
    this.pushTokenBuyer = '',
  });

  ChatRoom.fromJson(Map<String, Object?> json)
      : this(
          buyerRef: json['buyerRef'] as DocumentReference,
          sellerRef: json['sellerRef'] as DocumentReference,
          productRef: json['productRef'] as DocumentReference,
          receiverRef: json['receiverRef'] as DocumentReference,
          lastMessageRef: json['lastMessageRef'] as DocumentReference,
          lastMessageTime: (json['lastMessageTime'] ?? Timestamp.now()) as Timestamp,
          isOnlineSeller: json['isOnlineSeller'] as bool,
          isOnlineBuyer: json['isOnlineBuyer'] as bool,
          isNotiOnSeller: json['isNotiOnSeller'] as bool,
          isNotiOnBuyer: json['isNotiOnBuyer'] as bool,
          isLeaveRoomSeller: json['isLeaveRoomSeller'] as bool,
          isLeaveRoomBuyer: json['isLeaveRoomBuyer'] as bool,
          pushTokenSeller: json['pushTokenSeller'] as String,
          pushTokenBuyer: json['pushTokenBuyer'] as String,
        );

  Map<String, Object?> toJson() {
    return {
      'buyerRef': buyerRef,
      'sellerRef': sellerRef,
      'productRef': productRef,
      'receiverRef': receiverRef,
      'lastMessageRef': lastMessageRef,
      'lastMessageTime': lastMessageTime,
      'isOnlineSeller': isOnlineSeller,
      'isOnlineBuyer': isOnlineBuyer,
      'isNotiOnSeller': isNotiOnSeller,
      'isNotiOnBuyer': isNotiOnBuyer,
      'isLeaveRoomSeller': isLeaveRoomSeller,
      'isLeaveRoomBuyer': isLeaveRoomBuyer,
      'pushTokenSeller': pushTokenSeller,
      'pushTokenBuyer': pushTokenBuyer,
    };
  }

////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////
  static Stream<QuerySnapshot<ChatRoom>> getMyInquiryRooms() {
    try {
      var snapshot = FirebaseFirestore.instance
          .collectionGroup(ZConfig.chatRoomsGroupCollection)
          .where('buyerRef', isEqualTo: ZConfig.userAccount.docId)
          .orderBy('lastMessageTime', descending: true)
          .withConverter<ChatRoom>(
              fromFirestore: (snapshot, _) => ChatRoom.fromJson(snapshot.data()!),
              toFirestore: (chatRoom, _) => chatRoom.toJson())
          .snapshots();
      return snapshot;
    } catch (e) {
      print(e);
    }
    throw Exception('MyInquiryRoom Exception');
  }

  static Stream<QuerySnapshot<ChatRoom>> getChatRooms({
    required sellerRef,
  }) {
    try {
      var snapshot = FirebaseFirestore.instance
          .collectionGroup(ZConfig.chatRoomsGroupCollection)
          .where('sellerRef', isEqualTo: sellerRef)
          .orderBy('lastMessageTime', descending: true)
          .withConverter<ChatRoom>(
              fromFirestore: (snapshot, _) => ChatRoom.fromJson(snapshot.data()!),
              toFirestore: (chatRoom, _) => chatRoom.toJson())
          .snapshots();

      return snapshot;
    } catch (e) {
      print(e);
    }
    throw Exception('ChatRoom Stream Exception');
  }

  static Future<DocumentReference<ChatRoom>> _getChatRoomDocRef({
    required buyerRef,
    required sellerRef,
    required productRef,
  }) async {
    try {
      var snapshot = await FirebaseFirestore.instance
          .collection(ZConfig.productsCollection)
          .where('docId', isEqualTo: productRef)
          .get();

      print('seller : ${sellerRef.path}');
      print('product : ${productRef.path}');

      print(snapshot.docs.length);

      // chatRooms 다큐먼트를 얻는다.
      DocumentReference<ChatRoom> chatRoomDoc = snapshot.docs.first.reference // 상품은 유일하므로. first;
          .collection(ZConfig.chatRoomsGroupCollection)
          .doc(buyerRef.id.toString())
          .withConverter<ChatRoom>(
              fromFirestore: (snapshot, _) => ChatRoom.fromJson(snapshot.data()!),
              toFirestore: (chatRoom, _) => chatRoom.toJson());

      return chatRoomDoc;
    } catch (e) {
      print(e);
    }
    throw Exception('ChatRoom Document Exception');
  }

  bool isMeSeller() {
    if (sellerRef == ZConfig.userAccount.docId) {
      return true;
    } else {
      return false;
    }
  }

  static Future<DocumentReference> _getChatRoomDocRefRaw({
    required buyerRef,
    required sellerRef,
    required productRef,
  }) async {
    try {
      var snapshot = await FirebaseFirestore.instance
          .collection(ZConfig.productsCollection)
          .where('docId', isEqualTo: productRef)
          .get();

      print('seller : ${sellerRef.path}');
      print('product : ${productRef.path}');

      print(snapshot.docs.length);

      // chatRooms 다큐먼트를 얻는다.
      var chatRoomDoc = snapshot.docs.first.reference // 상품은 유일하므로. first;
          .collection(ZConfig.chatRoomsGroupCollection)
          .doc(buyerRef.id.toString());
      return chatRoomDoc;
    } catch (e) {
      print(e);
    }

    throw Exception('ChatRoom Document Raw Exception');
  }

  Future<void> exitRoom() async {
    var chatRoomDoc =
        await _getChatRoomDocRef(buyerRef: buyerRef, sellerRef: sellerRef, productRef: productRef);

    var chatRoom = await chatRoomDoc.get();
    if (chatRoom.exists) {
      if (isMeSeller()) {
        isOnlineSeller = false;
        await chatRoomDoc.update({'isOnlineSeller': isOnlineSeller});
      } else {
        isOnlineBuyer = false;
        await chatRoomDoc.update({'isOnlineBuyer': isOnlineBuyer});
      }
    }
  }

  Future<void> enterRoom() async {
    var chatRoomDoc =
        await _getChatRoomDocRef(buyerRef: buyerRef, sellerRef: sellerRef, productRef: productRef);

    var chatRoom = await chatRoomDoc.get();
    if (chatRoom.exists) {
      if (isMeSeller()) {
        isOnlineSeller = true;
        await chatRoomDoc.update({'isOnlineSeller': isOnlineSeller});
      } else {
        isOnlineBuyer = true;
        await chatRoomDoc.update({'isOnlineBuyer': isOnlineBuyer});
      }
    }
  }

  Future<void> leaveRoom() async {
    var chatRoomDoc =
        await _getChatRoomDocRef(buyerRef: buyerRef, sellerRef: sellerRef, productRef: productRef);

    var chatRoom = await chatRoomDoc.get();
    if (chatRoom.exists) {
      if (isMeSeller()) {
        isLeaveRoomSeller = true;
        await chatRoomDoc.update({'isLeaveRoomSeller': isLeaveRoomSeller, 'isOnlineSeller': false});
      } else {
        isLeaveRoomBuyer = true;
        await chatRoomDoc.update({'isLeaveRoomBuyer': isLeaveRoomBuyer, 'isOnlineBuyer': false});
      }
    }
  }

  Future<ChatRoom> joinRoom() async {
    var chatRoomDoc =
        await _getChatRoomDocRef(buyerRef: buyerRef, sellerRef: sellerRef, productRef: productRef);

    var chatRoom = await chatRoomDoc.get();
    if (chatRoom.exists) {
      isLeaveRoomSeller = false;
      isLeaveRoomBuyer = false;
      await chatRoomDoc
          .update({'isLeaveRoomSeller': isLeaveRoomSeller, 'isLeaveRoomBuyer': isLeaveRoomBuyer});
    }
    return chatRoom.data()!;
  }

  Future<bool> isReceiverOnline() async {
    var chatRoomDoc =
        await _getChatRoomDocRef(buyerRef: buyerRef, sellerRef: sellerRef, productRef: productRef);

    var chatRoom = await chatRoomDoc.get();
    if (chatRoom.exists) {
      if (isMeSeller()) {
        return chatRoom.data()!.isOnlineBuyer;
      } else {
        return chatRoom.data()!.isOnlineSeller;
      }
    }
    return false;
  }

  Future updateNotification() async {
    var chatRoomDoc =
        await _getChatRoomDocRef(buyerRef: buyerRef, sellerRef: sellerRef, productRef: productRef);

    var chatRoom = await chatRoomDoc.get();
    if (chatRoom.exists) {
      if (isMeSeller()) {
        await chatRoomDoc.update({'isNotiOnSeller': !chatRoom.data()!.isNotiOnSeller});
      } else {
        await chatRoomDoc.update({'isNotiOnBuyer': !chatRoom.data()!.isNotiOnBuyer});
      }
    }
    return;
  }

  Future<DocumentReference> getCurrentChatRomDocRef() async {
    var chatRoomDoc = await _getChatRoomDocRefRaw(
        buyerRef: buyerRef, sellerRef: sellerRef, productRef: productRef);

    return chatRoomDoc;
  }

  Future<ChatRoom?> getCurrnetChatRoom() async {
    var chatRoomDoc =
        await _getChatRoomDocRef(buyerRef: buyerRef, sellerRef: sellerRef, productRef: productRef);

    var chatRoom = await chatRoomDoc.get();
    return chatRoom.data();
  }

  Future<void> addMessaage(String message) async {
    await FirebaseFirestore.instance
        .runTransaction((transaction) async {
          try {
            var chatRoomDoc = await _getChatRoomDocRef(
                buyerRef: buyerRef, sellerRef: sellerRef, productRef: productRef);

            var messageDoc = chatRoomDoc.collection(ZConfig.chatmessagesCollection).doc();

            // 메세지 다큐먼트 내용을 채운다.
            var chatType = ChatType.Text;
            if (message.startsWith('http://') || message.startsWith('https://')) {
              chatType = ChatType.Photo;
            }

            await messageDoc.set({
              'senderRef': ZConfig.userAccount.docId!,
              'receiverRef': receiverRef,
              'creationTime': FieldValue.serverTimestamp(),
              'chatType': chatType.index,
              'regionCode': ZConfig.userAccount.regionCode ?? 0,
              'text': message,
              'isReadMesage': false
            });

            lastMessageRef = messageDoc;
            // lastMessageTime = now;

            // chatRoom 정보 저장.
            if (isMeSeller()) {
              isOnlineSeller = true;
              var chatRoom = await chatRoomDoc.get();
              if (chatRoom.exists) {
                transaction.update(chatRoomDoc, {
                  'lastMessageRef': lastMessageRef,
                  'lastMessageTime': FieldValue.serverTimestamp(),
                  'isOnlineSeller': isOnlineSeller,
                });
              } else {
                transaction.set(chatRoomDoc, this);
              }
            } else {
              isOnlineBuyer = true;
              var chatRoom = await chatRoomDoc.get();
              if (chatRoom.exists) {
                transaction.update(chatRoomDoc, {
                  'lastMessageRef': lastMessageRef,
                  'lastMessageTime': FieldValue.serverTimestamp(),
                  'isOnlineBuyer': isOnlineBuyer,
                });
              } else {
                transaction.set(chatRoomDoc, this);
                await ZConfig.analytics.logEvent(name: 'ChatRoom Created');
              }
            }
          } catch (e) {
            print(e);
          }
        })
        .then((value) => print('ChatRoom lastMessageRef, lastMessageTime  is updated'))
        .catchError(
            (error) => print('Failed to update ChatRoom lastMessageRef, lastMessageTime: $error'));
  }

  static Future<Stream<QuerySnapshot<ChatMessage>>> getMessages({
    required buyerRef,
    required sellerRef,
    required productRef,
  }) async {
    try {
      var chatRoomDoc = await _getChatRoomDocRef(
          buyerRef: buyerRef, sellerRef: sellerRef, productRef: productRef);
      var messages = chatRoomDoc
          .collection(ZConfig.chatmessagesCollection)
          .orderBy('creationTime', descending: false)
          .withConverter<ChatMessage>(
              fromFirestore: (snapshot, _) => ChatMessage.fromJson(snapshot.data()!),
              toFirestore: (chatMessage, _) => chatMessage.toJson())
          .snapshots();
      return messages;
    } catch (e) {
      print(e);
    }
    throw Exception('ChatMessage Stream Exception');
  }
}
