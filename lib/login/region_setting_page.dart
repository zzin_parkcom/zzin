import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:geolocator/geolocator.dart';
import 'package:get/get.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:zzin/common/firestore/document_skima/user_account.dart';
import 'package:zzin/common/hive/address_collection.dart';
import 'package:zzin/common/widget/zbutton_blue.dart';
import 'package:zzin/common/zconfig.dart';
import 'package:zzin/common/zutils.dart';

class RegionSettingPage extends StatefulWidget {
  const RegionSettingPage({Key? key}) : super(key: key);

  @override
  _RegionSettingPageState createState() => _RegionSettingPageState();
}

class _RegionSettingPageState extends State<RegionSettingPage> {
  List<String> searchList = [];
  final TextEditingController _controller = TextEditingController();
  final FocusNode _node = FocusNode();

  int locationId = -1;
  String locationName = '';
  ZUserAccount? userAccount;

  @override
  void initState() {
    super.initState();
    SchedulerBinding.instance!.addPostFrameCallback((_) => frameCallback(context));
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  void frameCallback(BuildContext context) async {
    userAccount = Get.arguments;
    onSetLocation();
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
          resizeToAvoidBottomInset: false,
          appBar:
              AppBar(title: const Text('회원가입', style: TextStyle(fontSize: ZSize.appBarTextSize))),
          body: Container(
            padding: const EdgeInsets.fromLTRB(20, 10, 20, 20),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                const Padding(
                  padding: EdgeInsets.fromLTRB(0, 0, 10, 10),
                  child: Text(
                    '지역 설정',
                    style: TextStyle(fontSize: 22),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(vertical: 10),
                  child: searchBar(),
                ),
                _controller.text.trim().isEmpty && searchList.isEmpty
                    ? initView()
                    : const SizedBox.shrink(),
                searchList.isNotEmpty ? middleColumn() : const SizedBox.shrink(),
                saleareaList(),
              ],
            ),
          ),
          bottomNavigationBar: Padding(
            padding: ZSize.bottomNavigationBarButtonOutPadding,
            child: ZButtonYellow(
              isSelected: locationId != -1,
              text: '완료',
              fontSize: 19,
              padding: ZSize.bottomNavigationBarButtonInPadding,
              onPressed: onSubmit,
            ),
          )),
    );
  }

  void onSubmit() async {
    var locationId = AddressCollection.getRegionCode(locationName);
    var geoPoint = AddressCollection.getGeoPoint(locationName);

    userAccount!.regionGeoPoint = geoPoint;
    userAccount!.regionCode = locationId;
    userAccount!.regionName = locationName;
    userAccount!.isSetLocation = true;
    Get.back(result: userAccount);
  }

  Widget searchBar() {
    return TextField(
      focusNode: _node,
      style: const TextStyle(fontSize: 18),
      controller: _controller,
      decoration: InputDecoration(
        suffixIcon: _controller.text.isNotEmpty
            ? GestureDetector(
                onTap: resetSearchListAndRefresh,
                child: const Icon(
                  Icons.cancel,
                  color: ZConfig.grey_text,
                ),
              )
            : null,
        prefixIcon: const Icon(Icons.search, color: ZConfig.grey_text),
        contentPadding: const EdgeInsets.symmetric(vertical: 5),
        enabledBorder: const OutlineInputBorder(
            borderSide: BorderSide(color: ZConfig.grey_border, width: 2),
            borderRadius: BorderRadius.all(Radius.circular(30.0))),
        hintStyle: const TextStyle(
          fontSize: 18,
          fontWeight: FontWeight.normal,
          color: ZConfig.grey_text,
          decorationColor: ZConfig.grey_text,
        ),
        filled: true,
        fillColor: ZConfig.grey_border,
        labelStyle: const TextStyle(
            color: ZConfig.zzin_black,
            decorationColor: ZConfig.grey_text,
            backgroundColor: ZConfig.grey_text),
        border: const OutlineInputBorder(
          borderRadius: BorderRadius.all(
            Radius.circular(30.0),
          ),
        ),
        hintText: '지역명을 입력해주세요.',
      ),
      onChanged: (search) async {
        if (ZConfig.regExpKo.allMatches(search).length == search.length) {
          // if (search.isEmpty) {
          //   resetSearchListAndRefresh();
          //   return;
          // }

          if (AddressCollection.to.values
              .any((value) => value.regionName.toLowerCase().contains(search.toLowerCase()))) {
            searchList.clear();

            searchList.addAll(AddressCollection.to.values
                .where((value) => value.regionName.toLowerCase().contains(search.toLowerCase()))
                .map((e) => e.regionName));
            setState(() {});
          }
        }
      },
    );
  }

  Widget brandTileText(int index) {
    var stringList = searchList[index].split('|');
    if (stringList.length > 1) {
      return Row(
        children: [
          Text(stringList.first, style: const TextStyle(fontSize: 19, fontWeight: FontWeight.bold)),
          const SizedBox(width: 15),
          Text(
            stringList.last,
            style: const TextStyle(fontSize: 17, color: Colors.grey),
          )
        ],
      );
    } else {
      return Text(searchList[index]);
    }
  }

  void resetSearchListAndRefresh() {
    locationId = -1;
    locationName = '';
    searchList.clear();
    _controller.text = '';
    setState(() {});
  }

  Widget middleColumn() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Padding(
          padding: const EdgeInsets.only(left: 10),
          child: Text(
            _controller.text.isEmpty && searchList.isNotEmpty ? '근처 동네' : '지역 검색 결과',
            style: const TextStyle(
              fontSize: 18,
            ),
          ),
        ),
        TextButton(
          onPressed: () {
            resetSearchListAndRefresh();
            _node.unfocus();
            onSetLocation();
          },
          style: ButtonStyle(foregroundColor: MaterialStateProperty.all(ZConfig.zzin_pointColor)),
          child: const Text('현 위치로 설정'),
        ),
      ],
    );
  }

  Widget saleareaList() {
    return searchList.isNotEmpty || _controller.text.isEmpty
        ? Expanded(
            child: Padding(
              padding: const EdgeInsets.fromLTRB(10, 0, 10, 0),
              child: ListView.separated(
                separatorBuilder: (context, index) => const Divider(),
                itemCount: searchList.length,
                itemBuilder: (context, index) {
                  return ListTile(
                    contentPadding: const EdgeInsets.fromLTRB(0, 3, 0, 3),
                    title: brandTileText(index),
                    onTap: () {
                      locationName = searchList[index];
                      locationId = AddressCollection.getRegionCode(locationName);
                      _controller.text = locationName;
                      _node.unfocus();
                      setState(() {});
                    },
                  );
                },
              ),
            ),
          )
        : noSearchResultWidget();
  }

  Widget noSearchResultWidget() {
    return Align(
      alignment: Alignment.topCenter,
      child: Container(
        padding: const EdgeInsets.fromLTRB(30, 50, 30, 0),
        // color: Colors.blue,
        child: Wrap(
          alignment: WrapAlignment.center,
          runSpacing: 15,
          children: [
            Column(
              children: const [
                Text(
                  '검색 결과가 없습니다.',
                  maxLines: 2,
                  style: TextStyle(
                      color: ZConfig.grey_text, fontWeight: FontWeight.bold, fontSize: 16),
                ),
                Text(
                  '검색어를 다시 입력해주세요.',
                  maxLines: 2,
                  style: TextStyle(
                      color: ZConfig.grey_text, fontWeight: FontWeight.bold, fontSize: 16),
                ),
              ],
            ),
            SizedBox(
                width: 220,
                height: 50,
                child: ZButtonYellow(
                  text: '지역 다시 검색',
                  isSelected: true,
                  fontSize: 20,
                  onPressed: () {
                    resetSearchListAndRefresh();
                  },
                )),
          ],
        ),
      ),
    );
  }

  Widget initView() {
    return Align(
      alignment: Alignment.topCenter,
      child: Container(
        padding: const EdgeInsets.fromLTRB(30, 50, 30, 0),
        // color: Colors.blue,
        child: Column(
          // alignment: WrapAlignment.center,
          // runSpacing: 15,
          children: [
            Column(
              children: const [
                Text(
                  '상품검색 범위 설정시',
                  maxLines: 2,
                  style: TextStyle(color: ZConfig.grey_text, fontSize: 16),
                ),
                Text(
                  '검색할 기준 지역을 설정해 주세요.',
                  maxLines: 2,
                  style: TextStyle(color: ZConfig.grey_text, fontSize: 16),
                ),
              ],
            ),
            const SizedBox(height: 20),
            SizedBox(
                width: 200,
                height: 50,
                child: ZButtonYellow(
                  text: '현재 위치로 설정',
                  isSelected: true,
                  fontSize: 18,
                  onPressed: () => onSetLocation(),
                )),
          ],
        ),
      ),
    );
  }

  Future<bool> checkLocationService() async {
    var permission = await Geolocator.checkPermission();
    if (permission == LocationPermission.denied) {
      permission = await Geolocator.requestPermission();
      if (permission == LocationPermission.denied) {
        return false;
      } else if (permission == LocationPermission.deniedForever) {
        var response = await showDialogYesNo(
            context, '위치정보 이용에 대한 액세스 권한이 필요합니다.', '설정 앱에서 권한을 설정하십시요.', '설정하기', '취소');

        if (response == DialogResponse.YES) {
          await openAppSettings();
        }
        return false;
      }
    }

    return true;
  }

  void onSetLocation() async {
    var bSuccess = await checkLocationService();

    if (bSuccess) {
      await searchbyGPS();
    }
    /*  else {
      Get.rawSnackbar(title: '퍼미션 필요', message: '해당 기능을 사용하기 위해서는 퍼미션이 필요합니다.');
    } */
  }

  Future<void> searchbyGPS() async {
    await EasyLoading.show(
        status: '검색중...', maskType: EasyLoadingMaskType.black, dismissOnTap: true);
    var resultDistanceList = await getGPSAddressList(
        initDistance: 500, maxDistance: 20000, deltaDistance: 100, minResultCount: 5);

    if (resultDistanceList == null || resultDistanceList.length == 0) {
      print('resultDistanceList is null');
      Get.rawSnackbar(
          duration: const Duration(seconds: 5),
          message: '위치 서비스를 사용할 수 없습니다. \n지역명을 입력하여 지역 설정을 해 주십시오.');
      await EasyLoading.dismiss();
      return;
    }
    searchList = resultDistanceList.map<String>((e) {
      String a = e['regionName'];
      return a;
    }).toList();

    print('searchByGPS length: ${searchList.length}');

    await EasyLoading.dismiss();
    setState(() {});
  }
}
