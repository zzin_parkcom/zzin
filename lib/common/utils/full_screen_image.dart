import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:transparent_image/transparent_image.dart';
import 'package:zzin/common/zcache_manager.dart';
import 'package:zzin/common/zconfig.dart';
import 'package:zzin/product_page/product_detail_page/dots_indicator.dart';

class FullScreenImage extends StatefulWidget {
  final List<String> images;

  final Object tag;
  final int initialPage;

  FullScreenImage({required this.images, required this.tag, required this.initialPage, Key? key})
      : super(key: key) {
    SystemChrome.setSystemUIOverlayStyle(const SystemUiOverlayStyle(
      statusBarColor: Colors.transparent,
    ));
  }

  @override
  _FullScreenImageState createState() => _FullScreenImageState();
}

class _FullScreenImageState extends State<FullScreenImage> {
  static const _kDuration = Duration(milliseconds: 300);
  static const _kCurve = Curves.easeInOut;
  late final PageController _controllerPage;
  @override
  void initState() {
    super.initState();
    _controllerPage = PageController(initialPage: widget.initialPage);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: ZConfig.zzin_black,
      body: Stack(
        children: [
          GestureDetector(
            // onTap: () => Get.back(),
            child: Center(
              child: PageView.builder(
                controller: _controllerPage,
                itemCount: widget.images.length,
                itemBuilder: (context, index) {
                  return InteractiveViewer(
                    boundaryMargin: const EdgeInsets.all(0.0),
                    minScale: 0.1,
                    maxScale: 10,
                    child: Hero(
                      tag: widget.tag,
                      child: CachedNetworkImage(
                        fadeInDuration: const Duration(milliseconds: 0),
                        fadeOutDuration: const Duration(milliseconds: 0),
                        placeholderFadeInDuration: const Duration(milliseconds: 0),
                        placeholder: (_, __) => Image.memory(kTransparentImage),
                        imageUrl: widget.images[index],
                        fit: BoxFit.contain,
                        errorWidget: (context, url, error) => const Icon(Icons.error),
                        cacheManager: ZCacheManager.instance,
                      ),
                    ),
                  );
                },
              ),
            ),
          ),
          Positioned(
            bottom: 20,
            left: 0.0,
            right: 0.0,
            child: Container(
              color: Colors.transparent,
              child: Center(
                child: DotsIndicator(
                  controller: _controllerPage,
                  itemCount: widget.images.length,
                  onPageSelected: (int page) {
                    _controllerPage.animateToPage(page, duration: _kDuration, curve: _kCurve);
                  },
                ),
              ),
            ),
          ),
          Positioned(
            right: 0,
            top: AppBar().preferredSize.height,
            child: CircleAvatar(
              radius: 15,
              backgroundColor: Colors.white24,
              child: IconButton(
                padding: const EdgeInsets.all(0),
                icon: const Icon(Icons.close),
                color: Colors.white,
                onPressed: () => Get.back(),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
