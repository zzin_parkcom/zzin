import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:zzin/common/zconfig.dart';

class ReportUser {
  int reportChannel;
  DocumentReference producDoctId;
  DocumentReference? chatRoomDocId;
  int reportReasonType;
  DocumentReference reportUserDocId;
  DocumentReference targetUserDocId;
  Timestamp? reportDate;
  String reportDetail;

  int? processStatus;
  Timestamp? processStartDate;
  Timestamp? processCompleteDate;

  ReportUser({
    required this.reportChannel,
    required this.producDoctId,
    required this.chatRoomDocId,
    required this.reportReasonType,
    required this.reportUserDocId,
    required this.targetUserDocId,
    this.reportDate,
    required this.reportDetail,
    this.processStatus,
    this.processStartDate,
    this.processCompleteDate,
  });

  ReportUser.fromJson(Map<String, Object?> json)
      : this(
          reportChannel: json['reportChannel'] as int,
          producDoctId: json['producDoctId'] as DocumentReference,
          chatRoomDocId: json['chatRoomDocId'] as DocumentReference,
          reportReasonType: json['reportReasonType'] as int,
          reportUserDocId: json['reportUserDocId'] as DocumentReference,
          targetUserDocId: json['targetUserDocId'] as DocumentReference,
          reportDate: (json['reportDate'] ?? Timestamp.now()) as Timestamp,
          reportDetail: json['reportDetail'] as String,
          processStatus: (json['processStatus'] ?? -1) as int,
          processStartDate: (json['processStartDate'] ??
              Timestamp.fromDate(DateTime(2000, 1, 1, 1, 1))) as Timestamp,
          processCompleteDate: (json['processCompleteDate'] ??
              Timestamp.fromDate(DateTime(2000, 1, 1, 1, 1))) as Timestamp,
        );

  Map<String, Object?> toJson() {
    return {
      'reportChannel': reportChannel,
      'producDoctId': producDoctId,
      'chatRoomDocId': chatRoomDocId,
      'reportReasonType': reportReasonType,
      'reportUserDocId': reportUserDocId,
      'targetUserDocId': targetUserDocId,
      'reportDate': FieldValue.serverTimestamp(),
      'reportDetail': reportDetail,
      'processStatus': processStatus,
      'processStartDate': processStartDate,
      'processCompleteDate': processCompleteDate,
    };
  }

  Future<bool> addReportUser() async {
    try {
      var reportUsers = FirebaseFirestore.instance
          .collection(ZConfig.reportUserCollection)
          .withConverter<ReportUser>(
              fromFirestore: (snapshots, _) => ReportUser.fromJson(snapshots.data()!),
              toFirestore: (reportUser, _) => reportUser.toJson());

      reportUsers.add(this);
      return true;
    } catch (e) {
      print(e);
    }
    return false;
  }
}

enum ReportChannel { productDetail, chatRoom }
enum ReportReasonType { inappropriate, badManner, inaccuracy, fraud, badProduct, etc }
enum ReportUserProcessType { receiption, processing, completed }

List<String> reportReasonTitle = ['게시글 부적절', '비매너 행위', '정보 부정확', '사기', '부적절 상품 판매', '기타'];
List<String> reportReasonSubTitle = [
  '전문 판매업자, 홍보글, 낚시, 도배 등 중고 거래와 상관 없는 게시물일 경우',
  '비방, 욕설, 성희롱 등 비매너 행위를 한 경우',
  '게시된 상품의 정보가 부정확한 경우',
  '사기 의심 또는 사기 피해를 입은 경우',
  '성인, 담배, 주류, 장물 등 부적절 상품을 판매하는 경우',
  '이외 기타 사유인 경우'
];
