import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:zzin/common/zconfig.dart';
import 'package:get/get.dart';
import 'package:zzin/postbox/postbox.dart';

class QueryPostbox extends GetxController {
  List<DocumentSnapshot<Postbox>> postboxes = /* <DocumentSnapshot<Postbox>> */ [] /* .obs */;
  DocumentSnapshot? lastDocument;
  bool isLoading = false; // track if products fetching
  bool hasMore = true; // flag for more products available or not
  int documentLimit = ZConfig.maxDocumentCount; // documents to

  static QueryPostbox get to => Get.find();

  Future<void> queryPostbox(
      {required bool isPublic,
      PostboxType? type,
      bool byScroll = false,
      bool isContainIsRemoved = false}) async {
    if (!byScroll) {
      clearProduct();
    }
    if (!hasMore) {
      print('더이상 ${isPublic ? '공용' : '개인'} 알림 내용이 없습니다.');
      return;
    }
    if (isLoading) {
      print('쿼리중... ');
      return;
    }
    isLoading = true;

    Query query;
    if (isPublic) {
      query = FirebaseFirestore.instance
          .collection(ZConfig.postboxCollection)
          .where('expirationDate', isGreaterThanOrEqualTo: Timestamp.now())
          .orderBy('expirationDate', descending: false);
    } else {
      query = FirebaseFirestore.instance
          .doc(ZConfig.userAccount.docId!.path)
          .collection(ZConfig.postboxCollection)
          .where('expirationDate', isGreaterThanOrEqualTo: Timestamp.now())
          .orderBy('expirationDate', descending: false);
    }

    if (!isContainIsRemoved) {
      query = query.where('isRemoved', isEqualTo: false);
    }

    query = type != null
        ? query.where('type', isEqualTo: type.index)
        : query.withConverter<Postbox>(
            fromFirestore: (snapshots, _) => Postbox.fromJson(snapshots.data()!),
            toFirestore: (commonPostbox, _) => commonPostbox.toJson());

    if (lastDocument == null) {
      // 처음 검색하는 경우.
      // query = query.limit(documentLimit);
    } else {
      query = query.startAfterDocument(lastDocument!) /* .limit(documentLimit) */;
    }

    // 쿼리 실행.
    try {
      var snapshots = await query
          .withConverter<Postbox>(
              fromFirestore: (snapshots, _) => Postbox.fromJson(snapshots.data()!),
              toFirestore: (commonPostbox, _) => commonPostbox.toJson())
          .get();
      print('${isPublic ? '공용' : '개인'} 알림 검색된 갯수: ${snapshots.docs.length}');

      if (snapshots.docs.length < documentLimit) {
        hasMore = false;
      }

      if (snapshots.docs.isNotEmpty) {
        lastDocument = snapshots.docs[snapshots.docs.length - 1];
        postboxes.addAll(snapshots.docs);
        //작성날짤로 정렬
        postboxes.sort((left, right) {
          return right.data()!.reportingDate.compareTo(left.data()!.reportingDate);
        });
        print('${isPublic ? '공용' : '개인'} 알림 리스트 갯수: ${postboxes.length}');

        isLoading = false;
      }
    } catch (e) {
      print('복합 인덱스가 필요한지 보세요~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~');
      print('복합 인덱스가 필요한지 보세요~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~');
      print('복합 인덱스가 필요한지 보세요~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~');

      print(e);
    }
  }

  void clearProduct() {
    postboxes.clear();
    lastDocument = null;
    isLoading = false;
    hasMore = true;
  }
}
