import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:zzin/common/widget/zbutton_blue.dart';
import 'package:zzin/common/zconfig.dart';

class RemoveProductBottomSheet extends StatelessWidget {
  const RemoveProductBottomSheet({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          const Text(
            '장말로 상품을 삭제하시겠습니까?',
            style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20),
          ),
          const SizedBox(height: 24),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              ZButtonYellow(
                onPressed: () => Get.back(result: DialogResponse.NO.index),
                text: '취소하기',
                fontSize: 14,
                isSelected: true,
              ),
              ZButtonYellow(
                text: '삭제하기',
                isSelected: true,
                fontSize: 14,
                onPressed: () => Get.back(result: DialogResponse.YES.index),
              ),
            ],
          )
        ],
      ),
    );
  }
}
