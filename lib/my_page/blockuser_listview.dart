import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:transparent_image/transparent_image.dart';
import 'package:zzin/chat_page/chat_ui/chatroom_menu/block_user_bottom_sheet.dart';
import 'package:zzin/common/bottomsheet_custom.dart';
import 'package:zzin/common/firestore/document_skima/block_user_controller.dart';
import 'package:zzin/common/firestore/document_skima/user_account.dart';
import 'package:zzin/common/firestore/query_blockuser.dart';
import 'package:zzin/common/hive/address_collection.dart';
import 'package:zzin/common/zcache_manager.dart';
import 'package:zzin/common/zconfig.dart';
import 'package:zzin/common/zutils.dart';

class BlockUserListView extends StatefulWidget {
  const BlockUserListView({Key? key}) : super(key: key);

  @override
  _BlockUserListViewState createState() => _BlockUserListViewState();
}

class _BlockUserListViewState extends State<BlockUserListView> {
  final ScrollController _scrollController = ScrollController();
  QueryBlockUser queryBlockUser = QueryBlockUser();
  @override
  void initState() {
    super.initState();
    _scrollController.addListener(_scrollListener);
    // Get.put(queryBlockUser);
  }

  @override
  void dispose() {
    _scrollController.removeListener(_scrollListener);
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: getBlockList(),
      builder: (context, snapshot) {
        if (snapshot.connectionState == ConnectionState.done) {
          return Expanded(
            child: queryBlockUser.blockUsers.isEmpty
                ? const NoBlockUser()
                : RefreshIndicator(
                    onRefresh: onRefresh,
                    child: ListView.separated(
                      physics: const BouncingScrollPhysics(parent: AlwaysScrollableScrollPhysics()),
                      separatorBuilder: (context, index) => const Divider(),
                      controller: _scrollController,
                      itemCount: queryBlockUser.blockUsers.length,
                      itemBuilder: (context, index) {
                        var blockUser = queryBlockUser.blockUsers[index].data();
                        if (blockUser == null) {
                          throw Exception('blockUsers is NULL!!!');
                        }
                        return queryBlockUser.blockUsers.isEmpty
                            ? const NoBlockUser()
                            : ListTile(
                                title: Row(
                                  children: [
                                    SizedBox(
                                      height: 50,
                                      width: 50,
                                      child: ClipRRect(
                                        borderRadius: BorderRadius.circular(50),
                                        child: CachedNetworkImage(
                                          fadeInDuration: const Duration(milliseconds: 0),
                                          fadeOutDuration: const Duration(milliseconds: 0),
                                          placeholderFadeInDuration:
                                              const Duration(milliseconds: 0),
                                          placeholder: (_, __) => Image.memory(kTransparentImage),
                                          imageUrl: blockUser.getThumbnailPath(),
                                          fit: BoxFit.contain,
                                          errorWidget: (context, url, error) =>
                                              const Icon(Icons.error),
                                          cacheManager: ZCacheManager.instance,
                                        ),
                                      ),
                                    ),
                                    const SizedBox(width: 5),
                                    Column(
                                      crossAxisAlignment: CrossAxisAlignment.start,
                                      children: [
                                        Text(
                                          (blockUser.nickname ?? ''),
                                          style: const TextStyle(
                                              fontSize: 16,
                                              fontWeight: FontWeight.bold,
                                              height: 1.5),
                                        ),
                                        Text(
                                          AddressCollection.getRegionName(blockUser.regionCode!),
                                          style: const TextStyle(
                                              fontSize: 12, color: ZConfig.grey_text, height: 1.5),
                                        ),
                                      ],
                                    ),
                                  ],
                                ),
                                trailing: IconButton(
                                  icon: const Icon(Icons.delete),
                                  onPressed: () => onRemoveBlockUser(blockUser),
                                ),
                              );
                      },
                    )),
          );
        } else {
          return const SizedBox.shrink();
        }
      },
    );
  }

  void onRemoveBlockUser(ZUserAccount blockUser) async {
    var result = await showModalBottomSheetInt(
        context,
        (_) => BottomSheetCustom(
            contents: BlockUserBottomSheet(blockUser: blockUser.nickname!, isBlockedUser: true),
            sizeRatio: 0.3));
    print(result);

    if (result == 1) {
      await BlockUserController(user: blockUser.docId!).removeUser();
      Get.rawSnackbar(message: '${blockUser.nickname} 차단 해제하였습니다.');
    }
    queryBlockUser.clearProduct();
    setState(() {});
  }

  Future<void> getBlockList() async {
    await queryBlockUser.queryBlockUser();
  }

  Future onRefresh() async {
    await queryBlockUser.queryBlockUser();
  }

  void _scrollListener() {
    var maxScroll = _scrollController.position.maxScrollExtent;
    var currentScroll = _scrollController.position.pixels;
    var delta = maxScroll * ZConfig.scrollRatioQueryMore;

    if (maxScroll - currentScroll <= delta) {
      queryBlockUser.queryBlockUser(byScroll: true);
    }
  }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
class NoBlockUser extends StatelessWidget {
  const NoBlockUser({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return const Center(
      child: Text('차단한 사용자가 없습니다.',
          style: TextStyle(
            fontSize: 20,
            color: ZConfig.grey_text,
          )),
    );
  }
}
