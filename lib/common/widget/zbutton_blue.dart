import 'package:flutter/material.dart';
import 'package:zzin/common/zconfig.dart';

class ZButtonYellow extends StatelessWidget {
  final bool isSelected;
  final VoidCallback? onPressed;
  final String text;
  final double fontSize;
  final double borderRaius;
  final MaterialStateProperty<EdgeInsetsGeometry?>? padding;

  const ZButtonYellow(
      {required this.text,
      this.fontSize = 15,
      required this.isSelected,
      this.onPressed,
      this.borderRaius = 5,
      this.padding,
      Key? key})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return OutlinedButton(
      style: ButtonStyle(
        padding: padding,
        shape: MaterialStateProperty.all<RoundedRectangleBorder>(
          RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(borderRaius),
            side: const BorderSide(color: ZConfig.zzin_primaryColor),
          ),
        ),
        foregroundColor: isSelected
            ? MaterialStateProperty.all<Color>(ZConfig.zzin_white)
            : MaterialStateProperty.all<Color>(ZConfig.zzin_black),
        backgroundColor: isSelected
            ? MaterialStateProperty.all<Color>(ZConfig.zzin_pointColor)
            : MaterialStateProperty.all<Color>(ZConfig.zzin_white),
      ),
      onPressed: onPressed,
      child: Text(
        text,
        overflow: TextOverflow.ellipsis,
        style: TextStyle(fontSize: fontSize),
      ),
    );
  }
}
