import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:transparent_image/transparent_image.dart';
import 'package:zzin/common/zconfig.dart';

import '../../zcache_manager.dart';

class AppBarProfile extends StatelessWidget {
  final String imageUrl;
  final String name;
  const AppBarProfile({required this.name, required this.imageUrl, Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.start,
      children: <Widget>[
        SizedBox(
          height: 24,
          width: 24,
          child: ClipRRect(
            borderRadius: BorderRadius.circular(12),
            child: CachedNetworkImage(
              fadeInDuration: const Duration(milliseconds: 0),
              fadeOutDuration: const Duration(milliseconds: 0),
              placeholderFadeInDuration: const Duration(milliseconds: 0),
              placeholder: (_, __) => Image.memory(kTransparentImage),
              imageUrl: imageUrl,
              fit: BoxFit.contain,
              errorWidget: (context, url, error) => const Icon(Icons.error),
              cacheManager: ZCacheManager.instance,
            ),
          ),
        ),
        const SizedBox(width: 10),
        Text(name,
            style: const TextStyle(fontSize: ZSize.appBarTextSize, color: ZConfig.zzin_black)),
      ],
    );
  }
}
