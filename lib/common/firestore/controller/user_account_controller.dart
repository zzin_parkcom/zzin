import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:get/get.dart';
import 'package:zzin/common/firestore/document_skima/user_account.dart';

class ZUserAccountController extends GetxController {
  late ZUserAccount userAccount;

  static ZUserAccountController get to => Get.find();

  Future<void> queryUserAccount(DocumentReference userRef) async {
    var snapshot = await userRef
        .withConverter<ZUserAccount>(
            fromFirestore: (snapshot, _) => ZUserAccount.fromJson(snapshot.data()!),
            toFirestore: (chatMessage, _) => chatMessage.toJson())
        .get();
    userAccount = snapshot.data()!;
  }

  Future<void> queryUserAccountByPath(String userPath) async {
    var snapshot = await FirebaseFirestore.instance
        .doc(userPath)
        // .collection(ZConfig.userCollection)
        // .doc(userPath)
        .withConverter<ZUserAccount>(
            fromFirestore: (snapshot, _) => ZUserAccount.fromJson(snapshot.data()!),
            toFirestore: (chatMessage, _) => chatMessage.toJson())
        .get();
    userAccount = snapshot.data()!;
    print(userAccount.docId);
  }
}
