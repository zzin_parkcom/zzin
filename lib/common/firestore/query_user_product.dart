import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:get/get.dart';
import 'package:zzin/common/firestore/document_skima/product.dart';
import 'package:zzin/common/firestore/queryvalue.dart';
import 'package:zzin/common/zconfig.dart';

class QueryUserProduct extends GetxController {
  List<DocumentSnapshot<Product>> products = <DocumentSnapshot<Product>>[].obs;
  DocumentSnapshot? lastDocument;
  bool isLoading = false;
  bool hasMore = true;
  int documentLimit = ZConfig.maxDocumentCount;
  int _saleStatusIndex = -1;
  QueryValue queryValue = QueryValue();

  static QueryUserProduct get to => Get.find();

  Future<void> queryUserProduct(DocumentReference seller, int saleStatusIndex,
      {bool byScroll = false, bool isReload = false}) async {
    if (!byScroll) {
      var needSearch = queryValue.save(
        saleStatusParam: saleStatusIndex,
      );

      if (!needSearch && !isReload) {
        print('이전 조건과 같아 쿼리 하지 않습니다.');
        isLoading = false;
        return;
      }
      clearProduct();
    }
    if (!hasMore) {
      print('더이상 상품이 없습니다.');
      return;
    }
    if (isLoading) {
      print('쿼리중... ');
      return;
    }
    isLoading = true;

    Query query = FirebaseFirestore.instance
        // .collectionGroup(ZConfig.productGroupCollection)
        .collection(ZConfig.productsCollection)
        .withConverter<Product>(
            fromFirestore: (snapshots, _) => Product.fromJson(snapshots.data()!),
            toFirestore: (product, _) => product.toJson());

    query = query.where('sellerId', isEqualTo: seller);
    // 삭제되지 않은 상품
    query = query.where('articleStatus', isEqualTo: 0);
    if (saleStatusIndex != -1) {
      _saleStatusIndex = saleStatusIndex;
      query = query.where('saleStatus', isEqualTo: _saleStatusIndex);
    }

    query = query.orderBy('saleModifyDate', descending: true);
    if (lastDocument == null) {
      // 처음 검색하는 경우.
      query = query.limit(documentLimit);
    } else {
      query = query.startAfterDocument(lastDocument!).limit(documentLimit);
    }

    // 쿼리 실행.
    try {
      var snapshots = await query
          .withConverter<Product>(
              fromFirestore: (snapshots, _) => Product.fromJson(snapshots.data()!),
              toFirestore: (product, _) => product.toJson())
          .get();
      print('검색된 갯수: ${snapshots.docs.length}');

      if (snapshots.docs.length < documentLimit) {
        hasMore = false;
      }

      if (snapshots.docs.isNotEmpty) {
        lastDocument = snapshots.docs[snapshots.docs.length - 1];
        products.addAll(snapshots.docs);
        print('상품리스트 갯수: ${products.length}');

        // notifyListeners();
        isLoading = false;
      }
    } catch (e) {
      print('복합 인덱스가 필요한지 보세요~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~');
      print('복합 인덱스가 필요한지 보세요~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~');
      print('복합 인덱스가 필요한지 보세요~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~');

      print(e);
    }
  }

  void clearProduct() {
    products.clear();
    lastDocument = null;
    isLoading = false;
    hasMore = true;
  }
}
