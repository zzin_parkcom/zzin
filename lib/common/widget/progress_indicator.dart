import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:zzin/common/zconfig.dart';

class ZProgressIndicator {
  Future<void> Function() function;

  ZProgressIndicator(BuildContext context, this.function) {
    showDialog(
      context: context,
      barrierDismissible: false,
      barrierColor: Colors.white70,
      builder: (BuildContext context) {
        return Row(
          mainAxisAlignment: MainAxisAlignment.center,
          mainAxisSize: MainAxisSize.min,
          children: const [
            CircularProgressIndicator(
              valueColor: AlwaysStoppedAnimation<Color>(ZConfig.zzin_pointColor),
              strokeWidth: 4,
            ),
          ],
        );
      },
    );

    function().then(
      (value) {
        Get.back();
      },
    );
  }
}
