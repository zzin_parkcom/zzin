import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:flutter_fgbg/flutter_fgbg.dart';
import 'package:get/get.dart';
import 'package:zzin/chat_page/chat_ui/chatroom_menu/block_user_bottom_sheet.dart';
import 'package:zzin/chat_page/chat_ui/chatroom_menu/leave_chatroom_bottom_sheet.dart';
import 'package:zzin/chat_page/chat_ui/chatroom_menu/report_user_first_page.dart';
import 'package:zzin/common/bottomsheet_custom.dart';
import 'package:zzin/common/firestore/document_skima/block_user_controller.dart';
import 'package:zzin/common/firestore/document_skima/chatroom.dart';
import 'package:zzin/common/firestore/document_skima/product.dart';
import 'package:zzin/common/firestore/document_skima/user_account.dart';
import 'package:zzin/common/widget/appbar/appbar_profile.dart';
import 'package:zzin/common/zconfig.dart';
import 'package:zzin/common/zutils.dart';

class ChatUIAppBar extends StatefulWidget implements PreferredSizeWidget {
  final Product product;
  final ZUserAccount receiver;
  const ChatUIAppBar(this.receiver, this.product, {Key? key})
      : preferredSize = const Size.fromHeight(kToolbarHeight),
        super(key: key);
  @override
  final Size preferredSize;
  @override
  _ChatUIAppBarState createState() => _ChatUIAppBarState();
}

class _ChatUIAppBarState extends State<ChatUIAppBar> {
  late ChatRoom chatroom;
  bool isSeller = false;

  late final BlockUserController blockUserController;

  @override
  void initState() {
    super.initState();
    if (widget.product.sellerId == ZConfig.userAccount.docId) {
      // 내가 판매자일때
      isSeller = true;
      chatroom = ChatRoom(
          buyerRef: widget.receiver.docId!,
          sellerRef: widget.product.sellerId!,
          productRef: widget.product.docId!,
          receiverRef: widget.receiver.docId!);
    } else {
      // 내가 구매자 일때
      isSeller = false;
      chatroom = ChatRoom(
          buyerRef: ZConfig.userAccount.docId!,
          sellerRef: widget.product.sellerId!,
          productRef: widget.product.docId!,
          receiverRef: widget.receiver.docId!);
    }

    blockUserController = BlockUserController(user: widget.receiver.docId!);

    SchedulerBinding.instance!.addPostFrameCallback((_) => frameCallback(context));
  }

  void frameCallback(BuildContext context) async {
    var tempChatroom = await chatroom.getCurrnetChatRoom();
    if (tempChatroom != null) {
      chatroom = tempChatroom;
    }
  }

  List<String> events = [];

  @override
  void dispose() {
    // onExitChatRoom();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return FGBGNotifier(
      onEvent: onFGBGEvent,
      child: AppBar(
        leading: IconButton(
          onPressed: () {
            onExitChatRoom();
            Get.back();
          },
          icon: const Icon(Icons.arrow_back, color: ZConfig.zzin_black),
        ),
        titleSpacing: 0,
        title: AppBarProfile(
            name: widget.receiver.nickname ?? '', imageUrl: widget.receiver.getThumbnailPath()),
        actions: [
          PopupMenuButton<int>(
              icon: Icon(Icons.adaptive.more),
              offset: Offset(0, context.mediaQueryPadding.top),
              onSelected: onSelected,
              itemBuilder: (context) => [
                    const PopupMenuItem(value: 0, child: Text('채팅 나가기')),
                    const PopupMenuDivider(),
                    const PopupMenuItem(value: 1, child: Text('신고하기')),
                    const PopupMenuDivider(),
                    PopupMenuItem(value: 2, child: Text(isBlockUser() ? '차단 해제하기' : ' 차단하기')),
                    const PopupMenuDivider(),
                    PopupMenuItem(value: 3, child: Text(isNotiOn() ? '채팅알림 해제' : '채팅알림 켜기')),
                  ])
        ],
      ),
    );
  }

  bool isNotiOn() {
    if (isSeller) {
      return chatroom.isNotiOnSeller;
    } else {
      return chatroom.isNotiOnBuyer;
    }
  }

  bool isBlockUser() {
    return blockUserController.isBlocked();
  }

  void onSelected(int index) async {
    switch (index) {
      // 채팅 나가기.
      case 0:
        onLeaveChatRoom();
        break;
      // 신고하기 //////////////
      case 1:
        reportUser();
        break;
      // 차단하기 ///////////////
      case 2:
        blockUserOnOff();
        break;
      // 채팅 알림 해제
      case 3:
        turnOnOffNotification();
        break;
    }
  }

  void onLeaveChatRoom() async {
    var result = await showModalBottomSheetInt(context,
        (_) => const BottomSheetCustom(contents: LeaveChatRoomBottomSheet(), sizeRatio: 0.3));
    print(result);

    if (result == 1) {
      await chatroom.leaveRoom();
      Get.back();
      Get.rawSnackbar(title: '채팅나가기 완료', message: '채팅방리스트를 리플레시 하시면 방리스트가 업데이트 됩니다.');
    }
  }

  void reportUser() async {
    var chatroomDocId = await chatroom.getCurrentChatRomDocRef();
    var result = await Get.to(
        () => ReportUserFirstPage(
              productDocId: widget.product.docId!,
              chatroomDocId: chatroomDocId,
              targetUserDocId: widget.receiver.docId!,
            ),
        transition: Transition.rightToLeftWithFade);
    if (result == true) {}
  }

  void blockUserOnOff() async {
    bool isBlockedUser = isBlockUser();
    var result = await showModalBottomSheetInt(
        context,
        (_) => BottomSheetCustom(
            contents: BlockUserBottomSheet(
                blockUser: widget.receiver.nickname!, isBlockedUser: isBlockedUser),
            sizeRatio: 0.3));
    print(result);

    if (result == 1) {
      if (isBlockedUser) {
        await blockUserController.removeUser();
      } else {
        await blockUserController.addUser();
      }

      var tempChatroom = await chatroom.getCurrnetChatRoom();
      chatroom = tempChatroom!; // 채팅방에 이미 들어와 있기 때문에 null 일수 없음.
      Get.rawSnackbar(
          message: isBlockedUser
              ? '${widget.receiver.nickname}님을 차단 해제하였습니다.'
              : '${widget.receiver.nickname} 님을 차단하였습니다.');
    }
  }

  void turnOnOffNotification() async {
    await chatroom.updateNotification();
    var tempChatroom = await chatroom.getCurrnetChatRoom();
    chatroom = tempChatroom!; // 채팅방에 이미 들어와 있기 때문에 null 일수 없음.

    Get.rawSnackbar(message: '해당 채팅방의 알림이 해제되었습니다.');
    // 채팅 알림 해제시,
    // 토스트 UI로 “해당 채팅의 알림이 해제되었습니다“
    // “채팅 알림 해제” -> “채팅 알림 받기” 메뉴명 변경

    // 채팅 알림 받기시,
    // 토스트 UI로 “해당 채팅의 알림이 설정되었습니다”
  }

  void onExitChatRoom() async {
    await chatroom.exitRoom();
  }

  void onEnterRoom() async {
    await chatroom.enterRoom();
  }

  void onFGBGEvent(FGBGType event) {
    if (event == FGBGType.background) {
      onExitChatRoom();
    } else if (event == FGBGType.foreground) {
      onEnterRoom();
    }
  }
}
