import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:flutter_multi_formatter/formatters/money_input_formatter.dart';
import 'package:zzin/common/hive/upload_collection.dart';
import 'package:zzin/common/utils/upload_utils.dart';
import 'package:zzin/common/zutils.dart';
import 'package:flutter/services.dart';

class PriceSelection extends StatefulWidget {
  final UploadCollection uploadCollection;
  final String defaultName = '가격 입력';
  final double fontSize;
  final int defaultValue = 0;

  const PriceSelection(this.fontSize, {required this.uploadCollection, Key? key}) : super(key: key);
  @override
  _PriceSelectionState createState() => _PriceSelectionState();
}

class _PriceSelectionState extends State<PriceSelection> {
  var price = 0;
  TextEditingController controller = TextEditingController();

  @override
  void dispose() {
    // onSaved(controller.text);
    controller.dispose();
    super.dispose();
  }

  @override
  void initState() {
    super.initState();

    SchedulerBinding.instance!.addPostFrameCallback((_) => frameCallback(context));
  }

  void frameCallback(BuildContext context) async {
    if (price == widget.uploadCollection.price) {
      if (price == 0) {
        controller.text = '';
      }
      return;
    }
    price = widget.uploadCollection.price;
    if (price != widget.defaultValue) {
      controller.text = numberWithComma(price);
    } else {
      controller.text = '';
    }

    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    SchedulerBinding.instance!.addPostFrameCallback((_) => frameCallback(context));
    return ListTile(
      title: TextFormField(
        validator: validator,
        onSaved: onSaved,
        // focusNode: _node,
        autovalidateMode: AutovalidateMode.disabled,
        style: priceStyle(widget.fontSize),
        controller: controller,
        keyboardType: TextInputType.number,
        inputFormatters: [
          MoneyInputFormatter(mantissaLength: 0),
          LengthLimitingTextInputFormatter(14),
        ],
        decoration: InputDecoration(
          border: InputBorder.none,
          hintText: '가격 입력',
          hintStyle: normalStyle(true, widget.fontSize),
          suffix: koreanPrice(controller.text),
        ),
        onFieldSubmitted: (value) => onFieldSubmitted(value),
        onChanged: (String value) => onChaned(value),
      ),
    );
  }

  void onChaned(String value) async {
    if (value.isNotEmpty) {
      if (value.startsWith('0')) {
        value = value.replaceFirst('0', '');
      }

      controller.text = value;
      controller.selection =
          TextSelection.fromPosition(TextPosition(offset: controller.text.length));
      if (value.isNotEmpty) {
        price = int.parse(value.replaceAll(',', ''));
        widget.uploadCollection.price = price;
        await widget.uploadCollection.save();
      } else {
        price = 0;
        widget.uploadCollection.price = price;
        await widget.uploadCollection.save();
      }
      setState(() {});
    }

    if (value.length >= 14) {
      controller.text = toCurrencyString('9999999999', mantissaLength: 0);
      controller.selection =
          TextSelection.fromPosition(TextPosition(offset: controller.text.length));

      price = int.parse(value.replaceAll(',', ''));
      widget.uploadCollection.price = price;
      await widget.uploadCollection.save();
    }
    print('onChaned');
  }

  void onFieldSubmitted(String value) async {
    value = value.replaceAll(',', '');
    var temp = int.tryParse(value);
    if (temp != null) {
      price = temp;
      widget.uploadCollection.price = price;
      await widget.uploadCollection.save();
    } else {
      price = 0;
      widget.uploadCollection.price = price;
      await widget.uploadCollection.save();
    }

    print('onFieldSubmitted');
  }

  String? validator(String? value) {
    var isValid = true;
    if (value == null || value.isEmpty) {
      isValid = false;
    } else {
      value = value.replaceAll(',', '');
      var temp = int.parse(value);
      if (temp == 0) {
        isValid = false;
      }
    }

    return isValid ? null : '가격을 입력해 주세요.';
  }

  void onSaved(String? value) async {
    widget.uploadCollection.price = price;
    await widget.uploadCollection.save();

    print('onSaved');
  }

  Widget koreanPrice(String value) {
    value = value.replaceAll(',', '');
    var joinList = ['원', '십', '백', '천', '만', '십', '백', '천', '억', '십', '백', '천', '조'];

    var reverse = value.split('').reversed.join();

    var reverseResult = '';
    for (var i = 0; i < reverse.length; i++) {
      if (i != 0 && i != 4) {
        if (reverse[i] == '0') {
          continue;
        } else {
          reverseResult += (joinList[i] + reverse[i]);
        }
      } else {
        if (i == 4 &&
            reverse.length >= 9 &&
            reverse[7] == '0' &&
            reverse[6] == '0' &&
            reverse[5] == '0' &&
            reverse[4] == '0') {
          continue;
        } else {
          reverseResult += (joinList[i] + (reverse[i] == '0' ? '' : reverse[i]));
        }
      }
    }
    var result = reverseResult.split('').reversed.join();

    return Text(result, overflow: TextOverflow.ellipsis, style: normalStyle(true, widget.fontSize));
  }
}
