import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:zzin/common/bottomsheet_custom.dart';
import 'package:zzin/common/zconfig.dart';
import 'package:zzin/common/zutils.dart';
import 'package:zzin/entrance/skeleton.dart';
import 'package:zzin/my_page/logout_bottom_sheet.dart';
import 'package:zzin/my_page/profile_configuration_page.dart';

class AccountConfigurationPage extends StatefulWidget {
  const AccountConfigurationPage({Key? key}) : super(key: key);

  @override
  _AccountConfigurationPageState createState() => _AccountConfigurationPageState();
}

class _AccountConfigurationPageState extends State<AccountConfigurationPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: AppBar(
          title: const Text('계정 설정', style: TextStyle(fontSize: 26, fontWeight: FontWeight.bold))),
      body: SafeArea(
        child: Padding(
          padding: const EdgeInsets.fromLTRB(0, 20, 0, 0),
          child: ListView(
            padding: const EdgeInsets.fromLTRB(20, 0, 20, 0),
            children: [
              ListTile(
                  title: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  const Text('로그인 정보', style: TextStyle(fontSize: 22, fontWeight: FontWeight.bold)),
                  Flexible(
                    child: Text('${ZConfig.userAccount.getSocialName()}계정 연결',
                        overflow: TextOverflow.ellipsis,
                        style: const TextStyle(
                            fontSize: 18, fontWeight: FontWeight.bold, color: ZConfig.grey_text)),
                  ),
                ],
              )),
              ListTile(
                title: const Text('프로필 변경',
                    style: TextStyle(
                      fontSize: 22,
                      fontWeight: FontWeight.bold,
                      color: ZConfig.zzin_black,
                    )),
                onTap: onProfile,
              ),
              ListTile(
                title: const Text(
                  '로그아웃',
                  style: TextStyle(
                    fontSize: 22,
                    fontWeight: FontWeight.bold,
                    color: ZConfig.zzin_black,
                  ),
                ),
                onTap: onLogout,
              ),
            ],
          ),
        ),
      ),
    );
  }

  void onProfile() async {
    await Get.to(() => const ProfileConfigurationPage(),
        transition: Transition.rightToLeftWithFade);
    setState(() {});
  }

  void onLogout() async {
    var result = await showModalBottomSheetInt(
        context, (_) => const BottomSheetCustom(contents: LogoutBottomSheet(), sizeRatio: 0.25));
    print(result);

    if (result == 1) {
      await ZConfig.userAccount.logout();
      // showInfo(context, '${ZConfig.userAccount.getSocialName()} 으로 로그인 되었습니다.');

      MainTabController.to.setIndex(0);
      await Get.offAllNamed('/');
    }
  }
}
