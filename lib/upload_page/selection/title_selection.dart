import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:zzin/common/hive/upload_collection.dart';
import 'package:zzin/common/utils/upload_utils.dart';
import 'package:zzin/common/zconfig.dart';

class TitleSelection extends StatefulWidget {
  final UploadCollection uploadCollection;
  final double fontSize;
  final String defaultName = '';

  const TitleSelection(this.fontSize, {required this.uploadCollection, Key? key}) : super(key: key);

  @override
  _TitleSelectionState createState() => _TitleSelectionState();
}

class _TitleSelectionState extends State<TitleSelection> {
  final TextEditingController controller = TextEditingController();

  var title = Empty;

  @override
  void dispose() {
    // onSaved(controller.text);

    controller.dispose();
    super.dispose();
  }

  @override
  void initState() {
    super.initState();

    SchedulerBinding.instance!.addPostFrameCallback((_) => frameCallback(context));
  }

  void frameCallback(BuildContext context) async {
    title = widget.uploadCollection.title;
    controller.text = title;
  }

  @override
  Widget build(BuildContext context) {
    return ListTile(
      title: TextFormField(
        controller: controller,
        style: normalStyle(false, widget.fontSize),
        decoration: InputDecoration(
          border: InputBorder.none,
          hintText: '제목 입력',
          hintStyle: normalStyle(true, widget.fontSize),
        ),
        onFieldSubmitted: (value) async {
          widget.uploadCollection.title = value;
          await widget.uploadCollection.save();
        },
        onChanged: (value) async {
          widget.uploadCollection.title = value;
          await widget.uploadCollection.save();
        },
        onSaved: onSaved,
        validator: validator,
        autovalidateMode: AutovalidateMode.disabled,
      ),
    );
  }

  void onSaved(String? value) async {
    if (controller.text.length > 200) {
      title = controller.text.substring(0, 200);
    } else {
      title = controller.text;
    }
    widget.uploadCollection.title = title;
    await widget.uploadCollection.save();
  }

  String? validator(String? value) {
    if (value == null || value.isEmpty) {
      return '제목을 입력해 주세요';
    }
    return null;
  }
}
