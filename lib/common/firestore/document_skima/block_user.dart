import 'package:cloud_firestore/cloud_firestore.dart';

class BlockUser {
  DocumentReference userDocId;
  bool isBlocked;

  BlockUser({required this.userDocId, required this.isBlocked});

  BlockUser.fromJson(Map<String, Object?> json)
      : this(
          userDocId: json['userDocId'] as DocumentReference,
          isBlocked: json['isBlocked'] as bool,
        );

  Map<String, Object?> toJson() {
    return {
      'userDocId': userDocId,
      'isBlocked': isBlocked,
    };
  }
}
