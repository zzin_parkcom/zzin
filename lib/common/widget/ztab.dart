import 'package:flutter/material.dart';
import 'package:zzin/common/hive/zhive.dart';
import 'package:zzin/common/zconfig.dart';

class ZTab extends StatefulWidget {
  final List<Widget> tabs;
  final List<Widget> tabViews;
  final String keyString;
  const ZTab({required this.tabs, required this.tabViews, required this.keyString, Key? key})
      : super(key: key);

  @override
  _ZTabState createState() => _ZTabState();
}

class _ZTabState extends State<ZTab> with TickerProviderStateMixin {
  late TabController _tabController;
  late String _keyString;
  @override
  void initState() {
    super.initState();
    _keyString = widget.keyString + '_' + widget.toString();

    _tabController = TabController(
        length: widget.tabs.length, vsync: this, initialIndex: ZHive.to.get(_keyString) ?? 0);

    _tabController.addListener(listenr);
    _tabController.animation!.addListener(animationListener);
  }

  void listenr() {
    print('$_keyString index: ${_tabController.index}');
    print('widget length : ${widget.tabs.length}');
    ZHive.to.put(_keyString, _tabController.index);
  }

  void animationListener() {}

  @override
  Widget build(BuildContext context) {
    return Column(crossAxisAlignment: CrossAxisAlignment.stretch, children: <Widget>[
      Padding(
        padding: const EdgeInsets.symmetric(horizontal: 10),
        child: TabBar(
          controller: _tabController,
          isScrollable: true,
          labelColor: ZConfig.zzin_pointColor,
          unselectedLabelColor: ZConfig.zzin_black,
          indicatorColor: ZConfig.zzin_pointColor,
          indicatorWeight: 3,
          tabs: widget.tabs,
          overlayColor: MaterialStateProperty.all<Color>(ZConfig.zzin_primaryColor),
        ),
      ),
      Expanded(child: TabBarView(controller: _tabController, children: widget.tabViews)),
      const Divider(indent: 10, endIndent: 10)
    ]);
  }
}
