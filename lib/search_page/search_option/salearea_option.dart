import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:zzin/common/bottomsheet_custom.dart';
import 'package:zzin/common/hive/address_collection.dart';
import 'package:zzin/common/hive/search_collection.dart';
import 'package:zzin/common/hive/upload_collection.dart';
import 'package:zzin/common/hive/zhive.dart';
import 'package:zzin/common/utils/upload_utils.dart';
import 'package:zzin/common/zconfig.dart';
import 'package:zzin/common/zutils.dart';
import 'package:zzin/product_page/bottomsheet/salearea_bottonsheet.dart';
import 'package:zzin/upload_page/customFormfield/custom_formfield.dart';

class SaleAreaOption extends StatefulWidget {
  final String defaultName = '검색지역 선택';
  final double fontSize;

  // ignore: prefer_const_constructors_in_immutables
  SaleAreaOption(this.fontSize, {Key? key}) : super(key: key);

  late final _SaleAreaOptionState _state;
  @override
  // ignore: no_logic_in_create_state
  _SaleAreaOptionState createState() {
    _state = _SaleAreaOptionState();
    return _state;
  }

  int getAddressId() => _state.id.value;
  void setAddressId(int id) {
    _state.id.value = id;
    _state.name.value = AddressCollection.getRegionName(id);
  }

  int getAddressStep() => _state.step.value;
  void setAddressStep(int step) => _state.step.value = step;

  bool isEmpty() => _state.id.value == NotSelected;

  void clearState() {
    SearchCollection.instance.addressId = NotSelected;
    SearchCollection.instance.addressName = defaultName;
    SearchCollection.instance.save();
    _state.id.value = NotSelected;
    _state.name.value = defaultName;
    _state.step.value = NotSelected;
    ZHive.generalBox.put('SearchOption_areaStep', NotSelected.toDouble());
    _state.clearState();
  }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
class _SaleAreaOptionState extends State<SaleAreaOption> {
  var id = SearchCollection.instance.addressId.obs;
  var name = SearchCollection.instance.addressName.obs;
  var step = SearchCollection.instance.addressStep.obs;

  @override
  void initState() {
    super.initState();
    setName();
  }

  void clearState() {
    setName();
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return CustomFormField(
      initialValue: id.value,
      validator: validator,
      onSaved: onSaved,
      autovalidateMode: AutovalidateMode.onUserInteraction,
      builder: listTileBuilder,
    );
  }

  Widget listTileBuilder(FormFieldState<int> state) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        ListTile(
            title: Row(
              children: [
                Text(name.value,
                    style: normalStyle((id.value == Cancel || step.value == 3), widget.fontSize)),
                Expanded(
                  child: Text(' ${getStep()}',
                      overflow: TextOverflow.ellipsis, style: normalStyle(true, widget.fontSize)),
                ),
                state.hasError
                    ? Text(
                        state.errorText!,
                        style: errorStyle(widget.fontSize),
                      )
                    : const SizedBox.shrink(),
              ],
            ),
            trailing: const Icon(Icons.expand_more),
            onTap: onSalesArea),
      ],
    );
  }

  String? validator(int? value) {
    return id.value == Cancel ? '거래지역을 선택해 주세요' : null;
  }

  void onSaved(int? value) {}

////////////////////////////////////////////////////////////////////////////////////////////////////
  void onSalesArea() async {
    await showModalBottomSheetInt(
        context,
        (_) => BottomSheetCustom(
            contents: const SalesAreaBottomSheet(),
            sizeRatio: Get.mediaQuery.textScaleFactor >= 1.5 ? 0.43 : 0.35));
    id.value = ZHive.generalBox.get('SearchOption_addressId', defaultValue: NotSelected);
    step.value = ZHive.generalBox.get('SearchOption_areaStep', defaultValue: NotSelected).toInt();
    setName();

    if (ZConfig.userAccount.isVerified &&
        // id.value != NotSelected &&
        ZConfig.userAccount.regionCode != id.value) {
      Get.rawSnackbar(message: '기준 지역 업데이트: ${AddressCollection.getRegionName(id.value)}');

      var regionName = AddressCollection.getRegionName(id.value);
      var regionGeoPoint = AddressCollection.getGeoPointByRegionCode(id.value);

      ZConfig.userAccount.regionCode = id.value;
      ZConfig.userAccount.regionName = regionName;
      ZConfig.userAccount.regionGeoPoint = regionGeoPoint;

      await ZConfig.userAccount.docId!.update(
          {'regionCode': id.value, 'regionName': regionName, 'regionGeoPoint': regionGeoPoint});

      //상품등록 디폴트 업데이트
      UploadCollection.instance.saleAreaId = id.value;
      UploadCollection.instance.saleAreaName = regionName;
      await UploadCollection.instance.save();
    }
  }

  void setName() {
    if (id.value == NotSelected || step.value == NotSelected || step.value == 3) {
      name.value = widget.defaultName;
    } else {
      name.value = AddressCollection.getRegionName(id.value).split(' ').reversed.first;
    }

    setState(() {});
  }

  String getStep() {
    switch (step.value) {
      case 0:
        return '500m 이내';
      case 1:
        return '2km 이내';
      case 2:
        return '10km 이내';
      case 3:
        return '';
      default:
        return '';
    }
  }
}
