import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:get/get.dart';
import 'package:zzin/common/firestore/document_skima/chatroom.dart';

import 'package:zzin/common/firestore/queryvalue.dart';
import 'package:zzin/common/zconfig.dart';

class QueryChatRoom extends GetxController {
  final DocumentReference? product;
  List<DocumentSnapshot<ChatRoom>> chatrooms = <DocumentSnapshot<ChatRoom>>[].obs;
  DocumentSnapshot<ChatRoom>? lastDocument;
  bool isLoading = false;
  bool hasMore = true;
  int documentLimit = ZConfig.maxDocumentCount;
  QueryValue queryValue = QueryValue();

  QueryChatRoom({this.product});
  bool canQuery() {
    return hasMore && !isLoading;
  }

  void clearProduct() {
    print('clearProduct');
    chatrooms.clear();
    lastDocument = null;
    isLoading = false;
    hasMore = true;
  }

  Future<void> queryChatRoom(
      {required userRef, required bool isSeller, required bool byScroll}) async {
    if (!byScroll) {
      clearProduct();
    }
    if (!hasMore) {
      print('더이상 상품이 없습니다.');
      return;
    }
    if (isLoading) {
      print('쿼리중... ');
      return;
    }
    isLoading = true;

    Query query = FirebaseFirestore.instance.collectionGroup(ZConfig.chatRoomsGroupCollection)
      ..withConverter<ChatRoom>(
          fromFirestore: (snapshots, _) => ChatRoom.fromJson(snapshots.data()!),
          toFirestore: (chatRoom, _) => chatRoom.toJson());

    if (isSeller) {
      query = query.where('sellerRef', isEqualTo: userRef);
      query = query.where('isLeaveRoomSeller', isEqualTo: false);
      if (product != null) {
        query = query.where('productRef', isEqualTo: product);
      }
    } else {
      query = query.where('buyerRef', isEqualTo: userRef);
      query = query.where('isLeaveRoomBuyer', isEqualTo: false);
    }

    query = query.orderBy('lastMessageTime', descending: true);

    if (lastDocument == null) {
      // 처음 검색하는 경우.
      query = query.limit(documentLimit);
    } else {
      query = query.startAfterDocument(lastDocument!).limit(documentLimit);
    }

    // 쿼리 실행.
    try {
      var snapshots = await query
          .withConverter<ChatRoom>(
              fromFirestore: (snapshots, _) => ChatRoom.fromJson(snapshots.data()!),
              toFirestore: (chatRoom, _) => chatRoom.toJson())
          .get();
      print('검색된 갯수: ${snapshots.docs.length}');

      if (snapshots.docs.length < documentLimit) {
        hasMore = false;
      }

      if (snapshots.docs.isNotEmpty) {
        lastDocument = snapshots.docs[snapshots.docs.length - 1];
        print(lastDocument!.data()!.lastMessageTime);
        chatrooms.addAll(snapshots.docs);
        print('상품리스트 갯수: ${chatrooms.length}');

        isLoading = false;
      }
    } catch (e) {
      print('복합 인덱스가 필요한지 보세요~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~');
      print('복합 인덱스가 필요한지 보세요~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~');
      print('복합 인덱스가 필요한지 보세요~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~');

      print(e);
    }
  }
}
