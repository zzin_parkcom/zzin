import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:transparent_image/transparent_image.dart';
import 'package:zzin/common/firestore/document_skima/product.dart';
import 'package:zzin/common/firestore/document_skima/user_account.dart';
import 'package:zzin/common/firestore/query_user_product.dart';
import 'package:zzin/common/zcache_manager.dart';
import 'package:zzin/common/zconfig.dart';
import 'package:zzin/common/zutils.dart';
import 'package:zzin/product_page/product_detail_page/seller_page/seller_list_page.dart';

import '../product_detail_view.dart';

class GridSellerView extends StatefulWidget {
  final QueryUserProduct _queryUserProduct;
  final ZUserAccount _sellerAccount;
  final String tagHeroPrefix;
  const GridSellerView(this._queryUserProduct, this._sellerAccount, this.tagHeroPrefix, {Key? key})
      : super(key: key);

  @override
  _GridSellerViewState createState() => _GridSellerViewState();
}

class _GridSellerViewState extends State<GridSellerView> {
  final ScrollController controller = ScrollController();

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      padding: const EdgeInsets.fromLTRB(10, 0, 10, 0),
      child: Column(
        children: [
          const Divider(height: 1),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Row(
                children: [
                  CircleAvatar(
                    radius: 10,
                    backgroundImage: CachedNetworkImageProvider(
                      widget._sellerAccount.getThumbnailPath(),
                      cacheManager: ZCacheManager.instance,
                    ),
                  ),
                  const SizedBox(width: 5),
                  Text(
                    (widget._sellerAccount.nickname ?? ''),
                    style: const TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
                  ),
                ],
              ),
              TextButton(
                  onPressed: () {
                    onSellerButton(context);
                  },
                  child: const Text('판매자 정보 더보기'))
            ],
          ),
          GridView.builder(
              padding: const EdgeInsets.all(0),
              shrinkWrap: true,
              itemCount: widget._queryUserProduct.products.length,
              controller: controller,
              gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
                childAspectRatio: 3 / 4,
                crossAxisCount: 2,
                mainAxisSpacing: 10,
                crossAxisSpacing: 10,
              ),
              itemBuilder: (context, index) {
                var product = widget._queryUserProduct.products[index].data();
                if (product == null) {
                  throw Exception('Product is NULL!!!');
                }

                return GestureDetector(
                  onTap: () => onListItemTap(context, product),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      ClipRRect(
                        borderRadius: BorderRadius.circular(15),
                        child: Hero(
                          tag: widget.tagHeroPrefix + 'GridSeller_' + product.docId.toString(),
                          child: CachedNetworkImage(
                            fadeInDuration: const Duration(milliseconds: 0),
                            fadeOutDuration: const Duration(milliseconds: 0),
                            placeholderFadeInDuration: const Duration(milliseconds: 0),
                            placeholder: (_, __) => Image.memory(kTransparentImage),
                            imageUrl: product.thumbnail,
                            fit: BoxFit.contain,
                            errorWidget: (context, url, error) => const Icon(Icons.error),
                            cacheManager: ZCacheManager.instance,
                          ),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.fromLTRB(5, 5, 5, 5),
                        child: Text(
                          product.title,
                          maxLines: 1,
                          overflow: TextOverflow.ellipsis,
                          style: const TextStyle(fontSize: 15.0),
                        ),
                      ),
                      Flexible(
                        child: Padding(
                          padding: const EdgeInsets.fromLTRB(5, 0, 5, 0),
                          child: Text(
                            '${numberWithComma(product.price)}원',
                            overflow: TextOverflow.ellipsis,
                            style: const TextStyle(
                              fontSize: 12.0,
                              color: ZConfig.zzin_black,
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                );
              })
        ],
      ),
    );
  }

  void onSellerButton(BuildContext context) {
    Get.to(() => SellerListPage(seller: widget._sellerAccount),
        transition: Transition.rightToLeftWithFade);
  }

  void onListItemTap(BuildContext context, Product product) {
    var tag = DateTime.now().toString();
    Get.put(Product.fromJson(product.toJson()), tag: tag);
    Get.to(() => ProductDetailView(tag: tag, tagHeroPrefix: widget.tagHeroPrefix + 'GridSeller_'),
        curve: Curves.linear,
        transition: Transition.fadeIn,
        duration: ZConfig.heroAnimationDuration,
        preventDuplicates: false);
  }
}
