import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:zzin/common/zconfig.dart';

class SortBottomSheet extends StatefulWidget {
  final int indexArrange;
  const SortBottomSheet(this.indexArrange, {Key? key}) : super(key: key);

  @override
  _SortBottomSheetState createState() => _SortBottomSheetState();
}

class _SortBottomSheetState extends State<SortBottomSheet> {
  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Center(
          child: ListView.separated(
              itemBuilder: (context, index) {
                return SizedBox(
                  height: 65,
                  child: ListTile(
                    onTap: () => Get.back(result: index),
                    title: Align(
                      alignment: Alignment.centerLeft,
                      child: Text(
                        ZConfig.sortNameList[index],
                        style: widget.indexArrange == index
                            ? const TextStyle(
                                fontSize: 18,
                                fontWeight: FontWeight.bold,
                                backgroundColor: ZConfig.zzin_white,
                                color: ZConfig.zzin_pointColor,
                              )
                            : const TextStyle(
                                fontSize: 18,
                                fontWeight: FontWeight.bold,
                                backgroundColor: ZConfig.zzin_white,
                                color: ZConfig.grey_text,
                              ),
                      ),
                    ),
                    trailing: widget.indexArrange == index
                        ? const Icon(
                            Icons.done,
                            color: ZConfig.zzin_pointColor,
                          )
                        : null,
                  ),
                );
              },
              separatorBuilder: (context, index) => const Divider(),
              itemCount: ZConfig.sortNameList.length),
        ),
      ),
    );
  }
}
