// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'address_collection.dart';

// **************************************************************************
// TypeAdapterGenerator
// **************************************************************************

class AddressCollectionAdapter extends TypeAdapter<AddressCollection> {
  @override
  final int typeId = 2;

  @override
  AddressCollection read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return AddressCollection(
      regionCode: fields[0] as int,
      regionName: fields[1] as String,
      latitude: fields[2] as double,
      longitude: fields[3] as double,
    );
  }

  @override
  void write(BinaryWriter writer, AddressCollection obj) {
    writer
      ..writeByte(4)
      ..writeByte(0)
      ..write(obj.regionCode)
      ..writeByte(1)
      ..write(obj.regionName)
      ..writeByte(2)
      ..write(obj.latitude)
      ..writeByte(3)
      ..write(obj.longitude);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is AddressCollectionAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}
