// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'category_collection.dart';

// **************************************************************************
// TypeAdapterGenerator
// **************************************************************************

class CategoryCollectionAdapter extends TypeAdapter<CategoryCollection> {
  @override
  final int typeId = 4;

  @override
  CategoryCollection read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return CategoryCollection(
      id: fields[0] as int,
      nameKr: fields[1] as String,
      nameEn: fields[2] as String,
    );
  }

  @override
  void write(BinaryWriter writer, CategoryCollection obj) {
    writer
      ..writeByte(3)
      ..writeByte(0)
      ..write(obj.id)
      ..writeByte(1)
      ..write(obj.nameKr)
      ..writeByte(2)
      ..write(obj.nameEn);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is CategoryCollectionAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}
