import 'dart:convert';
import 'dart:math';

import 'package:crypto/crypto.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/services.dart';
import 'package:sign_in_with_apple/sign_in_with_apple.dart';
import 'package:zzin/common/firestore/document_skima/user_account.dart';
import 'package:zzin/login/signin/social_signin.dart';

class ZAppleSignin implements SocialSignin {
  late AuthorizationCredentialAppleID appleCredential;
  late OAuthCredential oauthCredential;

  @override
  Future<bool> login() async {
    // To prevent replay attacks with the credential returned from Apple, we
    // include a nonce in the credential request. When signing in with
    // Firebase, the nonce in the id token returned by Apple, is expected to
    // match the sha256 hash of `rawNonce`.
    // final rawNonce = generateNonce();
    // final nonce = sha256ofString(rawNonce);

    appleCredential = await SignInWithApple.getAppleIDCredential(
      scopes: [
        AppleIDAuthorizationScopes.email,
        AppleIDAuthorizationScopes.fullName,
      ],
    );

    return true;
  }

  Future<AuthCredential> authenticate() async {
    // Create an `OAuthCredential` from the credential returned by Apple.
    return oauthCredential = OAuthProvider('apple.com').credential(
      idToken: appleCredential.identityToken,
      accessToken: appleCredential.authorizationCode,
    );
  }

  @override
  Future<ZUserAccount> getAccountInfo() async {
    try {
      return ZUserAccount(apple: {
        /// An identifier associated with the authenticated user.
        ///
        /// This will always be provided on iOS and macOS systems. On Android, however, this will not be present.
        /// This will stay the same between sign ins, until the user deauthorizes your App.
        'userIdentifier': appleCredential.userIdentifier,

        /// The users given name, in case it was requested.
        /// You will need to provide the [AppleIDAuthorizationScopes.fullName] scope to the [AppleIDAuthorizationRequest] for requesting this information.
        ///
        /// This information will only be provided on the first authorizations.
        /// Upon further authorizations, you will only get the [userIdentifier],
        /// meaning you will need to store this data securely on your servers.
        /// For more information see: https://forums.developer.apple.com/thread/121496
        'givenName': appleCredential.givenName,

        /// The users family name, in case it was requested.
        /// You will need to provide the [AppleIDAuthorizationScopes.fullName] scope to the [AppleIDAuthorizationRequest] for requesting this information.
        ///
        /// This information will only be provided on the first authorizations.
        /// Upon further authorizations, you will only get the [userIdentifier],
        /// meaning you will need to store this data securely on your servers.
        /// For more information see: https://forums.developer.apple.com/thread/121496
        'familyName': appleCredential.familyName,

        /// The users email in case it was requested.
        /// You will need to provide the [AppleIDAuthorizationScopes.email] scope to the [AppleIDAuthorizationRequest] for requesting this information.
        ///
        /// This information will only be provided on the first authorizations.
        /// Upon further authorizations, you will only get the [userIdentifier],
        /// meaning you will need to store this data securely on your servers.
        /// For more information see: https://forums.developer.apple.com/thread/121496
        'email': appleCredential.email,

        /// The verification code for the current authorization.
        ///
        /// This code should be used by your server component to validate the authorization with Apple within 5 minutes upon receiving it.
        'authorizationCode': appleCredential.authorizationCode,

        /// A JSON Web Token (JWT) that securely communicates information about the user to your app.
        'identityToken': appleCredential.identityToken,

        /// The `state` parameter that was passed to the request.
        ///
        /// This data is not modified by Apple.
        'state': appleCredential.state
      }, productList: [], recentList: [], likeList: []);
    } on PlatformException catch (e) {
      print('${e.code} ${e.details}');
    }
    return ZUserAccount.initZero();
  }

  @override
  Future<void> unlink() async {
    print('apple unlink');
  }

  @override
  String social() {
    return 'apple';
  }

  @override
  Future<void> logout() async {
    print('apple logout');
  }

  /// Generates a cryptographically secure random nonce, to be included in a
  /// credential request.
  String generateNonce([int length = 32]) {
    const charset = '0123456789ABCDEFGHIJKLMNOPQRSTUVXYZabcdefghijklmnopqrstuvwxyz-._';
    final random = Random.secure();
    return List.generate(length, (_) => charset[random.nextInt(charset.length)]).join();
  }

  /// Returns the sha256 hash of [input] in hex notation.
  String sha256ofString(String input) {
    final bytes = utf8.encode(input);
    final digest = sha256.convert(bytes);
    return digest.toString();
  }
}
