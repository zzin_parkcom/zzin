import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:get/get.dart';
import 'package:package_info_plus/package_info_plus.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:zzin/common/bottomsheet_custom.dart';
import 'package:zzin/common/zconfig.dart';
import 'package:zzin/common/zutils.dart';
import 'package:zzin/entrance/skeleton.dart';
import 'package:zzin/login/webbrowserpage.dart';
import 'package:zzin/my_page/block_user_manage_page.dart';
import 'package:zzin/my_page/notice_page.dart';
import 'package:zzin/my_page/profile_configuration_page.dart';
import 'logout_bottom_sheet.dart';

class ConfigurationPage extends StatefulWidget {
  const ConfigurationPage({Key? key}) : super(key: key);

  @override
  _ConfigurationPageState createState() => _ConfigurationPageState();
}

class _ConfigurationPageState extends State<ConfigurationPage> {
  final double fontSize = 18;
  String versionInfo = '';

  @override
  void initState() {
    super.initState();
    SchedulerBinding.instance!.addPostFrameCallback((_) => frameCallback(context));
  }

  void frameCallback(BuildContext context) async {
    var packageInfo = await PackageInfo.fromPlatform();

    versionInfo =
        packageInfo.version + '(' + packageInfo.buildNumber + (kReleaseMode ? 'R' : 'D') + ')';

    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return ListView(
      children: [
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            ListTile(
                minVerticalPadding: 0,
                title: Text('${ZConfig.userAccount.getSocialName()}계정 연결',
                    overflow: TextOverflow.ellipsis,
                    style: const TextStyle(fontSize: 18, color: ZConfig.grey_text)),
                subtitle: Text('버전정보: $versionInfo',
                    overflow: TextOverflow.ellipsis,
                    style: const TextStyle(fontSize: 10, color: ZConfig.grey_text))),
            sectionItem('공지사항', const NoticePage()),
            sectionItem('프로필 변경', const ProfileConfigurationPage()),
            sectionItem('차단 사용자 관리', const BlockUserManagePage()),
            ListTile(title: Text('문의하기', style: TextStyle(fontSize: fontSize)), onTap: onInquiry),
            sectionItem('이용 약관', const WebBrowserPage(ZConfig.termsAndConditions, '이용 약관')),
            sectionItem('개인정보 처리방침', const WebBrowserPage(ZConfig.privacyPolicy, '개인정보 처리방침')),
            ListTile(title: Text('로그아웃', style: TextStyle(fontSize: fontSize)), onTap: onLogout),
          ],
        ),
      ],
    );
  }

  Widget sectionTitle(String title) {
    return ListTile(
      title: Text(title,
          style:
              const TextStyle(fontSize: 22, fontWeight: FontWeight.bold, color: ZConfig.grey_text)),
    );
  }

  Widget sectionItem(String title, Widget openPage) {
    return ListTile(
      title: Text(title, style: TextStyle(fontSize: fontSize)),
      onTap: () => Get.to(() => openPage, transition: Transition.rightToLeftWithFade),
    );
  }

  void onInquiry() {
    final _emailLaunchUri = Uri(
        scheme: 'mailto',
        path: 'general@zzinmarket.kr',
        query:
            'subject=문의하기:${ZConfig.userAccount.nickname},$versionInfo&body=여기에 문의 내용을 작성해 주세요.');
    launch(_emailLaunchUri.toString());
  }

  void onLogout() async {
    var result = await showModalBottomSheetInt(
        context, (_) => const BottomSheetCustom(contents: LogoutBottomSheet(), sizeRatio: 0.25));
    print(result);

    if (result == 1) {
      await ZConfig.userAccount.logout();
      // showInfo(context, '${ZConfig.userAccount.getSocialName()} 으로 로그인 되었습니다.');

      MainTabController.to.setIndex(0);
      await Get.offAllNamed('/');
    }
  }
}
