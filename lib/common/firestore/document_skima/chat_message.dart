import 'package:cloud_firestore/cloud_firestore.dart';

// ignore: constant_identifier_names
enum ChatType { Text, Photo }

class ChatMessage {
  DocumentReference senderRef;
  DocumentReference receiverRef;
  Timestamp creationTime;
  int chatType;
  int regionCode;
  String text;
  bool isReadMesage;

  ChatMessage(
      {required this.senderRef,
      required this.receiverRef,
      required this.creationTime,
      required this.chatType,
      this.text = '',
      required this.regionCode,
      this.isReadMesage = false});

  ChatMessage.fromJson(Map<String, Object?> json)
      : this(
          senderRef: json['senderRef'] as DocumentReference,
          receiverRef: json['receiverRef'] as DocumentReference,
          creationTime: (json['creationTime'] ?? Timestamp.now()) as Timestamp,
          chatType: json['chatType'] as int,
          text: json['text'] as String,
          regionCode: json['regionCode'] as int,
          isReadMesage: json['isReadMesage'] as bool,
        );

  Map<String, Object?> toJson() {
    return {
      'senderRef': senderRef,
      'receiverRef': receiverRef,
      'creationTime': creationTime,
      'chatType': chatType,
      'text': text,
      'regionCode': regionCode,
      'isReadMesage': isReadMesage,
    };
  }
}
