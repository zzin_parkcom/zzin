import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:zzin/common/firestore/document_skima/chatroom.dart';
import 'package:zzin/common/firestore/document_skima/product.dart';
import 'package:zzin/common/zconfig.dart';

class QueryChatProduct {
  // QuerySnapshot<ChatRoom>? snapshots;
  List<DocumentSnapshot<Product>> products = [];
  DocumentSnapshot? lastDocument;
  bool isLoading = false; // track if products fetching
  bool hasMore = true; // flag for more products available or not
  int documentLimit = ZConfig.maxDocumentCount; // documents to
  int _saleStatusIndex = -1;

  Stream<QuerySnapshot<ChatRoom>> queryChatRoom(DocumentReference seller, int saleStatusIndex,
      {bool byScroll = false}) {
    // Query query = FirebaseFirestore.instance.collectionGroup(ZConfig.productGroupCollection);
    Query query = FirebaseFirestore.instance.collection(ZConfig.productsCollection);

    query = query.where('sellerId', isEqualTo: seller);
    if (saleStatusIndex != -1) {
      _saleStatusIndex = saleStatusIndex;
      query = query.where('saleStatus', isEqualTo: _saleStatusIndex);
    }

    // query.snapshots().where((event) => eve)

    var result = query.firestore
        .collectionGroup(ZConfig.chatRoomsGroupCollection)
        .withConverter<ChatRoom>(
            fromFirestore: (snapshots, _) => ChatRoom.fromJson(snapshots.data()!),
            toFirestore: (chatRoom, _) => chatRoom.toJson())
        .snapshots();

    return result;
  }
}
