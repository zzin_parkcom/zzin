import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:zzin/common/firestore/query_opentalk_list.dart';
import 'package:zzin/common/zconfig.dart';
import 'package:zzin/opentalk_page/opentalk_list_generator.dart';

class OpenTalkList extends StatefulWidget {
  const OpenTalkList({Key? key}) : super(key: key);

  @override
  _OpenTalkListState createState() => _OpenTalkListState();
}

class _OpenTalkListState extends State<OpenTalkList> {
  final ScrollController _scrollController = ScrollController();
  late final QueryOpentalkList queryOpentalk;

  @override
  void initState() {
    super.initState();
    queryOpentalk = Get.put(QueryOpentalkList(), tag: widget.toString());
    _scrollController.addListener(_scrollListener);
  }

  @override
  void dispose() {
    _scrollController.removeListener(_scrollListener);
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
        future: getOpentalkList(userRef: ZConfig.userAccount.docId, isSeller: true),
        builder: (context, snapshot) {
          if (snapshot.connectionState == ConnectionState.done) {
            return OpentalkListGenerator(
                onRefresh: () async {
                  await onRefresh(isSeller: true);
                },
                opentalks: queryOpentalk.opentalks,
                scrollController: _scrollController);
          } else {
            return const SizedBox.shrink();
          }
        });
  }

  Future<void> getOpentalkList({required userRef, required isSeller}) async {
    print('getOpentalkList Future');
    await queryOpentalk.queryOpentalk(byScroll: false);
  }

  Future onRefresh({required isSeller}) async {
    print('onRefresh');
    await queryOpentalk.queryOpentalk(byScroll: false);
  }

  void _scrollListener() async {
    var maxScroll = _scrollController.position.maxScrollExtent;
    var currentScroll = _scrollController.position.pixels;
    var delta = maxScroll * ZConfig.scrollRatioQueryMore;

    if (maxScroll - currentScroll <= delta && queryOpentalk.canQuery()) {
      print('scroll queryOpentalk');

      await queryOpentalk.queryOpentalk(byScroll: true);
    }
  }
}
