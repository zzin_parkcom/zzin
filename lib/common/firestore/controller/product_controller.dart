import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:get/get.dart';
import 'package:zzin/common/firestore/document_skima/product.dart';

class ProductController extends GetxController {
  late Product product;

  static ProductController get to => Get.find();

  Future<void> queryProduct(DocumentReference productRef) async {
    var snapshot = await productRef
        .withConverter<Product>(
            fromFirestore: (snapshots, _) => Product.fromJson(snapshots.data()!),
            toFirestore: (product, _) => product.toJson())
        .get();

    product = snapshot.data()!;
  }

  Future<void> queryProductByPath(String productPath) async {
    var snapshot = await FirebaseFirestore.instance
        .doc(productPath)
        // .collection(ZConfig.userCollection)
        // .doc(productPath)
        .withConverter<Product>(
            fromFirestore: (snapshot, _) => Product.fromJson(snapshot.data()!),
            toFirestore: (product, _) => product.toJson())
        .get();
    product = snapshot.data()!;
    print(product.docId);
  }
}
