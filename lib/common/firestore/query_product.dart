import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:get/get.dart';
import 'package:zzin/common/firestore/document_skima/product.dart';
import 'package:zzin/common/firestore/queryvalue.dart';
import 'package:zzin/common/hive/brand_collection.dart';
import 'package:zzin/common/hive/category_collection.dart';
import 'package:zzin/common/zconfig.dart';

class QueryProduct extends GetxController {
  List<DocumentSnapshot<Product>> products = <DocumentSnapshot<Product>>[].obs;
  QueryValue queryValue = QueryValue();
  DocumentSnapshot? lastDocument;
  bool isLoading = false; // track if products fetching
  bool hasMore = true; // flag for more products available or not
  int documentCount = ZConfig.maxDocumentCount; // documents to be fetched per request

  QueryProduct({this.documentCount = ZConfig.maxDocumentCount});

  // @override
  // void onInit() {
  //   super.onInit();
  // }

  bool canQuery() {
    return hasMore && !isLoading;
  }

  void clearProduct() {
    products.clear();
    lastDocument = null;
    isLoading = false;
    hasMore = true;
  }

  Future<int> queryProductByKeyword(String keyword,
      {bool byScroll = false, bool isReload = false}) async {
    return await queryProductDetail(
        keyword: keyword,
        categoryIndex: NotSelected,
        brandIndex: NotSelected,
        statusIndex: NotSelected,
        saleStausIndex: NotSelected,
        sortIndex: 0,
        addressStep: NotSelected,
        addressId: NotSelected,
        byScroll: byScroll,
        isReload: isReload);
  }

  Future<int> queryProductByCategory(int categoryIndex,
      {required int saleStausIndex, bool byScroll = false, bool isReload = false}) async {
    return await queryProductDetail(
        keyword: queryValue.keyword,
        categoryIndex: categoryIndex,
        brandIndex: queryValue.brand,
        statusIndex: queryValue.status,
        saleStausIndex: saleStausIndex,
        sortIndex: queryValue.sort,
        addressStep: queryValue.addressStep,
        addressId: queryValue.addressId,
        byScroll: byScroll,
        isReload: isReload);
  }

  Future<int> queryProductByBrand(int brandIndex,
      {bool byScroll = false, bool isReload = false}) async {
    return await queryProductDetail(
        keyword: queryValue.keyword,
        categoryIndex: queryValue.category,
        brandIndex: brandIndex,
        statusIndex: queryValue.status,
        saleStausIndex: queryValue.saleStatus,
        sortIndex: queryValue.sort,
        addressStep: queryValue.addressStep,
        addressId: queryValue.addressId,
        byScroll: byScroll,
        isReload: isReload);
  }

  Future<int> queryProductByStatus(int statusIndex,
      {bool byScroll = false, bool isReload = false}) async {
    return await queryProductDetail(
        keyword: queryValue.keyword,
        categoryIndex: queryValue.category,
        brandIndex: queryValue.brand,
        statusIndex: statusIndex,
        saleStausIndex: queryValue.saleStatus,
        sortIndex: queryValue.sort,
        addressStep: queryValue.addressStep,
        addressId: queryValue.addressId,
        byScroll: byScroll,
        isReload: isReload);
  }

  Future<int> queryProductBySaleStatus(int saleStatusIndex,
      {bool byScroll = false, bool isReload = false}) async {
    return await queryProductDetail(
        keyword: queryValue.keyword,
        categoryIndex: queryValue.category,
        brandIndex: queryValue.brand,
        statusIndex: queryValue.status,
        saleStausIndex: saleStatusIndex,
        sortIndex: queryValue.sort,
        addressStep: queryValue.addressStep,
        addressId: queryValue.addressId,
        byScroll: byScroll,
        isReload: isReload);
  }

  Future<int> queryProductBySort(int sortIndex,
      {required int saleStausIndex, bool byScroll = false, bool isReload = false}) async {
    return await queryProductDetail(
        keyword: queryValue.keyword,
        categoryIndex: queryValue.category,
        brandIndex: queryValue.brand,
        statusIndex: queryValue.status,
        saleStausIndex: saleStausIndex,
        sortIndex: sortIndex,
        addressStep: queryValue.addressStep,
        addressId: queryValue.addressId,
        byScroll: byScroll,
        isReload: isReload);
  }

  Future<int> queryProductByFilter(int statusIndex, int saleStatusIndex,
      {bool byScroll = false, bool isReload = false}) async {
    return await queryProductDetail(
        keyword: queryValue.keyword,
        categoryIndex: queryValue.category,
        brandIndex: queryValue.brand,
        statusIndex: statusIndex,
        saleStausIndex: saleStatusIndex,
        sortIndex: queryValue.sort,
        addressStep: queryValue.addressStep,
        addressId: queryValue.addressId,
        byScroll: byScroll,
        isReload: isReload);
  }

  Future<int> queryProductByLocation(int locationScope, int locationList,
      {bool byScroll = false, bool isReload = false}) async {
    return await queryProductDetail(
        keyword: queryValue.keyword,
        categoryIndex: queryValue.category,
        brandIndex: queryValue.brand,
        statusIndex: queryValue.status,
        saleStausIndex: queryValue.saleStatus,
        sortIndex: queryValue.sort,
        addressStep: locationScope,
        addressId: locationList,
        byScroll: byScroll,
        isReload: isReload);
  }

  //////////////////////////////////////////////////////////////////////////////////////////////////
  ///

  Future<int> queryProductDetail(
      {required String keyword,
      required int categoryIndex,
      required int brandIndex,
      required int statusIndex,
      required int saleStausIndex,
      required int sortIndex,
      required int addressStep,
      required int addressId,
      required bool byScroll,
      bool isReload = false}) async {
    // 검색 시작전 products 및 기타 clear. 스크롤로 들어오면 기존 products 유지.
    if (!byScroll) {
      var needSearch = queryValue.save(
        keywordParam: keyword,
        categoryParam: categoryIndex,
        brandParam: brandIndex,
        statusParam: statusIndex,
        saleStatusParam: saleStausIndex,
        sortParam: sortIndex,
        addressStepParam: addressStep,
        addressIdParam: addressId,
      );

      if (!needSearch && !isReload) {
        print('이전 조건과 같아 쿼리 하지 않습니다.');
        isLoading = false;
        return NotFound;
      }
      clearProduct();
    }

    if (!hasMore) {
      print('더이상 상품이 없습니다.');
      return NotFound;
    }
    if (isLoading) {
      print('쿼리중... ');
      return NotFound;
    }
    isLoading = true;

    Query query = FirebaseFirestore.instance
        // .collectionGroup(ZConfig.productGroupCollection)
        .collection(ZConfig.productsCollection)
        .withConverter<Product>(
            fromFirestore: (snapshots, _) => Product.fromJson(snapshots.data()!),
            toFirestore: (product, _) => product.toJson());

    // 삭제된 상품은 무조건 제거.
    // 0: 일반 1: 삭제된 상품
    query = query.where(
      'articleStatus',
      isEqualTo: 0,
    );

    var keywordList = <String>[];
    if (keyword.isNotEmpty) {
      queryValue.keyword.trim();
      keywordList = queryValue.keyword.split(' ');
    }

    // 카테고리 분류 처리
    if (queryValue.category != -1) {
      query = query.where('category', isEqualTo: queryValue.category);
      print('선택된 카테고리 : ${CategoryCollection.getNameKr(queryValue.category)}');
    }

    // 브랜드 분류 처리
    if (queryValue.brand != -1) {
      query = query.where('brand', isEqualTo: queryValue.brand);
      print('선택된 브랜드 : ${BrandCollection.getNameKr(queryValue.brand)}');
    }

    // 상품 상태 분류 처리
    if (queryValue.status != -1) {
      query = query.where('status', isEqualTo: queryValue.status);
    }

    // 거래 상태 분류 처리
    if (queryValue.saleStatus != -1) {
      query = query.where('saleStatus', isEqualTo: queryValue.saleStatus);
    }

    // 지역 분류 처리
    if (queryValue.addressStep != NotSelected && queryValue.addressId != NotSelected) {
      switch (queryValue.addressStep) {
        case 0: // 위례동
          query = query.where('regionCode', isEqualTo: queryValue.addressId);
          break;
        case 1: // 성남시 수정구
          var regionCode = int.parse(queryValue.addressId.toString().substring(0, 5));

          query = query.where('region2Depth', isEqualTo: regionCode);
          break;

        case 2: // 경기도
          var regionCode = int.parse(queryValue.addressId.toString().substring(0, 2));
          query = query.where('region1Depth', isEqualTo: regionCode);
          break;
        case 3: // 전체
        default:
          break;
      }
    }

    // 키워드 태그 검색 처리.
    if (keyword.isNotEmpty) {
      query = query.where('tags', arrayContainsAny: keywordList);
    }

    // 정렬 쿼리
    switch (queryValue.sort) {
      case 0: // 최신순
        query = query.orderBy('saleModifyDate', descending: true);
        break;
      case 1: // 가격 낮은 순
        query = query.orderBy('price', descending: false);
        break;
      case 2: // 가격 높은 순
        query = query.orderBy('price', descending: true);
        break;
      case 3: // likeCounts
        query = query.orderBy('likeCounts', descending: true);
        break;
      default:
        query = query.orderBy('saleModifyDate', descending: true);
        break;
    }

    do {
      // 이어서 쿼리하기.
      if (lastDocument == null) {
        // 처음 검색하는 경우.
        query = query.limit(documentCount);
      } else {
        query = query.startAfterDocument(lastDocument!).limit(documentCount);
      }

      // 쿼리 실행.
      try {
        var snapshots = await query
            .withConverter<Product>(
                fromFirestore: (snapshots, _) => Product.fromJson(snapshots.data()!),
                toFirestore: (product, _) => product.toJson())
            .get();

        print('검색된 갯수: ${snapshots.docs.length}');

        if (snapshots.docs.length < documentCount) {
          hasMore = false;
        }

        if (snapshots.docs.isNotEmpty) {
          lastDocument = snapshots.docs[snapshots.docs.length - 1];
          products.addAll(snapshots.docs);
          print('상품리스트 갯수: ${products.length}');

          if (ZConfig.userAccount.isRegistered() && ZConfig.blockUsers.isNotEmpty) {
            products.removeWhere((product) => ZConfig.blockUsers
                .any((blockUser) => blockUser.userDocId == product.data()!.sellerId));
          }
          print('차단 리스트 제거 후 상품리스트 갯수: ${products.length}');
          isLoading = false;
        }

        // for (var i = 0; i < products.length; i++) {
        //   precacheImage(CachedNetworkImageProvider(products[i].data()!.thumbnail), Get.context!);
        // }

        // return products.length;
      } catch (e) {
        print('복합 인덱스가 필요한지 보세요~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~');
        print('복합 인덱스가 필요한지 보세요~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~');
        print('복합 인덱스가 필요한지 보세요~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~');

        print(e);
        return NotFound;
      }
    } while (hasMore && products.length < documentCount);

    return products.length;
  }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////
