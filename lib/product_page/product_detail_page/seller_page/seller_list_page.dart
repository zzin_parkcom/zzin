import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:transparent_image/transparent_image.dart';
import 'package:zzin/common/firestore/document_skima/user_account.dart';
import 'package:zzin/common/utils/zenum.dart';
import 'package:zzin/common/widget/ztab.dart';
import 'package:zzin/common/zcache_manager.dart';
import 'package:zzin/entrance/skeleton.dart';
import 'package:zzin/product_page/product_detail_page/seller_page/seller_listview.dart';

class SellerListPage extends StatefulWidget {
  final ZUserAccount seller;
  const SellerListPage({required this.seller, Key? key}) : super(key: key);

  @override
  _SellerListPageState createState() => _SellerListPageState();
}

class _SellerListPageState extends State<SellerListPage> {
  List<Tab> tabs = [
    const Tab(
      child: Text('판매중', style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold)),
    ),
    const Tab(child: Text('거래완료', style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold))),
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: AppBar(),
      body: SafeArea(
        child: Column(
          children: [
            sellerInfo(),
            // topTab(),
            // Divider(),
            Expanded(
              child: ZTab(
                keyString: MainTabController.to.tabIndex.value.toString(),
                tabs: tabs,
                tabViews: [
                  SellerListView(
                      seller: widget.seller,
                      saleStatus: ProductSaleStatus.ONSALE,
                      noProductMessages: const ['판매중인 상품이 없습니다.']),
                  SellerListView(
                      seller: widget.seller,
                      saleStatus: ProductSaleStatus.SOLDOUT,
                      noProductMessages: const ['거래완료된 상품이 없습니다.'])
                ],
              ),
            )
          ],
        ),
      ),
    );
  }

  Widget sellerInfo() {
    return Container(
      padding: const EdgeInsets.fromLTRB(20, 0, 20, 0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Row(
            children: [
              SizedBox(
                height: 20,
                width: 20,
                child: ClipRRect(
                  borderRadius: BorderRadius.circular(30),
                  child: CachedNetworkImage(
                    fadeInDuration: const Duration(milliseconds: 0),
                    fadeOutDuration: const Duration(milliseconds: 0),
                    placeholderFadeInDuration: const Duration(milliseconds: 0),
                    placeholder: (_, __) => Image.memory(kTransparentImage),
                    imageUrl: widget.seller.getThumbnailPath(),
                    fit: BoxFit.contain,
                    errorWidget: (context, url, error) => const Icon(Icons.error),
                    cacheManager: ZCacheManager.instance,
                  ),
                ),
              ),
              const SizedBox(width: 5),
              Text(
                ('${widget.seller.nickname} 님의 판매 정보'),
                style: const TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
              ),
            ],
          ),
        ],
      ),
    );
  }

  // void searchBySaleStatus(BuildContext context, int index) async {
  //   //버튼 그룹 처리.
  //   setState(() {
  //     for (var buttonIndex = 0; buttonIndex < _isSelectedList.length; buttonIndex++) {
  //       if (buttonIndex == index) {
  //         _isSelectedList[buttonIndex] = true;
  //       } else {
  //         _isSelectedList[buttonIndex] = false;
  //       }
  //     }
  //   });
  //   var saleStatusIndex =
  //       _isSelectedList.first == true ? SaleStatus.ONSALE.index : SaleStatus.SOLDOUT.index;

  //   await _queryUserProduct.queryUserProduct(widget.seller.id!, saleStatusIndex);
  // }
}
