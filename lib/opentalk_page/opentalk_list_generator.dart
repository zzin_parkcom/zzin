import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:zzin/common/firestore/document_skima/opentalk_data.dart';
import 'package:zzin/common/zconfig.dart';
import 'package:zzin/opentalk_page/join_opentalk.dart';

class OpentalkListGenerator extends StatelessWidget {
  final List<DocumentSnapshot<OpentalkData>> opentalks;
  final Future<void> Function() onRefresh;
  final ScrollController scrollController;
  final List<String> noOpentalkMessages = ['활성화된 오픈런이 없습니다.'];
  OpentalkListGenerator(
      {Key? key, required this.onRefresh, required this.opentalks, required this.scrollController})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return opentalks.isEmpty
        ? noOpentalk()
        : RefreshIndicator(
            onRefresh: onRefresh,
            child: Obx(
              () => ListView.separated(
                padding: const EdgeInsets.all(0),
                separatorBuilder: (context, index) => const Divider(),
                physics: const BouncingScrollPhysics(parent: AlwaysScrollableScrollPhysics()),
                controller: scrollController,
                itemCount: opentalks.length,
                itemBuilder: (context, index) {
                  var opentalk = opentalks[index].data()!;
                  return ListTile(
                    title: Text(opentalk.name),
                    trailing: Text(' (${opentalks[index].data()!.participants.length})'),
                    onTap: () => onJoinOpentalk(opentalk),
                  );
                },
              ),
            ),
          );
  }

  Widget noOpentalk() {
    return RefreshIndicator(
      onRefresh: onRefresh,
      child: ListView.builder(
        itemExtent: Get.height / 3,
        itemCount: 1,
        itemBuilder: (context, index) {
          return Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: List<Widget>.generate(noOpentalkMessages.length, (index) {
                return Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Text(
                    noOpentalkMessages[index],
                    style: const TextStyle(fontSize: 20, color: ZConfig.grey_text),
                  ),
                );
              }));
        },
      ),
    );
  }

  void onJoinOpentalk(OpentalkData opentalk) {
    Get.to(() => JoinOpentalkView(opentalk: opentalk));
  }
}
