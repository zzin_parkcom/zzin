import 'package:flutter/material.dart';
import 'package:zzin/common/zconfig.dart';
import 'package:zzin/search_page/search_option/search_option_chips.dart';

class SearchOptionButton extends StatefulWidget {
  final Future<bool> Function() onSearchOption;
  const SearchOptionButton({required this.onSearchOption, Key? key}) : super(key: key);

  @override
  _SearchOptionButtonState createState() => _SearchOptionButtonState();
}

class _SearchOptionButtonState extends State<SearchOptionButton> {
  final ScrollController scrollController = ScrollController();
  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        OutlinedButton(
          style: ButtonStyle(
            padding: MaterialStateProperty.all(const EdgeInsets.fromLTRB(10, 0, 10, 0)),
            shape: MaterialStateProperty.all<RoundedRectangleBorder>(
              RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(5.0),
                side: const BorderSide(color: ZConfig.zzin_pointColor),
              ),
            ),
            foregroundColor: MaterialStateProperty.all<Color>(ZConfig.zzin_black),
            backgroundColor: MaterialStateProperty.all<Color>(ZConfig.zzin_white),
          ),
          onPressed: () async {
            var result = await widget.onSearchOption();
            if (result == true) {
              setState(() {
                print('refresh');
              });
            }
          },
          child: Row(
            children: const [
              Icon(Icons.tune, size: 15),
              SizedBox(width: 5),
              Text('검색 설정', style: TextStyle(fontSize: 15)),
            ],
          ),
        ),
        const SizedBox(width: 5),
        Expanded(
          child: SingleChildScrollView(
            controller: scrollController,
            scrollDirection: Axis.horizontal,
            child: SearchOptionChips(),
          ),
        ),
      ],
    );
  }
}
