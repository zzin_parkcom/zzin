import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:transparent_image/transparent_image.dart';
import 'package:zzin/common/firestore/document_skima/product.dart';
import 'package:zzin/common/firestore/document_skima/user_account.dart';
import 'package:zzin/common/zcache_manager.dart';
import 'package:zzin/common/zconfig.dart';
import 'package:zzin/product_page/product_detail_page/product_detail_view.dart';
import 'package:zzin/common/firestore/query_user_product.dart';
import 'package:zzin/product_page/product_detail_page/seller_page/seller_list_page.dart';

class HorizontalSellerView extends StatelessWidget {
  final ScrollController controller = ScrollController();
  late final QueryUserProduct _queryUserProduct;
  late final ZUserAccount _sellerAccount;

  HorizontalSellerView(this._queryUserProduct, this._sellerAccount, {Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      padding: const EdgeInsets.fromLTRB(20, 5, 20, 0),
      child: Column(
        children: [
          const Divider(),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Row(
                children: [
                  SizedBox(
                    height: 20,
                    width: 20,
                    child: ClipRRect(
                      borderRadius: BorderRadius.circular(30),
                      child: CachedNetworkImage(
                        fadeInDuration: const Duration(milliseconds: 0),
                        fadeOutDuration: const Duration(milliseconds: 0),
                        placeholderFadeInDuration: const Duration(milliseconds: 0),
                        placeholder: (_, __) => Image.memory(kTransparentImage),
                        imageUrl: _sellerAccount.getThumbnailPath(),
                        fit: BoxFit.contain,
                        errorWidget: (context, url, error) => const Icon(Icons.error),
                        cacheManager: ZCacheManager.instance,
                      ),
                    ),
                  ),
                  const SizedBox(width: 5),
                  Text(
                    (_sellerAccount.nickname ?? ''),
                    style: const TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
                  ),
                ],
              ),
              TextButton(
                  onPressed: () {
                    onSellerButton(context);
                  },
                  child: const Text('판매자 정보 더보기'))
            ],
          ),
          SizedBox(
            height: 110,
            child: ListView.separated(
                scrollDirection: Axis.horizontal,
                itemCount: _queryUserProduct.products.length,
                separatorBuilder: (context, index) => const SizedBox(width: 10),
                itemBuilder: (context, index) {
                  var product = _queryUserProduct.products[index].data();
                  if (product == null) {
                    throw Exception('Product is NULL!!!');
                  }
                  return GestureDetector(
                    onTap: () {
                      onListItemTap(context, product);
                    },
                    child: Container(
                      width: 110,
                      height: 110,
                      color: ZConfig.zzin_white,
                      child: ClipRRect(
                        borderRadius: BorderRadius.circular(5),
                        child: CachedNetworkImage(
                          fadeInDuration: const Duration(milliseconds: 0),
                          fadeOutDuration: const Duration(milliseconds: 0),
                          placeholderFadeInDuration: const Duration(milliseconds: 0),
                          placeholder: (_, __) => Image.memory(kTransparentImage),
                          imageUrl: product.thumbnail,
                          fit: BoxFit.contain,
                          errorWidget: (context, url, error) => const Icon(Icons.error),
                          cacheManager: ZCacheManager.instance,
                        ),
                      ),
                    ),
                  );
                }),
          ),
        ],
      ),
    );
  }

  void onSellerButton(BuildContext context) {
    Get.to(() => SellerListPage(seller: _sellerAccount),
        transition: Transition.rightToLeftWithFade);
  }

  void onListItemTap(BuildContext context, Product product) {
    var tag = DateTime.now().toString();
    Get.put(Product.fromJson(product.toJson()), tag: tag);
    Get.to(
        () => ProductDetailView(
              tag: tag,
              tagHeroPrefix: 'Horizontal_',
            ),
        transition: Transition.fadeIn,
        duration: ZConfig.heroAnimationDuration,
        preventDuplicates: false);
  }
}
