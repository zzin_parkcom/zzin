import 'dart:async';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:get/get.dart';
import 'package:kakaomap_webview/kakaomap_webview.dart';
import 'package:transparent_image/transparent_image.dart';
import 'package:zzin/chat_page/chat_ui/full_screen_map_view.dart';
import 'package:zzin/chat_page/chat_ui/full_screen_view.dart';
import 'package:zzin/common/firestore/document_skima/chat_message.dart';
import 'package:zzin/common/firestore/document_skima/chatroom.dart';
import 'package:zzin/common/firestore/document_skima/product.dart';
import 'package:zzin/common/firestore/document_skima/user_account.dart';

import 'package:zzin/common/zcache_manager.dart';
import 'package:zzin/common/zconfig.dart';
import 'package:zzin/common/zutils.dart';

class ChatUIMessageList extends StatefulWidget {
  final ScrollController scrollController;
  final Product product;
  final ZUserAccount sender; // 나
  final ZUserAccount receiver; // 판매자.
  const ChatUIMessageList(
      {required this.sender,
      required this.receiver,
      required this.product,
      required this.scrollController,
      Key? key})
      : super(key: key);

  @override
  _ChatUIMessageListState createState() => _ChatUIMessageListState();
}

class _ChatUIMessageListState extends State<ChatUIMessageList> {
  Stream<QuerySnapshot<ChatMessage>>? stream;
  @override
  void dispose() {
    widget.scrollController.dispose();
    super.dispose();
  }

  @override
  void initState() {
    super.initState();

    SchedulerBinding.instance!.addPostFrameCallback((_) => frameCallback(context));
  }

  void frameCallback(BuildContext context) async {
    stream = await ChatRoom.getMessages(
        buyerRef: widget.sender.docId != widget.product.sellerId
            ? widget.sender.docId!
            : widget.receiver.docId,
        sellerRef: widget.product.sellerId!,
        productRef: widget.product.docId);
    setState(() {});
  }

  Future<void> scrollDown() async {
    print('scrollDown');
    // await Future.delayed(Duration(milliseconds: 100));
    if (mounted) {
      await widget.scrollController.animateTo(
        widget.scrollController.position.maxScrollExtent,
        duration: const Duration(milliseconds: 100),
        curve: Curves.fastOutSlowIn,
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    return StreamBuilder<QuerySnapshot<ChatMessage>>(
      stream: stream,
      builder: (context, snapshot) {
        if (!snapshot.hasData) {
          return const Center(child: Text('채팅 메시지가 없네요...'));
        } else {
          Timer(const Duration(milliseconds: 100), () => scrollDown());
          Timer(const Duration(milliseconds: 300), () => scrollDown());
          lastDisplayDay = 0;
          return Padding(
            padding: const EdgeInsets.fromLTRB(0, 0, 0, 5),
            child: ListView.separated(
              separatorBuilder: (context, index) => const SizedBox(height: 10),
              controller: widget.scrollController,
              itemCount: snapshot.data!.size,
              shrinkWrap: true,
              padding: const EdgeInsets.only(top: 110, bottom: 5),
              physics: const ClampingScrollPhysics(),
              itemBuilder: (context, index) {
                var message = snapshot.data!.docs[index].data();
                return Column(
                  children: [
                    displayFirstDateTime(message),
                    Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        isTheOther(message) ? sellerImage() : const SizedBox.shrink(),
                        Expanded(
                            child: Row(
                          crossAxisAlignment: CrossAxisAlignment.end,
                          mainAxisAlignment:
                              isTheOther(message) ? MainAxisAlignment.start : MainAxisAlignment.end,
                          children: [
                            isTheOther(message)
                                ? const SizedBox.shrink()
                                : messageTime(message.creationTime),
                            Flexible(
                              child: IntrinsicWidth(
                                child: ConstrainedBox(
                                  constraints: BoxConstraints(
                                      maxWidth: MediaQuery.of(context).size.width / 5 * 3),
                                  child: messageBox(message),
                                ),
                              ),
                            ),
                            isTheOther(message)
                                ? messageTime(message.creationTime)
                                : const SizedBox.shrink()
                          ],
                        )),
                      ],
                    ),
                  ],
                );
              },
            ),
          );
        }
      },
    );
  }

  int lastDisplayDay = 0;
  Widget displayFirstDateTime(ChatMessage message) {
    if (lastDisplayDay != message.creationTime.toDate().day) {
      lastDisplayDay = message.creationTime.toDate().day;
      return Center(
          child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Text(
          formatYYYYMMDDW(message.creationTime),
          style: const TextStyle(color: ZConfig.grey_text, fontSize: 12),
        ),
      ));
    } else {
      return const SizedBox.shrink();
    }
  }

  Widget messageBox(ChatMessage message) {
    return Align(
      alignment: (isTheOther(message) ? Alignment.topLeft : Alignment.topRight),
      child: Container(
        decoration: BoxDecoration(
          borderRadius: BorderRadius.only(
              topLeft: isTheOther(message) ? const Radius.circular(0) : const Radius.circular(10),
              topRight: const Radius.circular(10),
              bottomLeft: const Radius.circular(10),
              bottomRight:
                  isTheOther(message) ? const Radius.circular(10) : const Radius.circular(0)),
          color: (isTheOther(message)
              ? ZConfig.grey_border
              : (isLocationInfo(message.text) || isImageUrl(message.text))
                  ? Colors.white
                  : ZConfig.chat_yellow),
        ),
        padding: (isLocationInfo(message.text) || isImageUrl(message.text))
            ? const EdgeInsets.all(0)
            : const EdgeInsets.all(10),
        child: processingMessage(message.text),
      ),
    );
  }

  Widget processingMessage(String message) {
    if (isLocationInfo(message)) {
      var lat = double.parse(message.split('|')[1]);
      var lng = double.parse(message.split('|')[2]);
      return SizedBox(
        width: Get.width / 2,
        height: Get.width / 3,
        child: ClipRRect(
          borderRadius: BorderRadius.circular(10),
          child: ColoredBox(
            color: ZConfig.zzin_pointColor,
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                Expanded(
                  flex: 4,
                  child: KakaoMapView(
                    width: Get.width / 2,
                    height: Get.width / 3,
                    kakaoMapKey: '3188407d880a18cb063d84ecef05766d',
                    lat: lat,
                    lng: lng,
                    showMapTypeControl: false,
                    showZoomControl: false,
                  ),
                ),
                Expanded(
                  flex: 1,
                  child: ListTile(
                    minVerticalPadding: 0,
                    horizontalTitleGap: 0,
                    visualDensity: const VisualDensity(vertical: -4),
                    dense: true,
                    contentPadding: const EdgeInsets.all(0.0),
                    title: const Center(
                        child: Text(
                      '확대하기',
                      style: TextStyle(color: ZConfig.zzin_white, fontSize: 12, height: 1),
                    )),
                    onTap: () async {
                      KakaoMapUtil util = KakaoMapUtil();
                      String url = await util.getMapScreenURL(lat, lng, name: '현재위치');
                      print(url);
                      Get.to(() => FullScreenMapView(lat: lat, lng: lng));
                    },
                  ),
                ),
              ],
            ),
          ),
        ),
      );
    } else if (isImageUrl(message)) {
      return SizedBox(
        width: Get.width / 2,
        height: Get.width / 3,
        child: ClipRRect(
          borderRadius: BorderRadius.circular(10),
          child: ColoredBox(
            color: ZConfig.zzin_pointColor,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              mainAxisSize: MainAxisSize.min,
              children: [
                Expanded(
                  flex: 4,
                  child: GestureDetector(
                    onTap: () {
                      Get.to(() => FullScreenView(imageUrl: message));
                    },
                    child: CachedNetworkImage(
                      fadeInDuration: const Duration(milliseconds: 0),
                      fadeOutDuration: const Duration(milliseconds: 0),
                      placeholderFadeInDuration: const Duration(milliseconds: 0),
                      placeholder: (_, __) => Image.memory(kTransparentImage),
                      imageUrl: message,
                      fit: BoxFit.cover,
                      errorWidget: (context, url, error) => const Icon(Icons.error),
                      cacheManager: ZCacheManager.instance,
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      );
    }

    return Text(
      message,
      overflow: TextOverflow.ellipsis,
      maxLines: 10,
      style: const TextStyle(fontSize: 14),
    );
  }

  bool isLocationInfo(String message) {
    return message.startsWith('locationInfo|');
  }

  bool isImageUrl(String message) {
    try {
      var result = Uri.parse(message);
      return result.isAbsolute && message.contains('.jpg?alt=media&token');
    } on FormatException {
      return false;
    }
  }

  bool isTheOther(ChatMessage message) {
    return message.receiverRef == ZConfig.userAccount.docId;
  }

  Widget messageTime(Timestamp time) {
    return Padding(
      padding: const EdgeInsets.fromLTRB(5, 0, 5, 0),
      child: SizedBox(
          width: 55,
          height: 30,
          child: Align(
              alignment: Alignment.bottomCenter,
              child: Text(formatHHMM(time),
                  style: const TextStyle(fontSize: 10, color: ZConfig.grey_text)))),
    );
  }

  Widget sellerImage() {
    return Padding(
      padding: const EdgeInsets.only(right: 10),
      child: SizedBox(
        width: 25,
        height: 25,
        child: ClipRRect(
          borderRadius: BorderRadius.circular(30),
          child: CachedNetworkImage(
            fadeInDuration: const Duration(milliseconds: 0),
            fadeOutDuration: const Duration(milliseconds: 0),
            placeholderFadeInDuration: const Duration(milliseconds: 0),
            placeholder: (_, __) => Image.memory(kTransparentImage),
            imageUrl: widget.receiver.getThumbnailPath(),
            fit: BoxFit.contain,
            errorWidget: (context, url, error) => const Icon(Icons.error),
            cacheManager: ZCacheManager.instance,
          ),
        ),
      ),
    );
  }
}
