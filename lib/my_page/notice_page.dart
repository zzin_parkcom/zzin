import 'package:flutter/material.dart';
import 'package:zzin/common/zconfig.dart';
import 'package:zzin/my_page/notice_listview.dart';

class NoticePage extends StatelessWidget {
  const NoticePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: AppBar(title: const Text('공지사항', style: TextStyle(fontSize: ZSize.appBarTextSize))),
      body: SafeArea(
        child: Padding(
          padding: const EdgeInsets.fromLTRB(20, 20, 20, 0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: const [
              NoticeListView(),
            ],
          ),
        ),
      ),
    );
  }
}
