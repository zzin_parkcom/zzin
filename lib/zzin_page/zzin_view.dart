import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:zzin/common/elastic_search/es_product.dart';
import 'package:zzin/common/firestore/query_opentalk_list.dart';
import 'package:zzin/common/utils/zenum.dart';
import 'package:zzin/common/zconfig.dart';
import 'package:zzin/common/zutils.dart';
import 'package:zzin/postbox/postbox_editor.dart';
import 'package:zzin/postbox/postbox_editor/postbox_modify.dart';
import 'package:zzin/zzin_page/test/heic_to_jpg_test_page.dart';

class ZzinView extends StatefulWidget {
  const ZzinView({Key? key}) : super(key: key);

  @override
  _ZzinViewState createState() => _ZzinViewState();
}

class _ZzinViewState extends State<ZzinView> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: Padding(
          padding: const EdgeInsets.all(8.0),
          child: ListView(
            children: [
              productCount(),
              onSaleProductCount(),
              onSoldOutProductCount(),
              esProductCount(),
              registeredUserCount(),
              ElevatedButton(
                child: const Text('알림함 메시지 작성'),
                onPressed: () => onPostboxEditor(),
              ),
              ElevatedButton(
                child: const Text('알림함 메시지 수정'),
                onPressed: () => onPostboxModify(),
              ),
              const Divider(
                height: 50,
              ),
              ElevatedButton(
                child: const Text('오픈런 추가'),
                onPressed: () => onAddOpentalk(context),
              ),
              ElevatedButton(
                child: const Text('heic image 테스트'),
                onPressed: () => onTestHeic(),
              ),
            ],
          ),
        ),
      ),
    );
  }

////////////////////////////////////////////////////////////////////////////////////////////////////
  Widget productCount() {
    return FutureBuilder(
      future: countProduct(),
      builder: (context, AsyncSnapshot snapshot) {
        if (snapshot.connectionState == ConnectionState.done) {
          var count = snapshot.data!;
          return Text('총 등록상품 개수: $count');
        } else {
          return const SizedBox.shrink();
        }
      },
    );
  }

  Widget onSaleProductCount() {
    return FutureBuilder(
      future: countOnSaleProduct(),
      builder: (context, AsyncSnapshot snapshot) {
        if (snapshot.connectionState == ConnectionState.done) {
          var count = snapshot.data!;
          return Text('총 판매중인 상품 개수: $count');
        } else {
          return const SizedBox.shrink();
        }
      },
    );
  }

  Widget onSoldOutProductCount() {
    return FutureBuilder(
      future: countSoldOutProduct(),
      builder: (context, AsyncSnapshot snapshot) {
        if (snapshot.connectionState == ConnectionState.done) {
          var count = snapshot.data!;
          return Text('총 판매완료 상품 개수: $count');
        } else {
          return const SizedBox.shrink();
        }
      },
    );
  }

  Future<int> countProduct() async {
    QuerySnapshot _myDoc =
        await FirebaseFirestore.instance.collection(ZConfig.productsCollection).get();
    List<DocumentSnapshot> _myDocCount = _myDoc.docs;
    return _myDocCount.length;
  }

  Future<int> countOnSaleProduct() async {
    QuerySnapshot _myDoc = await FirebaseFirestore.instance
        .collection(ZConfig.productsCollection)
        .where('saleStatus', isEqualTo: ProductSaleStatus.ONSALE.index)
        .get();
    List<DocumentSnapshot> _myDocCount = _myDoc.docs;
    return _myDocCount.length;
  }

  Future<int> countSoldOutProduct() async {
    QuerySnapshot _myDoc = await FirebaseFirestore.instance
        .collection(ZConfig.productsCollection)
        .where('saleStatus', isEqualTo: ProductSaleStatus.SOLDOUT.index)
        .get();
    List<DocumentSnapshot> _myDocCount = _myDoc.docs;
    return _myDocCount.length;
  }

////////////////////////////////////////////////////////////////////////////////////////////////////
  Widget registeredUserCount() {
    return FutureBuilder(
      future: countRegisteredUser(),
      builder: (context, AsyncSnapshot snapshot) {
        if (snapshot.connectionState == ConnectionState.done) {
          var count = snapshot.data!;
          return Text('총 회원가입 유저: $count');
        } else {
          return const SizedBox.shrink();
        }
      },
    );
  }

  Future<int> countRegisteredUser() async {
    QuerySnapshot _myDoc =
        await FirebaseFirestore.instance.collection(ZConfig.userCollection).get();
    List<DocumentSnapshot> _myDocCount = _myDoc.docs;
    return _myDocCount.length;
  }

////////////////////////////////////////////////////////////////////////////////////////////////////
  Widget esProductCount() {
    return FutureBuilder(
      future: countEsProduct(),
      builder: (context, AsyncSnapshot snapshot) {
        if (snapshot.connectionState == ConnectionState.done) {
          var count = snapshot.data!;
          return Text('총 Elastic 등록상품 개수: $count');
        } else {
          return const SizedBox.shrink();
        }
      },
    );
  }

  Future<int> countEsProduct() async {
    var count = await ESProduct().boolSearch();
    return count;
  }

  void onPostboxEditor() {
    Get.to(() => const PostboxEditor(),
        transition: Transition.upToDown, duration: ZConfig.heroAnimationDuration);
  }

  void onPostboxModify() {
    Get.to(() => const PostboxModify(),
        transition: Transition.upToDown, duration: ZConfig.heroAnimationDuration);
  }

  void onTestHeic() {
    Get.to(() => const Heic2JpgTestPage(),
        transition: Transition.upToDown, duration: ZConfig.heroAnimationDuration);
  }

  void onAddOpentalk(BuildContext context) async {
    var name = await showInputDialog(context, '오픈런 이름', '추가할까요?');
    if (name.isNotEmpty) {
      QueryOpentalkList().addOpentalk(name);
    }
  }
}
