/* 
PUT products_0701
{
  "settings": {
    "analysis": {
      "analyzer": {
        "nori": {
          "tokenizer": "nori_tokenizer"
        }
      },
      "tokenizer": {
        "nori_tokenizer": {
          "type": "nori_tokenizer",
          "decompound_mode ": "mixed"
        }
      }
    }
  },
  "mappings": {
    "dynamic_templates": [
      {
        "geolocation to geo_point": {
          "match": "regionGeoPoint",
          "match_mapping_type": "string",
          "runtime": {
            "type": "geo_point"
          }
        }
      },
      {
        "Register Timestamp to Date": {
          "match": "saleRegistDate._seconds",
          "match_mapping_type": "long",
          "runtime": {
            "type": "date"
          }
        }
      },
      {
        "Modify Timestamp to Date": {
          "match": "saleModifyDate._seconds",
          "match_mapping_type": "long",
          "runtime": {
            "type": "date"
          }
        }
      }
    ],
    "runtime": {
      "regionGeoPoint": {
        "type": "geo_point"
      }
    },
    "properties": {
      "articleStatus": {
        "type": "long"
      },
      "brand": {
        "type": "long"
      },
      "category": {
        "type": "long"
      },
      "contents": {
        "type": "text",
        "analyzer": "nori",
        "fields": {
          "keyword": {
            "type": "keyword",
            "ignore_above": 256
          }
        }
      },
      "docId": {
        "properties": {
          "_converter": {
            "type": "object"
          },
          "_firestore": {
            "properties": {
              "_backoffSettings": {
                "properties": {
                  "backoffFactor": {
                    "type": "float"
                  },
                  "initialDelayMs": {
                    "type": "long"
                  },
                  "maxDelayMs": {
                    "type": "long"
                  }
                }
              },
              "_clientPool": {
                "properties": {
                  "activeClients": {
                    "type": "object"
                  },
                  "concurrentOperationLimit": {
                    "type": "long"
                  },
                  "failedClients": {
                    "type": "object"
                  },
                  "maxIdleClients": {
                    "type": "long"
                  },
                  "terminateDeferred": {
                    "properties": {
                      "promise": {
                        "type": "object"
                      }
                    }
                  },
                  "terminated": {
                    "type": "boolean"
                  }
                }
              },
              "_projectId": {
                "type": "text",
                "index": false
              },
              "_serializer": {
                "properties": {
                  "allowUndefined": {
                    "type": "boolean"
                  }
                }
              },
              "_settings": {
                "properties": {
                  "firebaseVersion": {
                    "type": "text",
                    "index": false
                  },
                  "libName": {
                    "type": "text",
                    "index": false
                  },
                  "libVersion": {
                    "type": "text",
                    "index": false
                  },
                  "projectId": {
                    "type": "text",
                    "index": false
                  }
                }
              },
              "_settingsFrozen": {
                "type": "boolean"
              },
              "bulkWritersCount": {
                "type": "long"
              },
              "registeredListenersCount": {
                "type": "long"
              }
            }
          },
          "_path": {
            "properties": {
              "segments": {
                "type": "text",
                "index": false
              }
            }
          }
        }
      },
      "images": {
        "type": "text",
        "index": false
      },
      "imagesCache": {
        "type": "text",
        "index": false
      },
      "imagesId": {
        "type": "text",
        "index": false
      },
      "likeCounts": {
        "type": "long"
      },
      "price": {
        "type": "long"
      },
      "region1Depth": {
        "type": "long"
      },
      "region2Depth": {
        "type": "long"
      },
      "regionCode": {
        "type": "long"
      },
      "regionName": {
        "type": "text",
        "analyzer": "nori",
        "fields": {
          "keyword": {
            "type": "keyword",
            "ignore_above": 256
          }
        }
      },
      "saleModifyDate": {
        "type": "long"
      },
      "saleRegistDate": {
        "type": "long"
      },
      "saleStatus": {
        "type": "long"
      },
      "sellerId": {
        "properties": {
          "_converter": {
            "type": "object"
          },
          "_firestore": {
            "properties": {
              "_backoffSettings": {
                "properties": {
                  "backoffFactor": {
                    "type": "float"
                  },
                  "initialDelayMs": {
                    "type": "long"
                  },
                  "maxDelayMs": {
                    "type": "long"
                  }
                }
              },
              "_clientPool": {
                "properties": {
                  "activeClients": {
                    "type": "object"
                  },
                  "concurrentOperationLimit": {
                    "type": "long"
                  },
                  "failedClients": {
                    "type": "object"
                  },
                  "maxIdleClients": {
                    "type": "long"
                  },
                  "terminateDeferred": {
                    "properties": {
                      "promise": {
                        "type": "object"
                      }
                    }
                  },
                  "terminated": {
                    "type": "boolean"
                  }
                }
              },
              "_projectId": {
                "type": "text",
                "index": false
              },
              "_serializer": {
                "properties": {
                  "allowUndefined": {
                    "type": "boolean"
                  }
                }
              },
              "_settings": {
                "properties": {
                  "firebaseVersion": {
                    "type": "text",
                    "index": false
                  },
                  "libName": {
                    "type": "text",
                    "index": false
                  },
                  "libVersion": {
                    "type": "text",
                    "index": false
                  },
                  "projectId": {
                    "type": "text",
                    "index": false
                  }
                }
              },
              "_settingsFrozen": {
                "type": "boolean"
              },
              "bulkWritersCount": {
                "type": "long"
              },
              "registeredListenersCount": {
                "type": "long"
              }
            }
          },
          "_path": {
            "properties": {
              "segments": {
                "type": "text",
                "index": false
              }
            }
          }
        }
      },
      "status": {
        "type": "long"
      },
      "tags": {
        "type": "text",
        "analyzer": "nori",
        "fields": {
          "keyword": {
            "type": "keyword",
            "ignore_above": 256
          }
        }
      },
      "thumbnail": {
        "type": "text",
        "index": false
      },
      "title": {
        "type": "text",
        "analyzer": "nori",
        "fields": {
          "keyword": {
            "type": "keyword",
            "ignore_above": 256
          }
        }
      }
    }
  }
}
 */