import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:transparent_image/transparent_image.dart';
import 'package:zzin/common/firestore/document_skima/product.dart';
import 'package:zzin/common/utils/zenum.dart';
import 'package:zzin/common/zcache_manager.dart';
import 'package:zzin/common/zconfig.dart';

class ProductImage extends StatelessWidget {
  final Product product;
  final String tagHeroPrefix;
  const ProductImage({required this.product, required this.tagHeroPrefix, Key? key})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Opacity(
      opacity: product.saleStatus == ProductSaleStatus.SOLDOUT.index ? ZConfig.opacitySoldout : 1,
      child: Stack(
        children: [
          Hero(
            tag: tagHeroPrefix + product.docId.toString(),
            child: CachedNetworkImage(
              fadeInDuration: const Duration(milliseconds: 0),
              fadeOutDuration: const Duration(milliseconds: 0),
              placeholderFadeInDuration: const Duration(milliseconds: 300),
              placeholder: (_, __) => Image.memory(kTransparentImage),
              imageUrl: product.thumbnail,
              fit: BoxFit.contain,
              errorWidget: (context, url, error) => const Icon(Icons.error),
              cacheManager: ZCacheManager.instance,
            ),
          ),
          product.saleStatus == ProductSaleStatus.SOLDOUT.index
              ? Align(
                  alignment: Alignment.topRight,
                  child: Padding(
                    padding: const EdgeInsets.all(3.0),
                    child: ClipRRect(
                      borderRadius: BorderRadius.circular(5),
                      child: Container(
                        color: ZConfig.zzin_black,
                        constraints: const BoxConstraints.expand(width: 50, height: 20),
                        alignment: Alignment.center,
                        child: const Text(
                          '거래완료',
                          overflow: TextOverflow.ellipsis,
                          style: TextStyle(
                            fontSize: 10,
                            color: ZConfig.zzin_white,
                            backgroundColor: ZConfig.zzin_black,
                          ),
                        ),
                      ),
                    ),
                  ),
                )
              : const SizedBox.shrink(),
        ],
      ),
    );
  }

  Widget fovorateIcon(Product product) {
    if (ZConfig.userAccount.isVerified && ZConfig.userAccount.likeList.contains(product.docId)) {
      return const Icon(Icons.favorite, color: ZConfig.zzin_pointColor);
    } else {
      return const Icon(Icons.favorite_outline_outlined, color: ZConfig.grey_text);
    }
  }
}
