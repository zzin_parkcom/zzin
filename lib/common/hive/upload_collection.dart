import 'package:hive/hive.dart';

import 'package:zzin/common/hive/zhive.dart';
import 'package:zzin/common/utils/zenum.dart';
import 'package:zzin/common/zconfig.dart';

part 'upload_collection.g.dart';

@HiveType(typeId: 5)
class UploadCollection extends HiveObject {
  @HiveField(0)
  int brandId;
  @HiveField(1)
  String brandName;

  @HiveField(2)
  int categoryId;
  @HiveField(3)
  String categoryName;

  @HiveField(4)
  String description;

  @HiveField(5)
  int price;

  @HiveField(6)
  int saleAreaId;
  @HiveField(7)
  String saleAreaName;

  @HiveField(8)
  int status;

  @HiveField(9)
  String title;

  @HiveField(10)
  int mainImageIndex;

  @HiveField(11)
  List<String> images;

  @HiveField(12)
  List<String> imagesCache;

  @HiveField(13)
  List<String> imagesId;

  UploadCollection(
      {required this.brandId,
      required this.brandName,
      required this.categoryId,
      required this.categoryName,
      required this.description,
      required this.price,
      required this.saleAreaId,
      required this.saleAreaName,
      required this.status,
      required this.title,
      required this.mainImageIndex,
      required this.images,
      required this.imagesCache,
      required this.imagesId});

  static UploadCollection get instance => ZHive.uploadBox.getAt(0)!;
  static UploadCollection get modify => ZHive.uploadBox.getAt(1)!;

  Future initialize() async {
    brandId = NotSelected;
    brandName = '브랜드 선택';
    categoryId = NotSelected;
    categoryName = '카테고리 선택';
    description = '';
    price = 0;
    saleAreaId = NotSelected;
    saleAreaName = '거래지역 선택';
    status = ProductStatus.NEW.index;
    title = '';
    mainImageIndex = 0;
    images = [];
    imagesCache = [];
    imagesId = [];
    await save();
  }
}
