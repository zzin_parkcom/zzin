import 'package:cloud_firestore/cloud_firestore.dart';

class OpentalkData {
  String name;
  Timestamp creationTime;
  List participants = <String>[];

  OpentalkData({required this.name, required this.creationTime, required this.participants});

  OpentalkData.fromJson(Map<String, Object?> json)
      : this(
            name: json['name'] as String,
            creationTime: json['creationTime'] as Timestamp,
            participants: json['participants'] as List);

  Map<String, Object?> toJson() {
    return {
      'name': name,
      'creationTime': FieldValue.serverTimestamp(),
      'participants': participants
    };
  }
}
