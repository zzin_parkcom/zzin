import 'dart:io';
import 'package:flutter/material.dart';
import 'package:heic_to_jpg/heic_to_jpg.dart';
import 'package:image/image.dart' as image_lib;
import 'package:wechat_assets_picker/wechat_assets_picker.dart';
import 'package:zzin/common/utils/korean_text_delegate.dart';

class Heic2JpgTestPage extends StatefulWidget {
  const Heic2JpgTestPage({Key? key}) : super(key: key);

  @override
  _Heic2JpgTestPageState createState() => _Heic2JpgTestPageState();
}

class _Heic2JpgTestPageState extends State<Heic2JpgTestPage> {
  AssetEntity? asset;
  File? heicFile;
  File? jpgFile;

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        appBar: AppBar(),
        body: SingleChildScrollView(
          child: Column(
            children: [
              getImageEditor(),
              ElevatedButton(
                child: const Text('convert heic to jpg'),
                onPressed: () async {
                  await onConvertHeicToJpg();
                },
              ),
              jpgFile != null ? Image.file(jpgFile!) : const Text('jpg image'),
              const SizedBox(
                height: 30,
              )
            ],
          ),
        ),
      ),
    );
  }

  Widget getImageEditor() => Column(
        children: [
          IconButton(
            icon: const Icon(Icons.add_a_photo),
            tooltip: '사진을 선택해 주세요.',
            onPressed: () async {
              await onPickImage();
            },
          ),
          asset != null
              ? Row(
                  children: [
                    Expanded(
                      // child: Image(
                      //   image: AssetEntityImageProvider(asset!, isOriginal: true),
                      //   fit: BoxFit.fill,
                      // ),
                      child:
                          heicFile != null ? Image.file(heicFile!) : const Text("No heic Image."),
                    ),
                  ],
                )
              : const Text('No Image'),
        ],
      );

  Future onPickImage() async {
    var assets = await AssetPicker.pickAssets(context,
        maxAssets: 1, textDelegate: KoreanTextDelegate(), gridCount: 3, pageSize: 90);
    if (assets != null) {
      asset = assets.first;
      heicFile = await asset!.file;

      setState(() {});
    }
  }

  Future onConvertHeicToJpg() async {
    if (heicFile != null) {
      var path = await HeicToJpg.convert(heicFile!.path);
      var file = File(path!);
      var imageThumb = image_lib.decodeImage(file.readAsBytesSync());
      var image = image_lib.copyRotate(imageThumb!, 90);
      var buffer = image_lib.encodeNamedImage(image, '.jpg');
      file.writeAsBytes(buffer!);

      try {
        jpgFile = file;
        setState(() {});
      } catch (e) {
        print(e);
      }
    }
  }
}
