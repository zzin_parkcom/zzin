import 'package:animations/animations.dart';
import 'package:flutter/material.dart';
import 'package:zzin/common/zconfig.dart';

class ZOpenContainer<T> extends StatelessWidget {
  final CloseContainerBuilder closedBuilder;
  final OpenContainerBuilder<T> openBuilder;

  const ZOpenContainer({required this.closedBuilder, required this.openBuilder, Key? key})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return OpenContainer(
      closedElevation: 0,
      transitionType: ContainerTransitionType.fadeThrough,
      transitionDuration: ZConfig.transitionDuration,
      openBuilder: openBuilder,
      closedBuilder: closedBuilder,
    );
  }
}
