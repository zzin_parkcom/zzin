import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:zzin/chat_page/chat_ui/chat_ui.dart';
import 'package:zzin/common/firestore/document_skima/chat_message.dart';
import 'package:zzin/common/firestore/document_skima/chatroom.dart';
import 'package:zzin/common/firestore/document_skima/product.dart';
import 'package:zzin/common/firestore/document_skima/user_account.dart';
import 'package:zzin/common/hive/address_collection.dart';
import 'package:zzin/common/zconfig.dart';
import 'package:zzin/common/zutils.dart';
import 'package:zzin/product_page/widget/product_image.dart';

class ChatRoomListTile extends StatefulWidget {
  final ChatRoom chatRoom;
  final int index;
  const ChatRoomListTile(this.chatRoom, this.index, {Key? key}) : super(key: key);

  @override
  _ChatRoomListTileState createState() => _ChatRoomListTileState();
}

class _ChatRoomListTileState extends State<ChatRoomListTile> {
  ZUserAccount? _theOtherUserAccount;
  Product? _product;
  final double heightListTile = 150;
  final double verticalPadding = 10;

  @override
  Widget build(BuildContext context) {
    return FutureBuilder<DocumentSnapshot<ChatMessage>>(
        future: getChatMessage(),
        builder: (BuildContext context, AsyncSnapshot<DocumentSnapshot<ChatMessage>> snapshot) {
          if (snapshot.connectionState == ConnectionState.done) {
            var message = snapshot.data!.data()!;

            return GestureDetector(
              behavior: HitTestBehavior.opaque,
              onTap: onChat,
              child: SizedBox(
                height: heightListTile,
                child: Padding(
                  padding: const EdgeInsets.fromLTRB(10, 3, 10, 3),
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Container(
                        width: heightListTile - 2 * verticalPadding,
                        height: heightListTile - 2 * verticalPadding,
                        color: ZConfig.zzin_white,
                        child: ClipRRect(
                          borderRadius: BorderRadius.circular(10),
                          child: getProductImage(),
                        ),
                      ),
                      Expanded(
                        flex: 2,
                        child: Padding(
                          padding: const EdgeInsets.fromLTRB(10, 5, 10, 5),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.spaceAround,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              getNickname(),
                              getLocationAndTime(message),
                              Flexible(
                                child: Text(
                                  isLocationInfo(message.text)
                                      ? '(현재위치)'
                                      : isImageUrl(message.text)
                                          ? '(이미지)'
                                          : message.text,
                                  overflow: TextOverflow.ellipsis,
                                  maxLines: 2,
                                  style: const TextStyle(
                                    fontSize: 18,
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            );
          }

          return const SizedBox.shrink();
        });
  }

  bool isLocationInfo(String message) {
    return message.startsWith('locationInfo|');
  }

  bool isImageUrl(String message) {
    try {
      var result = Uri.parse(message);
      return result.isAbsolute && message.contains('.jpg?alt=media&token');
    } on FormatException {
      return false;
    }
  }

  void onChat() {
    if (_theOtherUserAccount == null) {
      Get.rawSnackbar(message: '채팅을 할수 없는 유저입니다.');
      return;
    }

    Get.to(
        () => ChatUI(
            sender: ZConfig.userAccount, // 메시지를 처음 보낸 사람.
            receiver: _theOtherUserAccount!,
            product: _product!),
        transition: Transition.rightToLeft,
        duration: ZConfig.heroAnimationDuration);
  }

  Widget getLocationAndTime(ChatMessage message) {
    return Flexible(
      child: Text(
        AddressCollection.getRegionName(message.regionCode) +
            '  ' +
            formatDateTimeBefore(message.creationTime.toDate()),
        overflow: TextOverflow.ellipsis,
        style: const TextStyle(fontSize: 14, color: ZConfig.grey_text),
      ),
    );
  }

  Widget getNickname() {
    return FutureBuilder<DocumentSnapshot<ZUserAccount>>(
      future: getTheOtherInfo(),
      builder: (context, AsyncSnapshot<DocumentSnapshot<ZUserAccount>> snapshot) {
        if (snapshot.connectionState == ConnectionState.done) {
          if (snapshot.data!.data() == null) {
            _theOtherUserAccount = null;
            return Row(
              children: const [
                Icon(
                  Icons.error_outline,
                  color: Colors.red,
                ),
                Text(
                  ' 탈퇴한 유저입니다.',
                  style: TextStyle(fontSize: 18),
                ),
              ],
            );
          }
          _theOtherUserAccount = snapshot.data!.data()!;
          return Text(
            _theOtherUserAccount!.nickname!,
            style: const TextStyle(fontSize: 18),
          );
        } else {
          return const SizedBox.shrink();
        }
      },
    );
  }

  Widget getProductImage() {
    return FutureBuilder<DocumentSnapshot<Product>>(
      future: getProduct(),
      builder: (context, AsyncSnapshot<DocumentSnapshot<Product>> snapshot) {
        if (snapshot.connectionState == ConnectionState.done) {
          _product = snapshot.data!.data()!;
          return Stack(
            children: [
              Opacity(
                opacity: _product!.articleStatus == 0 ? 1 : 0.5,
                child: ProductImage(
                    product: _product!, tagHeroPrefix: 'Chatroom_listtile${widget.index}'),
              ),
              _product!.articleStatus != 0
                  ? const Center(
                      child: Text(
                      '삭제된 상품입니다',
                      overflow: TextOverflow.ellipsis,
                    ))
                  : const SizedBox.shrink(),
            ],
          );
        } else {
          return const SizedBox.shrink();
        }
      },
    );
  }

  Future<DocumentSnapshot<ZUserAccount>> getTheOtherInfo() async {
    if (widget.chatRoom.sellerRef == ZConfig.userAccount.docId) {
      var snapshot = widget.chatRoom.buyerRef
          .withConverter<ZUserAccount>(
              fromFirestore: (snapshot, _) => ZUserAccount.fromJson(snapshot.data()!),
              toFirestore: (chatMessage, _) => chatMessage.toJson())
          .get();

      return snapshot;
    } else {
      var snapshot = widget.chatRoom.sellerRef
          .withConverter<ZUserAccount>(
              fromFirestore: (snapshot, _) => ZUserAccount.fromJson(snapshot.data()!),
              toFirestore: (chatMessage, _) => chatMessage.toJson())
          .get();

      return snapshot;
    }
  }

  Future<DocumentSnapshot<ChatMessage>> getChatMessage() async {
    var snapshot = widget.chatRoom.lastMessageRef!
        .withConverter<ChatMessage>(
            fromFirestore: (snapshot, _) => ChatMessage.fromJson(snapshot.data()!),
            toFirestore: (chatMessage, _) => chatMessage.toJson())
        .get();

    return snapshot;
  }

  Future<DocumentSnapshot<Product>> getProduct() async {
    var snapshot = await widget.chatRoom.productRef
        .withConverter<Product>(
            fromFirestore: (snapshots, _) => Product.fromJson(snapshots.data()!),
            toFirestore: (product, _) => product.toJson())
        .get();

    return snapshot;
  }
}
