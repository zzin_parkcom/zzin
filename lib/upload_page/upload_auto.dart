import 'dart:io';
import 'dart:math';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:wechat_assets_picker/wechat_assets_picker.dart';
import 'package:zzin/common/firestore/document_skima/product.dart';
import 'package:zzin/common/hive/address_collection.dart';
import 'package:zzin/common/hive/brand_collection.dart';
import 'package:zzin/common/hive/category_collection.dart';
import 'package:faker/faker.dart';
import 'package:zzin/common/utils/zenum.dart';
import 'package:zzin/common/zconfig.dart';
import 'package:zzin/upload_page/faker_data_provider_kor.dart';

class UploadProductAuto {
  int count;
  UploadProductAuto({required this.count});

  Future uploadProductAuto() async {
    var assetPathList = await PhotoManager.getAssetPathList();

    var data = assetPathList[0]; // 1st album in the list, typically the "Recent" or "All" album
    var imageList = await data.assetList;

    var files = <File>[];
    var ids = <String>[];

    for (var i = 0; i < imageList.length; i++) {
      var file = await imageList[i].file;
      if (file != null) {
        files.add(file);
        ids.add(imageList[i].id);
      }
    }

    if (files.isEmpty) return;

    var random = Random();
    var fakerKor = Faker(provider: FakerDataProviderKor());

    // await EasyLoading.show(
    //     status: '자동 상품 등록 중...', maskType: EasyLoadingMaskType.black, dismissOnTap: false);
    var start = DateTime.now();
    for (var i = 0; i < count; i++) {
      await EasyLoading.show(
          status: '${i + 1}/$count   자동 상품 등록 중...${DateTime.now().difference(start).toString()}',
          maskType: EasyLoadingMaskType.black,
          dismissOnTap: false);
      var title = fakerKor.lorem.word();
      var contents = fakerKor.lorem.sentence();
      var categoryId = random.nextInt(CategoryCollection.to.length);
      var brandId = random.nextInt(BrandCollection.to.length);
      var status = random.nextInt(2);

      var randomTimestamp = Timestamp.fromDate(DateTime(
        2021, // year
        random.nextInt(7), // month
        random.nextInt(30), // day
        random.nextInt(24), // hour
        random.nextInt(60), // minute
        random.nextInt(60), // second
        random.nextInt(1000), // millisecond
        random.nextInt(1000), // microsecond
      ));

      var address = AddressCollection.to.values.elementAt(random.nextInt(6561));
      var region1Depth = address.regionCode.toString().substring(0, 2);
      var region2Depth = address.regionCode.toString().substring(0, 5);
      var tags = makeTag(title, categoryId, brandId, address, status);

      var randomFileIndex = random.nextInt(files.length);

      var product = Product(
          articleStatus: ProductArticleStatus.REMOVED.index,
          title: title,
          category: categoryId,
          brand: brandId,
          thumbnail: ZConfig.defaultImageStorageURL,
          status: status,
          price: random.nextInt(9999) * 1000,
          likeCounts: random.nextInt(999),
          saleRegistDate: randomTimestamp,
          saleModifyDate: randomTimestamp,
          regionCode: address.regionCode,
          region1Depth: int.parse(region1Depth),
          region2Depth: int.parse(region2Depth),
          regionName: address.regionName,
          regionGeoPoint: GeoPoint(address.latitude, address.longitude),
          saleStatus: random.nextInt(2),
          images: [ZConfig.defaultImageStorageURL],
          imagesCache: [''],
          imagesId: [ids[randomFileIndex]],
          contents: contents,
          tags: tags);
      await product.addProduct([files[randomFileIndex]]);

      print('!!!!!!!!!!!!!!!!!      Product Auto Upload : $i/$count    !!!!!!!!!!!!!!!');
    }

    await EasyLoading.dismiss();
    return count;
  }

  List<String> makeTag(
      String title, int categoryId, int brandId, AddressCollection address, int status) {
    var tags = <String>[];
    // 타이블 태그
    // tags.addAll(title.split(' '));
    // 카테고리 태그
    var category = CategoryCollection.getCategoryById(categoryId);
    tags.addAll(category.makeTagList());
    // 브랜드 태그
    var brand = BrandCollection.getBrandById(brandId);
    tags.addAll(brand.makeTagList());
    // 주소 태그
    // tags.addAll(address.makeTagList());
    // 물건 상태
    if (status == ProductStatus.NEW.index) {
      var newProduct = [
        // '새상품',
        // '새제품',
        '새상품',
        // '새제품',
        // '신제품',
        // '새물건',
        // '새것',
        // '새거',
        // '박스채',
        // '뜯지않은',
        // '미개봉',
        // '미사용',
        // '미사용제품',
        // '미사용상품',
        // '미사용물건',
        // '신상'
      ];
      tags.addAll(newProduct);
    } else {
      var oldProduct = [
        '중고',
        // '빈티지',
        // '유니크',
        // '프리오운드',
        // '세컨핸드',
        // '사용감',
        // '생활기스'
      ];
      tags.addAll(oldProduct);
    }

    var tagsRemoveDuplicate = tags.toSet().toList(); // 중복 제거
    return tagsRemoveDuplicate;
  }
}
