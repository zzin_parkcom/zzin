import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:firebase_analytics/observer.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/services.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:get/get.dart';
import 'package:hive/hive.dart';
import 'package:path_provider/path_provider.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:zzin/common/firestore/document_skima/block_user_controller.dart';
import 'package:zzin/common/firestore/document_skima/user_account.dart';
import 'package:zzin/common/hive/zhive.dart';
import 'package:zzin/common/zconfig.dart';
import 'package:zzin/common/zutils.dart';
import 'package:zzin/entrance/emulator_settings.dart';
import 'package:zzin/entrance/no_internet_page.dart';
import 'package:zzin/entrance/splash.dart';
import 'package:zzin/notification/notification_count.dart';
import 'package:zzin/notification/notification_service.dart';
import 'common/zconfig.dart';
import 'entrance/skeleton.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp]);

  print('파이어베이스 초기화 ---------------------------------------------------------------------');
  await Firebase.initializeApp();

  print('Hive 초기화 ----------------------------------------------------------------------------');
  final documentDirectory = await getApplicationDocumentsDirectory();
  Hive.init(documentDirectory.path);

  print('ZConfig초기화 ---------------------------------------------------------------------------');
  await ZConfig.initialize();

  print('파이어베이스 에뮬레이터 셋팅-------------------------------------------------------------');
  await EmulatorSettings.useEmulator(ZConfig.isRealServer);

  print('네트워크 체크 --------------------------------------------------------------------------');
  var bSuccess = false;
  do {
    bSuccess = await isNetwork();
    if (bSuccess) break;
    await Future.delayed(const Duration(seconds: 1));
    runApp(const MaterialApp(home: NoInternetPage()));
  } while (!bSuccess);

  print('파이어 베이스 인증-------------------------------------------------------------------------');
  try {
    if (FirebaseAuth.instance.currentUser == null) {
      ZConfig.authUser = await _anonymouslySignin().then((value) => value);
      print(ZConfig.authUser);
    } else {
      ZConfig.authUser = FirebaseAuth.instance.currentUser!;
      print('Firebase Auth(기존유저): ${ZConfig.authUser}');
    }

    // var idToken = await FirebaseAuth.instance.currentUser!.getIdToken(true);
    // print('idToken: $idToken');
  } on FirebaseAuthException catch (e) {
    print('FirebaseAuthException:${e.message}');
    print(e.stackTrace);
  }

  print('유저 정보 가져오기 : ZConfig.userAccount 정보 셋팅----------------------------------------');
  ZConfig.userAccount = await getUserInfo();
  if (ZConfig.userAccount.isVerified) {
    await BlockUserController.getBlockList();
    print('차단 유저 : ${ZConfig.blockUsers}');
  }

  setupAuthChange();

  print('푸시 초기화 ----------------------------------------------------------------------------');
  Get.put(NotificationCounter());
  await NotificationService().init();

  configLoading();

  runApp(const MyApp());
  // runApp(DevicePreview(enabled: !kReleaseMode, builder: (context) => MyApp()));
}

/////////////

Future<bool> isNetwork() async {
  var isNetwork = false;

  var connectivityResult = await (Connectivity().checkConnectivity());
  if (connectivityResult == ConnectivityResult.mobile) {
    print('online: mobile network ***********************************************************');
    isNetwork = true;
  } else if (connectivityResult == ConnectivityResult.wifi) {
    print('online: wifi network ***********************************************************');
    isNetwork = true;
  } else {
    print('offline: no network !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!');
    isNetwork = false;
  }
  return isNetwork;
}

Future checkPermission() async {
  // final serviceStatus = await Permission.locationWhenInUse.serviceStatus;
  // bool isGpsOn = serviceStatus == ServiceStatus.enabled;

  var statuses = await [
    Permission.location,
    Permission.storage,
    Permission.notification,
    Permission.accessMediaLocation,
    Permission.locationWhenInUse
  ].request();
  print('locationWhenInUse:${statuses[Permission.locationWhenInUse]}');
  print('location:${statuses[Permission.location]}');
  print('storage:${statuses[Permission.storage]}');
  print('notification:${statuses[Permission.notification]}');
  print('accessMediaLocation:${statuses[Permission.accessMediaLocation]}');
}

void configLoading() {
  EasyLoading.instance
    ..displayDuration = const Duration(milliseconds: 10000)
    ..indicatorType = EasyLoadingIndicatorType.fadingCircle
    ..loadingStyle = EasyLoadingStyle.light
    ..indicatorSize = 45.0
    ..radius = 10.0
    ..progressColor = Colors.yellow
    ..backgroundColor = Colors.green
    ..indicatorColor = Colors.yellow
    ..textColor = Colors.yellow
    ..maskColor = Colors.blue.withOpacity(0.5)
    ..userInteractions = false
    ..dismissOnTap = false;
}

Future initialize(BuildContext context) async {
  print('GPS 초기화 ------------------------------------------------------------------------------');
  await initializeGeoLocator();

  // await versionCheck(context);
  await ZConfig.analytics.setUserId(ZConfig.authUser!.uid);
  await ZHive.to.put('MainTabController_ZTab', 0);
}

Future initializeGeoLocator() async {
// 게스트 유저 위치정보 저장.
  GeoPoint position;
  try {
    if (ZConfig.userAccount.isVerified == false) {
      print('게스트 유저 위치정보 저장 -----------------------------------------------------------');
      var resultDistanceList = await getGPSAddressList(
          initDistance: 0, maxDistance: 20000, deltaDistance: 100, minResultCount: 1);
      if (resultDistanceList == null || resultDistanceList.isEmpty) {
        print('GPS 정보를 얻을수 없어서 청담동으로 설정합니다.');
        ZConfig.userAccount.regionGeoPoint = const GeoPoint(37.5250412, 127.0492971);
        ZConfig.userAccount.regionCode = 1168056500;
        ZConfig.userAccount.regionName = '서울 강남구 청담동';
      } else {
        position =
            GeoPoint(resultDistanceList.first['latitude'], resultDistanceList.first['longitude']);

        ZConfig.userAccount.regionGeoPoint = position;
        ZConfig.userAccount.regionCode = resultDistanceList.first['regionCode'];
        ZConfig.userAccount.regionName = resultDistanceList.first['regionName'];
        print('현재 위치정보 저장...............................');
        await ZHive.generalBox.put(ZConfig.currentGeoKey, [position.latitude, position.longitude]);
        var currentGeo = ZHive.generalBox.get(ZConfig.currentGeoKey);
        print('${currentGeo[0]}, ${currentGeo[1]}');
      }
    }
  } catch (e) {
    print(e);
    return;
  }
}

Future<ZUserAccount> getUserInfo() async {
  if (ZConfig.authUser!.isAnonymous == false) {
    return await ZUserAccount.getInfo(uid: ZConfig.authUser!.uid);
  }
  return ZConfig.userAccount;
}

Future<User> _anonymouslySignin() async {
  print('anonymouslySignin..........................');
  dynamic credential;
  do {
    try {
      credential = await FirebaseAuth.instance.signInAnonymously();
      if (credential == null || credential.user == null) {
        print('retry............................................................................');
        await Future.delayed(const Duration(milliseconds: 200));
      }
    } on FirebaseAuthException catch (e) {
      print('Failed with error code: ${e.code}');
      print(e.message);
    }
  } while (credential.user == null);
  return credential.user;
}

void setupAuthChange() {
  FirebaseAuth.instance.authStateChanges().listen((user) async {
    print('authStateChanges...');
//Right after the listener has been registered.
//When a user is signed in.
//When the current user is signed out.
  });

  FirebaseAuth.instance.idTokenChanges().listen((user) {
    print('in idTokenChanges....');
// Right after the listener has been registered.
// When a user is signed in.
// When the current user is signed out.
// When there is a change in the current user's token.
  });

  FirebaseAuth.instance.userChanges().listen((User? user) {
    if (user == null) {
      print('User is currently signed out!');
    } else {
      print('User is signed in!');
    }
    // Right after the listener has been registered.
    // When a user is signed in.
    // When the current user is signed out.
    // When there is a change in the current user's token.
    // When the following methods provided by FirebaseAuth.instance.currentUser are called:
    // reload()
    // unlink()
    // updateEmail()
    // updatePassword()
    // updatePhoneNumber()
    // updateProfile()
  });
}

////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  static FirebaseAnalytics analytics = FirebaseAnalytics();
  static FirebaseAnalyticsObserver observer = FirebaseAnalyticsObserver(analytics: analytics);

  @override
  Widget build(BuildContext context) {
    ZConfig.analytics = analytics;
    ZConfig.analyticsObserver = observer;

    analytics.setAnalyticsCollectionEnabled(!ZConfig.isRealServer);
    analytics.setAnalyticsCollectionEnabled(ZConfig.isRealServer);
    print('MyApp build-----------------------------------------------');
    return FutureBuilder(
      future: initialize(context),
      builder: (context, AsyncSnapshot snapshot) {
        if (snapshot.connectionState == ConnectionState.waiting) {
          return const GetMaterialApp(home: Splash());
        } else {
          return GetMaterialApp(
            title: 'ZZIN Market!',
            // locale: DevicePreview.locale(context),

            theme: _themeData(),
            home: const Skeleton(),
            builder: EasyLoading.init(),
            navigatorObservers: <NavigatorObserver>[observer],
            debugShowCheckedModeBanner: false,
          );
        }
        // Loading is done, return the app:
      },
    );
  }

  ThemeData _themeData() {
    return ThemeData(
      fontFamily: 'SpoqaHanSansNeo',
      // accentColor: ZConfig.zzin_wh,
      elevatedButtonTheme: ElevatedButtonThemeData(
        style: ButtonStyle(
          backgroundColor: MaterialStateProperty.all<Color>(ZConfig.zzin_white),
          foregroundColor: MaterialStateProperty.all<Color>(ZConfig.zzin_black),
        ),
      ),
      textButtonTheme: TextButtonThemeData(
        style: ButtonStyle(
          foregroundColor: MaterialStateProperty.all<Color>(ZConfig.zzin_black),
          backgroundColor: MaterialStateProperty.all<Color>(ZConfig.zzin_white),
        ),
      ),
      textTheme: const TextTheme(
        subtitle1: TextStyle(fontSize: 15),
      ),
      primaryIconTheme: const IconThemeData(
        color: ZConfig.zzin_black, //change your color here
      ),
      appBarTheme: const AppBarTheme(
          elevation: 0.0,
          color: ZConfig.zzin_white,
          foregroundColor: ZConfig.zzin_black,
          shadowColor: ZConfig.zzin_pointColor),
      bottomNavigationBarTheme: const BottomNavigationBarThemeData(
        backgroundColor: ZConfig.zzin_white,
        selectedItemColor: ZConfig.zzin_black,
        unselectedItemColor: ZConfig.grey_text,
        selectedLabelStyle: TextStyle(
          height: 1.7,
          fontSize: 10,
          color: ZConfig.zzin_black,
        ),
        unselectedLabelStyle: TextStyle(fontSize: 10, height: 1.7),
        elevation: 10,
        selectedIconTheme: IconThemeData(color: ZConfig.zzin_black),
        unselectedIconTheme: IconThemeData(color: ZConfig.grey_text),
        type: BottomNavigationBarType.shifting,
      ),
      bottomSheetTheme: const BottomSheetThemeData(backgroundColor: ZConfig.zzin_background),
      dividerTheme: const DividerThemeData(color: ZConfig.grey_border, thickness: 2, space: 2),
      primaryColor: ZConfig.zzin_background,
      scaffoldBackgroundColor: ZConfig.zzin_background,
      primarySwatch: createMaterialColor(ZConfig.zzin_pointColor),
    );
  }
  ///////////////////////////////////////////////////////////////////////////////////////////////////
  ///////////////////////////////////////////////////////////////////////////////////////////////////
}
