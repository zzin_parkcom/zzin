import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:zzin/common/widget/zbutton_blue.dart';

class ContinueUploadBottomSheet extends StatelessWidget {
  final double sizeRatio;
  const ContinueUploadBottomSheet(this.sizeRatio, {Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: MediaQuery.of(context).size.height * sizeRatio,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          const Text(
            '작성중이던 상품이 있습니다.',
            style: TextStyle(fontSize: 26, fontWeight: FontWeight.bold),
            textAlign: TextAlign.left,
          ),
          const SizedBox(height: 20),
          const Text(
            '이어서 작성하시겠습니까?',
            style: TextStyle(fontSize: 17),
          ),
          const SizedBox(height: 20),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              ZButtonYellow(
                text: '새로 작성하기',
                isSelected: true,
                fontSize: 17,
                onPressed: () => Get.back(result: 0),
              ),
              ZButtonYellow(
                text: '이어서 작성하기',
                isSelected: true,
                fontSize: 17,
                onPressed: () => Get.back(result: 1),
              )
            ],
          ),
        ],
      ),
    );
  }
}
