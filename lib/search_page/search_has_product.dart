import 'package:flutter/material.dart';
import 'package:zzin/common/firestore/document_skima/product.dart';
import 'package:zzin/product_page/widget/product_list_generator.dart';
import 'package:zzin/search_page/search_option_button.dart';

class SearchHasProduct extends StatelessWidget {
  final Future<bool> Function() onSearchOption;
  final List<Product> products;
  final Future<dynamic> Function() onRefresh;
  final ScrollController scrollController;
  final void Function(Product) onDetailPage;
  final String tagHeroPrefix;

  const SearchHasProduct(
      {required this.products,
      required this.onSearchOption,
      required this.onDetailPage,
      required this.onRefresh,
      required this.tagHeroPrefix,
      required this.scrollController,
      Key? key})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 10),
          child: SearchOptionButton(onSearchOption: onSearchOption),
        ),
        const Divider(indent: 10, endIndent: 10),
        Expanded(
          child: ProductListGenerator(
            products: const [],
            esProducts: products,
            onRefresh: onRefresh,
            scrollController: scrollController,
            onDetailPage: onDetailPage,
            tagHeroPrefix: tagHeroPrefix,
            noProductMessages: const ['상품이 없습니다.'],
          ),
        ),
      ],
    );
  }
}
