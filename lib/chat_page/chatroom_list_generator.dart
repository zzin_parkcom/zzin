import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:zzin/chat_page/chatroom_listtile.dart';
import 'package:zzin/common/firestore/document_skima/chatroom.dart';
import 'package:zzin/common/zconfig.dart';

class ChatRoomListGenerator extends StatelessWidget {
  final List<DocumentSnapshot<ChatRoom>> chatrooms;
  final Future<void> Function() onRefresh;
  final ScrollController scrollController;
  static const List<String> noProductMessages = ['채팅중인 상품이 없습니다.'];
  const ChatRoomListGenerator(
      {Key? key, required this.onRefresh, required this.chatrooms, required this.scrollController})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return chatrooms.isEmpty
        ? noChatRoom()
        : RefreshIndicator(
            onRefresh: onRefresh,
            child: Obx(() => ListView.separated(
                padding: const EdgeInsets.all(0),
                separatorBuilder: (context, index) => const Divider(),
                physics: const BouncingScrollPhysics(parent: AlwaysScrollableScrollPhysics()),
                controller: scrollController,
                itemCount: chatrooms.length,
                itemBuilder: (context, index) {
                  var chatRoom = chatrooms[index].data()!;
                  return ChatRoomListTile(chatRoom, index);
                })),
          );
  }

  Widget noChatRoom() {
    return RefreshIndicator(
      onRefresh: onRefresh,
      child: ListView.builder(
        itemExtent: Get.height / 3,
        itemCount: 1,
        itemBuilder: (context, index) {
          return Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: List<Widget>.generate(noProductMessages.length, (index) {
                return Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Text(
                    noProductMessages[index],
                    style: const TextStyle(fontSize: 20, color: ZConfig.grey_text),
                  ),
                );
              }));
        },
      ),
    );
  }
}
