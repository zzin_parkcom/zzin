import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:cloud_functions/cloud_functions.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:zzin/common/zconfig.dart';

class EmulatorSettings {
  static const String _host = '192.168.219.100';

  static const int _authPort = 9099;
  static const int _firestorePort = 8080;
  static const int _functionsPort = 5001;
  static const int _storagePort = 9199;
  static const int _databasePort = 9000;
  static const String _region = 'asia-northeast3';

  static Future useEmulator(bool isRealServer) async {
    if (isRealServer) {
      ZConfig.database = FirebaseDatabase(
          app: Firebase.app(),
          databaseURL: 'https://zzin-b9524-default-rtdb.asia-southeast1.firebasedatabase.app');
      return;
    } else {
      print(
          '*********************  firebase emulator started ************************************');
      await FirebaseAuth.instance.useAuthEmulator(_host, _authPort);
      FirebaseFirestore.instance.useFirestoreEmulator(_host, _firestorePort);

      // final databaseReference = FirebaseDatabase.instance.reference();

      ZConfig.database = FirebaseDatabase(
        // app: Firebase.app(),
        databaseURL: 'http://$_host:$_databasePort',
      );

      print(ZConfig.database.toString());
      // await FirebaseFirestore.instance.clearPersistence();
      FirebaseFunctions.instanceFor(region: _region).useFunctionsEmulator(_host, _functionsPort);
      await FirebaseStorage.instance.useStorageEmulator(_host, _storagePort);
    }
  }
}
