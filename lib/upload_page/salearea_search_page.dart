// ignore_for_file: prefer_const_constructors

import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:geolocator/geolocator.dart';
import 'package:get/get.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:zzin/common/hive/address_collection.dart';
import 'package:zzin/common/widget/searchbar.dart';
import 'package:zzin/common/widget/zbutton_blue.dart';
import 'package:zzin/common/zconfig.dart';
import 'package:zzin/common/zutils.dart';

class SaleareaSearchPage extends StatefulWidget {
  const SaleareaSearchPage({Key? key}) : super(key: key);

  @override
  _SaleareaSearchPageState createState() => _SaleareaSearchPageState();
}

class _SaleareaSearchPageState extends State<SaleareaSearchPage> {
  List<String> searchList = [];
  final TextEditingController _controller = TextEditingController();
  final FocusNode _node = FocusNode();

  bool locationServiceEnabled = false;

  @override
  void initState() {
    super.initState();
    SchedulerBinding.instance!.addPostFrameCallback((_) => frameCallback(context));
  }

  @override
  void dispose() {
    _controller.dispose();
    _node.dispose();
    super.dispose();
  }

  void frameCallback(BuildContext context) async {
    locationServiceEnabled = await isLocationServiceEnabled();
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.fromLTRB(25, 25, 25, 0),
      height: Get.height - AppBar().preferredSize.height * 2,
      child: Column(
        children: [
          // zAppBar(),
          topTitleWidget(),
          searchBarWidget(),
          _controller.text.trim().isEmpty && searchList.isEmpty
              ? initView()
              : const SizedBox.shrink(),
          searchList.isNotEmpty ? middleColumn() : const SizedBox.shrink(),
          saleareaList(),
        ],
      ),
    );
  }

  Widget topTitleWidget() {
    return Padding(
      padding: const EdgeInsets.fromLTRB(10, 0, 10, 0),
      child: Align(
          alignment: Alignment.centerLeft,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(
                '거래지역',
                style: TextStyle(fontSize: ZSize.bottomSheetTitleFontSize),
                textAlign: TextAlign.left,
              ),
              TextButton(
                onPressed: () => Get.back(result: NotSelected),
                child: Text(
                  '닫기',
                  style: TextStyle(fontSize: 18, color: ZConfig.zzin_pointColor),
                ),
              ),
            ],
          )),
    );
  }

  Widget searchBarWidget() {
    return Column(
      children: [
        Padding(
          padding: const EdgeInsets.fromLTRB(0, 18, 0, 18),
          child: TextField(
              focusNode: _node,
              style: TextStyle(fontSize: 18),
              controller: _controller,
              decoration: InputDecoration(
                  suffixIcon: _controller.text.isNotEmpty
                      ? GestureDetector(
                          onTap: resetSearchListAndRefresh,
                          child: Icon(
                            Icons.cancel,
                            color: ZConfig.grey_text,
                          ),
                        )
                      : null,
                  prefixIcon: Icon(Icons.search, color: ZConfig.grey_text),
                  contentPadding: const EdgeInsets.symmetric(vertical: 5),
                  enabledBorder: OutlineInputBorder(
                      borderSide: const BorderSide(color: ZConfig.grey_border, width: 2),
                      borderRadius: const BorderRadius.all(Radius.circular(30.0))),
                  hintStyle: TextStyle(
                      fontSize: 18,
                      fontWeight: FontWeight.normal,
                      color: ZConfig.grey_text,
                      decorationColor: ZConfig.grey_text,
                      backgroundColor: ZConfig.grey_border),
                  filled: true,
                  fillColor: ZConfig.grey_border,
                  labelStyle: TextStyle(
                      color: ZConfig.zzin_black,
                      decorationColor: ZConfig.grey_text,
                      backgroundColor: ZConfig.grey_text),
                  border: OutlineInputBorder(
                    borderRadius: const BorderRadius.all(
                      Radius.circular(30.0),
                    ),
                  ),
                  hintText: '거래지역 검색'),
              onChanged: onChanged),
        ),
      ],
    );
  }

  Widget zAppBar() => Row(
        children: [
          SearchBar(
            controller: _controller,
            hintText: '지역명을 입력해 주세요',
            focusnode: _node,
            onResetAndRefresh: resetSearchListAndRefresh,
            onChanged: onChanged,
          ),
        ],
      );

  void onChanged(String search) {
    // if (search.isEmpty) {
    //   resetSearchListAndRefresh();
    //   return;
    // }
    searchList.clear();

    searchList.addAll(AddressCollection.to.values
        .where((value) => value.regionName.toLowerCase().contains(search.toLowerCase()))
        .map((e) => e.regionName));

    setState(() {});
  }

  void resetSearchListAndRefresh() {
    searchList.clear();
    _controller.text = '';
  }

  Widget middleColumn() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Text(
          _controller.text.isEmpty && searchList.isNotEmpty ? '근처 동네' : '지역 검색 결과',
          style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
        ),
        TextButton(
          onPressed: () {
            resetSearchListAndRefresh();
            _node.unfocus();
            onSetLocation();
          },
          child: Text(
            '현 위치로 설정',
            overflow: TextOverflow.ellipsis,
            style: TextStyle(color: ZConfig.zzin_pointColor),
          ),
        ),
      ],
    );
  }

  Widget saleareaList() {
    return searchList.isNotEmpty || _controller.text.isEmpty
        ? Expanded(
            child: Padding(
              padding: const EdgeInsets.fromLTRB(10, 0, 10, 0),
              child: ListView.separated(
                separatorBuilder: (context, index) => Divider(),
                itemCount: searchList.length,
                itemBuilder: (context, index) {
                  return ListTile(
                    contentPadding: EdgeInsets.fromLTRB(0, 3, 0, 3),
                    title: Text(searchList[index]),
                    onTap: () {
                      onTapSaleAreaListItem(index);
                    },
                  );
                },
              ),
            ),
          )
        : noSearchResultWidget();
  }

  void onTapSaleAreaListItem(int index) async {
    var address = AddressCollection.getAddressByRegionName(searchList[index]);
    Get.back(result: address.regionCode);
  }

  Widget noSearchResultWidget() {
    return Align(
      alignment: Alignment.topCenter,
      child: Container(
        padding: EdgeInsets.fromLTRB(30, 50, 30, 0),
        // color: Colors.blue,
        child: Wrap(
          alignment: WrapAlignment.center,
          runSpacing: 15,
          children: [
            Column(
              children: const [
                Text(
                  '검색 결과가 없습니다.',
                  maxLines: 2,
                  style: TextStyle(color: ZConfig.grey_text, fontSize: 16),
                ),
                Text(
                  '검색어를 다시 입력해주세요.',
                  maxLines: 2,
                  style: TextStyle(color: ZConfig.grey_text, fontSize: 16),
                ),
              ],
            ),
            SizedBox(
                width: 220,
                height: 50,
                child: ZButtonYellow(
                  text: '지역 다시 검색',
                  isSelected: true,
                  borderRaius: 30,
                  fontSize: 20,
                  onPressed: () {
                    resetSearchListAndRefresh();
                  },
                )),
          ],
        ),
      ),
    );
  }

  Widget initView() {
    return Align(
      alignment: Alignment.topCenter,
      child: Container(
        padding: EdgeInsets.fromLTRB(30, 50, 30, 0),
        // color: Colors.blue,
        child: Wrap(
          alignment: WrapAlignment.center,
          runSpacing: 15,
          children: [
            Column(
              children: const [
                Text(
                  '거래 범위 설정시',
                  maxLines: 2,
                  style: TextStyle(color: ZConfig.grey_text, fontSize: 16),
                ),
                Text(
                  '검색할 기준 지역을 설정해 주세요.',
                  maxLines: 2,
                  style: TextStyle(color: ZConfig.grey_text, fontSize: 16),
                ),
              ],
            ),
            SizedBox(
              width: 220,
              height: 50,
              child: ZButtonYellow(
                text: '현재 위치로 설정',
                isSelected: true,
                fontSize: 20,
                onPressed: () => onSetLocation(),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Future<bool> isLocationServiceEnabled() async {
    return await Geolocator.isLocationServiceEnabled();
  }

  void onSetLocation() async {
    var bSuccess = await checkLocationService();

    if (bSuccess) {
      await searchbyGPS();
    } else {
      Get.rawSnackbar(title: '퍼미션 필요', message: '해당 기능을 사용하기 위해서는 퍼미션이 필요합니다.');
    }
  }

  Future<bool> checkLocationService() async {
    var permission = await Geolocator.checkPermission();
    if (permission == LocationPermission.denied) {
      permission = await Geolocator.requestPermission();
      if (permission == LocationPermission.denied) {
        return false;
      } else if (permission == LocationPermission.deniedForever) {
        var response = await showDialogYesNo(
            context, '위치정보 이용에 대한 액세스 권한이 필요합니다.', '설정 앱에서 권한을 설정하십시요.', '설정하기', '취소');

        if (response == DialogResponse.YES) {
          await openAppSettings();
        }
        return false;
      }
    }

    return true;
  }

  Future searchbyGPS() async {
    await EasyLoading.show(
        status: '검색중...', maskType: EasyLoadingMaskType.black, dismissOnTap: false);
    var initDistance = 500; // 시작 거리. 미터 단위
    var maxDistance = 20000; // 최대 거리. 미터 단위
    var deltaDistance = 100; // 단계별 증가 거리. 미터 단위
    var minResultCount = 5; // 최종 결과물이 5개 정도 나오게.

    // GPS로 현재 위치 찾기.
    var position = await Geolocator.getCurrentPosition(desiredAccuracy: LocationAccuracy.best);
    print(position);

    var distance = initDistance;

    Iterable<AddressCollection> resultList;
    do {
      var latitudeMin = position.latitude - distance * ZConfig.latitudePerMeter;
      var latitudeMax = position.latitude + distance * ZConfig.latitudePerMeter;
      var longitudeMin = position.longitude - distance * ZConfig.longitudePerMeter;
      var longitudeMax = position.longitude + distance * ZConfig.longitudePerMeter;

      // print('반경:$distance');

      resultList = AddressCollection.to.values.where((e) =>
          e.latitude > latitudeMin &&
          e.latitude < latitudeMax &&
          e.longitude > longitudeMin &&
          e.longitude < longitudeMax);

      for (var element in resultList) {
        print(element);
        var result = Geolocator.distanceBetween(
            position.latitude, position.longitude, element.latitude, element.longitude);
        print(result);
        print('\n');
      }

      // 검색 범위를 100m 씩 늘려준다.
      if (distance < maxDistance) {
        distance += deltaDistance;
      } else if (distance >= maxDistance) {
        break;
      }
    } while (resultList.length < minResultCount);

    var resultDistanceList = resultList
        .map((e) => {
              'regionCode': e.regionCode,
              'regionName': e.regionName,
              'latitude': e.latitude,
              'longitude': e.longitude,
              'distance': Geolocator.distanceBetween(
                  position.latitude, position.longitude, e.latitude, e.longitude)
            })
        .toList();

    // 거리순으로 올림차순 정렬
    resultDistanceList.sort((a, b) {
      return (a['distance'] as double) >= (b['distance'] as double) ? 1 : -1;
    });

    for (var element in resultDistanceList) {
      print(element);
    }

    searchList = resultDistanceList.map<String>((e) {
      String a = e['regionName'] as String;
      return a;
    }).toList();

    await EasyLoading.dismiss();
    setState(() {});
  }
}
