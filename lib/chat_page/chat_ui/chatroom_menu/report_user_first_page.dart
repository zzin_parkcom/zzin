import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:zzin/chat_page/chat_ui/chatroom_menu/report_user_second_page.dart';
import 'package:zzin/common/firestore/document_skima/report_user.dart';
import 'package:zzin/common/widget/zbutton_blue.dart';
import 'package:zzin/common/zconfig.dart';

class ReportUserFirstPage extends StatefulWidget {
  final DocumentReference productDocId;
  final DocumentReference? chatroomDocId;
  final DocumentReference targetUserDocId;
  const ReportUserFirstPage(
      {Key? key,
      required this.productDocId,
      required this.chatroomDocId,
      required this.targetUserDocId})
      : super(key: key);

  @override
  _ReportUserFirstPageState createState() => _ReportUserFirstPageState();
}

class _ReportUserFirstPageState extends State<ReportUserFirstPage> {
  ReportReasonType _character = ReportReasonType.inappropriate;
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        appBar: AppBar(),
        body: ListView(
          children: [
            Padding(
              padding: const EdgeInsets.all(30.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: const [
                  Text(
                    '신고하기',
                    style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
                  ),
                  Text('1/2')
                ],
              ),
            ),
            ListView.builder(
                shrinkWrap: true,
                itemCount: reportReasonTitle.length,
                itemBuilder: (context, index) {
                  return reportReasonListItem(reportReasonTitle[index], reportReasonSubTitle[index],
                      ReportReasonType.values[index]);
                })
          ],
        ),
        bottomNavigationBar: Padding(
          padding: ZSize.bottomNavigationBarButtonOutPadding,
          child: ZButtonYellow(
              isSelected: true,
              text: '다음 단계',
              fontSize: ZSize.BNB_ButtonFontSize,
              padding: ZSize.bottomNavigationBarButtonInPadding,
              onPressed: () async {
                var result = await Get.to(
                    () => ReportUserSecondPage(
                        productDocId: widget.productDocId,
                        chatroomDocId: widget.chatroomDocId,
                        targetUserDocId: widget.targetUserDocId),
                    arguments: _character.index,
                    transition: Transition.rightToLeftWithFade);

                if (result == true) {
                  Get.back();
                  Get.rawSnackbar(message: '신고하기 완료');
                }
              }),
        ),
      ),
    );
  }

  Widget reportReasonListItem(String title, String subTitle, ReportReasonType radioValue) {
    return ListTile(
      title: Text(title, style: const TextStyle(height: 2, fontWeight: FontWeight.bold)),
      subtitle: Text(subTitle, style: const TextStyle(fontSize: 12)),
      leading: Radio<ReportReasonType>(
        value: radioValue,
        groupValue: _character,
        onChanged: (ReportReasonType? value) {
          setState(() {
            _character = value!;
          });
        },
      ),
    );
  }
}
