import 'package:zzin/common/zconfig.dart';

class QueryValue {
  String keyword = Empty;
  int category = NotSelected;
  int brand = NotSelected;
  int status = NotSelected;
  int saleStatus = NotSelected;
  int sort = NotSelected;
  int addressStep = NotSelected;
  int addressId = NotSelected;

  bool isDirty = false;

  bool save({
    String keywordParam = '',
    int categoryParam = 0,
    int brandParam = 0,
    int statusParam = 0,
    int saleStatusParam = 0,
    int sortParam = 0,
    int addressStepParam = 0,
    int addressIdParam = 0,
  }) {
    isDirty = false;

    if (keywordParam != keyword) {
      keyword = keywordParam;
      isDirty = true;
    }
    if (category != categoryParam) {
      category = categoryParam;
      isDirty = true;
    }
    if (brand != brandParam) {
      brand = brandParam;
      isDirty = true;
    }
    if (status != statusParam) {
      status = statusParam;
      isDirty = true;
    }
    if (saleStatus != saleStatusParam) {
      saleStatus = saleStatusParam;
      isDirty = true;
    }
    if (sort != sortParam) {
      sort = sortParam;
      isDirty = true;
    }

    if (addressStep != addressStepParam || addressId != addressIdParam) {
      addressStep = addressStepParam;
      addressId = addressIdParam;
      isDirty = true;
    }

    printValue();
    return isDirty;
  }

  void printValue() {
    print('********** 쿼리 조건 **********');
    if (keyword.isNotEmpty) {
      print('keyword: $keyword');
    }
    if (category != -1) {
      print('category: $category');
    }
    if (brand != -1) {
      print('brand: $brand');
    }
    if (status != -1) {
      print('status: $status');
    }
    if (saleStatus != -1) {
      print('salesStatus: $saleStatus');
    }
    if (sort != -1) {
      print('sort: $sort');
    }
    if (addressStep != -1 && addressId != -1) {
      print('locationScope : $addressStep');
      print('locationList : $addressId');
    }

    print('******************************');
  }
}
