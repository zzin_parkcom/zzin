import 'package:flutter/material.dart';
import 'package:zzin/common/widget/appbar/zappbar.dart';
import 'package:zzin/common/widget/ztab.dart';
import 'package:zzin/common/zconfig.dart';
import 'package:zzin/entrance/skeleton.dart';
import 'package:zzin/opentalk_page/opentalk_list.dart';

class OpentalkView extends StatelessWidget {
  final List<Tab> tabs = [];
  final List<Widget> tabViews = [];
  OpentalkView({Key? key}) : super(key: key) {
    tabs.add(const Tab(child: Text('명품 Q&A', style: TextStyle(fontSize: 18))));
    tabs.add(const Tab(child: Text('신상토크', style: TextStyle(fontSize: 18))));
    tabs.add(const Tab(child: Text('오픈런', style: TextStyle(fontSize: 18))));

    tabViews.add(const Text('명품 Q&A'));
    tabViews.add(const Text('신상토크'));
    tabViews.add(const OpenTalkList());
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      children: [
        zAppBar(),
        Expanded(
          child: ZTab(
            keyString: MainTabController.to.tabIndex.value.toString() + toString(),
            tabs: tabs,
            tabViews: tabViews,
          ),
        )
        // Divider(),
      ],
    );
  }

  Widget zAppBar() => ZAppBar(
        title: const Text('명품톡',
            style: TextStyle(color: ZConfig.zzin_black, fontSize: ZSize.appBarTextSize)),
      );
}
