import 'package:flutter/material.dart';
import 'package:zzin/common/utils/zenum.dart';
import 'package:zzin/common/zconfig.dart';
import 'package:zzin/common/zutils.dart';

class ProductListItemDetail extends StatelessWidget {
  const ProductListItemDetail({
    Key? key,
    required this.goodsTitle,
    required this.goodsPrice,
    required this.address,
    required this.registerDateTime,
    required this.favoriteCount,
    required this.productStatus,
  }) : super(key: key);

  final String goodsTitle;
  final int goodsPrice;
  final String address;
  final DateTime registerDateTime;
  final int favoriteCount;
  final int productStatus;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.fromLTRB(10, 10, 0, 10),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Column(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                goodsTitle,
                maxLines: 2,
                overflow: TextOverflow.ellipsis,
                style: const TextStyle(fontSize: 17.0, height: 1.3),
              ),
              const SizedBox(height: 5),
              Text(
                '$address  ${formatDateTimeBefore(registerDateTime)}',
                overflow: TextOverflow.ellipsis,
                style: TextStyle(fontSize: 13, color: Colors.grey[600]),
              ),
              const SizedBox(height: 5),
              Text(
                '${numberWithComma(goodsPrice)}원',
                style: const TextStyle(
                  fontSize: 15.0,
                  color: ZConfig.zzin_black,
                ),
              ),
            ],
          ),
          Flexible(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                ProductStatus.values[productStatus] == ProductStatus.NEW
                    ? Container(
                        padding: const EdgeInsets.all(2.0),
                        decoration: BoxDecoration(
                          color: ZConfig.zzin_white,
                          borderRadius: BorderRadius.circular(5),
                          border: Border.all(color: ZConfig.zzin_black, width: 0.5),
                        ),
                        child: Text(
                          ProductStatusString.toStr(productStatus),
                          style: const TextStyle(fontSize: 12, color: ZConfig.zzin_black),
                        ),
                      )
                    : const SizedBox.shrink(),
                favoriteCount > 0
                    ? Flexible(
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.end,
                          children: [
                            const Icon(
                              Icons.favorite_outline,
                              size: 14, /*  color: ZConfig.zzin_pointColor */
                            ),
                            Text(
                              '$favoriteCount',
                              style: const TextStyle(fontSize: 14.0, color: ZConfig.zzin_black),
                            ),
                          ],
                        ),
                      )
                    : const SizedBox.shrink(),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
