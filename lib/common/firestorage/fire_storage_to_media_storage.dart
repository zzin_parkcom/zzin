import 'dart:typed_data';

import 'package:flutter/services.dart';
import 'package:uuid/uuid.dart';
import 'package:wechat_assets_picker/wechat_assets_picker.dart';
import 'package:image/image.dart';

class FireStorageToMediaStorage {
  final String filePath;
  late final String filename;
  FireStorageToMediaStorage({required this.filePath}) {
    filename = const Uuid().v1().toString();
  }

  Future<AssetEntity?> getAsset() async {
    var result = await PhotoManager.requestPermissionExtend();
    if (result.isAuth) {
    } else {
      // fail
      PhotoManager.openSetting();
    }

    print(filePath);
    var byteData = await NetworkAssetBundle(Uri.parse(filePath)).load(filePath);
    var byte8List = byteData.buffer.asUint8List();

    print('decodeImage');
    var rawImage = decodeImage(List.from(byte8List));
    if (rawImage != null) {
      print('encodeJpg');
      var jpgImage = encodeJpg(rawImage);
      var byteList = Uint8List.fromList(jpgImage);
      print('photo saveImage');
      print('$filename.jpg');
      final asset = await PhotoManager.editor.saveImage(byteList, title: '$filename.jpg');

      if (asset != null) {
        // await PhotoManager.refreshAssetProperties(asset.id);
        return asset;
      }
    }
    return Future.value(null);
  }
}
