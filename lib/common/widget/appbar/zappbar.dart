// ignore_for_file: overridden_fields

import 'package:flutter/material.dart';

class ZAppBar extends AppBar {
  @override
  final Widget title;

  @override
  final Widget? leading;

  @override
  final List<Widget>? actions;

  ZAppBar({required this.title, this.leading, this.actions, Key? key}) : super(key: key);

  Widget build(BuildContext context) {
    return AppBar(
      titleSpacing: 0.0,
      leading: leading,
      title: title,
      actions: actions ?? [const SizedBox(width: 20)],
    );
  }
}
