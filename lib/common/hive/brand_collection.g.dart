// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'brand_collection.dart';

// **************************************************************************
// TypeAdapterGenerator
// **************************************************************************

class BrandCollectionAdapter extends TypeAdapter<BrandCollection> {
  @override
  final int typeId = 1;

  @override
  BrandCollection read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return BrandCollection(
      id: fields[0] as int,
      nameKr: fields[1] as String,
      nameEn: fields[2] as String,
    );
  }

  @override
  void write(BinaryWriter writer, BrandCollection obj) {
    writer
      ..writeByte(3)
      ..writeByte(0)
      ..write(obj.id)
      ..writeByte(1)
      ..write(obj.nameKr)
      ..writeByte(2)
      ..write(obj.nameEn);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is BrandCollectionAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}
