import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/material.dart';
import 'package:zzin/common/zconfig.dart';
import 'package:zzin/opentalk_page/opentalk_chat_ui/opentalk_message.dart';

class OpentalkChatUIMessageList extends StatefulWidget {
  final String opentalkName;
  final ScrollController scrollController;
  const OpentalkChatUIMessageList(
      {required this.opentalkName, required this.scrollController, Key? key})
      : super(key: key);

  @override
  _OpentalkChatUIMessageListState createState() => _OpentalkChatUIMessageListState();
}

class _OpentalkChatUIMessageListState extends State<OpentalkChatUIMessageList> {
  late DatabaseReference openrunRef;

  @override
  void initState() {
    openrunRef = ZConfig.database.reference().child('openrun');
    super.initState();
  }

  @override
  void dispose() {
    widget.scrollController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: StreamBuilder(
        stream: openrunRef.orderByChild('age').onValue,
        builder: (BuildContext context, snap) {
          if (snap.hasData && (snap.data as Event).snapshot.value != null) {
            Map data = (snap.data as Event).snapshot.value;
            if (data.entries.isNotEmpty) {
              data.forEach((key, value) {
                print('$key : $value');
              });

              return Padding(
                padding: const EdgeInsets.fromLTRB(0, 0, 0, 5),
                child: ListView.separated(
                  controller: widget.scrollController,
                  separatorBuilder: (context, index) => const SizedBox(height: 10),
                  itemCount: data.length,
                  itemBuilder: (context, index) {
                    List item = [];
                    data.forEach((index, data) => item.add({"key": index, ...data}));
                    OpentalkMessage message = OpentalkMessage.fromJson(item[index]);

                    return Column(children: [
                      Row(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          Text(
                            message.text + message.time.toDate().toString(),
                          )
                        ],
                      )
                    ]);
                  },
                ),
              );
            }
            return const Text('no meesage');
          } else if (snap.hasError) {
            return const Text('에러');
          } else {
            return Text('오픈런  \"${widget.opentalkName}\"  입니다.');
          }
        },
      ),
    );
  }
}
