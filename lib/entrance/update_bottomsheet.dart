import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:zzin/common/widget/zbutton_blue.dart';

class UpdateBottomSheet extends StatelessWidget {
  final String appStoreLink;
  final bool isForced;
  final String description = '유저 여러분의 의견을 반영하여 사용성을 개선하였습니다. 지금 바로 업데이트하고 경험해보세요.!';
  final double sizeToRatio;
  const UpdateBottomSheet(
      {required this.appStoreLink, required this.isForced, required this.sizeToRatio, Key? key})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(20.0),
      child: SizedBox(
        height: Get.height * sizeToRatio,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            const Text(
              '새로운 버전이 업데이트 되었습니다!',
              style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20),
            ),
            Padding(
              padding: const EdgeInsets.fromLTRB(40, 20, 40, 20),
              child: Text(description),
            ),
            isForced
                ? ZButtonYellow(
                    text: '업데이트',
                    isSelected: true,
                    fontSize: 14,
                    onPressed: () => onUpdate(),
                  )
                : Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      ZButtonYellow(
                        onPressed: onNext,
                        text: '다음에 하기',
                        fontSize: 14,
                        isSelected: true,
                      ),
                      ZButtonYellow(
                        text: '업데이트',
                        isSelected: true,
                        fontSize: 14,
                        onPressed: () => onUpdate(),
                      ),
                    ],
                  )
          ],
        ),
      ),
    );
  }

  void onUpdate() async {
    print(appStoreLink);
    launch(appStoreLink);
    Get.back(result: 1);
  }

  void onNext() async {
    Get.back(result: -1);
  }
}
