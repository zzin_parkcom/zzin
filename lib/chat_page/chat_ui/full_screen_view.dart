import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:transparent_image/transparent_image.dart';
import 'package:zzin/common/zcache_manager.dart';
import 'package:zzin/common/zconfig.dart';

class FullScreenView extends StatelessWidget {
  final String imageUrl;
  const FullScreenView({Key? key, required this.imageUrl}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(children: [
        Center(
          child: InteractiveViewer(
            minScale: 0.1,
            maxScale: 2,
            clipBehavior: Clip.none,
            child: CachedNetworkImage(
              fadeInDuration: const Duration(milliseconds: 0),
              fadeOutDuration: const Duration(milliseconds: 0),
              placeholderFadeInDuration: const Duration(milliseconds: 0),
              placeholder: (_, __) => Image.memory(kTransparentImage),
              imageUrl: imageUrl,
              fit: BoxFit.fill,
              errorWidget: (context, url, error) => const Icon(Icons.error),
              cacheManager: ZCacheManager.instance,
            ),
          ),
        ),
        Positioned(
          right: 5,
          top: Get.mediaQuery.padding.top,
          child: IconButton(
            icon: const CircleAvatar(
                radius: 15,
                backgroundColor: Colors.white24,
                child: Icon(Icons.close, size: 30, color: ZConfig.zzin_black)),
            onPressed: () => Get.back(),
          ),
        ),
      ]),
    );
  }
}
