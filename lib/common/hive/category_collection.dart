import 'package:hive/hive.dart';
import 'package:zzin/common/hive/zhive.dart';
import 'package:zzin/common/zconfig.dart';

part 'category_collection.g.dart';

@HiveType(typeId: 4)
class CategoryCollection extends HiveObject {
  @HiveField(0)
  int id;
  @HiveField(1)
  String nameKr;
  @HiveField(2)
  String nameEn;

  CategoryCollection({required this.id, required this.nameKr, required this.nameEn});

  static Box<CategoryCollection> get to => ZHive.categoryBox;

  @override
  String toString() {
    return 'id: $id, nameKr: $nameKr, nameEn: $nameEn';
  }

  static String getNameKr(int id) {
    return ZHive.categoryBox.values.where((element) => element.id == id).first.nameKr;
  }

  static String getNameEn(int id) {
    return ZHive.categoryBox.values.where((element) => element.id == id).first.nameEn;
  }

  static String getNameKrEn(int id) {
    var category = ZHive.categoryBox.values.where((element) => element.id == id).first;
    return category.nameKr + '|' + category.nameEn;
  }

  static int getCollectionSize() {
    return ZHive.categoryBox.length;
  }

  static CategoryCollection getCategoryById(int id) {
    return ZHive.categoryBox.values.where((element) => element.id == id).first;
  }

  List<String> makeTagList() {
    var tags = <String>[];
    tags.addAll(nameKr.split(' '));
    tags.addAll(nameEn.split(' '));
    return tags;
  }

  static int findString(List<String> findList) {
    var categoryResult = ZHive.categoryBox.values.firstWhere(
        (category) => category
            .makeTagList()
            .any((tag) => findList.any((find) => tag.toLowerCase() == find.toLowerCase())),
        orElse: () => CategoryCollection(id: NotFound, nameKr: '결과없음', nameEn: '결과없음'));

    return categoryResult.id;
  }
}
