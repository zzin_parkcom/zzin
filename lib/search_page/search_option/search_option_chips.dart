import 'package:flutter/material.dart';
import 'package:zzin/common/hive/address_collection.dart';
import 'package:zzin/common/hive/brand_collection.dart';
import 'package:zzin/common/hive/category_collection.dart';
import 'package:zzin/common/hive/search_collection.dart';
import 'package:zzin/common/utils/zenum.dart';
import 'package:zzin/common/zconfig.dart';

class SearchOptionChips extends StatelessWidget {
  final List<String> searchOptions = [];
  final ScrollController scrollController = ScrollController();

  SearchOptionChips({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    makeSearchOptions();
    return Row(
      children: List.generate(
          searchOptions.length,
          (index) => Padding(
                padding: const EdgeInsets.fromLTRB(2, 0, 2, 0),
                child: Container(
                  padding: const EdgeInsets.all(3.0),
                  decoration: BoxDecoration(
                    color: ZConfig.zzin_white,
                    borderRadius: BorderRadius.circular(5),
                    border: Border.all(color: ZConfig.grey_border, width: 1),
                  ),
                  child: Text(
                    searchOptions[index],
                    style: const TextStyle(fontSize: 12, color: ZConfig.grey_text),
                  ),
                ),
              )),
    );
  }

  void makeSearchOptions() {
    searchOptions.clear();
    var category = SearchCollection.instance.categoryId;
    if (category != NotSelected) {
      searchOptions.add(CategoryCollection.getNameKr(category));
    }

    var brand = SearchCollection.instance.brandId;
    if (brand != NotSelected) {
      searchOptions.add(BrandCollection.getNameKr(brand));
    }

    var saleStatus = SearchCollection.instance.saleStatus;
    if (saleStatus != NotSelected && saleStatus != AllItems) {
      searchOptions.add(saleStatus == ProductSaleStatus.ONSALE.index ? '거래완료 제외' : '거래완료 포함');
    }

    var status = SearchCollection.instance.status;
    if (status != NotSelected && status != AllItems) {
      searchOptions.add(status == ProductStatus.NEW.index ? '신상품' : '중고');
    }

    var distanceString = ['500m', '2km', '10km', '50km+'];
    var regionCode = SearchCollection.instance.addressId;
    if ((regionCode != NotSelected) && (SearchCollection.instance.addressStep != 3)) {
      var regionName = AddressCollection.getRegionName(regionCode).split(' ').last;
      var distance = SearchCollection.instance.addressStep;
      searchOptions.add(regionName + ' ${distanceString[distance]}');
    }

    var sort = SearchCollection.instance.sort;
    if (sort != NotSelected) {
      searchOptions.add(ZConfig.sortNameList[sort]);
    }
  }
}
