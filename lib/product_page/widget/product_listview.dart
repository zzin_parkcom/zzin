import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:get/get.dart';
import 'package:zzin/common/firestore/document_skima/product.dart';
import 'package:zzin/common/firestore/query_product.dart';
import 'package:zzin/common/utils/zenum.dart';
import 'package:zzin/common/zconfig.dart';
import 'package:zzin/product_page/product_detail_page/product_detail_view.dart';
import 'package:zzin/product_page/widget/product_list_generator.dart';

class ProductListView extends StatefulWidget {
  final ProductCategory category;
  final bool isListView;
  const ProductListView(this.category, this.isListView, {Key? key}) : super(key: key);
  @override
  _ProductListViewState createState() => _ProductListViewState();
}

class _ProductListViewState extends State<ProductListView> {
  final ScrollController _scrollController = ScrollController();
  late final QueryProduct queryProduct;
  final String tagHeroPrefix = 'ProductView_';

  @override
  void initState() {
    super.initState();
    queryProduct = Get.put(QueryProduct(documentCount: 15), tag: widget.category.toString());
    _scrollController.addListener(_scrollListener);
    ZConfig.analytics.setCurrentScreen(
      screenName: widget.category.toString(),
    );

    SchedulerBinding.instance!.addPostFrameCallback((_) => frameCallback(context));
  }

  void frameCallback(BuildContext context) async {
    await ZConfig.analytics.logEvent(name: widget.category.toString().replaceAll('.', '_'));
  }

  @override
  void dispose() {
    _scrollController.removeListener(_scrollListener);
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
        future: getProduct(),
        builder: (context, snapshot) {
          if (snapshot.connectionState == ConnectionState.done) {
            return ProductListGenerator(
                products: queryProduct.products,
                onRefresh: onRefresh,
                scrollController: _scrollController,
                onDetailPage: onDetailPage,
                noProductMessages: const ['상품이 없습니다.'],
                isListView: widget.isListView,
                tagHeroPrefix: tagHeroPrefix);
          } else {
            return const SizedBox.shrink();
          }
        });
  }

  Future getProduct() async {
    if (widget.category == ProductCategory.ALL) {
      await queryProduct.queryProductBySort(0, saleStausIndex: ProductSaleStatus.ONSALE.index);
    } else {
      await queryProduct.queryProductByCategory(widget.category.index,
          saleStausIndex: ProductSaleStatus.ONSALE.index);
    }
  }

  void onDetailPage(Product product) async {
    // 이미지 프리로딩...
    for (var image in product.images) {
      await precacheImage(CachedNetworkImageProvider(image), context);
    }

    var tag = DateTime.now().toString();
    Get.put(Product.fromJson(product.toJson()), tag: tag);

    await Get.to(() => ProductDetailView(tag: tag, tagHeroPrefix: tagHeroPrefix),
        curve: Curves.linear,
        transition: Transition.fadeIn,
        duration: ZConfig.heroAnimationDuration);

    await Get.delete(tag: tag);
  }

  Future onRefresh() async {
    print('onRefresh');
    var queryValue = queryProduct.queryValue;
    await queryProduct.queryProductDetail(
        keyword: queryValue.keyword,
        categoryIndex: queryValue.category,
        brandIndex: queryValue.brand,
        statusIndex: queryValue.status,
        saleStausIndex: queryValue.saleStatus,
        sortIndex: queryValue.sort,
        addressStep: queryValue.addressStep,
        addressId: queryValue.addressId,
        byScroll: false,
        isReload: true);
  }

  void _scrollListener() async {
    var maxScroll = _scrollController.position.maxScrollExtent;
    var currentScroll = _scrollController.position.pixels;
    var delta = maxScroll * ZConfig.scrollRatioQueryMore;

    if (maxScroll - currentScroll <= delta && queryProduct.canQuery()) {
      var queryValue = queryProduct.queryValue;
      await queryProduct.queryProductDetail(
          keyword: queryValue.keyword,
          categoryIndex: queryValue.category,
          brandIndex: queryValue.brand,
          statusIndex: queryValue.status,
          saleStausIndex: queryValue.saleStatus,
          sortIndex: queryValue.sort,
          addressStep: queryValue.addressStep,
          addressId: queryValue.addressId,
          byScroll: true);
    }
  }
}
