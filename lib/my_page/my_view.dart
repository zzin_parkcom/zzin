import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:transparent_image/transparent_image.dart';
import 'package:zzin/common/utils/zenum.dart';
import 'package:zzin/common/widget/appbar/zappbar.dart';
import 'package:zzin/common/widget/ztab.dart';
import 'package:zzin/common/zcache_manager.dart';
import 'package:zzin/common/zconfig.dart';
import 'package:zzin/entrance/skeleton.dart';
import 'package:zzin/my_page/configuration_page.dart';
import 'package:zzin/my_page/profile_configuration_page.dart';
import 'package:zzin/product_page/product_detail_page/seller_page/seller_listview.dart';

class MyView extends StatefulWidget {
  const MyView({Key? key}) : super(key: key);

  @override
  _MyViewState createState() => _MyViewState();
}

class _MyViewState extends State<MyView> {
  List<Tab> tabs = [
    const Tab(child: Text('판매중', style: TextStyle(fontSize: 18))),
    const Tab(child: Text('거래완료', style: TextStyle(fontSize: 18))),
  ];

  final ConfigurationPage configurationPage = const ConfigurationPage();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        resizeToAvoidBottomInset: false,
        appBar: zAppBar(),
        body: ZTab(
          keyString: MainTabController.to.tabIndex.value.toString(),
          tabs: tabs,
          tabViews: [
            SellerListView(
                seller: ZConfig.userAccount,
                saleStatus: ProductSaleStatus.ONSALE,
                noProductMessages: const ['판매중인 상품이 없습니다.']),
            SellerListView(
                seller: ZConfig.userAccount,
                saleStatus: ProductSaleStatus.SOLDOUT,
                noProductMessages: const ['거래완료된 상품이 없습니다.'])
          ],
        ),
        endDrawer: SizedBox(width: Get.width / 2, child: Drawer(child: configurationPage)),
        onEndDrawerChanged: (isChange) {
          print('$isChange');
          if (isChange == false) {
            setState(() {});
          }
        });
  }

  AppBar zAppBar() => ZAppBar(
        title: Row(
          children: [
            Text(
              '${ZConfig.userAccount.nickname} 님의 페이지',
              style: const TextStyle(color: ZConfig.zzin_black, fontSize: ZSize.appBarTextSize),
            ),
            kDebugMode
                ? SizedBox(
                    width: 120,
                    child: Text(
                      ZConfig.userAccount.docId!.path.split('/').last,
                      overflow: TextOverflow.clip,
                      style: const TextStyle(fontSize: 10, color: ZConfig.grey_text),
                    ),
                  )
                : const SizedBox.shrink()
          ],
        ),
      );

  Widget profileWidget() => ListTile(
        onTap: onProfile,
        title: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  '${ZConfig.userAccount.nickname}',
                  style: const TextStyle(fontWeight: FontWeight.bold, fontSize: 24),
                ),
                const SizedBox(height: 10),
                Text(
                  '${ZConfig.userAccount.getSocialName()} 계정 연결',
                  style: const TextStyle(
                      fontWeight: FontWeight.bold, fontSize: 16, color: ZConfig.grey_text),
                ),
              ],
            ),
            SizedBox(
              height: 60,
              width: 60,
              child: ClipRRect(
                borderRadius: BorderRadius.circular(30),
                child: CachedNetworkImage(
                  fadeInDuration: const Duration(milliseconds: 0),
                  fadeOutDuration: const Duration(milliseconds: 0),
                  placeholderFadeInDuration: const Duration(milliseconds: 0),
                  placeholder: (_, __) => Image.memory(kTransparentImage),
                  imageUrl: ZConfig.userAccount.getThumbnailPath(),
                  fit: BoxFit.contain,
                  errorWidget: (context, url, error) => const Icon(Icons.error),
                  cacheManager: ZCacheManager.instance,
                ),
              ),
            ),
          ],
        ),
      );

  void onProfile() async {
    await Get.to(() => const ProfileConfigurationPage(),
        transition: Transition.rightToLeftWithFade);
  }

  void onAccountConfiguration() async {
    await Get.to(() => const ConfigurationPage(), transition: Transition.rightToLeftWithFade);
  }
}
