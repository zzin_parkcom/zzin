import 'package:flutter/material.dart';
import 'package:zzin/common/zconfig.dart';

class SearchBar extends StatelessWidget {
  final TextEditingController controller;
  final String hintText;
  final FocusNode? focusnode;
  final void Function()? onResetAndRefresh;
  final void Function(String)? onChanged;
  final void Function(String)? onSubmitted;

  const SearchBar(
      {required this.controller,
      required this.hintText,
      this.focusnode,
      this.onResetAndRefresh,
      this.onChanged,
      this.onSubmitted,
      Key? key})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return TextField(
      textInputAction: TextInputAction.search,
      focusNode: focusnode,
      style: const TextStyle(fontSize: ZSize.searchTextSize),
      controller: controller,
      decoration: InputDecoration(
          prefixIcon: prefixIcon(),
          suffixIcon: suffixIcon(),
          contentPadding: const EdgeInsets.symmetric(vertical: 5),
          enabledBorder: const OutlineInputBorder(
              borderSide: BorderSide(color: ZConfig.grey_border, width: 2),
              borderRadius: BorderRadius.all(Radius.circular(30.0))),
          hintStyle: const TextStyle(
              fontSize: ZSize.searchTextSize,
              fontWeight: FontWeight.normal,
              color: ZConfig.grey_text,
              decorationColor: ZConfig.grey_text,
              backgroundColor: ZConfig.grey_border),
          filled: true,
          fillColor: ZConfig.grey_border,
          labelStyle: const TextStyle(
              color: ZConfig.zzin_black,
              decorationColor: ZConfig.grey_text,
              backgroundColor: ZConfig.grey_text),
          border: const OutlineInputBorder(
            borderRadius: BorderRadius.all(
              Radius.circular(30.0),
            ),
          ),
          hintText: hintText),
      onChanged: onChanged,
      onSubmitted: onSubmitted,
    );
  }

  Widget prefixIcon() => IconButton(
      onPressed: () {
        if (onSubmitted != null) {
          onSubmitted!(controller.text);
        }
      },
      icon: const Icon(Icons.search, color: ZConfig.zzin_black));

  Widget suffixIcon() => controller.text.isNotEmpty
      ? IconButton(
          onPressed: onResetAndRefresh, icon: const Icon(Icons.cancel, color: ZConfig.grey_text))
      : const SizedBox.shrink();
}
