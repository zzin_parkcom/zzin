import 'package:cloud_functions/cloud_functions.dart';
import 'package:flutter_kakao_login/flutter_kakao_login.dart';
import 'package:kakao_flutter_sdk/all.dart';
import 'package:zzin/common/firestore/document_skima/user_account.dart';
import 'package:zzin/login/signin/social_signin.dart';

class ZKakaoSignin implements SocialSignin {
  static final FlutterKakaoLogin kakaoSignIn = FlutterKakaoLogin();
  OAuthToken? oAuthToken;
  bool _isKakaoTalkInstalled = false;
  ZKakaoSignin() {
    _load();
  }

  void _load() async {
    KakaoContext.clientId = '3131b65d5e7a1366fe0c3ca828614f82';
    await _initKakaoTalkInstalled();
  }

  Future<void> _initKakaoTalkInstalled() async {
    _isKakaoTalkInstalled = await isKakaoTalkInstalled();
  }

  @override
  Future<bool> login() async {
    try {
      if (_isKakaoTalkInstalled) {
        var bSuccess = await loginWithTalk();
        if (!bSuccess) {
          return false;
        }
        return true;
      } else {
        var bSuccess = await loginWithKakao();
        if (!bSuccess) {
          return false;
        }
        return true;
      }
    } catch (e) {
      print(e);
    }
    return false;
  }

  Future<bool> loginWithKakao() async {
    try {
      var code = await AuthCodeClient.instance.request();
      await _issueAccessToken(code);
      return true;
    } catch (e) {
      print(e);
      return false;
    }
  }

  Future<bool> loginWithTalk() async {
    try {
      var code = await AuthCodeClient.instance.requestWithTalk();
      await _issueAccessToken(code);
      return true;
    } catch (e) {
      print(e);
      return false;
    }
  }

  Future<void> _issueAccessToken(String authCode) async {
    try {
      var token = await AuthApi.instance.issueAccessToken(authCode);
      oAuthToken = await AccessTokenStore.instance.toStore(token);
    } catch (e) {
      print('error on issuing access token: $e');
    }
  }

  @override
  Future<ZUserAccount> getAccountInfo() async {
    try {
      var user = await UserApi.instance.me();
      var account = user.kakaoAccount;
      if (account == null) {
        throw Exception('유저정보가 존재하지 않습니다.');
      }
      print(account);
      return ZUserAccount(kakao: {
        'id': user.id,
        'profile_needs_agreement': account.profileNeedsAgreement,
        'nickname': account.profile!.nickname,
        'profileImageUrl': account.profile!.profileImageUrl,
        'thumbnailImageUrl': account.profile!.thumbnailImageUrl,
        'email_needs_agreement': account.emailNeedsAgreement,
        'is_email_valid': account.isEmailValid,
        'email': account.email,
        'birthyear_needs_agreement': account.birthdayNeedsAgreement,
        'birthyear': account.birthyear,
        'birthday_needs_agreement': account.birthdayNeedsAgreement,
        'birthday': account.birthday,
        'birthday_type': account.birthdayType.toString(),
        'gender_needs_agreement': account.genderNeedsAgreement,
        'gender': account.gender.toString(),
        'phone_number_needs_agreement': account.phoneNumberNeedsAgreement,
        'phone_number': account.phoneNumber,
      }, productList: [], recentList: [], likeList: []);
    } catch (e) {
      print(e);
    }
    return ZUserAccount.initZero();
  }

  @override
  Future logout() async {
    try {
      await UserApi.instance.logout();
    } catch (e) {
      print(e);
    }
  }

  @override
  Future<void> unlink() async {
    try {
      var user = await UserApi.instance.me();
      print('Kakao user unlink : $user');
      await UserApi.instance.unlink();
    } catch (e) {
      print(e);
    }
  }

  @override
  String social() {
    return 'kakao';
  }

  Future<HttpsCallableResult<dynamic>> authenticate() async {
    final verifyKakaoToken =
        FirebaseFunctions.instanceFor(region: 'asia-northeast3').httpsCallable('verifyKakaoToken');
    final result = await verifyKakaoToken.call(<String, String>{'token': oAuthToken!.accessToken!});
    print(result);
    // result.data['token'];
    return result;
  }
}
