import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:zzin/common/firestore/document_skima/block_user.dart';
import 'package:zzin/common/zconfig.dart';

class BlockUserController {
  final DocumentReference user;
  BlockUserController({required this.user});

  static Future<QuerySnapshot<BlockUser>> _getSnapShot() async {
    return await _getCollection().get();
  }

  static CollectionReference<BlockUser> _getCollection() {
    return ZConfig.userAccount.docId!.collection('blockUserCollection').withConverter<BlockUser>(
        fromFirestore: (snapshot, _) => BlockUser.fromJson(snapshot.data()!),
        toFirestore: (blockUser, _) => blockUser.toJson());
  }

  static Future<bool> getBlockList() async {
    var snapshot = await _getSnapShot();

    if (snapshot.size > 0) {
      for (var element in snapshot.docs) {
        ZConfig.blockUsers.add(element.data());
      }
      return true;
    }
    return false;
  }

  BlockUser? _findUser(DocumentReference targetUser) {
    var users = ZConfig.blockUsers.where((element) => element.userDocId == targetUser);
    if (users.isNotEmpty) {
      return users.first;
    } else {
      return null;
    }
  }

  Future<bool> addUser() async {
    var blockUser = _findUser(user);
    if (blockUser != null) {
      return false;
    } else {
      blockUser = BlockUser(userDocId: user, isBlocked: true);
      ZConfig.blockUsers.add(blockUser);
      await _getCollection().add(blockUser);
      return true;
    }
  }

  Future<bool> removeUser() async {
    var blockUser = _findUser(user);
    if (blockUser != null) {
      var collection = _getCollection();
      ZConfig.blockUsers.remove(blockUser);
      var snapshot = await collection.where('userDocId', isEqualTo: user).get();
      await snapshot.docs.first.reference.delete();
      return true;
    }
    return false;
  }

  Future<bool> blockUser(bool isBlock) async {
    var tempUser = _findUser(user);
    if (tempUser != null) {
      ZConfig.blockUsers.where((element) => element.userDocId == user).first.isBlocked = isBlock;
      _getCollection().doc(user.path).update({'isBlocked': isBlock});
      return true;
    }
    return false;
  }

  bool isBlocked([DocumentReference? targetUser]) {
    var blockUser = targetUser == null ? _findUser(user) : _findUser(targetUser);
    if (blockUser != null) {
      return blockUser.isBlocked;
    }
    return false;
  }
}
