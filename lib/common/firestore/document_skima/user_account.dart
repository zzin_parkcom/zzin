import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:zzin/common/firestore/document_skima/block_user_controller.dart';
import 'package:zzin/common/hive/address_collection.dart';
import 'package:zzin/common/zconfig.dart';
import 'package:zzin/login/signin/apple_signin.dart';
import 'package:zzin/login/signin/google_signin.dart';
import 'package:zzin/login/signin/social_signin.dart';
import 'package:zzin/login/signin/zkakaosignin.dart';

class ZUserAccount {
  DocumentReference? docId;
  Timestamp? joinedDate;
  bool isVerified = false;
  Timestamp? verifiedDate;
  String? nickname;
  Timestamp? nicknameChangeDate;
  List? agreeAdsInfo;
  Map? kakao;
  Map? google;
  Map? apple;
  bool isSetLocation = false;
  int? regionCode;
  String? regionName;
  GeoPoint? regionGeoPoint;
  List likeList = [];
  List recentList = [];
  List? device;
  List productList = [];
  String? profileImage;
  String pushToken = '';
  String advertisingId = '';

  ZUserAccount(
      {this.docId,
      this.joinedDate,
      this.isVerified = false,
      this.verifiedDate,
      this.nickname,
      this.nicknameChangeDate,
      this.agreeAdsInfo,
      this.kakao,
      this.google,
      this.apple,
      this.isSetLocation = false,
      this.regionGeoPoint,
      this.regionCode,
      this.regionName,
      required this.likeList,
      required this.recentList,
      required this.productList,
      this.device,
      this.profileImage,
      this.pushToken = '',
      this.advertisingId = ''});

  ZUserAccount.initZero()
      : this(
            docId: null,
            joinedDate: null,
            isVerified: false,
            verifiedDate: null,
            nickname: null,
            nicknameChangeDate: null,
            agreeAdsInfo: null,
            kakao: null,
            google: null,
            apple: null,
            isSetLocation: false,
            regionGeoPoint: null,
            regionCode: null,
            regionName: null,
            likeList: [],
            recentList: [],
            productList: [],
            device: null,
            profileImage: null,
            pushToken: '',
            advertisingId: '');

  ZUserAccount.fromJson(Map<String, Object?> json)
      : this(
            docId: json['docId'] as DocumentReference,
            joinedDate: json['joinedDate'] as Timestamp?,
            isVerified: json['isVerified'] as bool,
            verifiedDate: json['verifiedDate'] as Timestamp?,
            nickname: json['nickname'] as String?,
            nicknameChangeDate: json['nicknameChangeDate'] as Timestamp?,
            agreeAdsInfo: json['agreeAdsInfo'] as List?,
            kakao: json['kakao'] as Map?,
            google: json['google'] as Map?,
            apple: json['apple'] as Map?,
            isSetLocation: json['isSetLocation'] as bool,
            regionGeoPoint: json['regionGeoPoint'] as GeoPoint?,
            regionCode: json['regionCode'] as int?,
            regionName: json['regionName'] as String?,
            likeList: json['likeList'] as List,
            recentList: json['recentList'] as List,
            device: json['device'] as List?,
            productList: json['productList'] as List,
            profileImage: json['profileImage'] as String,
            pushToken: json['pushToken'] as String,
            advertisingId: json['advertisingId'] as String);

  Map<String, Object?> toJson() {
    return {
      'docId': docId,
      'joinedDate': joinedDate,
      'isVerified': isVerified,
      'verifiedDate': verifiedDate,
      'nickname': nickname,
      'nicknameChangeDate': nicknameChangeDate,
      'agreeAdsInfo': agreeAdsInfo,
      'kakao': kakao,
      'google': google,
      'apple': apple,
      'isSetLocation': isSetLocation,
      'regionGeoPoint': regionGeoPoint,
      'regionCode': regionCode,
      'regionName': regionName,
      'likeList': likeList,
      'recentList': recentList,
      'device': device,
      'productList': productList,
      'profileImage': profileImage,
      'pushToken': pushToken,
      'advertisingId': advertisingId,
    };
  }

////////////////////////////////////////////////////////////////////////////////////////////////////
  /// static functions.///////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////
  static Future<ZUserAccount> getInfo({required String uid}) async {
    print('user_account::getInfo');
    try {
      print(uid);
      var snapshot = await FirebaseFirestore.instance
          .collection(ZConfig.userCollection)
          .doc(uid)
          .withConverter<ZUserAccount>(
              fromFirestore: (snapshots, _) => ZUserAccount.fromJson(snapshots.data()!),
              toFirestore: (userAccount, _) => userAccount.toJson())
          .get();

      print(snapshot.data()!.docId);
      print(snapshot.data()!.joinedDate);
      print(snapshot.data()!.isVerified);
      print(snapshot.data()!.verifiedDate);
      print(snapshot.data()!.nickname);
      print(snapshot.data()!.nicknameChangeDate);
      print(snapshot.data()!.agreeAdsInfo);
      print(snapshot.data()!.kakao);
      print(snapshot.data()!.google);
      print(snapshot.data()!.apple);
      print(snapshot.data()!.isSetLocation);
      print(snapshot.data()!.regionCode);
      print(snapshot.data()!.regionName);
      print(snapshot.data()!.regionGeoPoint);
      print(snapshot.data()!.likeList);
      print(snapshot.data()!.recentList);
      print(snapshot.data()!.device);
      print(snapshot.data()!.productList);
      print(snapshot.data()!.profileImage);
      print(snapshot.data()!.pushToken);
      print(snapshot.data()!.advertisingId);
      print(snapshot.data()!.nickname);
      print(snapshot.data()!.advertisingId);

      return ZUserAccount(
        docId: snapshot.data()!.docId,
        joinedDate: snapshot.data()!.joinedDate,
        isVerified: snapshot.data()!.isVerified,
        verifiedDate: snapshot.data()!.verifiedDate,
        nickname: snapshot.data()!.nickname,
        nicknameChangeDate: snapshot.data()!.nicknameChangeDate,
        agreeAdsInfo: snapshot.data()!.agreeAdsInfo,
        kakao: snapshot.data()!.kakao,
        google: snapshot.data()!.google,
        apple: snapshot.data()!.apple,
        isSetLocation: snapshot.data()!.isSetLocation,
        regionCode: snapshot.data()!.regionCode,
        regionName: snapshot.data()!.regionName,
        regionGeoPoint: snapshot.data()!.regionGeoPoint,
        likeList: snapshot.data()!.likeList,
        recentList: snapshot.data()!.recentList,
        device: snapshot.data()!.device,
        productList: snapshot.data()!.productList,
        profileImage: snapshot.data()!.profileImage,
        pushToken: snapshot.data()!.pushToken,
        advertisingId: snapshot.data()!.advertisingId,
      );
    } catch (e) {
      print('UserAccount Failed... : $e');
    }

    return ZUserAccount.initZero();
  }

////////////////////////////////////////////////////////////////////////////////////////////////////
  /// instance functions.///////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////
  void initialize() {
    docId = null;
    joinedDate = null;
    isVerified = false;
    verifiedDate = null;
    nickname = null;
    nicknameChangeDate = null;
    agreeAdsInfo = null;
    kakao = null;
    google = null;
    apple = null;
    isSetLocation = false;
    regionCode = null;
    regionName = null;
    regionGeoPoint = null;
    likeList = [];
    recentList = [];
    device = null;
    productList = [];
    pushToken = '';
    advertisingId = '';
  }

  SocialSignin? getSocial() {
    if (kakao != null) {
      return ZKakaoSignin();
    } else if (google != null) {
      return ZGoogleSignin();
    } else if (apple != null) {
      return ZAppleSignin();
    } else {
      // 게스트유저는 파이어베이스 로그아웃 못함.
    }
    return null;
  }

  String getSocialName() {
    var social = getSocial();
    if (social != null) {
      return social.social();
    } else {
      return 'geust';
    }
  }

  bool isRegistered() {
    var social = getSocial();
    return FirebaseAuth.instance.currentUser!.isAnonymous == false && isVerified && social != null;
  }

  String getThumbnailPath() {
    if (profileImage != null && profileImage!.isNotEmpty) {
      return profileImage!;
    } else {
      switch (getSocialName()) {
        case 'google':
          return google!['photoUrl'] ?? ZConfig.defaultImageStorageURL;
        case 'kakao':
          return kakao!['thumbnailImageUrl'] ?? ZConfig.defaultImageStorageURL;
        default:
          return ZConfig.defaultImageStorageURL;
      }
    }
  }

  Future<void> logout() async {
    var social = getSocial();
    if (social != null) {
      try {
        await FirebaseAuth.instance.signOut();
        await social.logout();
        initialize();
        var credential = await FirebaseAuth.instance.signInAnonymously();
        ZConfig.authUser = credential.user;
        print(ZConfig.authUser);
      } on FirebaseAuthException catch (e) {
        print('Failed with error code: ${e.code}');
        print(e.message);
      }
    }
  }

  Future<void> unlink() async {
    var social = getSocial();
    // if (social != null) {
    await social!.unlink();
    // }
  }

  GeoPoint? getLocationGeoPoint() {
    return regionGeoPoint;
  }

  int getLocationIdByGeoPoint() {
    if (regionGeoPoint != null) {
      return AddressCollection.to.values
          .firstWhere((element) =>
              element.latitude == regionGeoPoint!.latitude &&
              element.longitude == regionGeoPoint!.longitude)
          .regionCode;
    }
    return -1;
  }

  Future<void> updateLocationGeoPointByIndex(int indexSaleArea) async {
    if (isVerified && (isSetLocation == false || regionGeoPoint == null)) {
      var address =
          AddressCollection.to.values.where((element) => element.regionCode == indexSaleArea).first;

      var userGeoPoint = GeoPoint(address.latitude, address.longitude);
      await FirebaseFirestore.instance
          .doc(ZConfig.userAccount.docId!.path)
          .update({'regionGeoPoint': userGeoPoint, 'isSetLocation': true});
    }
  }

  @override
  String toString() {
    var result = 'joinedDate : $joinedDate\nisVerified : $isVerified\n';
    result += 'verifiedDate : $verifiedDate\nnickname : $nickname\n';
    result += 'nicknameChangeDate : $nicknameChangeDate\nagreeAdsInfo : $agreeAdsInfo\n';
    result += 'kakao : $kakao\ngoogle : $google\napple : $apple\nisSetLocation : $isSetLocation\n';
    result += 'regionCode : $regionCode\regionName : $regionName\n';
    result += 'regionGeoPoint : $regionGeoPoint\nlikeList : $likeList\n';
    result += 'recentList : $recentList\ndevice : $device\n';
    result += 'pushToken: $pushToken';

    return result;
  }

  Future<int> addRecent(DocumentReference recent) async {
    if (recentList.contains(recent)) {
      print('이미 리스트에 있습니다.');
      return recentList.length;
    }

    if (recentList.length < ZConfig.maxRecentListCount) {
      recentList.add(recent);
    } else if (recentList.length == ZConfig.maxRecentListCount) {
      recentList.removeAt(0);

      recentList.add(recent);
    }

    if (isVerified) {
      await docId!.update({'recentList': recentList});
    }

    return recentList.length;
  }

  bool isBlocked(DocumentReference targetUser) {
    return BlockUserController(user: targetUser).isBlocked();
  }

  bool isZzinMember() {
    if (isVerified &&
        google != null &&
        google!['email'] != null &&
        google!['email'].contains('@zzinmarket.kr')) {
      return true;
    }
    return false;
  }
}
