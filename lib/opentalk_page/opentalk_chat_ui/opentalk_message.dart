import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_database/firebase_database.dart';

enum OpentalkMessageType {
  text,
  image,
  location,
}

class OpentalkMessage {
  final String writer;
  final OpentalkMessageType type;
  final String text;
  final Timestamp time;

  OpentalkMessage(this.writer, this.type, this.text, this.time);
  OpentalkMessage.fromJson(Map<dynamic, dynamic> json)
      : writer = json['writer'] as String,
        type = OpentalkMessageType.values[json['type']],
        time = Timestamp.fromMillisecondsSinceEpoch(json['time']),
        text = json['text'] as String;

  Map<dynamic, dynamic> toJson() => <dynamic, dynamic>{
        'writer': writer,
        'type': type.index,
        'time': ServerValue.timestamp,
        'text': text,
      };
}
