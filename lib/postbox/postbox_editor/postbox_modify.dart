import 'dart:async';

import 'package:animations/animations.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:zzin/common/widget/zbutton_white.dart';
import 'package:zzin/common/zconfig.dart';
import 'package:zzin/common/zutils.dart';
import 'package:zzin/postbox/postbox.dart';
import 'package:zzin/postbox/postbox_controller.dart';
import 'package:zzin/postbox/postbox_editor.dart';

class PostboxModify extends StatefulWidget {
  const PostboxModify({Key? key}) : super(key: key);

  @override
  _PostboxModifyState createState() => _PostboxModifyState();
}

class _PostboxModifyState extends State<PostboxModify> {
  double fontSize = 10;

  final StreamController<QuerySnapshot<Postbox>> streamController = StreamController();
  final TextEditingController titleController = TextEditingController();

  final List columnList = [
    'type',
    'title',
    'reportingDate',
    'expirationDate',
    'contents',
    'isRemoved',
    'isRead',
    'docId',
    'reporterRef',
  ];

  @override
  void dispose() {
    streamController.close();
    super.dispose();
  }

  @override
  void initState() {
    super.initState();
    streamController.addStream(getPostbox());
  }

  Stream<QuerySnapshot<Postbox>> getPostbox() {
    var stream = FirebaseFirestore.instance
        .collection(ZConfig.postboxCollection)
        .withConverter<Postbox>(
            fromFirestore: (snapshots, _) => Postbox.fromJson(snapshots.data()!),
            toFirestore: (commonPostbox, _) => commonPostbox.toJson())
        .snapshots();
    return stream;
  }

  Future updatePostbox(Postbox postbox) async {
    await PostboxController().updatePostbox(postbox);
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
          appBar: AppBar(),
          body: StreamBuilder<QuerySnapshot<Postbox>>(
            stream: streamController.stream,
            builder: (context, snapshot) {
              if (!snapshot.hasData) {
                return const Center(
                  child: Text('알림 데이타가 없습니다.'),
                );
              } else {
                return InteractiveViewer(
                  constrained: false,
                  child: DataTable(
                    headingRowColor: MaterialStateProperty.all<Color>(Colors.lightBlue),
                    columnSpacing: 10,
                    headingTextStyle: TextStyle(fontSize: fontSize, color: Colors.black),
                    dataTextStyle: TextStyle(fontSize: fontSize, color: Colors.black),
                    columns: List.generate(
                        columnList.length, (index) => DataColumn(label: Text(columnList[index]))),
                    rows: List<DataRow>.generate(
                      snapshot.data!.docs.length,
                      (index) {
                        Postbox postbox = snapshot.data!.docs[index].data();
                        return DataRow(cells: [
                          DataCell(getTypeWidget(postbox)),
                          DataCell(
                              SizedBox(
                                  width: 100,
                                  child: Text(postbox.title, overflow: TextOverflow.ellipsis)),
                              onTap: () => onTitle(postbox)),
                          DataCell(
                              Text(formatyMd(postbox.reportingDate) +
                                  ' ${formatJMV(postbox.reportingDate)}'),
                              onTap: () => onReportingDate(postbox)),
                          DataCell(
                              Text(formatyMd(postbox.expirationDate) +
                                  ' ${formatJMV(postbox.expirationDate)}'),
                              onTap: () => onExpirationDate(postbox)),
                          DataCell(SizedBox(width: 200, child: Text('${postbox.contents}')),
                              onTap: () => onContents(postbox)),
                          DataCell(Text('${postbox.isRemoved}')),
                          DataCell(Text('${postbox.isRead}')),
                          DataCell(Text(postbox.docId)),
                          DataCell(Text(postbox.reporterRef.path.split('/').last)),
                        ]);
                      },
                    ),
                  ),
                );
              }
            },
          )),
    );
  }

  Widget getTypeWidget(Postbox postbox) => DropdownButton<String>(
        value: postbox.type.korText,
        icon: const Icon(Icons.arrow_downward),
        iconSize: fontSize,
        elevation: 16,
        style: const TextStyle(color: ZConfig.zzin_pointColor),
        underline: Container(height: 2, color: Colors.black),
        onChanged: (String? newValue) async {
          if (newValue == PostboxType.Notice.korText) {
            postbox.type = PostboxType.Notice;
          } else if (newValue == PostboxType.Event.korText) {
            postbox.type = PostboxType.Event;
          } else if (newValue == PostboxType.UrgentNotice.korText) {
            postbox.type = PostboxType.UrgentNotice;
          }
          print(postbox.type);
          await updatePostbox(postbox);

          setState(() {});
        },
        items:
            List.generate(PostboxType.values.length, (index) => PostboxType.values[index].korText)
                .map<DropdownMenuItem<String>>(
          (String value) {
            return DropdownMenuItem<String>(
              value: value,
              child: Text(
                value,
                style: TextStyle(fontSize: fontSize),
              ),
            );
          },
        ).toList(),
      );

  void onTitle(Postbox postbox) async {
    showModal(
      context: context,
      builder: (context) {
        titleController.text = postbox.title;
        return Center(
          child: Container(
            width: Get.width - 100,
            color: Colors.white,
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                TextField(
                  controller: titleController,
                ),
                ZButtonWhite(
                  text: '수정',
                  isSelected: false,
                  onPressed: () async {
                    postbox.title = titleController.text.trim();
                    await updatePostbox(postbox);
                    Get.back();
                  },
                )
              ],
            ),
          ),
        );
      },
    );
  }

  void onReportingDate(Postbox postbox) async {
    var date = await showDatePicker(
      context: context,
      initialDate: postbox.reportingDate.toDate(),
      firstDate: DateTime(2021),
      lastDate: postbox.expirationDate.toDate(),
    );

    if (date != null) {
      var time = await showTimePicker(context: context, initialTime: TimeOfDay.now());
      if (time != null) {
        DateTime dateTime = DateTime(date.year, date.month, date.day, time.hour, time.minute);
        Timestamp timeStamp = Timestamp.fromDate(dateTime);
        postbox.reportingDate = timeStamp;
        await updatePostbox(postbox);
      }
    }
  }

  void onExpirationDate(Postbox postbox) async {
    var date = await showDatePicker(
      context: context,
      initialDate: postbox.expirationDate.toDate(),
      firstDate: postbox.reportingDate.toDate(),
      lastDate: DateTime(2100),
    );

    if (date != null) {
      var time = await showTimePicker(context: context, initialTime: TimeOfDay.now());
      if (time != null) {
        DateTime dateTime = DateTime(date.year, date.month, date.day, time.hour, time.minute);
        Timestamp timeStamp = Timestamp.fromDate(dateTime);
        postbox.expirationDate = timeStamp;
        await updatePostbox(postbox);
      }
    }
  }

  void onContents(Postbox postbox) async {
    await Get.to(() => PostboxEditor(postbox: postbox));
  }
}
