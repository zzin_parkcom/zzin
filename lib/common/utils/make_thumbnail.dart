import 'dart:io';
import 'dart:isolate';
import 'dart:typed_data';
import 'package:image/image.dart' as image_lib;

class MakeThumbnail {
  String originFilePath;
  String targetFilePath;
  int width;
  int height;
  int quality;
  MakeThumbnail(
      {required this.originFilePath,
      required this.targetFilePath,
      this.width = 300,
      this.height = 300,
      this.quality = 100});

  Future<List<int>> makeMemory() async {
    var receivePort = ReceivePort();
    await Isolate.spawn(
        decodeIsolate, DecodeParam(File(originFilePath), receivePort.sendPort, width, height));

    // Get the processed image from the isolate.
    var image = await receivePort.first as image_lib.Image;

    // 디렉토리 체크
    var isTargetPathExist = await Directory(_getFilePath()).exists();
    if (!isTargetPathExist) {
      await Directory(targetFilePath).create(recursive: true);
    }

    return image_lib.encodeJpg(image, quality: quality);
  }

  Future<File> makeFile(Uint8List bytes) async {
    var fileThumbnail = File(targetFilePath);
    var resultThumbnail = await fileThumbnail.writeAsBytes(bytes);

    return resultThumbnail;
  }

  static Future<void> deleteFile(String deleteFilePath) async {
    var isTargetFileExist = await File(deleteFilePath).exists();
    if (isTargetFileExist) {
      print('delete');
      await File(deleteFilePath).delete();
    }
  }

  String _getFilePath() {
    var fileName = (targetFilePath.split('/').last);
    var filePath = targetFilePath.replaceAll('/$fileName', '');
    return filePath;
  }
}

class DecodeParam {
  final File file;
  final SendPort sendPort;
  int width;
  int height;
  DecodeParam(this.file, this.sendPort, this.width, this.height);
}

void decodeIsolate(DecodeParam param) {
  // Read an image from file (webp in this case).
  // decodeImage will identify the format of the image and use the appropriate
  // decoder.
  var image = image_lib.decodeImage(param.file.readAsBytesSync())!;
  // Resize the image to a 120x? thumbnail (maintaining the aspect ratio).
  var thumbnail = image_lib.copyResize(image, width: param.width, height: param.height);
  param.sendPort.send(thumbnail);
}
