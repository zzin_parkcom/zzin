import 'package:flutter/material.dart';
import 'package:zzin/common/zconfig.dart';
import 'package:zzin/my_page/blockuser_listview.dart';

class BlockUserManagePage extends StatefulWidget {
  const BlockUserManagePage({Key? key}) : super(key: key);

  @override
  _BlockUserManagePageState createState() => _BlockUserManagePageState();
}

class _BlockUserManagePageState extends State<BlockUserManagePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar:
          AppBar(title: const Text('차단 사용자 관리', style: TextStyle(fontSize: ZSize.appBarTextSize))),
      body: SafeArea(
        child: Padding(
          padding: const EdgeInsets.fromLTRB(20, 20, 20, 20),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: const [
              BlockUserListView(),
            ],
          ),
        ),
      ),
    );
  }
}
