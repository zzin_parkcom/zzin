import 'package:flutter/material.dart';
import 'package:zzin/common/zconfig.dart';

class NoInternetPage extends StatelessWidget {
  const NoInternetPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    print('NoInternetPage build');
    return Scaffold(
      body: Padding(
        padding: const EdgeInsets.all(1.0),
        child: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: const [
              Icon(
                Icons.wifi_off,
                size: 100,
                color: ZConfig.grey_text,
              ),
              Text(
                '인터넷 끊김',
                textAlign: TextAlign.center,
                style: TextStyle(fontSize: 20, color: ZConfig.grey_text),
              ),
              Text(
                '네트워크 상태를 확인해 주세요',
                textAlign: TextAlign.center,
                style: TextStyle(fontSize: 14, color: ZConfig.grey_text),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
