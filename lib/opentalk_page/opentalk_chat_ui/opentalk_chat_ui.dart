import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:keyboard_dismisser/keyboard_dismisser.dart';
import 'package:zzin/common/firestore/document_skima/opentalk_data.dart';
import 'package:zzin/opentalk_page/opentalk_chat_ui/opentalk_chat_ui_message_list.dart';
import 'package:zzin/opentalk_page/opentalk_chat_ui/opentalk_chat_ui_write_message.dart';

class OpentalkChatUI extends StatefulWidget {
  final OpentalkData opentalk;
  const OpentalkChatUI({required this.opentalk, Key? key}) : super(key: key);

  @override
  _OpentalkChatUIState createState() => _OpentalkChatUIState();
}

class _OpentalkChatUIState extends State<OpentalkChatUI> {
  final ScrollController _scrollController = ScrollController();

  @override
  void initState() {
    super.initState();

    SchedulerBinding.instance!.addPostFrameCallback((_) => frameCallback(context));
  }

  @override
  void dispose() {
    super.dispose();
  }

  void frameCallback(BuildContext context) async {}

  @override
  Widget build(BuildContext context) {
    return KeyboardDismisser(
      child: SafeArea(
        child: Scaffold(
          appBar: AppBar(
            title: Text(widget.opentalk.name),
          ),
          resizeToAvoidBottomInset: true,
          body: Stack(
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.fromLTRB(20, 0, 20, 0),
                child: OpentalkChatUIMessageList(
                    opentalkName: widget.opentalk.name, scrollController: _scrollController),
              )
            ],
          ),
          bottomNavigationBar: OpentalkChatUIWriteMessage(scrollController: _scrollController),
        ),
      ),
    );
  }
}

///////////////////////////////////////////////////////////////////
