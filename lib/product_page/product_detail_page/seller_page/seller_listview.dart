import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:uuid/uuid.dart';
import 'package:zzin/common/firestore/document_skima/user_account.dart';
import 'package:zzin/common/firestore/query_user_product.dart';
import 'package:zzin/common/utils/zenum.dart';
import 'package:zzin/common/zconfig.dart';
import 'package:zzin/product_page/widget/product_list_generator.dart';

class SellerListView extends StatefulWidget {
  final ZUserAccount seller;
  final ProductSaleStatus saleStatus;
  final List<String> noProductMessages;
  const SellerListView(
      {required this.seller,
      this.saleStatus = ProductSaleStatus.ONSALE,
      required this.noProductMessages,
      Key? key})
      : super(key: key);
  @override
  _SellerListViewState createState() => _SellerListViewState();
}

class _SellerListViewState extends State<SellerListView> {
  final ScrollController _scrollController = ScrollController();
  late final QueryUserProduct queryUserProduct;
  @override
  void initState() {
    super.initState();

    queryUserProduct = Get.put(QueryUserProduct(), tag: const Uuid().v1().toString());

    _scrollController.addListener(_scrollListener);
  }

  @override
  void dispose() {
    _scrollController.removeListener(_scrollListener);
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: getUserProduct(),
      builder: (context, snapshot) {
        if (snapshot.connectionState == ConnectionState.done) {
          return ProductListGenerator(
            products: queryUserProduct.products,
            onRefresh: onRefresh,
            scrollController: _scrollController,
            onDetailPage: null,
            tagHeroPrefix: 'Seller_',
            noProductMessages: widget.noProductMessages,
          );
        } else {
          return const SizedBox.shrink();
        }
      },
    );
  }

  Future<void> getUserProduct() async {
    return await queryUserProduct.queryUserProduct(widget.seller.docId!, widget.saleStatus.index);
  }

  Future onRefresh() async {
    await queryUserProduct.queryUserProduct(
        widget.seller.docId!, queryUserProduct.queryValue.saleStatus,
        isReload: true);
  }

  void _scrollListener() {
    var maxScroll = _scrollController.position.maxScrollExtent;
    var currentScroll = _scrollController.position.pixels;
    var delta = maxScroll * ZConfig.scrollRatioQueryMore;

    if (maxScroll - currentScroll <= delta && queryUserProduct.isLoading == false) {
      queryUserProduct.queryUserProduct(
          widget.seller.docId!, queryUserProduct.queryValue.saleStatus,
          byScroll: true);
    }
  }
}
