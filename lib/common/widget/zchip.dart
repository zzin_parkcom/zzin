import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:zzin/common/zconfig.dart';

class ZChip extends StatelessWidget {
  final String text;
  final bool selected;
  final ValueChanged<bool> onSelected;

  const ZChip({Key? key, required this.text, required this.selected, required this.onSelected})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    var textStyle = const TextStyle(fontSize: 16);
    return ChoiceChip(
      label: Text(
        text,
        style: textStyle,
      ),
      selected: selected,
      selectedColor: ZConfig.zzin_primaryColor,
      onSelected: onSelected,
      shape: const StadiumBorder(side: BorderSide(color: ZConfig.zzin_pointColor, width: 1.0)),
      backgroundColor: ZConfig.zzin_white,
      labelStyle: const TextStyle(color: ZConfig.zzin_pointColor),
    );
  }

  static List<Widget> generate({
    required List textList,
    required int length,
    // required List<bool> isSelectedList,
    required List<ValueChanged<bool>> onSelectedList,
  }) {
    return List.generate(length, (index) {
      return ZChip(text: textList[index], selected: false, onSelected: onSelectedList[index]);
    });
  }
}
