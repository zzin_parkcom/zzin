import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:zzin/common/firestore/document_skima/user_account.dart';
import 'package:zzin/common/zconfig.dart';

class ZUserProcess {
  var users = FirebaseFirestore.instance.collection(ZConfig.userCollection);

  Future<bool> addUser(BuildContext context, ZUserAccount user, String uid) async {
    var bNewUser = await isNewUser(context, user);
    if (bNewUser) {
      var result = _addUser(context, user, uid);
      ZConfig.userAccount = await ZUserAccount.getInfo(uid: uid);
      return result;
    } else {
      print('이미 존재하는 유저입니다.');

      ZConfig.userAccount = await ZUserAccount.getInfo(uid: uid);
      await ZConfig.userAccount.docId!.update({'pushToken': ZConfig.pushToken});
      print('pushToken update: ${ZConfig.pushToken}');
    }
    return true;
  }

  Future<bool> isNewUser(BuildContext context, ZUserAccount user) async {
    var bNewUser = false;
    if (user.google != null) {
      await users
          .where('google.id', isEqualTo: user.google!['id'])
          .get()
          .then((value) => bNewUser = (value.size > 0 ? false : true));
    } else if (user.kakao != null) {
      await users
          .where('kakao.id', isEqualTo: user.kakao!['id'])
          .get()
          .then((value) => bNewUser = (value.size > 0 ? false : true));
    } else if (user.apple != null) {
      await users
          .where('apple.userIdentifier', isEqualTo: user.apple!['userIdentifier'])
          .get()
          .then((value) => bNewUser = (value.size > 0 ? false : true));
    } else {
      bNewUser = true;
    }
    return bNewUser;
  }

  Future<bool> _addUser(BuildContext context, ZUserAccount user, String uid) async {
    print('_addUser');
    print(uid);
    print(FieldValue.serverTimestamp());
    print(user.nickname);
    print(user.agreeAdsInfo);
    print(user.kakao);
    print(user.google);
    print(user.apple);
    print(user.isSetLocation);
    print(user.regionCode);
    print(user.regionName);
    print(user.regionGeoPoint);
    print(user.likeList);
    print(user.recentList);
    print(user.device);
    print(user.getThumbnailPath());
    print(ZConfig.pushToken);
    print(user.advertisingId);
    await users
        .doc(uid)
        .set({
          'docId': users.doc(uid),
          'joinedDate': FieldValue.serverTimestamp(),
          'isVerified': (user.google != null) || (user.kakao != null) || (user.apple != null),
          'verifiedDate': FieldValue.serverTimestamp(),
          'nickname': user.nickname,
          'nicknameChangeDate': FieldValue.serverTimestamp(),
          'agreeAdsInfo': user.agreeAdsInfo,
          'kakao': user.kakao,
          'google': user.google,
          'apple': user.apple,
          'isSetLocation': user.isSetLocation,
          'regionCode': user.regionCode,
          'regionName': user.regionName,
          'regionGeoPoint': user.regionGeoPoint,
          'likeList': user.likeList,
          'recentList': user.recentList,
          'device': user.device,
          'profileImage': user.getThumbnailPath(),
          'productList': [],
          'pushToken': ZConfig.pushToken,
          'advertisingId': user.advertisingId,
        })
        .then((value) => print('user added...'))
        .catchError((e) => print('Failed to add user: $e'));

    await ZConfig.analytics.logEvent(name: 'registration_complete');
    print('_addUser end');
    return true;
  }
}
