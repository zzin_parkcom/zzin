import 'dart:io';
import 'package:flutter_image_compress/flutter_image_compress.dart';
import 'package:heic_to_jpg/heic_to_jpg.dart';
import 'package:image/image.dart' as image_lib;
import 'package:flutter/foundation.dart';
import 'package:path_provider/path_provider.dart';

class ImageProcessing {
  final List<File> _files;
  ImageProcessing(this._files);

  Future<ThumbnailParam> processImage() async {
    Directory? externalStorageDir;
    if (Platform.isAndroid) {
      externalStorageDir = await getExternalStorageDirectory();
      if (externalStorageDir == null) {
        print('getExternalStorageDirectory Failed!!!');
        throw Exception('getExternalStorageDirectory Failed!!!');
      }
    } else if (Platform.isIOS) {
      externalStorageDir = await getApplicationDocumentsDirectory();
      // if (externalStorageDir == null) {
      //   print('getApplicationDocumentsDirectory Failed!!!');
      //   throw Exception('getApplicationDocumentsDirectory Failed!!!');
      // }
    }

    var filesWebp = <File>[];
    File? fileThumb;

    for (var file in _files) {
      filesWebp.add(File(
          '${externalStorageDir!.path}/${file.path.split('/').last.split('.').first}_orgin.webp'));
      fileThumb ??= File(
          '${externalStorageDir.path}/${file.path.split('/').last.split('.').first}_thumbnail.jpg');
    }
    if (fileThumb == null) {
      print('fileThumb is null');
      throw Exception('fileThumb is null!!!');
    }

    var filesTemp = <File>[];
    for (var i = 0; i < _files.length; i++) {
      if (_files[i].path.toLowerCase().endsWith('.heic')) {
        var path = await HeicToJpg.convert(_files[i].path);
        var file = File(path!);

        // heic 파일 90도 회전
        var imageThumb = image_lib.decodeImage(file.readAsBytesSync());
        var image = image_lib.copyRotate(imageThumb!, 90);
        var buffer = image_lib.encodeNamedImage(image, '.jpg');
        file.writeAsBytes(buffer!);

        filesTemp.add(file);
      } else {
        filesTemp.add(_files[i]);
      }
    }
    var resultParam = await compute(makeThumbnail, ThumbnailParam(filesTemp, filesWebp, fileThumb));

    for (var i = 0; i < filesWebp.length; i++) {
      print('Webp[$i] Processing...');
      filesWebp[i] =
          await compressAndGetFile(_files[i], filesWebp[i].path, resultParam.minWidth, 80);
    }
    return resultParam;
  }
}

//////////////////////////////////////////////////////////////////////////////
///
class ThumbnailParam {
  final List<File> files;
  final List<File> filesWebp;
  final File fileThumb;
  int minWidth = 300;
  ThumbnailParam(this.files, this.filesWebp, this.fileThumb);
}

class CompressParam {
  final File file;
  final String targetPath;
  final int minWidth;
  final int quality;
  CompressParam(this.file, this.targetPath, this.minWidth, this.quality);
}

// isolate Function
Future<ThumbnailParam> makeThumbnail(ThumbnailParam param) async {
  var quality = 80;
  print('From isolate......................');
  for (var element in param.files) {
    print(element.path);
  }

  for (var element in param.filesWebp) {
    print(element.path);
  }
  print(param.fileThumb);

  var imageThumb = image_lib.decodeImage(param.files[0].readAsBytesSync());
  if (imageThumb == null) {
    print('decodeImage Thumb is null');
    throw Exception('decodeImage Thumb is null');
  }
  imageThumb = image_lib.copyResizeCropSquare(imageThumb, 300);

  var ratio = imageThumb.width / imageThumb.height;
  param.minWidth = (1440 / ratio).round();

  print('Thumbnail Processing...');
  await param.fileThumb.writeAsBytes(image_lib.encodeJpg(imageThumb, quality: quality));

  print('makeThumbnail end');
  return param;
}

Future<File> compressAndGetFile(File file, String targetPath, int minWidth, int quality) async {
  print('minWidth: $minWidth');
  print('quality: $quality');
  print('$file         $targetPath             $minWidth      $quality');
  var result = await FlutterImageCompress.compressAndGetFile(file.absolute.path, targetPath,
      // minHeight: 1440,
      minWidth: minWidth,
      quality: quality,
      format: CompressFormat.webp);

  print('compressAndGetFile end');
  return result!;
}
