import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:zzin/common/zconfig.dart';

class RecentOverlayEntry extends OverlayEntry {
  final List<String> recentSearchList;
  final TextEditingController editingController;
  RecentOverlayEntry({
    required this.recentSearchList,
    required this.editingController,
  }) : super(builder: (context) {
          return Positioned(
            top: Get.mediaQuery.padding.top + AppBar().preferredSize.height,
            left: 100,
            child: Column(
                children: List.generate(
              recentSearchList.length,
              (index) => SizedBox(
                height: 40,
                width: Get.width / 2,
                child: GestureDetector(
                  onTap: () {
                    editingController.text = editingController.text +
                        (editingController.text.endsWith(' ') ? '' : ' ') +
                        recentSearchList[index];
                    editingController.selection = TextSelection.fromPosition(
                        TextPosition(offset: editingController.text.length));
                  },
                  child: Container(
                    color: ZConfig.zzin_white,
                    child: Align(
                        alignment: Alignment.centerLeft,
                        child: Text(
                          recentSearchList[index],
                          style: const TextStyle(fontSize: 18, color: ZConfig.zzin_black),
                        )),
                  ),
                ),
              ),
            )),
          );
        });
}
