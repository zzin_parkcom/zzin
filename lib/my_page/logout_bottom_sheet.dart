import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:zzin/common/widget/zbutton_blue.dart';

class LogoutBottomSheet extends StatelessWidget {
  const LogoutBottomSheet({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          const Text(
            '정말 로그아웃 하시겠어요?',
            style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20),
          ),
          const SizedBox(height: 24),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              ZButtonYellow(
                onPressed: () => Get.back(result: 0),
                text: '취소하기',
                fontSize: 14,
                isSelected: true,
              ),
              ZButtonYellow(
                text: '로그아웃',
                isSelected: true,
                fontSize: 14,
                onPressed: () => Get.back(result: 1),
              ),
            ],
          )
        ],
      ),
    );
  }
}
