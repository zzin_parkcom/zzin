import 'package:elastic_client/elastic_client.dart';
import 'package:get/get.dart';
import 'package:zzin/common/firestore/document_skima/product.dart';
import 'package:zzin/common/hive/address_collection.dart';
import 'package:zzin/common/zconfig.dart';

class ESProduct extends GetxController {
  final _transport = HttpTransport(
      url:
          'https://compute-optimized-deployment-9a116e.es.ap-northeast-2.aws.elastic-cloud.com:9243',
      authorization: basicAuthorization('elastic', 'kbjNLkIIX1fnAk5RstMnTvx6'));

  late final Client client;
  String? scrollId;
  int totalCount = 0;
  int currentCount = 0;
  final List<Product> products = <Product>[].obs;

  ESProduct() {
    client = Client(_transport);
  }

  Future searchByDistance(
      {int radius = 2,
      int size = 10,
      required double lat,
      required double lon,
      bool isAscending = true,
      bool isClear = true}) async {
    try {
      var rs = await client.search(
          scroll: const Duration(hours: 1),
          size: size,
          index: ZConfig.productsCollection,
          type: '_doc',
          query: Query.bool(
            must: {
              'match': {'articleStatus': 0}
            },
            filter: {
              'geo_distance': {
                'distance': radius.toString() + 'km',
                'regionGeoPoint': {'lat': lat, 'lon': lon}
              }
            },
          ),
          sort: [
            {
              '_geo_distance': {
                'regionGeoPoint': {'lat': lat, 'lon': lon},
                'order': isAscending ? 'asc' : 'desc',
                'unit': 'km',
                'mode': 'min',
                'distance_type': 'arc',
                'ignore_unmapped': false
              }
            }
          ],
          source: true);

      var productsCount = await processResult(rs, isClear);
      if (productsCount < 10) {
        await searchNext();
      }
    } catch (e) {
      print(e);
    }
  }

  Future<int> boolSearch({
    int size = 10,
    bool isClear = true,
    String queryString = '',
    int brand = NotSelected,
    int category = NotSelected,
    int saleStatus = NotSelected,
    int status = NotSelected,
    int regionCode = NotSelected,
    int distance = NotSelected,
    String sortField = '',
    String sortOrder = '',
  }) async {
    print('boolSearch---start-------------------------------------------------------------------');

    // 검색어 Pharse 로 변경.
    var queryPharseList = <String>[];
    if (queryString.isNotEmpty) {
      var queryStringList = queryString.split(' ');
      queryPharseList = queryStringList.map((string) => '"' + string + '"').toList();
    }

    var must = <Map>[];
    if (queryString.isNotEmpty) {
      var searchString = queryPharseList.join(' ');
      must.add({
        'query_string': {
          'query': searchString,
          'fields': ['title', 'contents', 'tags', 'regionName']
        }
      });
      print('검색어: $searchString');
    }

    // 기본 필터 추가.
    must.add({
      'match': {'articleStatus': 0}
    });

    // 브랜드 필터
    if (brand != NotSelected) {
      must.add({
        'match': {'brand': brand}
      });
      print('brand: $brand');
    }
    // 카테고리 필터
    if (category != NotSelected) {
      must.add({
        'match': {'category': category}
      });
      print('category: $category');
    }
    // 판매상태 필터
    if (saleStatus != NotSelected && saleStatus != AllItems) {
      must.add({
        'match': {'saleStatus': saleStatus}
      });
      print('saleStatus: $saleStatus');
    }
    // 상품상태 필터
    if (status != NotSelected && status != AllItems) {
      must.add({
        'match': {'status': status}
      });
      print('status: $status');
    }

    // 거리 필터
    var filter = <Map>[];
    if (distance != NotSelected && regionCode != NotSelected) {
      var position = AddressCollection.getGeoPointByRegionCode(regionCode);
      filter.add({
        'geo_distance': {
          'distance': distance.toString() + 'm',
          'regionGeoPoint': {'lat': position.latitude, 'lon': position.longitude}
        }
      });
      print('filter : ${filter.toString()}');
    }

    // 정렬 필터.
    var sort = <Map>[];
    if (sortField.isNotEmpty) {
      sort.add({
        sortField: {'order': sortOrder}
      });
      print('sort : ${sort.toString()}, $sortOrder');
    }

    try {
      var rs = await client.search(
        scroll: const Duration(hours: 1),
        size: size,
        index: ZConfig.productsCollection,
        type: '_doc',
        query: Query.bool(
          must: must,
          filter: distance != NotSelected ? filter : null,
        ),
        sort: sortField.isNotEmpty ? sort : null,
      );
      var productsCount = await processResult(rs, isClear);
      if (productsCount < 10) {
        await searchNext();
      }

      print(
          'boolSearch---end---------------------------------------------------------------------');
      return totalCount;
    } catch (e) {
      print(e);
    }
    return -1;
  }

  Future<int> processResult(SearchResult result, bool isClear) async {
    if (isClear) {
      await clearProducts();
    }

    scrollId = result.scrollId;

    for (var hit in result.hits) {
      var product = Product.fromElastic(hit.doc);
      print('정확도: ${hit.score}    ${product.toJson()}');
      products.add(product);

      if (ZConfig.userAccount.isRegistered()) {
        products.removeWhere((product) =>
            ZConfig.blockUsers.any((element) => element.userDocId == product.sellerId));
      }
    }
    currentCount += result.hits.length;
    totalCount = result.totalCount;

    print('SearchResult count: ${result.hits.length}');
    print('currentCount: $currentCount');
    print('totalCount: ${result.totalCount}');
    return products.length;
  }

  Future clearProducts() async {
    print('clearProducts');
    products.clear();
    totalCount = 0;
    currentCount = 0;
    await clearScroll();
  }

  Future searchNext() async {
    if (scrollId == null) {
      print('scrollId is null!!!!');
      return;
    }

    if (!hasMore()) {
      print('더이상 상품이 없습니다.');
      return;
    }
    var rs = await client.scroll(scrollId: scrollId!, duration: const Duration(hours: 1));

    await processResult(rs, false);
  }

  bool hasMore() {
    return totalCount > currentCount;
  }

  Future clearScroll() async {
    if (scrollId != null) {
      print('clear scroll: $scrollId');
      await client.clearScrollId(scrollId: scrollId!);
    }
  }
}
