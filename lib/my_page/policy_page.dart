import 'package:flutter/material.dart';
import 'package:zzin/common/animations/zopen_container.dart';
import 'package:zzin/common/zconfig.dart';
import 'package:zzin/login/webbrowserpage.dart';

class PolicyPage extends StatefulWidget {
  const PolicyPage({Key? key}) : super(key: key);

  @override
  _PolicyPageState createState() => _PolicyPageState();
}

class _PolicyPageState extends State<PolicyPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: AppBar(
        title: const Text('정책보기', style: TextStyle(fontSize: ZSize.appBarTextSize)),
      ),
      body: SafeArea(
        child: Padding(
          padding: const EdgeInsets.fromLTRB(0, 20, 0, 0),
          child: ListView(
            padding: const EdgeInsets.fromLTRB(20, 0, 20, 0),
            children: [
              sectionItem('이용 약관', const WebBrowserPage(ZConfig.termsAndConditions, '이용 약관')),
              sectionItem('개인정보 처리방침', const WebBrowserPage(ZConfig.privacyPolicy, '개인정보 처리방침')),
            ],
          ),
        ),
      ),
    );
  }

  Widget sectionItem(String title, Widget openPage) {
    return ZOpenContainer(
        closedBuilder: (context, action) => ListTile(
            title: Text(title, style: const TextStyle(fontSize: 22, fontWeight: FontWeight.bold))),
        openBuilder: (context, action) => openPage);
  }
}
