import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:zzin/common/bottomsheet_custom.dart';
import 'package:zzin/common/widget/zbutton_blue.dart';
import 'package:zzin/common/zconfig.dart';
import 'package:zzin/common/zutils.dart';
import 'package:zzin/login/webbrowserpage.dart';
import 'package:zzin/my_page/notice_page.dart';
import 'package:zzin/postbox/postbox.dart';
import 'package:zzin/postbox/postbox_controller.dart';

class PostboxViewer extends StatelessWidget {
  final Postbox postbox;
  final bool isBottomSheet;
  const PostboxViewer({required this.postbox, this.isBottomSheet = false, Key? key})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return isBottomSheet
        ? Column(
            children: [
              Padding(
                padding: const EdgeInsets.only(top: 20.0),
                child: getHeader(),
              ),
              const SizedBox(height: 10),
              const Divider(),
              Expanded(child: getBody()),
              isBottomSheet
                  ? ZButtonYellow(
                      text: '확인',
                      isSelected: true,
                      onPressed: () => Get.back(),
                    )
                  : const SizedBox.shrink(),
            ],
          )
        : SafeArea(
            child: Scaffold(
              appBar: AppBar(
                actions: [
                  if (ZConfig.userAccount.isVerified && postbox.type != PostboxType.UrgentNotice)
                    IconButton(
                        onPressed: () => onRemove(context), icon: const Icon(Icons.delete_forever))
                ],
              ),
              body: getBody(),
            ),
          );
  }

  Widget getBody() => Padding(
        padding: const EdgeInsets.all(0.0),
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              isBottomSheet ? const SizedBox.shrink() : getHeader(),
              isBottomSheet ? const SizedBox.shrink() : const Divider(height: 30),
              isBottomSheet ? const SizedBox(height: 10) : const SizedBox.shrink(),
              Center(
                child: Text(
                  postbox.title,
                  style: const TextStyle(
                      fontSize: 18, fontWeight: FontWeight.bold, color: ZConfig.zzin_black),
                ),
              ),
              // ignore: prefer_const_constructors
              Padding(
                padding: const EdgeInsets.only(bottom: 10),
              ),
              Padding(
                  padding: const EdgeInsets.all(0.0),
                  child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: processContents(postbox))),
            ],
          ),
        ),
      );

  Widget getHeader() => Padding(
        padding: const EdgeInsets.symmetric(horizontal: 10.0, vertical: 0),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text(
              postbox.docId.startsWith('private_') ? '개인우편' : postbox.getEventTypeName(),
              style: TextStyle(
                  fontWeight: FontWeight.bold,
                  color: postbox.docId.startsWith('private_')
                      ? ZConfig.zzin_pointColor
                      : postbox.getEventTypeColor()),
            ),
            Text(
              formatDateTimeBefore(postbox.reportingDate.toDate()),
              style: const TextStyle(fontSize: 12, color: ZConfig.grey_text),
            )
          ],
        ),
      );

  List<Widget> processContents(Postbox postbox) {
    List<Widget> result = <Widget>[];

    List orderList = postbox.contents['order'];
    for (var order in orderList) {
      if (order is String && order.startsWith('text')) {
        result.add(Padding(
          padding: const EdgeInsets.fromLTRB(20, 5, 20, 5),
          child: Text(
            postbox.contents[order],
            style: const TextStyle(
              height: 1.5,
            ),
          ),
        ));
      } else if (order is String && order.startsWith('image')) {
        result.add(Row(
          children: [
            Expanded(
              child: Padding(
                padding: const EdgeInsets.symmetric(vertical: 10.0),
                child: Image.network(
                  postbox.contents[order],
                  fit: BoxFit.fill,
                ),
              ),
            ),
          ],
        ));
      } else if (order is String && order.startsWith('link')) {
        result.add(GestureDetector(
            child: Padding(
              padding: const EdgeInsets.all(20.0),
              child: Text(postbox.contents[order],
                  style: const TextStyle(
                      decoration: TextDecoration.underline, color: Colors.blue, height: 1.5)),
            ),
            onTap: () {
              if ((postbox.contents[order] as String).contains('#공지사항')) {
                Get.to(() => const NoticePage());
              } else if ((postbox.contents[order] as String).isURL) {
                Get.to(() => WebBrowserPage(postbox.contents[order], postbox.contents[order]));
              }
            }));
      }
    }
    return result;
  }

  void onRemove(BuildContext context) async {
    var response = await showModalBottomSheetInt(
        context,
        (_) => BottomSheetCustom(
            contents: Expanded(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  const Text(
                    '장말로 알림을 삭제하시겠습니까?',
                    style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20),
                  ),
                  const SizedBox(height: 24),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      ZButtonYellow(
                        onPressed: () => Get.back(result: DialogResponse.NO.index),
                        text: '취소하기',
                        fontSize: 14,
                        isSelected: true,
                      ),
                      ZButtonYellow(
                        text: '삭제하기',
                        isSelected: true,
                        fontSize: 14,
                        onPressed: () => Get.back(result: DialogResponse.YES.index),
                      ),
                    ],
                  )
                ],
              ),
            ),
            sizeRatio: 0.25));

    if (response == DialogResponse.YES.index) {
      await PostboxController().updatePrivatePostboxItem(postbox, 'isRemoved', true);
      Get.back(result: 'removed');
    }
  }
}
