import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:zzin/common/firestore/query_recent.dart';
import 'package:zzin/product_page/widget/product_list_generator.dart';

class RecentList extends StatefulWidget {
  const RecentList({Key? key}) : super(key: key);

  @override
  _RecentListState createState() => _RecentListState();
}

class _RecentListState extends State<RecentList> {
  final ScrollController _scrollController = ScrollController();
  late final QueryRecent queryRecent;

  @override
  void initState() {
    super.initState();
    queryRecent = Get.put(QueryRecent(), tag: widget.toString());
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: getRecentList(),
      builder: (context, snapshot) {
        if (snapshot.connectionState == ConnectionState.done) {
          return ProductListGenerator(
            products: queryRecent.products,
            onRefresh: onRefresh,
            scrollController: _scrollController,
            onDetailPage: null,
            tagHeroPrefix: 'Recent_',
            noProductMessages: const ['최근에 본 상품이 없습니다.', '상품을 클릭해서 상세 정보를 보세요'],
          );
        } else {
          return const SizedBox.shrink();
        }
      },
    );
  }

  Future<void> getRecentList() async {
    var result = await queryRecent.queryRecent();
    return result;
  }

  Future onRefresh() async {
    await queryRecent.queryRecent();
  }
}
