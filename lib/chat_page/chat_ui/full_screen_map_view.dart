import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:kakaomap_webview/kakaomap_webview.dart';
import 'package:zzin/common/zconfig.dart';

class FullScreenMapView extends StatelessWidget {
  final double lat;
  final double lng;
  const FullScreenMapView({Key? key, required this.lat, required this.lng}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: Stack(children: [
          Center(
            child: KakaoMapView(
              width: Get.width,
              height: Get.height,
              kakaoMapKey: '3188407d880a18cb063d84ecef05766d',
              lat: lat,
              lng: lng,
              showMapTypeControl: true,
              showZoomControl: true,
              customOverlayStyle: '''<style>
              .customoverlay {position:relative;bottom:45px;border-radius:6px;border: 1px solid #ccc;border-bottom:2px solid #ddd;float:left;}
 .customoverlay .title {display:block;text-align:center;background:#fff;padding:5px 10px;font-size:14px;font-weight:bold;}
 .customoverlay:after {content:'';position:absolute;margin-left:-12px;left:50%;bottom:-12px;width:22px;height:12px;background:url('https://t1.daumcdn.net/localimg/localimages/07/mapapidoc/vertex_white.png')}
              </style>''',
              customOverlay: '''
var content = '<div class="customoverlay">' +
    // '  <a href="http://map.kakao.com/?urlX=530768.0&urlY=1101415.0&name=%ED%98%84%EC%9E%AC%EC%9C%84%EC%B9%98" target="_blank">' +
    '    <span class="title">현재위치</span>' +
    '</div>';

var position = new kakao.maps.LatLng($lat, $lng);

var customOverlay = new kakao.maps.CustomOverlay({
    map: map,
    position: position,
    content: content,
    yAnchor: 1
});
              ''',
            ),
          ),
          Positioned(
            left: 5,
            top: 5,
            child: IconButton(
              icon: const CircleAvatar(
                  radius: 15,
                  backgroundColor: Colors.black12,
                  child: Icon(Icons.close, size: 30, color: ZConfig.zzin_black)),
              onPressed: () => Get.back(),
            ),
          ),
        ]),
      ),
    );
  }
}
