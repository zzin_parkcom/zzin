// ignore_for_file: constant_identifier_names

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:zzin/common/zconfig.dart';

enum PostboxType {
  Notice,
  Event,
  UrgentNotice,
}

extension PotTypeKoString on PostboxType {
  String get korText {
    switch (this) {
      case PostboxType.Notice:
        return '공지';
      case PostboxType.Event:
        return '이벤트';
      case PostboxType.UrgentNotice:
        return '긴급공지';
    }
  }
}

class Postbox {
  Timestamp reportingDate;
  Timestamp expirationDate;
  PostboxType type;
  String title;
  DocumentReference reporterRef;
  bool isRemoved = false;
  String docId;
  Map contents;
  bool isRead = false;

  Postbox({
    required this.reportingDate,
    required this.expirationDate,
    required this.type,
    required this.title,
    required this.reporterRef,
    required this.isRemoved,
    required this.docId,
    required this.contents,
    required this.isRead,
  });

  Postbox.fromJson(Map<String, Object?> json)
      : this(
          reportingDate: json['reportingDate'] as Timestamp,
          expirationDate: json['expirationDate'] as Timestamp,
          type: PostboxType.values[(json['type'] as int)],
          title: json['title'] as String,
          reporterRef: json['reporterRef'] as DocumentReference,
          isRemoved: json['isRemoved'] as bool,
          docId: json['docId'] as String,
          contents: json['contents'] as Map,
          isRead: json['isRead'] as bool,
        );

  Map<String, Object?> toJson() {
    return {
      'reportingDate': reportingDate,
      'expirationDate': expirationDate,
      'type': type.index,
      'title': title,
      'reporterRef': reporterRef,
      'isRemoved': isRemoved,
      'docId': docId,
      'contents': contents,
      'isRead': isRead,
    };
  }

  Map<String, Object?> toJsonWithoutPrivate() {
    return {
      'reportingDate': reportingDate,
      'expirationDate': expirationDate,
      'type': type.index,
      'title': title,
      'reporterRef': reporterRef,
      'docId': docId,
      'contents': contents,
    };
  }

  String getEventTypeName() {
    switch (type.index) {
      case 0:
        return '공지 알림';
      case 1:
        return '이벤트 알림';
      case 2:
        return '긴급 공지';
      default:
        return '알림';
    }
  }

  Color getEventTypeColor() {
    switch (type.index) {
      case 0:
        return ZConfig.zzin_black;
      case 1:
        return ZConfig.zzin_black;
      case 2:
        return Colors.red;
      default:
        return ZConfig.zzin_black;
    }
  }
}
