import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:zzin/common/widget/zbutton_blue.dart';
import 'package:zzin/common/zconfig.dart';
import 'package:zzin/search_page/recent_keyword_widget.dart';
import 'package:zzin/search_page/search_option_button.dart';

class SearchNoProduct extends StatelessWidget {
  final void Function() onClear;
  final Future<bool> Function() onSearchOption;
  final void Function(String search) onSearchProduct;
  final TextEditingController controller;
  const SearchNoProduct(
      {required this.onClear,
      required this.onSearchOption,
      required this.controller,
      required this.onSearchProduct,
      Key? key})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 20),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          SearchOptionButton(onSearchOption: onSearchOption),
          Padding(
            padding: const EdgeInsets.fromLTRB(0, 20, 0, 10),
            child: RecentKeywordWidget(controller: controller, onSearchProduct: onSearchProduct),
          ),
          Center(
            child: Container(
              padding: const EdgeInsets.fromLTRB(30, 50, 30, 0),
              // color: Colors.blue,
              child: Wrap(
                alignment: WrapAlignment.center,
                runSpacing: 15,
                children: [
                  Column(
                    children: const [
                      Text(
                        '검색 결과가 없습니다.',
                        maxLines: 2,
                        style: TextStyle(color: ZConfig.grey_text, fontSize: 16),
                      ),
                      Text(
                        '검색어를 다시 입력해 주세요.',
                        maxLines: 2,
                        style: TextStyle(color: ZConfig.grey_text, fontSize: 16),
                      ),
                    ],
                  ),
                  SizedBox(
                      width: Get.width / 2,
                      height: 50,
                      child: ZButtonYellow(
                        text: '다시 입력하기',
                        onPressed: onClear,
                        isSelected: true,
                      )),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
