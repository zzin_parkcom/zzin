import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:zzin/common/zconfig.dart';

class TipBottomSheet extends StatelessWidget {
  final String title;
  final String subTitle;
  final String buttonTitle;
  final int buttonReturn;
  final int itemCount;
  final List<String> itemList;
  final double sizeRatio;
  const TipBottomSheet(
      this.title, this.subTitle, this.buttonTitle, this.buttonReturn, this.itemCount, this.itemList,
      {this.sizeRatio = 0.4, Key? key})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.fromLTRB(25, 25, 25, 0),
      height: MediaQuery.of(context).size.height * sizeRatio,
      child: Column(
        children: [
          topTitleWidget(context),
          tipList(),
        ],
      ),
    );
  }

  Widget topTitleWidget(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.fromLTRB(10, 0, 10, 0),
      child: Align(
          alignment: Alignment.centerLeft,
          child: Column(
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Flexible(
                    child: Text(
                      title,
                      overflow: TextOverflow.ellipsis,
                      style: const TextStyle(fontSize: 26, fontWeight: FontWeight.bold),
                      textAlign: TextAlign.left,
                    ),
                  ),
                  TextButton(
                    onPressed: () => Get.back(result: buttonReturn),
                    child: Text(
                      buttonTitle,
                      style: const TextStyle(
                          fontSize: 18,
                          fontWeight: FontWeight.bold,
                          color: ZConfig.zzin_pointColor),
                    ),
                  ),
                ],
              ),
              const SizedBox(height: 10),
              Text(
                subTitle,
                style: const TextStyle(fontSize: 17),
              ),
            ],
          )),
    );
  }

  Widget tipList() {
    return Expanded(
      child: Padding(
        padding: const EdgeInsets.fromLTRB(10, 0, 10, 0),
        child: ListView.builder(
          itemExtent: 25,
          itemCount: itemCount,
          itemBuilder: (context, index) {
            return ListTile(
              contentPadding: const EdgeInsets.fromLTRB(0, 3, 0, 0),
              title: Text(
                itemList[index],
                style: const TextStyle(fontSize: 17),
              ),
            );
          },
        ),
      ),
    );
  }
}
