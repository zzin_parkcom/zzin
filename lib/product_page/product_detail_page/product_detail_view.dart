import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:get/get.dart';
import 'package:zzin/common/firestore/document_skima/product.dart';
import 'package:zzin/common/firestore/document_skima/user_account.dart';
import 'package:zzin/common/zconfig.dart';
import 'package:zzin/product_page/product_detail_page/detail_page_appbar.dart';
import 'package:zzin/product_page/product_detail_page/detail_page_bottom.dart';
import 'package:zzin/common/firestore/query_user_product.dart';
import 'package:zzin/product_page/product_detail_page/seller_page/grid_seller_view.dart';

class ProductDetailView extends StatefulWidget {
  final String tag;
  final String tagHeroPrefix;
  const ProductDetailView({required this.tag, required this.tagHeroPrefix, Key? key})
      : super(key: key);

  @override
  _ProductDetailViewState createState() => _ProductDetailViewState();
}

class _ProductDetailViewState extends State<ProductDetailView> {
  late Product _product;

  var queryUserProduct = QueryUserProduct();
  final _controllerScroll = ScrollController();
  ZUserAccount? _sellerAccount;
  bool _isMyProduct = true;

  @override
  void dispose() {
    _controllerScroll.dispose();
    super.dispose();
  }

  @override
  void initState() {
    super.initState();
    _product = Get.find(tag: widget.tag);
    SchedulerBinding.instance!.addPostFrameCallback((_) => frameCallback(context));
  }

  void frameCallback(BuildContext context) async {
    // 판매자 유저 정보 획득.
    var snapshot = await _product.sellerId!
        .withConverter<ZUserAccount>(
            fromFirestore: (snapshots, _) => ZUserAccount.fromJson(snapshots.data()!),
            toFirestore: (userAccount, _) => userAccount.toJson())
        .get();

    if (snapshot.data() != null) {
      _sellerAccount = snapshot.data()!;
      await queryUserProduct.queryUserProduct(
          _product.sellerId!, queryUserProduct.queryValue.saleStatus);
      _isMyProduct = _sellerAccount!.docId == ZConfig.userAccount.docId;

      await ZConfig.userAccount.addRecent(_product.docId!);

      setState(() {});
    }
  }

  //////////////////////////////////////////////////////////////////////////////////////////////////
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        resizeToAvoidBottomInset: false,
        body: CustomScrollView(
          controller: _controllerScroll,
          shrinkWrap: true,
          slivers: [
            DetailPageAppBar(
              tag: widget.tag,
              tagHeroPrefix: widget.tagHeroPrefix,
              onRefresh: onRefresh,
            ),
            SliverList(
              delegate: SliverChildListDelegate(
                [
                  const Divider(),
                  Padding(
                    padding: const EdgeInsets.fromLTRB(20, 10, 20, 10),
                    child: Container(
                      constraints: BoxConstraints(
                        minHeight: (Get.height - AppBar().preferredSize.height) / 2,
                        minWidth: double.infinity,
                      ),
                      child: Text(
                        _product.contents,
                        style: const TextStyle(fontSize: 16, height: 1.5),
                      ),
                    ),
                  ),
                  _isMyProduct
                      ? const SizedBox.shrink()
                      // : HorizontalSellerView(queryUserProduct, _sellerAccount!),
                      : GridSellerView(queryUserProduct, _sellerAccount!, widget.tagHeroPrefix),
                ],
              ),
            ),
          ],
        ),
        bottomNavigationBar: DetailPageBottom(seller: _sellerAccount, tag: widget.tag));
  }

  void onRefresh() {
    setState(() {
      // _product;
    });
  }
}
