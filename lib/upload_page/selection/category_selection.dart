import 'package:flutter/material.dart';
import 'package:zzin/common/hive/category_collection.dart';
import 'package:zzin/common/hive/upload_collection.dart';
import 'package:zzin/common/utils/upload_utils.dart';
import 'package:zzin/upload_page/bottomsheet/bottomsheet_upload.dart';
import 'package:zzin/common/zconfig.dart';
import 'package:zzin/common/zutils.dart';
import 'package:zzin/upload_page/customFormfield/custom_formfield.dart';

class CategorySelection extends StatefulWidget {
  final UploadCollection uploadCollection;
  final String defaultName = '카테고리 선택';
  final double fontSize;

  const CategorySelection(this.fontSize, {required this.uploadCollection, Key? key})
      : super(key: key);

  @override
  _CategorySelectionState createState() => _CategorySelectionState();
}

class _CategorySelectionState extends State<CategorySelection> {
  var id = NotSelected;
  var name = Empty;

  @override
  Widget build(BuildContext context) {
    id = widget.uploadCollection.categoryId;
    name = widget.uploadCollection.categoryName;

    return CustomFormField(
      initialValue: id,
      onSaved: onSaved,
      validator: validator,
      autovalidateMode: AutovalidateMode.disabled,
      builder: listTileBuilder,
    );
  }

  Widget listTileBuilder(FormFieldState<int> state) {
    return Column(
      children: [
        ListTile(
          title: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              categoryTitle(),
              state.hasError
                  ? Text(
                      state.errorText!,
                      style: errorStyle(widget.fontSize),
                    )
                  : const SizedBox.shrink(),
            ],
          ),
          trailing: const Icon(Icons.expand_more),
          onTap: () async {
            var idReturn =
                await showModalBottomSheetInt(state.context, _categoryBottomSheetContent);
            if (idReturn != NotSelected) {
              id = idReturn;
              name = CategoryCollection.getNameKrEn(id);
              widget.uploadCollection.categoryId = id;
              widget.uploadCollection.categoryName = CategoryCollection.getNameKr(id);
              await widget.uploadCollection.save();
              state.didChange(id);
            }
          },
        ),
      ],
    );
  }

  String? validator(int? value) {
    return id == NotSelected ? '카테고리를 선택해 주세요' : null;
  }

  void onSaved(int? value) {}

  Widget categoryTitle() {
    return id == NotSelected
        ? Text(
            name,
            style: normalStyle(id == NotSelected, widget.fontSize),
          )
        : Row(
            children: [
              Text(CategoryCollection.getNameKr(id),
                  style: normalStyle(id == NotSelected, widget.fontSize)),
              const SizedBox(width: 5),
              Flexible(
                child: Text(
                  CategoryCollection.getNameEn(id),
                  overflow: TextOverflow.ellipsis,
                  style: greyEngStyle(widget.fontSize - 4.0),
                ),
              )
            ],
          );
  }

  Widget _categoryBottomSheetContent(BuildContext context) {
    return BottomSheetUpload(
      '카테고리',
      '닫기',
      NotSelected,
      CategoryCollection.getCollectionSize(),
      CategoryCollection.to.values.map((e) => e.nameKr + '|' + e.nameEn).toList(),
      false,
      height: 420,
    );
  }
}
