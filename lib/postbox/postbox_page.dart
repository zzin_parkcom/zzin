import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:transparent_image/transparent_image.dart';
import 'package:zzin/common/zcache_manager.dart';
import 'package:zzin/common/zconfig.dart';
import 'package:zzin/common/zutils.dart';
import 'package:zzin/postbox/postbox.dart';
import 'package:zzin/postbox/postbox_controller.dart';
import 'package:zzin/postbox/postbox_setting_page.dart';
import 'package:zzin/postbox/postbox_viewr.dart';
import 'package:zzin/postbox/query_postbox.dart';

class PostboxPage extends StatefulWidget {
  const PostboxPage({Key? key}) : super(key: key);

  @override
  _PostboxPageState createState() => _PostboxPageState();
}

class _PostboxPageState extends State<PostboxPage> {
  final ScrollController scrollController = ScrollController();

  @override
  void initState() {
    super.initState();
    scrollController.addListener(_scrollListener);
  }

  @override
  void dispose() {
    scrollController.removeListener(_scrollListener);
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        appBar: AppBar(),
        body: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 10),
          child: Column(
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: const [
                  Padding(
                    padding: EdgeInsets.fromLTRB(8.0, 10, 10, 0),
                    child: Text(
                      '알림함',
                      style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
                    ),
                  ),
                ],
              ),
              FutureBuilder(
                future: getPostboxList(),
                builder: (context, snapshot) {
                  if (snapshot.connectionState == ConnectionState.done) {
                    return Expanded(
                      child: QueryPostbox.to.postboxes.isEmpty
                          ? const NoPostbox()
                          : RefreshIndicator(
                              onRefresh: onRefresh,
                              child: ListView.separated(
                                physics: const BouncingScrollPhysics(
                                    parent: AlwaysScrollableScrollPhysics()),
                                separatorBuilder: (context, index) => const Divider(),
                                controller: scrollController,
                                itemCount: QueryPostbox.to.postboxes.length,
                                itemBuilder: (context, index) {
                                  var postbox = QueryPostbox.to.postboxes[index].data();
                                  if (postbox == null) {
                                    throw Exception('notice is NULL!!!');
                                  }
                                  return ListTile(
                                    // dense: true,
                                    // visualDensity: VisualDensity.compact,
                                    minLeadingWidth: 0,
                                    leading: SizedBox(
                                      width: 60,
                                      height: 60,
                                      child: CachedNetworkImage(
                                        fadeInDuration: const Duration(milliseconds: 0),
                                        fadeOutDuration: const Duration(milliseconds: 0),
                                        placeholderFadeInDuration: const Duration(milliseconds: 0),
                                        placeholder: (_, __) => Image.memory(kTransparentImage),
                                        imageUrl: getFirstImageUrl(postbox.contents),
                                        fit: BoxFit.cover,
                                        errorWidget: (context, url, error) =>
                                            const Icon(Icons.error),
                                        cacheManager: ZCacheManager.instance,
                                      ),
                                    ),
                                    isThreeLine: true,
                                    title: Row(
                                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                      children: [
                                        Text(
                                          postbox.docId.startsWith('private_')
                                              ? '개인우편'
                                              : postbox.getEventTypeName(),
                                          style: TextStyle(
                                              fontWeight: FontWeight.bold,
                                              color: postbox.docId.startsWith('private_')
                                                  ? ZConfig.zzin_pointColor
                                                  : postbox.getEventTypeColor()),
                                        ),
                                        Text(
                                          '${ZConfig.userAccount.isVerified && postbox.isRead == false ? '읽지않음     ' : ''}${formatDateTimeBefore(postbox.reportingDate.toDate())}',
                                          style: const TextStyle(
                                              fontSize: 12, color: ZConfig.grey_text),
                                        )
                                      ],
                                    ),
                                    subtitle: Column(
                                      crossAxisAlignment: CrossAxisAlignment.start,
                                      children: [
                                        Text(
                                          postbox.title,
                                          overflow: TextOverflow.ellipsis,
                                          style:
                                              const TextStyle(color: ZConfig.zzin_black, height: 2),
                                        ),
                                        Text(
                                          getFirstText(postbox.contents),
                                          overflow: TextOverflow.ellipsis,
                                        )
                                      ],
                                    ),
                                    onTap: () => onPostbox(postbox),
                                  );
                                },
                              )),
                    );
                  } else {
                    return const Expanded(
                        child: Center(
                            child: Text(
                      '알림함 확인중...',
                      style: TextStyle(fontWeight: FontWeight.bold),
                    )));
                  }
                },
              ),
            ],
          ),
        ),
      ),
    );
  }

  String getFirstImageUrl(Map contents) {
    var iterator = contents.keys.where((element) => (element as String).startsWith('image'));
    if (iterator.isNotEmpty) {
      return contents[iterator.first];
    } else {
      return ZConfig.defaultImageStorageURL2;
    }
  }

  String getFirstText(Map contents) {
    var iterator = contents.keys.where((element) => (element as String).startsWith('text'));
    if (iterator.isNotEmpty) {
      return contents[iterator.first];
    } else {
      return '';
    }
  }

  Future<void> getPostboxList() async {
    // QueryPostbox publicQueryPostbox = QueryPostbox();
    // await Get.put(publicQueryPostbox, tag: 'public_postbox').queryPostbox(isPublic: true);
    // for (var index = 0; index < publicQueryPostbox.postboxes.length; index++) {
    //   await PostboxController().addPostBox(publicQueryPostbox.postboxes[index].data()!, false);
    // }

    if (ZConfig.userAccount.isVerified) {
      var result = await Get.put(QueryPostbox()).queryPostbox(isPublic: false);
      return result;
    } else {
      var result = await Get.put(QueryPostbox()).queryPostbox(isPublic: true);
      return result;
    }
  }

  Future onRefresh() async {
    await QueryPostbox.to.queryPostbox(isPublic: false);
  }

  void _scrollListener() async {
    var maxScroll = scrollController.position.maxScrollExtent;
    var currentScroll = scrollController.position.pixels;
    var delta = maxScroll * ZConfig.scrollRatioQueryMore;

    if (maxScroll - currentScroll <= delta) {
      await QueryPostbox.to.queryPostbox(isPublic: false, byScroll: true);
    }
  }

  void onSetting() async {
    await Get.to(() => const PostboxSettingPage(),
        transition: Transition.upToDown, duration: ZConfig.heroAnimationDuration);
  }

  void onPostbox(Postbox postbox) async {
    if (ZConfig.userAccount.isVerified) {
      await PostboxController().updatePrivatePostboxItem(postbox, 'isRead', true);
    }

    await Get.to(() => PostboxViewer(postbox: postbox),
        transition: Transition.upToDown, duration: ZConfig.heroAnimationDuration);

    setState(() {});
  }
}

class NoPostbox extends StatelessWidget {
  const NoPostbox({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return const Center(
      child: Text('알림이 없습니다.',
          style: TextStyle(
            fontSize: 20,
            color: ZConfig.grey_text,
          )),
    );
  }
}
