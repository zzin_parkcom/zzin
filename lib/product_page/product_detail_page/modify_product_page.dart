import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:zzin/common/firestore/document_skima/product.dart';
import 'package:zzin/common/hive/address_collection.dart';
import 'package:zzin/common/hive/brand_collection.dart';
import 'package:zzin/common/hive/category_collection.dart';

import 'package:zzin/common/hive/upload_collection.dart';
import 'package:zzin/upload_page/upload_view.dart';

class ModifyProductPage extends StatefulWidget {
  final String tag;
  const ModifyProductPage({required this.tag, Key? key}) : super(key: key);

  @override
  _ModifyProductPageState createState() => _ModifyProductPageState();
}

class _ModifyProductPageState extends State<ModifyProductPage> {
  late final Product product;

  @override
  void initState() {
    super.initState();
    product = Get.find(tag: widget.tag);
    UploadCollection.modify.initialize();
    UploadCollection.modify.brandId = product.brand;
    UploadCollection.modify.brandName = BrandCollection.getNameKr(product.brand);
    UploadCollection.modify.categoryId = product.category;
    UploadCollection.modify.categoryName = CategoryCollection.getNameKrEn(product.category);
    UploadCollection.modify.description = product.contents;
    UploadCollection.modify.price = product.price;
    UploadCollection.modify.saleAreaId = product.regionCode;
    UploadCollection.modify.saleAreaName = AddressCollection.getRegionName(product.regionCode);
    UploadCollection.modify.status = product.status;
    UploadCollection.modify.title = product.title;
    UploadCollection.modify.mainImageIndex = 0;
    UploadCollection.modify.images.addAll(product.images);
    UploadCollection.modify.imagesCache.addAll(product.imagesCache);
    UploadCollection.modify.imagesId.addAll(product.imagesId);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: true,
      body: UploadView(
        uploadCollection: UploadCollection.modify,
        isModify: true,
        tag: widget.tag,
      ),
    );
  }
}
