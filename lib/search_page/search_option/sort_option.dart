import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:zzin/common/bottomsheet_custom.dart';
import 'package:zzin/common/hive/search_collection.dart';
import 'package:zzin/common/utils/upload_utils.dart';
import 'package:zzin/common/zconfig.dart';
import 'package:zzin/common/zutils.dart';
import 'package:zzin/search_page/search_option/sort_option_bottom_sheet.dart';
import 'package:zzin/upload_page/customFormfield/custom_formfield.dart';

class SortOption extends StatefulWidget {
  final String defaultName = '정렬옵션 선택';
  final double fontSize;

  // ignore: prefer_const_constructors_in_immutables
  SortOption(this.fontSize, {Key? key}) : super(key: key);

  late final _SortOptionState _state;
  @override
  // ignore: no_logic_in_create_state
  _SortOptionState createState() {
    _state = _SortOptionState();
    return _state;
  }

  int getSort() => _state.sort;
  void setSort(int sort) => _state.sort = sort;

  bool isEmpty() => _state.sort == NotSelected;

  void clearState() {
    _state.sort = 0;
    SearchCollection.instance.sort = 0;
    SearchCollection.instance.save();
    _state.clearState();
  }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
class _SortOptionState extends State<SortOption> {
  int sort = NotSelected;

  void clearState() {
    setState(() {});
  }

  @override
  void initState() {
    super.initState();
    sort = SearchCollection.instance.sort;
  }

  @override
  Widget build(BuildContext context) {
    return CustomFormField(
      initialValue: sort,
      validator: validator,
      onSaved: onSaved,
      autovalidateMode: AutovalidateMode.onUserInteraction,
      builder: listTileBuilder,
    );
  }

  Widget listTileBuilder(FormFieldState<int> state) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        ListTile(
            title: Row(
              children: [
                Text(getSort(), style: normalStyle(sort == Cancel, widget.fontSize)),
                state.hasError
                    ? Text(
                        state.errorText!,
                        style: errorStyle(widget.fontSize),
                      )
                    : const SizedBox.shrink(),
              ],
            ),
            trailing: const Icon(Icons.expand_more),
            onTap: onSortSelected),
      ],
    );
  }

  String? validator(int? value) {
    // return id.value == Cancel ? '거래지역을 선택해 주세요' : null;
  }

  void onSaved(int? value) {}

  String getSort() {
    switch (sort) {
      case 0:
      case 1:
      case 2:
      case 3:
      case 4:
        return ZConfig.sortNameList[sort];
      default:
        return widget.defaultName;
    }
  }

////////////////////////////////////////////////////////////////////////////////////////////////////
  void onSortSelected() async {
    await showModalBottomSheetInt(
        context,
        (_) => BottomSheetCustom(
            contents: const SortOptionBottomSheet(),
            sizeRatio: Get.mediaQuery.textScaleFactor >= 1.5 ? 0.46 : 0.40));
    sort = SearchCollection.instance.sort;
    setState(() {});
  }
}
