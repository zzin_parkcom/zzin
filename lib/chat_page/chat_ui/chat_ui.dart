import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:keyboard_dismisser/keyboard_dismisser.dart';
import 'package:zzin/chat_page/chat_ui/chat_ui_appbar.dart';
import 'package:zzin/chat_page/chat_ui/chat_ui_message_list.dart';
import 'package:zzin/chat_page/chat_ui/chat_ui_product_info.dart';
import 'package:zzin/chat_page/chat_ui/chat_ui_write_mesage.dart';
import 'package:zzin/common/firestore/document_skima/chatroom.dart';
import 'package:zzin/common/firestore/document_skima/product.dart';
import 'package:zzin/common/firestore/document_skima/user_account.dart';
import 'package:zzin/common/zconfig.dart';

class ChatUI extends StatefulWidget {
  final Product product;
  final ZUserAccount sender;
  final ZUserAccount receiver;
  const ChatUI({required this.sender, required this.receiver, required this.product, Key? key})
      : super(key: key);
  @override
  _ChatUIState createState() => _ChatUIState();
}

class _ChatUIState extends State<ChatUI> {
  final ScrollController _scrollController = ScrollController();

  @override
  void initState() {
    super.initState();

    SchedulerBinding.instance!.addPostFrameCallback((_) => frameCallback(context));
  }

  @override
  void dispose() {
    super.dispose();
  }

  void frameCallback(BuildContext context) async {
    if (widget.product.sellerId == ZConfig.userAccount.docId) {
      // 판매자일 때
      await ChatRoom(
              buyerRef: widget.receiver.docId!,
              sellerRef: widget.product.sellerId!,
              productRef: widget.product.docId!,
              receiverRef: widget.receiver.docId!)
          .enterRoom();
    } else {
      // 구매자일때
      await ChatRoom(
              buyerRef: widget.sender.docId!,
              sellerRef: widget.product.sellerId!,
              productRef: widget.product.docId!,
              receiverRef: widget.receiver.docId!)
          .enterRoom();
    }
  }

  @override
  Widget build(BuildContext context) {
    return KeyboardDismisser(
      child: Scaffold(
        resizeToAvoidBottomInset: true,
        appBar: ChatUIAppBar(
          widget.receiver,
          widget.product,
        ),
        body: Scaffold(
          resizeToAvoidBottomInset: true,
          body: Stack(
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.fromLTRB(20, 0, 20, 0),
                child: ChatUIMessageList(
                    sender: widget.sender,
                    receiver: widget.receiver,
                    product: widget.product,
                    scrollController: _scrollController),
              ),
              ChatUIProductInfo(product: widget.product, tagHeroPrefix: 'ChatUI_'),
            ],
          ),
          bottomNavigationBar: ChatUIWriteMesage(
              receiver: widget.receiver,
              product: widget.product,
              scrollController: _scrollController),
        ),
      ),
    );
  }
}

///////////////////////////////////////////////////////////////////
