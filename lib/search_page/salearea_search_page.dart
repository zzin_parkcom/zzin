import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:zzin/common/elastic_search/es_product.dart';
import 'package:zzin/common/firestore/document_skima/product.dart';
import 'package:zzin/common/zconfig.dart';
import 'package:zzin/product_page/product_detail_page/product_detail_view.dart';
import 'package:zzin/product_page/widget/product_list_generator.dart';

class SaleAreaSearchPage extends StatefulWidget {
  final String tagHeroPrefix;
  const SaleAreaSearchPage({Key? key, required this.tagHeroPrefix}) : super(key: key);

  @override
  _SaleAreaSearchPageState createState() => _SaleAreaSearchPageState();
}

class _SaleAreaSearchPageState extends State<SaleAreaSearchPage> {
  final ScrollController _scrollController = ScrollController();
  final ESProduct esProduct = ESProduct();
  String areaName = ZConfig.userAccount.regionName!.split(' ').last;

  @override
  void initState() {
    super.initState();
    _scrollController.addListener(_scrollListener);
  }

  @override
  void dispose() {
    _scrollController.removeListener(_scrollListener);
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Center(
            child: Text('$areaName 주변 상품', style: const TextStyle(fontSize: ZSize.appBarTextSize))),
        elevation: 1,
        actions: [SizedBox(width: AppBar().preferredSize.height)],
      ),
      body: FutureBuilder(
        future: getProduct(),
        builder: (context, snapshot) {
          if (snapshot.connectionState == ConnectionState.done) {
            return ProductListGenerator(
              products: const [],
              esProducts: esProduct.products,
              onRefresh: onRefresh,
              scrollController: _scrollController,
              onDetailPage: onListItemTap,
              tagHeroPrefix: 'Salearea_Search_',
              noProductMessages: const ['상품이 없습니다.'],
            );
          } else {
            return noProduct();
          }
        },
      ),
    );
  }

  Future<void> getProduct() async {
    GeoPoint position;
    position = ZConfig.userAccount.regionGeoPoint!;
    areaName = ZConfig.userAccount.regionName!.split(' ').last;

    await esProduct.searchByDistance(
        lat: position.latitude,
        lon: position.longitude,
        size: 10,
        radius: 500,
        isAscending: true,
        isClear: true);
  }

  Future onRefresh() async {
    GeoPoint position;
    position = ZConfig.userAccount.regionGeoPoint!;
    areaName = ZConfig.userAccount.regionName!.split(' ').last;

    await esProduct.searchByDistance(
        lat: position.latitude,
        lon: position.longitude,
        size: 10,
        radius: 500,
        isAscending: true,
        isClear: true);
  }

  bool isScrolling = false;
  void _scrollListener() async {
    var maxScroll = _scrollController.position.maxScrollExtent;
    var currentScroll = _scrollController.position.pixels;
    var delta = maxScroll * ZConfig.scrollRatioQueryMore;

    if (maxScroll - currentScroll <= delta && isScrolling == false) {
      isScrolling = true;
      await esProduct.searchNext();
      isScrolling = false;
    }
  }

  void onListItemTap(Product product) {
    var tag = DateTime.now().toString();
    var savedProduct = Get.put(Product.fromJson(product.toJson()), tag: tag);
    Get.to(() => ProductDetailView(tag: tag, tagHeroPrefix: widget.tagHeroPrefix),
        transition: Transition.fadeIn, duration: ZConfig.heroAnimationDuration);
    if (!savedProduct.isEqualValue(product)) {
      setState(() {});
    }

    Get.delete(tag: tag);
  }

  Widget noProduct() {
    return const Padding(
      padding: EdgeInsets.all(20.0),
      child: Text(
        '주변 상품이 없습니다',
        style: TextStyle(fontSize: 20, color: ZConfig.grey_text),
      ),
    );
  }
}
