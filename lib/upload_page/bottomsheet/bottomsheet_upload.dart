import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:zzin/common/widget/zbutton_blue.dart';
import 'package:zzin/common/zconfig.dart';

class BottomSheetUpload extends StatefulWidget {
  final String sheetTitle;
  final String buttonTitle;
  final int buttonReturn;
  final int itemCount;
  final List<String> itemList;
  final bool hasSearchBar;
  final double height;
  const BottomSheetUpload(this.sheetTitle, this.buttonTitle, this.buttonReturn, this.itemCount,
      this.itemList, this.hasSearchBar,
      {this.height = 0.4, Key? key})
      : super(key: key);
  @override
  _BottomSheetUploadState createState() => _BottomSheetUploadState();
}

class _BottomSheetUploadState extends State<BottomSheetUpload> {
  List<String> searchList = [];

  final TextEditingController _controller = TextEditingController();
  final FocusNode _focusNode = FocusNode();
  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  void initState() {
    super.initState();
    searchList.addAll(widget.itemList);
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.fromLTRB(30, 30, 30, 0),
      height: widget.height,
      child: Column(
        children: [
          topTitleWidget(),
          searchBarWidget(),
          brandListWidget(),
        ],
      ),
    );
  }

  Widget brandTileText(int index) {
    return Row(
      children: [
        Text(widget.itemList[index].split('|').first,
            overflow: TextOverflow.ellipsis,
            style: const TextStyle(fontSize: ZSize.bottomSheetFontSize)),
        const SizedBox(width: 15),
        Expanded(
          child: Text(
            widget.itemList[index].split('|').last,
            overflow: TextOverflow.ellipsis,
            style: const TextStyle(fontSize: ZSize.bottomSheetFontSize - 3, color: Colors.grey),
          ),
        )
      ],
    );
  }

  void resetSearchListAndRefresh() {
    searchList.clear();
    searchList.addAll(widget.itemList);
    _controller.text = '';
    setState(() {});
  }

  Widget topTitleWidget() {
    return Align(
        alignment: Alignment.centerLeft,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text(
              widget.sheetTitle,
              style: const TextStyle(fontSize: ZSize.bottomSheetTitleFontSize),
              textAlign: TextAlign.left,
            ),
            TextButton(
              onPressed: () => Get.back(result: widget.buttonReturn),
              child: Text(
                widget.buttonTitle,
                style: const TextStyle(
                    fontSize: ZSize.bottomSheetFontSize, color: ZConfig.zzin_pointColor),
              ),
            ),
          ],
        ));
  }

  Widget searchBarWidget() {
    return widget.hasSearchBar
        ? Column(
            children: [
              Padding(
                padding: const EdgeInsets.fromLTRB(0, 18, 0, 18),
                child: TextField(
                  focusNode: _focusNode,
                  style: const TextStyle(fontSize: ZSize.searchTextSize),
                  controller: _controller,
                  decoration: InputDecoration(
                      suffixIcon: _controller.text.isNotEmpty
                          ? GestureDetector(
                              onTap: resetSearchListAndRefresh,
                              child: const Icon(
                                Icons.cancel,
                                color: ZConfig.grey_text,
                              ),
                            )
                          : null,
                      prefixIcon: const Icon(Icons.search, color: ZConfig.grey_text),
                      contentPadding: const EdgeInsets.symmetric(vertical: 5),
                      enabledBorder: const OutlineInputBorder(
                          borderSide: BorderSide(color: ZConfig.grey_border, width: 2),
                          borderRadius: BorderRadius.all(Radius.circular(30.0))),
                      hintStyle: const TextStyle(
                          fontSize: ZSize.searchTextSize,
                          fontWeight: FontWeight.normal,
                          color: ZConfig.grey_text,
                          decorationColor: ZConfig.grey_text,
                          backgroundColor: ZConfig.grey_border),
                      filled: true,
                      fillColor: ZConfig.grey_border,
                      labelStyle: const TextStyle(
                          color: ZConfig.zzin_black,
                          decorationColor: ZConfig.grey_text,
                          backgroundColor: ZConfig.grey_text),
                      border: const OutlineInputBorder(
                        borderRadius: BorderRadius.all(
                          Radius.circular(30.0),
                        ),
                      ),
                      hintText: '브랜드 검색'),
                  onChanged: (search) {
                    // if (search.isEmpty) {
                    //   resetSearchListAndRefresh();
                    //   return;
                    // }
                    searchList.clear();
                    searchList.addAll(widget.itemList.where((value) {
                      return value.toLowerCase().contains(search.toLowerCase());
                    }));
                    setState(() {
                      // _controller.text = search;
                    });
                  },
                ),
              ),
            ],
          )
        : const SizedBox.shrink();
  }

  Widget brandListWidget() {
    return searchList.isNotEmpty || _controller.text.isEmpty
        ? Expanded(
            child: Padding(
              padding: const EdgeInsets.fromLTRB(10, 0, 10, 0),
              child: ListView.separated(
                separatorBuilder: (context, index) => const Divider(),
                itemCount: searchList.length,
                itemBuilder: (context, index) {
                  return ListTile(
                    contentPadding: const EdgeInsets.fromLTRB(0, 3, 0, 3),
                    title: brandTileText(widget.itemList.indexOf(searchList[index])),
                    onTap: () {
                      _focusNode.unfocus();
                      var indexSelected = widget.itemList.indexOf(searchList[index]);
                      Get.back(result: indexSelected, closeOverlays: true);
                    },
                  );
                },
              ),
            ),
          )
        : noSearchResultWidget();
  }

  Widget noSearchResultWidget() {
    return Container(
      padding: const EdgeInsets.fromLTRB(30, 50, 30, 0),
      // color: Colors.blue,
      child: Wrap(
        alignment: WrapAlignment.center,
        runSpacing: 15,
        children: [
          const Text(
            '등록되지 않은 브랜드입니다.',
            style: TextStyle(color: ZConfig.grey_text, fontSize: 20),
          ),
          Column(
            children: const [
              Text(
                '검색하신 브랜드는 내부적으로 확인 후',
                maxLines: 2,
                style: TextStyle(color: ZConfig.grey_text, fontSize: 16),
              ),
              Text(
                '브랜드 리스트에 업데이트 하도록 하겠습니다.',
                maxLines: 2,
                style: TextStyle(color: ZConfig.grey_text, fontSize: 16),
              ),
            ],
          ),
          SizedBox(
              width: 220,
              height: 50,
              child: ZButtonYellow(
                  text: '브랜드 다시 검색',
                  fontSize: 20,
                  isSelected: true,
                  onPressed: resetSearchListAndRefresh)),
        ],
      ),
    );
  }
}
