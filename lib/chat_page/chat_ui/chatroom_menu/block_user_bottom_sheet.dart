import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:zzin/common/widget/zbutton_blue.dart';

class BlockUserBottomSheet extends StatelessWidget {
  final String blockUser;
  final bool isBlockedUser;
  const BlockUserBottomSheet({Key? key, required this.blockUser, required this.isBlockedUser})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Text(
            isBlockedUser ? '$blockUser님을 차단해제 하시겠습니까?' : '$blockUser님을 차단하시겠습니까?',
            style: const TextStyle(fontWeight: FontWeight.bold, fontSize: 20),
          ),
          const SizedBox(height: 24),
          const Padding(
            padding: EdgeInsets.symmetric(horizontal: 60),
            child: Text('상대방을 차단하는 경우, 상대방이 작성한 게시물 및 상대방과의 채팅이 불가합니다.',
                style: TextStyle(fontSize: 14), textAlign: TextAlign.center),
          ),
          const SizedBox(height: 24),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              ZButtonYellow(
                onPressed: () => Get.back(result: 0),
                text: '취소하기',
                fontSize: 14,
                isSelected: true,
              ),
              ZButtonYellow(
                text: isBlockedUser ? '차단해제' : '차단하기',
                isSelected: true,
                fontSize: 14,
                onPressed: () => Get.back(result: 1),
              ),
            ],
          )
        ],
      ),
    );
  }
}
