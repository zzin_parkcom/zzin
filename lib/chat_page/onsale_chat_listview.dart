import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:zzin/chat_page/chatroom_list_generator.dart';
import 'package:zzin/common/firestore/document_skima/product.dart';
import 'package:zzin/common/firestore/query_chatroom.dart';
import 'package:zzin/common/zconfig.dart';

class OnsaleChatListView extends StatefulWidget {
  const OnsaleChatListView({this.product, Key? key}) : super(key: key);
  final Product? product;
  @override
  _OnsaleChatListViewState createState() => _OnsaleChatListViewState();
}

class _OnsaleChatListViewState extends State<OnsaleChatListView> {
  final ScrollController _scrollController = ScrollController();
  late final QueryChatRoom queryChatRoom;

  @override
  void initState() {
    super.initState();
    queryChatRoom = Get.put(
        QueryChatRoom(product: widget.product != null ? widget.product!.docId : null),
        tag: widget.toString());
    _scrollController.addListener(_scrollListener);
  }

  @override
  void dispose() {
    _scrollController.removeListener(_scrollListener);
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
        future: getChatRoom(userRef: ZConfig.userAccount.docId, isSeller: true),
        builder: (context, snapshot) {
          if (snapshot.connectionState == ConnectionState.done) {
            return ChatRoomListGenerator(
                onRefresh: () async {
                  await onRefresh(userRef: ZConfig.userAccount.docId, isSeller: true);
                },
                chatrooms: queryChatRoom.chatrooms,
                scrollController: _scrollController);
          } else {
            return const SizedBox.shrink();
          }
        });
  }

  Future<void> getChatRoom({required userRef, required isSeller}) async {
    print('getChatRoom Future');
    await queryChatRoom.queryChatRoom(userRef: userRef, isSeller: isSeller, byScroll: false);
  }

  Future onRefresh({required userRef, required isSeller}) async {
    print('onRefresh');
    await queryChatRoom.queryChatRoom(userRef: userRef, isSeller: isSeller, byScroll: false);
  }

  void _scrollListener() async {
    var maxScroll = _scrollController.position.maxScrollExtent;
    var currentScroll = _scrollController.position.pixels;
    var delta = maxScroll * ZConfig.scrollRatioQueryMore;

    if (maxScroll - currentScroll <= delta && queryChatRoom.canQuery()) {
      print('scroll queryproduct');

      await queryChatRoom.queryChatRoom(
          userRef: ZConfig.userAccount.docId, isSeller: true, byScroll: true);
    }
  }
}
