import 'package:flutter/material.dart';
import 'package:zzin/common/hive/category_collection.dart';
import 'package:zzin/common/hive/search_collection.dart';
import 'package:zzin/common/utils/upload_utils.dart';
import 'package:zzin/upload_page/bottomsheet/bottomsheet_upload.dart';
import 'package:zzin/common/zconfig.dart';
import 'package:zzin/common/zutils.dart';
import 'package:zzin/upload_page/customFormfield/custom_formfield.dart';
import 'package:get/get.dart';

class CategoryOption extends StatefulWidget {
  final String defaultName = '카테고리 선택';

  final double fontSize;

  // ignore: prefer_const_constructors_in_immutables
  CategoryOption(this.fontSize, {Key? key}) : super(key: key);

  late final _CategoryOptionState _state;
  @override
  // ignore: no_logic_in_create_state
  _CategoryOptionState createState() {
    _state = _CategoryOptionState();
    return _state;
  }

  int getCategoryId() => _state.id.value;
  void setCategoryId(int id) {
    _state.id.value = id;
    _state.name.value = CategoryCollection.getNameKrEn(id);
  }

  bool isEmpty() => _state.id.value == NotSelected;

  void clearState() {
    SearchCollection.instance.categoryId = NotSelected;
    SearchCollection.instance.categoryName = defaultName;
    SearchCollection.instance.save();
    _state.id.value = NotSelected;
    _state.name.value = defaultName;
    _state.clearState();
  }
}

class _CategoryOptionState extends State<CategoryOption> {
  var id = SearchCollection.instance.categoryId.obs;
  var name = SearchCollection.instance.categoryName.obs;

  @override
  Widget build(BuildContext context) {
    return CustomFormField(
      initialValue: id.value,
      onSaved: onSaved,
      validator: validator,
      autovalidateMode: AutovalidateMode.onUserInteraction,
      builder: listTileBuilder,
    );
  }

  void clearState() {
    setState(() {});
  }

  Widget listTileBuilder(FormFieldState<int> state) {
    return Column(
      children: [
        ListTile(
          title: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              categoryTitle(),
              state.hasError
                  ? Text(
                      state.errorText!,
                      style: errorStyle(widget.fontSize),
                    )
                  : const SizedBox.shrink(),
            ],
          ),
          trailing: const Icon(Icons.expand_more),
          onTap: () async {
            var idReturn =
                await showModalBottomSheetInt(state.context, _categoryBottomSheetContent);
            if (idReturn != NotSelected && idReturn != Init) {
              id.value = idReturn;
              name.value = CategoryCollection.getNameKrEn(id.value);
              state.didChange(id.value);
            } else if (idReturn == Init) {
              id.value = NotSelected;
              name.value = widget.defaultName;
              state.didChange(id.value);
            }
            /* else {
              id.value = NotSelected;
              name.value = widget.defaultName;
              state.didChange(id.value);
            } */
          },
        ),
      ],
    );
  }

  String? validator(int? value) {
    // return id.value == NotSelected ? '카테고리를 선택해 주세요' : null;
    return null;
  }

  void onSaved(int? value) {}

  Widget categoryTitle() {
    return id.value == NotSelected
        ? Text(
            widget.defaultName,
            style: normalStyle(id.value == NotSelected, widget.fontSize),
          )
        : Row(
            children: [
              Text(CategoryCollection.getNameKr(id.value),
                  style: normalStyle(id.value == NotSelected, widget.fontSize)),
              const SizedBox(width: 5),
              Expanded(
                child: Text(
                  CategoryCollection.getNameEn(id.value),
                  overflow: TextOverflow.ellipsis,
                  style: greyEngStyle(widget.fontSize - 4.0),
                ),
              )
            ],
          );
  }

  Widget _categoryBottomSheetContent(BuildContext context) {
    return BottomSheetUpload(
      '카테고리',
      '초기화',
      Init,
      CategoryCollection.getCollectionSize(),
      CategoryCollection.to.values.map((e) => e.nameKr + '|' + e.nameEn).toList(),
      false,
      height: 420,
    );
  }
}
