import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:get/get.dart';
import 'package:zzin/common/firestore/document_skima/opentalk_data.dart';
import 'package:zzin/common/zconfig.dart';

class QueryOpentalkList extends GetxController {
  List<DocumentSnapshot<OpentalkData>> opentalks = <DocumentSnapshot<OpentalkData>>[].obs;
  DocumentSnapshot<OpentalkData>? lastDocument;
  bool isLoading = false;
  bool hasMore = true;
  int documentLimit = ZConfig.maxDocumentCount;

  bool canQuery() {
    return hasMore && !isLoading;
  }

  void clearProduct() {
    print('clearProduct');
    opentalks.clear();
    lastDocument = null;
    isLoading = false;
    hasMore = true;
  }

  Future<void> addOpentalk(String name) {
    return FirebaseFirestore.instance
        .collection(ZConfig.opentalkListCollection)
        .withConverter<OpentalkData>(
            fromFirestore: (snapshots, _) => OpentalkData.fromJson(snapshots.data()!),
            toFirestore: (opentalk, _) => opentalk.toJson())
        .add(OpentalkData(name: name, creationTime: Timestamp.now(), participants: []));
  }

  Future<void> queryOpentalk({required bool byScroll}) async {
    if (!byScroll) {
      clearProduct();
    }
    if (!hasMore) {
      print('더이상 오픈톡이 없습니다.');
      return;
    }
    if (isLoading) {
      print('쿼리중... ');
      return;
    }
    isLoading = true;

    Query query = FirebaseFirestore.instance
        .collectionGroup(ZConfig.opentalkListCollection)
        .withConverter<OpentalkData>(
            fromFirestore: (snapshots, _) => OpentalkData.fromJson(snapshots.data()!),
            toFirestore: (opentalk, _) => opentalk.toJson());

    query = query.orderBy('name', descending: true);

    if (lastDocument == null) {
      // 처음 검색하는 경우.
      query = query.limit(documentLimit);
    } else {
      query = query.startAfterDocument(lastDocument!).limit(documentLimit);
    }

    // 쿼리 실행.
    try {
      var snapshots = await query
          .withConverter<OpentalkData>(
              fromFirestore: (snapshots, _) => OpentalkData.fromJson(snapshots.data()!),
              toFirestore: (opentalk, _) => opentalk.toJson())
          .get();
      print('검색된 오픈톡방 갯수: ${snapshots.docs.length}');

      if (snapshots.docs.length < documentLimit) {
        hasMore = false;
      }

      if (snapshots.docs.isNotEmpty) {
        lastDocument = snapshots.docs[snapshots.docs.length - 1];
        opentalks.addAll(snapshots.docs);
        print('오픈톡방 갯수: ${opentalks.length}');

        isLoading = false;
      }
    } catch (e) {
      print('복합 인덱스가 필요한지 보세요~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~');
      print('복합 인덱스가 필요한지 보세요~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~');
      print('복합 인덱스가 필요한지 보세요~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~');

      print(e);
    }
  }
}
