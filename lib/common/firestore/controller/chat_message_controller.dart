import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:get/get.dart';
import 'package:zzin/common/firestore/document_skima/chat_message.dart';

class ChatMessageController extends GetxController {
  late ChatMessage chatMessage;

  static ChatMessageController get to => Get.find();

  Future<void> queryChatMessage(DocumentReference lastMessageRef) async {
    var snapshot = await lastMessageRef
        .withConverter<ChatMessage>(
            fromFirestore: (snapshot, _) => ChatMessage.fromJson(snapshot.data()!),
            toFirestore: (chatMessage, _) => chatMessage.toJson())
        .get();
    chatMessage = snapshot.data()!;
  }
}
