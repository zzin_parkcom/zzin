import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:zzin/common/hive/brand_collection.dart';
import 'package:zzin/common/hive/upload_collection.dart';
import 'package:zzin/common/utils/upload_utils.dart';
import 'package:zzin/upload_page/bottomsheet/bottomsheet_upload.dart';
import 'package:zzin/common/zconfig.dart';
import 'package:zzin/common/zutils.dart';
import 'package:zzin/upload_page/customFormfield/custom_formfield.dart';

class BrandSelection extends StatefulWidget {
  final UploadCollection uploadCollection;
  final String defaultName = '브랜드 선택';
  final double fontSize;

  const BrandSelection(this.fontSize, {required this.uploadCollection, Key? key}) : super(key: key);

  @override
  _BrandSelectionState createState() => _BrandSelectionState();
}

class _BrandSelectionState extends State<BrandSelection> {
  var id = NotSelected;
  var name = Empty;

  @override
  Widget build(BuildContext context) {
    id = widget.uploadCollection.brandId;
    name = widget.uploadCollection.brandName;
    return CustomFormField(
      initialValue: id,
      validator: validator,
      onSaved: onSaved,
      autovalidateMode: AutovalidateMode.disabled,
      builder: listTileBuilder,
    );
  }

  Widget listTileBuilder(FormFieldState<int> state) {
    return Column(
      children: [
        ListTile(
          title: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              brandTitle(),
              state.hasError
                  ? Text(
                      state.errorText!,
                      style: errorStyle(widget.fontSize),
                    )
                  : const SizedBox.shrink()
            ],
          ),
          trailing: const Icon(Icons.expand_more),
          onTap: () async {
            var idReturn = await showModalBottomSheetInt(context, _brandBottomSheetContent);
            if (idReturn != NotSelected) {
              id = idReturn;
              name = BrandCollection.getNameKr(id);
              widget.uploadCollection.brandId = id;
              widget.uploadCollection.brandName = BrandCollection.getNameKr(id);
              await widget.uploadCollection.save();
              print(widget.uploadCollection.brandId.toString());
              state.didChange(id);
            }
          },
        ),
      ],
    );
  }

  String? validator(int? value) {
    return id == NotSelected ? '브랜드를 선택해 주세요' : null;
  }

  void onSaved(int? value) {}

  Widget brandTitle() {
    return id == NotSelected
        ? Text(name, style: normalStyle(id == NotSelected, widget.fontSize))
        : Row(
            children: [
              Text(
                BrandCollection.getNameKr(id),
                style: normalStyle(id == NotSelected, widget.fontSize),
              ),
              const SizedBox(width: 5),
              Flexible(
                  child: Text(BrandCollection.getNameEn(id),
                      overflow: TextOverflow.ellipsis, style: greyEngStyle(widget.fontSize - 4.0)))
            ],
          );
  }

  Widget _brandBottomSheetContent(BuildContext context) {
    return BottomSheetUpload('브랜드', '닫기', NotSelected, BrandCollection.to.length,
        BrandCollection.to.values.map((e) => e.nameKr + '|' + e.nameEn).toList(), true,
        height: Get.height - AppBar().preferredSize.height * 2);
  }
}
