import 'package:badges/badges.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:get/get.dart';
import 'package:zzin/common/hive/zhive.dart';
import 'package:zzin/common/widget/appbar/zappbar.dart';
import 'package:zzin/common/widget/ztab.dart';
import 'package:zzin/common/zconfig.dart';
import 'package:zzin/entrance/skeleton.dart';
import 'package:zzin/postbox/postbox_controller.dart';
import 'package:zzin/postbox/postbox_page.dart';
import 'package:zzin/postbox/postbox_viewr.dart';
import 'package:zzin/product_page/widget/product_listview.dart';
import 'package:zzin/search_page/search_page.dart';
import 'package:zzin/upload_page/upload_auto.dart';
import 'package:zzin/common/utils/zenum.dart' as zenum;

class ProductView extends StatefulWidget {
  const ProductView({Key? key}) : super(key: key);

  @override
  _ProductViewState createState() => _ProductViewState();
}

@override
class _ProductViewState extends State<ProductView> {
  List<Tab> tabs = [
    const Tab(child: Text('전체', style: TextStyle(fontSize: 18))),
    const Tab(child: Text('가방', style: TextStyle(fontSize: 18))),
    const Tab(child: Text('의류', style: TextStyle(fontSize: 18))),
    const Tab(child: Text('신발', style: TextStyle(fontSize: 18))),
    const Tab(child: Text('시계', style: TextStyle(fontSize: 18))),
    const Tab(child: Text('기타', style: TextStyle(fontSize: 18))),
  ];

  var isNewPostbox = false.obs;

  @override
  void initState() {
    super.initState();
    SchedulerBinding.instance!.addPostFrameCallback((_) => frameCallback(context));
  }

  void frameCallback(BuildContext context) async {
    var postboxController = PostboxController();
    if (ZConfig.userAccount.isRegistered()) {
      isNewPostbox.value = await postboxController.isNewPostbox();
      if (isNewPostbox.value == true) {
        print('isNewPostbox!!!!!!!!!');
      }
    }

    if (ZConfig.isShowUrgentNotice) {
      var urgentNotices = await postboxController.getUrgentNotice();
      if (urgentNotices.isNotEmpty) {
        for (var index = 0; index < urgentNotices.length; index++) {
          print(urgentNotices[index].data()!.title);

          await showModalBottomSheet<int>(
            shape: const RoundedRectangleBorder(
              borderRadius: BorderRadius.vertical(top: Radius.circular(10)),
            ),
            isScrollControlled: true,
            isDismissible: false,
            enableDrag: false,
            elevation: 10,
            context: context,
            builder: (context) {
              return SizedBox(
                height: MediaQuery.of(context).size.height * 0.9,
                child: PostboxViewer(
                  postbox: urgentNotices[index].data()!,
                  isBottomSheet: true,
                ),
              );
            },
          );

          ZConfig.isShowUrgentNotice = false;
        }
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: zAppBar(),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Expanded(
            child: ZTab(
              keyString: MainTabController.to.toString(),
              tabs: tabs,
              tabViews: [
                ProductListView(zenum.ProductCategory.ALL, isListView()), // All
                ProductListView(zenum.ProductCategory.BAGS, isListView()), // BAGS
                ProductListView(zenum.ProductCategory.CLOTHES, isListView()), // CLOTHES
                ProductListView(zenum.ProductCategory.SHOES, isListView()), // SHOES
                ProductListView(zenum.ProductCategory.WATCHES, isListView()), // WATCHES
                ProductListView(zenum.ProductCategory.ACCESORIES, isListView()), // ACCESORIES
              ],
            ),
          ),
        ],
      ),
    );
  }

  bool isListView() {
    return ZHive.generalBox.get('isListView', defaultValue: true);
  }

  ZAppBar zAppBar() => ZAppBar(
      leading: IconButton(
          icon: const Icon(Icons.search, color: ZConfig.zzin_black), onPressed: searchByKeyword),
      title: const Text('',
          style: TextStyle(fontSize: ZSize.appBarTextSize, fontWeight: FontWeight.bold)),
      actions: actions());

  void searchByKeyword() async {
    await Get.to(() => const SearchPage(),
        transition: Transition.upToDown, duration: ZConfig.heroAnimationDuration);
    await ZConfig.analytics.logEvent(name: 'Search_Page_Clicked');
  }

  List<Widget> actions() {
    var actionList = <Widget>[];

    if (kDebugMode) {
      actionList.addAll(
        [
          TextButton(
            onPressed: () async {
              if (ZConfig.userAccount.isVerified) {
                await UploadProductAuto(count: 10).uploadProductAuto();
              } else {
                Get.rawSnackbar(message: '로그인을 하세요');
              }
            },
            style: const ButtonStyle(visualDensity: VisualDensity.compact),
            child: const Text('10개'),
          ),
          TextButton(
            onPressed: () async {
              if (ZConfig.userAccount.isVerified) {
                await UploadProductAuto(count: 100).uploadProductAuto();
              } else {
                Get.rawSnackbar(message: '로그인을 하세요');
              }
            },
            style: const ButtonStyle(visualDensity: VisualDensity.compact),
            child: const Text('100개'),
          ),
          IconButton(
            icon: const Icon(Icons.add),
            onPressed: () async {
              PostboxController().addRandomPost();
            },
            visualDensity: VisualDensity.compact,
          ),
          IconButton(
            icon: const Icon(Icons.person_add),
            onPressed: () async {
              PostboxController().addRandomPost(false);
            },
            // visualDensity: VisualDensity.compact,
          ),
        ],
      );
    }

    actionList.add(
      IconButton(
        icon: isListView() ? const Icon(Icons.grid_view) : const Icon(Icons.view_list),
        onPressed: () async {
          await ZHive.generalBox.put('isListView', !isListView());
          setState(() {});
        },
        visualDensity: VisualDensity.compact,
      ),
    );

    // if (ZConfig.userAccount.isRegistered()) {
    actionList.add(
      Obx(
        () => Badge(
          badgeContent: null,
          shape: BadgeShape.circle,
          padding: EdgeInsets.all(isNewPostbox.value ? 5 : 0),
          animationDuration: const Duration(milliseconds: 500),
          animationType: BadgeAnimationType.fade,
          position: BadgePosition.topEnd(top: 15, end: 12),
          badgeColor: Colors.red,
          child: IconButton(
            icon: const Icon(Icons.notifications),
            onPressed: () async {
              await PostboxController().refreshPostBox();
              await Get.to(() => const PostboxPage(),
                  transition: Transition.upToDown, duration: ZConfig.heroAnimationDuration);
              if (ZConfig.userAccount.isVerified) {
                isNewPostbox.value = await PostboxController().isNewPostbox();
              }
            },
          ),
        ),
      ),
    );
    // }

    return actionList;
  }
}
