import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:hive/hive.dart';
import 'package:zzin/common/hive/zhive.dart';

part 'address_collection.g.dart';

@HiveType(typeId: 2)
class AddressCollection extends HiveObject {
  @HiveField(0)
  final int regionCode;
  @HiveField(1)
  final String regionName;
  @HiveField(2)
  final double latitude;
  @HiveField(3)
  final double longitude;

  AddressCollection(
      {required this.regionCode,
      required this.regionName,
      required this.latitude,
      required this.longitude});

  @override
  String toString() {
    return 'regionCode:$regionCode, regionName:$regionName, (latitude,longitude) = ($latitude, $longitude)';
  }

  static Box<AddressCollection> get to => ZHive.addressBox;

  static AddressCollection getAddressByRegionCode(int regionCode) {
    return ZHive.addressBox.values.where((element) => element.regionCode == regionCode).first;
  }

  static AddressCollection getAddressByRegionName(String regionName) {
    return ZHive.addressBox.values.where((element) => element.regionName == regionName).first;
  }

  static String getRegionName(int regionCode) {
    return ZHive.addressBox.values
        .where((element) => element.regionCode == regionCode)
        .first
        .regionName;
  }

  static int getRegionCode(String regionName) {
    return ZHive.addressBox.values
        .where((element) => element.regionName == regionName)
        .first
        .regionCode;
  }

  static GeoPoint getGeoPoint(String regionName) {
    var address =
        ZHive.addressBox.values.where((element) => element.regionName == regionName).first;
    return GeoPoint(address.latitude, address.longitude);
  }

  static GeoPoint getGeoPointByRegionCode(int regionCode) {
    var address =
        ZHive.addressBox.values.where((element) => element.regionCode == regionCode).first;
    return GeoPoint(address.latitude, address.longitude);
  }

  List<String> makeTagList() {
    var tags = <String>[];

    var splitList = regionName.split(' ');
    if (splitList.length == 3) {
      // 1
      for (var sido in sidoList) {
        if (sido.keys.first == splitList[0]) {
          tags.add(sido.keys.first);
          tags.add(sido.values.first);
        }
      }
      // #2
      sigungu(splitList[1], tags);
      // #3
      ymd(splitList[2], tags);
    } else if (splitList.length == 4) {
      // #1
      for (var sido in sidoList) {
        if (sido.keys.first == splitList[0]) {
          tags.add(sido.keys.first);
          tags.add(sido.values.first);
        }
      }
      // #2
      sigungu(splitList[1], tags);
      // #3
      sigungu(splitList[2], tags);
      // #4
      ymd(splitList[3], tags);
    } else {
      throw Exception('3, 4개 아닌 주소록이 존재합니다. 확인해 주세요');
    }

    tags.addAll(regionName.split(' '));

    var oneCharacters = tags.where((element) => element.length < 2);

    tags.removeWhere((element) => oneCharacters.contains(element));

    return tags;
  }

  // 시군구 태그 처리
  void sigungu(String keyword, List<String> tags) {
    if (keyword.endsWith('시') || keyword.endsWith('군') || keyword.endsWith('구')) {
      tags.add(keyword);
      tags.add(keyword.substring(0, keyword.length - 1));
    } else {
      tags.add(keyword);
    }
  }

  //읍면동 태그 처리
  void ymd(String keyword, List<String> tags) {
    var endSS = RegExp(r'[가-힣0-9]가동$');
    var endNS = RegExp(r'\d동$');
    if (keyword.contains('.')) {
      if (endSS.hasMatch(keyword)) {
        try {
          var numbers = RegExp(r'\d').allMatches(keyword);
          for (var i = 0; i < numbers.length; i++) {
            var headN = keyword.split('.').first;
            var head = headN.substring(0, headN.length - 1);
            tags.add(head);

            tags.add(head + numbers.elementAt(i).group(0).toString() + '가');
            tags.add(head + numbers.elementAt(i).group(0).toString() + '가동');
          }
        } catch (e) {
          print(e);
        }
      } else if (endNS.hasMatch(keyword)) {
        var numbers = RegExp(r'\d').allMatches(keyword);
        for (var i = 0; i < numbers.length; i++) {
          var headN = keyword.split('.').first;
          var head = headN.substring(0, headN.length - 1);
          tags.add(head);
          tags.add(head + '동');

          tags.add(head + numbers.elementAt(i).group(0).toString() + '동');
        }
      } else {
        var keywordwoLast = keyword.substring(0, keyword.length - 1);
        var keywordList = keywordwoLast.split('.');
        for (var element in keywordList) {
          tags.add(element);
          tags.add(element + '동');
        }
      }
    } else {
      var endNScontainJe = RegExp(r'제\d동$');
      var endNGa = RegExp(r'\d가$');
      if (keyword.endsWith('출장소')) {
        tags.add(keyword);
        tags.add(keyword.substring(0, keyword.length - 3));
      } else if (endNS.hasMatch(keyword)) {
        //  ?1동
        if (endNScontainJe.hasMatch(keyword)) {
          // 제1동
          var head = keyword.substring(0, endNScontainJe.firstMatch(keyword)!.start);
          tags.add(head);
          tags.add(head + '동');
        } else {
          // 1동
          var head = keyword.substring(0, endNS.firstMatch(keyword)!.start);
          tags.add(head);
          tags.add(head + '동');
        }
      } else if (endNGa.hasMatch(keyword)) {
        // 1가

        var head = keyword.substring(0, endNGa.firstMatch(keyword)!.start);
        tags.add(head);
      } else if (keyword.endsWith('읍') || keyword.endsWith('면') || keyword.endsWith('동')) {
        tags.add(keyword.substring(0, keyword.length - 1));
      }
    }
    tags.add(keyword);
  }

  List<Map<String, String>> sidoList = [
    {'서울': '서울특별시'},
    {'부산': '부산광역시'},
    {'대구': '대구광역시'},
    {'인천': '인천광역시'},
    {'광주': '광주광역시'},
    {'대전': '대전광역시'},
    {'울산': '울산광역시'},
    {'세종': '세종특별자치시'},
    {'경기': '경기도'},
    {'강원': '강원도'},
    {'충북': '충청북도'},
    {'충남': '충청남도'},
    {'전복': '전라북도'},
    {'전남': '전라남도'},
    {'경북': '경상북도'},
    {'경남': '경상남도'},
    {'제주': '제주특별자치도'}
  ];
}
